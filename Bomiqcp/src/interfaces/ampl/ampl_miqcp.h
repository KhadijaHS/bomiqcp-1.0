/* -*-c-*-
 *
 *    Source     $RCSfile: ampl_miqcp.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/31 10:00:00 $
 *    Authors    Khadija HADJ SALEM, Amelie LAMBERT, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "Bomiqcp",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#ifndef AMPL_MIQCP_H_INCLUDED
#define AMPL_MIQCP_H_INCLUDED

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>


#ifdef __cplusplus
class aaa{
 public:

 private:
  
#else

#endif

#ifdef __cplusplus
extern "C" {
#endif

  // #if defined(__STDC__) || defined(__cplusplus)
  //  extern void c_function(Fred*);   /* ANSI C prototypes */
  //  extern Fred* cplusplus_callback_function(Fred*);
  //#else
  //   extern void c_function();        /* K&R style */
  //   extern Fred* cplusplus_callback_function();
  //#endif
    
#ifdef __cplusplus
}
#endif
  


/*********************************************************************/
/****************************** Variable *****************************/
/*********************************************************************/

/*********************************************************************/
/***************************** Functions *****************************/
/*********************************************************************/


#endif // AMPL_MIQCP_H_INCLUDED
