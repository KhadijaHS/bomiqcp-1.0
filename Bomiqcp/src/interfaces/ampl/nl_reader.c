/* -*-c-*-
 *
 *    Source     $RCSfile: nl_reader.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/31 10:02:00 $
 *    Authors    Khadija HADJ SALEM, Amelie LAMBERT, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "Bomiqcp",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<sys/timeb.h>
#include<getopt.h>
#include<assert.h>
#include<unistd.h>
#include<fcntl.h>

#include"nl_reader.h"

#include"asl.h"
#include "getstub.h"

#include"nlp.h"
#include"getstub.h"
#include"opcode.hd"
#include"CoinHelperFunctions.hpp"
#include"CoinTime.hpp"

#include "quad_prog.h"

#if defined HAVE_CSTDINT
#include "cstdint"
#elif defined HAVE_STDINT_H
#include "stdint.h"
#endif

#define OBJ_DE    ((const ASL_fg *) asl) -> I.obj_de_
#define VAR_E     ((const ASL_fg *) asl) -> I.var_e_
#define CON_DE    ((const ASL_fg *) asl) -> I.con_de_
#define OBJ_sense ((const ASL_fg *) asl) -> i.objtype_

#define THRESHOLD_OUTPUT_READNL 10000
//#define DEBUG

/*********************************************************************/
ASL *asl;
asl = ASL_alloc(ASL_read_f);
FILE *jac0dim(char *stub, fint stub_len);

c = (real *)Malloc(n_var*sizeof(real));
for(i = 0; i < n_var; i++)
if (n_obj)

char *sname;          /* invocation name of solver */
char *bsname;         /* solver name in startup "banner" */
char *opname;         /* name of solver_options environment var */
keyword *keywds;      /* key words */
int n_keywds;         /* number of key words */
int want_funcadd;     /* whether funcadd will be called */
char *version;        /* for -v and Ver_key_ASL() */
char **usage;         /* solver-specific usage message */
Solver_KW_func *kwf;  /* solver-specific keyword function */
Fileeq_func *feq;     /* for n=filename */
keyword *options;     /* command-line options (with -) before stub */
int n_options;        /* number of options */


/*********************************************************************/
void read_nl_ampl(struct MIQCP qp, FILE *aa){
  /*struct*/
  MIQCP qp = new_miqcp();
  C_MIQCP cqp;
  MIQCP mqp;
  SDP psdp;
  Q_MIQCP qqp;

  /*parameters*/
  qp->n = n_var;
  qp->nb_int = 0;
  qp->m = n_con;
  qp->p = 0;
  qp->mq = 0;
  qp->pq = 0;
  
  /*variables*/


  /*functions*/

}

ASL_free(&asl);
