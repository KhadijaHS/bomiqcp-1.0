/* -*-c-*-
 *
 *    Source     $RCSfile: ampl_interface.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/31 10:01:00 $
 *    Authors    Khadija HADJ SALEM, Amelie LAMBERT, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "Bomiqcp",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"ampl_interface.h"

#include "asl.h"
#include "nlp.h"
#include "getstub.h"
#include "opcode.hd"


/*********************************************************************/
// get ASL op. code relative to function pointer passed as parameter 
int getOperator (efunc *);

#define OBJ_DE    ((const ASL_fg *) asl) -> I.obj_de_
#define VAR_E     ((const ASL_fg *) asl) -> I.var_e_
#define CON_DE    ((const ASL_fg *) asl) -> I.con_de_
#define OBJ_sense ((const ASL_fg *) asl) -> i.objtype_

#include "r_opn.hd" /* for N_OPS */

static fint timing = 0;

static keywords keywds[] = {KW(const_cast<char*>("timing"), L_val, &timing, const_cast<char*>("display timings for the run"))};


static Option_Info Oinfo = { const_cast<char*>("testampl"), const_cast<char*>("ANALYSIS TEST"), const_cast<char*>("concert_options"), keywds, nkeywds, 0, const_cast<char*>("ANALYSIS TEST") };


/*********************************************************************/



/*********************************************************************/


/*********************************************************************/
