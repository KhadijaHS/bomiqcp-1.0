/* -*-c-*-
 *
 *    Source     $RCSfile: quicksort.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include<time.h>

#include"quicksort.h"
#include"utilities.h"


/*****create_tab_ind_from_check_violated()*****/
TAB_IND * create_tab_ind_from_check_violated(double * check_violated, int n)
{
    int i;
    TAB_IND * tab = (TAB_IND*) malloc(n*sizeof(struct tab_ind *));
    for(i=0; i<n; i++)
        tab[i]= (TAB_IND) malloc(sizeof(struct tab_ind));

    for(i=0; i<n; i++)
    {
        tab[i]->val = check_violated[i];
        tab[i]->ind = i;
    }

    return tab;
}

/*****generate_pivot()*****/
int generate_pivot(int beg, int end)
{
    srand(time(NULL));
    int pivot = rand() % (end - beg)+ beg;
    return pivot;
}

/*****change()*****/
void change(TAB_IND ** tab, int i, int j)
{
    TAB_IND temp;
    temp = tab[0][i];
    tab[0][i] = tab[0][j];
    tab[0][j] = temp;
}

/*****partition()*****/
int partition(TAB_IND ** tab, int beg, int end, int pivot)
{
    int i, j;

    change(tab,pivot,end);
    j=beg;

    for(i=beg; i<end; i++)
    {
        if (tab[0][i]->val - tab[0][end]->val < -EPS_VIOL_CB)
        {
            change(tab,i,j);
            j++;
        }
    }
    change(tab,end,j);

    return j;
}

/*****quicksort()*****/
void quickSort(TAB_IND ** tab, int beg, int end)
{
    int pivot,i;
    if( beg < end )
    {
        pivot = generate_pivot(beg, end);
        pivot = partition( tab, beg, end,pivot);
        quickSort( tab, beg, pivot-1);
        quickSort( tab, pivot+1, end);
    }
}

/*****initialize_constraints_tab()*****/
int initialize_constraints_tab(TAB_IND ** tab,int * constraints, int * constraints_old, int nb_max_cont)
{
    int i;
    for (i=0; i< nb_max_cont; i++)
    {
        if (tab[0][i]->val > EPS_VIOL_CB)
            break;

        constraints[i]=tab[0][i]->ind+1;
        constraints_old[i]=tab[0][i]->ind+1;
    }
    return i;
}
