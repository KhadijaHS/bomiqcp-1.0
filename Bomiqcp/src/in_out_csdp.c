/* -*-c-*-
 *
 *    Source     $RCSfile: int_out_csdp.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors     Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>

#include<unistd.h>
#include<fcntl.h>

#include "in_out_csdp.h"
#include"utilities.h"
#include"in_out.h"
#include "declarations.h"

/******************************************************************************/
/****************** Write vectors and matrices for csdp ***********************/
/******************************************************************************/

void write_matrix_q_sdp_CSDP(FILE * temp,double * mat,int t)
{
    int i,j;
    for(i=0; i<t; i++)
    {
        for(j=i; j<t; j++)
            if(mat[ij2k(i,j,t)]!=0)
                fprintf(temp,"0 1 %d %d %lf\n",i+2,j+2,mat[ij2k(i,j,t)]);
    }
}

void write_vector_c_CSDP(FILE * temp,double * vect,int t)
{
    int i;
    for(i=0; i<t; i++)
        if(vect[i]!=0)
            fprintf(temp,"0 1 1 %d %lf\n",i+2,-vect[i]/2);
}




void write_matrix_q_CSDP(FILE * temp,double * mat,int t)
{
    int i,j;
    for(i=0; i<t; i++)
    {
        for(j=i; j<t; j++)
            if(mat[ij2k(i,j,t)]!=0)
                fprintf(temp,"0 1 %d %d %lf\n",i+2,j+2,(float)-mat[ij2k(i,j,t)]);
    }
}
void write_matrix_q_CSDP_g(FILE * temp,double * mat,int t,int t2)
{
    int i,j;
    for(i=0; i<t2; i++)
    {
        for(j=i; j<t2; j++)
            if(mat[ij2k(i,j,t)]!=0)
                fprintf(temp,"0 1 %d %d %lf\n",i+2,j+2,(float)-mat[ij2k(i,j,t)]);
    }
}
void write_vector_c_sdp_CSDP(FILE * temp,double * vect,int t)
{
    int i;
    for(i=0; i<t; i++)
        if(vect[i]!=0)
            fprintf(temp,"0 1 1 %d %lf\n",i+2,vect[i]/2);
}


/***************************************************************************/
/************************ Write data file for QCR **************************/
/***************************************************************************/

void write_file_csdp_qcr(MIQCP qp)
{
    FILE *fsdqp;
    int nb_cont,nb_ecart,i,j,k,lim;
    int compt_cont=1;
    int compt_ecart=qp->n+1;
    double * alpha;

    double temp;

    nb_cont=qp->n+1;
    if (!Zero(qp->m))
        nb_cont=nb_cont+qp->m+1;

    if (!Zero(qp->p))
        nb_cont=nb_cont+qp->p;

    nb_ecart=qp->p+qp->n+1;
    fsdqp=fopen(data_csdp,"w");
    fprintf(fsdqp,"%d\n1\n%d\n", nb_cont,nb_ecart+1);


    if(!Zero(qp->m))
    {
        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];
        fprintf(fsdqp,"%lf ",-temp);
    }

    for (i=0; i<qp->n; i++)
        fprintf(fsdqp,"0 ");

    for (i=0; i<qp->m; i++)
        fprintf(fsdqp,"%lf ",qp->b[i]);

    for (i=0; i<qp->p; i++)
        fprintf(fsdqp,"%lf ",qp->e[i]);

    fprintf(fsdqp,"1\n");

    write_vector_c_CSDP(fsdqp,qp->c,qp->n);
    write_matrix_q_CSDP(fsdqp,qp->q,qp->n);

    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+1,qp->n+1);
        for (i=0; i<qp->n+1; i++)
            for (j=0; j<qp->n+1; j++)
                alpha[ij2k(i,j,qp->n+1)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+1)]= alpha[ij2k(0,j+1,qp->n+1)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+1)]=alpha[ij2k(j+1,k+1,qp->n+1)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(alpha[ij2k(j,k,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,alpha[ij2k(j,k,qp->n+1)]);

        compt_cont++;
    }


    /* Xii = xi*/
    for (i=0; i<qp->n; i++)
    {
        fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
        fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,i+2);
        compt_cont++;
    }

    /* Ax=b */
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->a[ij2k(i,j,qp->n)]/2);
            compt_cont++;
        }

    /*Dx<=e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->d[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* X_11 = 1*/
    fprintf(fsdqp,"%d 1 1 1 1\n",compt_cont);
    fclose(fsdqp);

    if(!Zero(qp->m))
        free_vector_d(alpha);
}

void write_file_csdp_qcr_minkcut(MIQCP qp)
{
    FILE *fsdqp;
    int nb_cont,nb_ecart,i,j,k,l, kcut;
    int compt_cont=1;
    int compt_ecart=qp->n+1;
    double * alpha;

    double temp;

    kcut=(int)qp->n/(qp->m-1);
    printf("\nkcut:%d\n",kcut);
    nb_cont=qp->n+1+(qp->m-1)*(kcut-1)*kcut/2;
    printf("\nnb_cont:%d\n",nb_cont);
    if (!Zero(qp->m))
        nb_cont=nb_cont+qp->m+1;

    nb_ecart=qp->n+1;
    fsdqp=fopen(data_csdp,"w");
    fprintf(fsdqp,"%d\n1\n%d\n", nb_cont,nb_ecart+1);


    if(!Zero(qp->m))
    {
        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];
        fprintf(fsdqp,"%lf ",-temp);
    }

    for (i=0; i<qp->n; i++)
        fprintf(fsdqp,"0 ");

    for (i=0; i<(qp->m-1)*(kcut-1)*kcut/2; i++)
        fprintf(fsdqp,"0 ");

    for (i=0; i<qp->m; i++)
        fprintf(fsdqp,"%lf ",qp->b[i]);

    fprintf(fsdqp,"1\n");

    write_vector_c_CSDP(fsdqp,qp->c,qp->n);
    write_matrix_q_CSDP(fsdqp,qp->q,qp->n);

    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+1,qp->n+1);
        for (i=0; i<qp->n+1; i++)
            for (j=0; j<qp->n+1; j++)
                alpha[ij2k(i,j,qp->n+1)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+1)]= alpha[ij2k(0,j+1,qp->n+1)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+1)]=alpha[ij2k(j+1,k+1,qp->n+1)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(alpha[ij2k(j,k,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,alpha[ij2k(j,k,qp->n+1)]);

        compt_cont++;
    }


    /* Xii = xi*/
    for (i=0; i<qp->n; i++)
    {
        fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
        fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,i+2);
        compt_cont++;
    }

    /*y_ikil = 0 */
    for (i=0; i<qp->m-1; i++)
        for(k=0; k<qp->n; k++)
            for(l=k+1; l<qp->n; l++)
                if(!Zero(qp->a[ij2k(i,l,qp->n)]) && !Zero(qp->a[ij2k(i,k,qp->n)]) && k!=l)
                {
                    fprintf(fsdqp,"%d 1 %d %d 0.5\n",compt_cont,k+2,l+2);
                    compt_cont++;
                }


    /* Ax=b */
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            for (j=0; j<qp->n; j++)
                if (!Zero(qp->a[ij2k(i,j,qp->n)]))
                {
                    fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->a[ij2k(i,j,qp->n)]/2);
                }
            compt_cont++;

        }


    /* X_11 = 1*/
    fprintf(fsdqp,"%d 1 1 1 1\n",compt_cont);
    fclose(fsdqp);

    if(!Zero(qp->m))
        free_vector_d(alpha);
}



/***************************************************************************/
/************************ Write data file for CQCR *************************/
/***************************************************************************/

void write_file_csdp_cqcr(MIQCP qp)
{

    FILE *fsdqp;
    int nb_cont,nb_ecart,i,j,k,l,lim,nb;
    int compt_cont=1;
    int compt_ecart=qp->n+1;
    double * alpha, * alphabis;

    double temp;

    int cpt =0;
    for(i=0; i<qp->n; i++)
        if (qp->u[i] !=1)
            cpt++;

    nb_cont=4*cpt + qp->n-cpt +1;
    nb_ecart=qp->n+qp->p+2+4*cpt ;

    if (!Zero(qp->m))
        nb_cont+= 1+qp->m;

    if (!Zero(qp->p))
    {
        nb_cont+=1+qp->p;
    }

    fsdqp=fopen(data_csdp,"w");
    fprintf(fsdqp,"%d\n1\n%d\n", nb_cont,nb_ecart);

    if (!Zero(qp->m))
    {
        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];
        fprintf(fsdqp,"%lf ",-temp);
    }

    if (!Zero(qp->p))
    {
        temp=0;
        for (i=0; i<qp->p; i++)
            temp=temp + qp->e[i]*qp->e[i];
        fprintf(fsdqp,"%lf ",-temp);
    }

    /*X_ii <= u_ix_i */
    for (i=0; i<qp->n+qp->p; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /*X_ii >=  0 */
    for (i=0; i<qp->n+qp->p; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /*X_ii >= 2u_ix_i -u_i^2 */
    for (i=0; i<qp->n+qp->p; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"%lf ",qp->u[i]*qp->u[i]);

    /*X_ii >= x_i */
    for (i=0; i<qp->n+qp->p; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /*X_ii = x_i */
    for (i=0; i<qp->n+qp->p; i++)
        if (qp->u[i]==1)
            fprintf(fsdqp,"0 ");

    /* Ax = b*/
    for (i=0; i<qp->m; i++)
        fprintf(fsdqp,"%lf ",qp->b[i]);

    /* Dx + s= e */
    for (i=0; i<qp->p; i++)
        fprintf(fsdqp,"%lf ",qp->e[i]);

    /* x_00 = 1*/
    fprintf(fsdqp,"1\n");

    write_vector_c_CSDP(fsdqp,qp->c,qp->n);
    write_matrix_q_CSDP(fsdqp,qp->q,qp->n);

    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+1,qp->n+1);
        for (i=0; i<qp->n+1; i++)
            for (j=0; j<qp->n+1; j++)
                alpha[ij2k(i,j,qp->n+1)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+1)]= alpha[ij2k(0,j+1,qp->n+1)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+1)]=alpha[ij2k(j+1,k+1,qp->n+1)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(alpha[ij2k(j,k,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,alpha[ij2k(j,k,qp->n+1)]);

        compt_cont++;
    }

    /*d�X -2dex +e�*/
    if(!Zero(qp->p))
    {
        alphabis = alloc_matrix_d(qp->n+1,qp->n+1);
        for (i=0; i<qp->n+1; i++)
            for (j=0; j<qp->n+1; j++)
                alphabis[ij2k(i,j,qp->n+1)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->p; i++)
                alphabis[ij2k(0,j+1,qp->n+1)]= alphabis[ij2k(0,j+1,qp->n+1)] - qp->e[i]*qp->d[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->p; i++)
                    alphabis[ij2k(j+1,k+1,qp->n+1)]=alphabis[ij2k(j+1,k+1,qp->n+1)] + qp->d[ij2k(i,j,qp->n)]*qp->d[ij2k(i,k,qp->n)];

        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(alphabis[ij2k(j,k,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,alphabis[ij2k(j,k,qp->n+1)]);

        compt_cont++;
    }

    /* Xii<= uixi (for integer variables)*/
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* -Xii <= 0 (for integer variables)*/
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*- Xii <=  - 2uixi + ui� (for integer variables)*/
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->u[i]);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*- Xii <=  - xi (for integer variables)*/
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d 0.5\n",compt_cont,i+2);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*X_ii = x_i (for binary variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]==1)
        {
            fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
            compt_cont++;
        }

    /*Ax =b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->a[ij2k(i,j,qp->n)]/2);
            compt_cont++;
        }

    /*Dx + s = e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->d[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* X_11 = 1*/
    fprintf(fsdqp,"%d 1 1 1 1\n",compt_cont);

    fclose(fsdqp);

    if(!Zero(qp->m))
        free_vector_d(alpha);
}


/***************************************************************************/
/************************ Write data file for MIQCR ************************/
/***************************************************************************/

void write_file_csdp_miqcr(MIQCP qp)
{
    int taille;
    FILE * fsdqp;
    int cont,nb_ecart,nb_cont,i,j,k,l,lim,cont_yij=0;
    int compt_cont=1;
    int compt_ecart=qp->n+1;
    double * alpha;
    double * alphabis;
    double temp;
    cont=0;

    int cpt =0;
    for(i=0; i<qp->nb_int; i++)
        if (qp->u[i] !=1)
            cpt++;

    /*number of constraints without diagonal terms*/
    for(i=qp->n-1; i>=qp->n-qp->nb_int; i--)
        cont=cont+i;

    nb_cont=4*cont +1+2*(qp->n - qp->nb_int ) + 4 *cpt + qp->nb_int-cpt;
    nb_ecart=compt_ecart + 4*cont + 2*(qp->n-qp->nb_int)+ 4 *cpt;

    if(!Zero(qp->m))
        nb_cont+=qp->m+1;

    if(!Zero(qp->p))
        nb_cont+=1;

    fsdqp=fopen(data_csdp,"w");
    fprintf(fsdqp,"%d\n1\n%d\n", nb_cont,nb_ecart);

    if(!Zero(qp->m))
    {
        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];
        fprintf(fsdqp,"%lf ",-temp);
    }

    if(!Zero(qp->p))
    {
        temp=0;
        for (i=0; i<qp->p; i++)
            temp=temp + qp->e[i]*qp->e[i];
        fprintf(fsdqp,"%lf ",-temp);
    }

    /* constraints for i <j */
    for (i=0; i<3*cont; i++)
        fprintf(fsdqp,"0 ");

    for (i=0; i<qp->nb_int; i++)
        for (j=i+1; j<qp->n; j++)
            fprintf(fsdqp,"%lf ",qp->u[i]*qp->u[j]);

    /* Constraints for i==j */
    /* if x integer variable */
    /*X_ii <= u_ix_i */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /*X_ii >=  0 */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /*X_ii >= 2u_ix_i -u_i^2 */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"%lf ",qp->u[i]*qp->u[i]);

    /*X_ii >= x_i */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /* if x binary variable */
    /*X_ii >= x_i */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]==1)
            fprintf(fsdqp,"0 ");

    /* Ax = b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
            fprintf(fsdqp,"%lf ",qp->b[i]);

    /* Dx <= e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
            fprintf(fsdqp,"%lf ",qp->e[i]);

    /* Bounds for continuous variables*/
    for (i=qp->nb_int; i<qp->n; i++)
        fprintf(fsdqp,"%lf ", qp->u[i]);

    for (i=qp->nb_int; i<qp->n; i++)
        fprintf(fsdqp,"0 ");

    /* x_11 = 1*/
    fprintf(fsdqp,"1\n");

    write_vector_c_CSDP(fsdqp,qp->c,qp->n);
    write_matrix_q_CSDP(fsdqp,qp->q,qp->n);

    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+1,qp->n+1);
        for (i=0; i<qp->n+1; i++)
            for (j=0; j<qp->n+1; j++)
                alpha[ij2k(i,j,qp->n+1)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+1)]= alpha[ij2k(0,j+1,qp->n+1)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+1)]=alpha[ij2k(j+1,k+1,qp->n+1)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(alpha[ij2k(j,k,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,alpha[ij2k(j,k,qp->n+1)]);

        compt_cont++;
    }

    /*d�X -2dex +e�*/
    if(!Zero(qp->p))
    {
        alphabis = alloc_matrix_d(qp->n+1,qp->n+1);
        for (i=0; i<qp->n+1; i++)
            for (j=0; j<qp->n+1; j++)
                alphabis[ij2k(i,j,qp->n+2)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->p; i++)
                alphabis[ij2k(0,j+1,qp->n+1)]= alphabis[ij2k(0,j+1,qp->n+1)] - qp->e[i]*qp->d[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->p; i++)
                    alphabis[ij2k(j+1,k+1,qp->n+1)]=alphabis[ij2k(j+1,k+1,qp->n+1)] + qp->d[ij2k(i,j,qp->n)]*qp->d[ij2k(i,k,qp->n)];

        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(alphabis[ij2k(j,k,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,alphabis[ij2k(j,k,qp->n+1)]);

        compt_cont++;
    }

    /*produits de variables entieres par continues */

    /* Xij<= uixj (for i< j)*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d 1 %d %d 0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*Xij<= ujxi (for i< j) */
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)-qp->u[j]/2);
            fprintf(fsdqp,"%d 1 %d %d 0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* -Xij <= 0  (for i< j)*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 %d %d -0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* - Xij<= -uixj -ujxi +uiuj  (for i< j)*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->u[j]/2);
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,(double)qp->u[i]/2);
            fprintf(fsdqp,"%d 1 %d %d -0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*for i = j and x integer variable */
    /* Xii<= uixi (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* -Xii <= 0 (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*- Xii <=  - 2uixi + ui� (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->u[i]);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*- Xii <=  - xi (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d 0.5\n",compt_cont,i+2);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*X_ii = x_i (for binary variables) */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]==1)
        {
            fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
            compt_cont++;
        }

    /*Ax =b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->a[ij2k(i,j,qp->n)]/2);
            compt_cont++;
        }

    /*Dx +s = e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->d[ij2k(i,j,qp->n)]/2);
            compt_cont++;
        }

    /*xi <= ui*/
    for (j=qp->nb_int; j<qp->n; j++)
    {
        fprintf(fsdqp,"%d 1 1 %d 0.5\n",compt_cont,j+2);
        fprintf(fsdqp,"%d 1 1 %d 1\n",compt_cont,compt_ecart,compt_ecart);
        compt_ecart++;
        compt_cont++;
    }

    /*xi >= 0*/
    for (j=qp->nb_int; j<qp->n; j++)
    {
        fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,j+2);
        fprintf(fsdqp,"%d 1 1 %d 1\n",compt_cont,compt_ecart,compt_ecart);
        compt_ecart++;
        compt_cont++;
    }

    /*X_11 = 1*/
    fprintf(fsdqp,"%d 1 1 1 1\n",compt_cont);

    if(!Zero(qp->m))
        free_vector_d(alpha);
    if(!Zero(qp->p))
        free_vector_d(alphabis);

    fclose(fsdqp);
}


/***************************************************************************/
/************************ Write data file for MAXCUT ***********************/
/***************************************************************************/

void write_file_csdp_max_cut(MIQCP qp)
{

    FILE *fsdqp;
    int cont,nb_ecart,nb_cont,i,j,k,l,lim;
    int compt_cont=1;
    int compt_ecart=qp->n+1;
    double * alpha = alloc_matrix_d(qp->n+1,qp->n+1);
    double temp;

    nb_cont=4*qp->n*(qp->n -1)/2 + qp->n +1;
    nb_ecart=4*qp->n*(qp->n -1)/2;

    fsdqp=fopen(data_csdp,"w");
    fprintf(fsdqp,"%d\n1\n%d\n", nb_cont,nb_ecart);

    for (i=0; i<qp->n+3*qp->n*(qp->n -1)/2; i++)
        fprintf(fsdqp,"0 ");

    for (i; i<nb_cont; i++)
        fprintf(fsdqp,"1 ");

    fprintf(fsdqp,"\n");

    write_vector_c_CSDP(fsdqp,qp->c,qp->n);
    write_matrix_q_CSDP(fsdqp,qp->q,qp->n);

    /* Xii = xi*/
    for (i=0; i<qp->n; i++)
    {
        fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
        fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,i+2);
        compt_cont++;
    }

    /* Xij<= xi*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,-1.0);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*Xij<= xj*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,-1.0);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* -Xij <= 0*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* - Xij<= -xj -xi +1*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,1.0);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*X_11 = 1*/
    fprintf(fsdqp,"%d 1 1 1 1\n",compt_cont);

    fclose(fsdqp);
}

/***************************************************************************/
/******************** Write data file for MIQCR quad ***********************/
/***************************************************************************/

void write_file_csdp_miqcrq(MIQCP qp)
{
    int taille;
    FILE * fsdqp;
    int cont,nb_ecart,nb_cont,i,j,k,l,lim,cont_yij=0;
    int compt_cont=1;
    int compt_ecart=1;

    double temp;
    cont=0;

    int cpt =0;
    for(i=0; i<qp->nb_int; i++)
        if (qp->u[i] !=1)
            cpt++;

    /*number of constraints without diagonal terms*/
    for(i=qp->n-1; i>=qp->n-qp->nb_int; i--)
        cont=cont+i;

    nb_cont=4*cont +1+2*(qp->n - qp->nb_int) + 4 *cpt + qp->nb_int-cpt+ qp->m + qp->p +qp->pq +qp->mq;
    nb_ecart=qp->p+qp->pq + 4*cont + 2*(qp->n -qp->nb_int)+ 4 *cpt;

    fsdqp=fopen(data_csdp,"w");
    fprintf(fsdqp,"%d\n2\n%d -%d\n", nb_cont, qp->n+1,nb_ecart);


    /*A[i][0][j] = linear coefficients of quad constraint*/
    /*Aq X =bq */
    for (i=0; i<qp->mq; i++)
        fprintf(fsdqp,"%lf ",qp->bq[i]);

    /*Dq X = eq*/
    for (i=0; i<qp->pq; i++)
        fprintf(fsdqp,"%lf ",qp->eq[i]);

    /* constraints for i <j */
    for (i=0; i<3*cont; i++)
        fprintf(fsdqp,"0 ");

    for (i=0; i<qp->nb_int; i++)
        for (j=i+1; j<qp->n; j++)
            fprintf(fsdqp,"%lf ",qp->u[i]*qp->u[j]);

    /* Constraints for i==j */
    /* if x integer variable */
    /*X_ii <= u_ix_i */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /*X_ii >=  0 */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /*X_ii >= 2u_ix_i -u_i^2 */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"%lf ",qp->u[i]*qp->u[i]);

    /*X_ii >= x_i */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /* if x binary variable */
    /*X_ii = x_i */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]==1)
            fprintf(fsdqp,"0 ");

    /* Ax = b*/
    if(!Zero(qp->m))
    {
        for (i=0; i<qp->m; i++)
            fprintf(fsdqp,"%lf ",qp->b[i]);
    }

    /* Dx <= e */
    if(!Zero(qp->p))
    {
        for (i=0; i<qp->p; i++)
            fprintf(fsdqp,"%lf ",qp->e[i]);
    }

    /* Bounds for continuous variables*/
    for (i=qp->nb_int; i<qp->n; i++)
        fprintf(fsdqp,"%lf ", qp->u[i]);

    for (i=qp->nb_int; i<qp->n; i++)
        fprintf(fsdqp,"0 ");

    /* x_00 = 1*/
    fprintf(fsdqp,"1\n");

    write_vector_c_CSDP(fsdqp,qp->c,qp->n);
    write_matrix_q_CSDP(fsdqp,qp->q,qp->n);


    /*AqX = bq*/
    for (i=0; i<qp->mq; i++)
    {
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(qp->aq[ijk2l(i,j,k,qp->n+1,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,qp->aq[ijk2l(i,j,k,qp->n+1,qp->n+1)]);

        compt_cont++;
    }
    /*DqX = eq*/
    for (i=0; i<qp->pq; i++)
    {
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(qp->dq[ijk2l(i,j,k,qp->n+1,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,qp->dq[ijk2l(i,j,k,qp->n+1,qp->n+1)]);
        fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
        compt_ecart++;
        compt_cont++;
    }

    /*produits de variables entieres par continues */
    /* Xij<= uixj (for i< j)*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d 1 %d %d 0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*Xij<= ujxi (for i< j) */
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)-qp->u[j]/2);
            fprintf(fsdqp,"%d 1 %d %d 0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* -Xij <= 0  (for i< j)*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 %d %d -0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* - Xij<= -uixj -ujxi +uiuj  (for i< j)*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->u[j]/2);
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,(double)qp->u[i]/2);
            fprintf(fsdqp,"%d 1 %d %d -0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*for i = j and x integer variable */
    /* Xii<= uixi (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* -Xii <= 0 (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*- Xii <=  - 2uixi + ui� (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->u[i]);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*- Xii <=  - xi (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d 0.5\n",compt_cont,i+2);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*X_ii = x_i (for binary variables) */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]==1)
        {
            fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,i+2);
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
            compt_cont++;
        }

    /*Ax =b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->a[ij2k(i,j,qp->n)]/2);
            compt_cont++;
        }

    /*Dx +s = e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->d[ij2k(i,j,qp->n+qp->p)]/2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*xi <= ui*/
    for (j=qp->nb_int; j<qp->n; j++)
    {
        fprintf(fsdqp,"%d 1 1 %d 0.5\n",compt_cont,j+2);
        fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
        compt_ecart++;
        compt_cont++;
    }

    /*xi >= 0*/
    for (j=qp->nb_int; j<qp->n; j++)
    {
        fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,j+2);
        fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
        compt_ecart++;
        compt_cont++;
    }

    /*X_11 = 1*/
    fprintf(fsdqp,"%d 1 1 1 1\n",compt_cont);


    fclose(fsdqp);
}


void write_file_csdp_miqcrq_mixed(MIQCP qp)
{
    int taille;
    FILE * fsdqp;
    int nb_ecart,nb_cont,i,j,k,l,lim,cont_yij=0;
    int compt_cont=1;
    int compt_ecart=1;

    int cont;

    double temp;


    /*number of binary variables*/
    int cpt_binary =0;
    for(i=0; i<qp->n; i++)
        if ((qp->u[i] ==1) && (i< qp->nb_int))
            cpt_binary++;

    /*number of terms without diagonal terms*/

    cont=(qp->n-1)*qp->n/2;

    nb_cont=4*cont +1+  3*(qp->n-cpt_binary) + (qp->nb_int -cpt_binary) + cpt_binary + qp->m + qp->p+ qp->mq+ qp->pq;
    nb_ecart= qp->p+qp->pq + 4*cont + 3*(qp->n-cpt_binary) + (qp->nb_int -cpt_binary);


    fsdqp=fopen(data_csdp,"w");
    fprintf(fsdqp,"%d\n2\n%d -%d\n", nb_cont, qp->n+1, nb_ecart);


    /*A[i][0][j] = linear coefficients of quad constraint*/
    /*Aq X =bq */
    for (i=0; i<qp->mq; i++)
        fprintf(fsdqp,"%lf ",qp->bq[i]);

    /*Dq X <= eq*/
    for (i=0; i<qp->pq; i++)
        fprintf(fsdqp,"%lf ",qp->eq[i]);

    /* constraints for i <j */
    for (i=0; i<qp->n; i++)
        for (j=i+1; j<qp->n; j++)
            fprintf(fsdqp,"%lf ",-qp->u[i]*qp->l[j]);

    for (i=0; i<qp->n; i++)
        for (j=i+1; j<qp->n; j++)
            fprintf(fsdqp,"%lf ",-qp->l[i]*qp->u[j]);

    for (i=0; i<qp->n; i++)
        for (j=i+1; j<qp->n; j++)
            fprintf(fsdqp,"%lf ",qp->l[i]*qp->l[j]);


    for (i=0; i<qp->n; i++)
        for (j=i+1; j<qp->n; j++)
            fprintf(fsdqp,"%lf ",qp->u[i]*qp->u[j]);

    /* Constraints for i==j */
    /* if x non binary variable */
    /*X_ii <= (u_i + li)x_i - uili */
    for (i=0; i<qp->n; i++)
        if (!((qp->u[i]==1) && (i < qp->nb_int)))
            fprintf(fsdqp,"%lf ",-qp->u[i]*qp->l[i]);

    /*X_ii >= 2l_ix_i -l_i^2  */
    for (i=0; i<qp->n; i++)
        if (!((qp->u[i]==1) && (i < qp->nb_int)))
            fprintf(fsdqp,"%lf ",qp->l[i]*qp->l[i]);

    /*X_ii >= 2u_ix_i -u_i^2 */
    for (i=0; i<qp->n; i++)
        if (!((qp->u[i]==1) && (i < qp->nb_int)))
            fprintf(fsdqp,"%lf ",qp->u[i]*qp->u[i]);

    /*X_ii >= x_i */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]!=1)
            fprintf(fsdqp,"0 ");

    /* if x binary variable */
    /*X_ii = x_i */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]==1)
            fprintf(fsdqp,"0 ");

    /* Ax = b*/
    if(!Zero(qp->m))
    {
        for (i=0; i<qp->m; i++)
            fprintf(fsdqp,"%lf ",qp->b[i]);
    }

    /* Dx <= e */
    if(!Zero(qp->p))
    {
        for (i=0; i<qp->p; i++)
            fprintf(fsdqp,"%lf ",qp->e[i]);
    }


    /* x_00 = 1*/
    fprintf(fsdqp,"1\n");

    write_vector_c_CSDP(fsdqp,qp->c,qp->n);
    write_matrix_q_CSDP_g(fsdqp,qp->q,qp->n, qp->n);

    /*AqX = bq*/
    for (i=0; i<qp->mq; i++)
    {
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(qp->aq[ijk2l(i,j,k,qp->n+1,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,qp->aq[ijk2l(i,j,k,qp->n+1,qp->n+1)]);
        compt_cont++;
    }
    /*DqX = eq*/
    for (i=0; i<qp->pq; i++)
    {
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(qp->dq[ijk2l(i,j,k,qp->n+1,qp->n+1)] != 0)
                    fprintf(fsdqp,"%d 1 %d %d %lf\n",compt_cont,j+1,k+1,qp->dq[ijk2l(i,j,k,qp->n+1,qp->n+1)]);

        fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
        compt_ecart++;
        compt_cont++;
    }

    /*produits de variables entieres par continues */
    /* Xij<= uixj + ljx_i -u_il_j (for i< j)*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)-qp->l[j]/2);
            fprintf(fsdqp,"%d 1 %d %d 0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*Xij<= ujxi + lix_j -u_jl_i(for i< j) */
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)-qp->u[j]/2);
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,(double)-qp->l[i]/2);
            fprintf(fsdqp,"%d 1 %d %d 0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* -Xij <= -lixj -ljxi +lilj  (for i< j)*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->l[j]/2);
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,(double)qp->l[i]/2);
            fprintf(fsdqp,"%d 1 %d %d -0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* - Xij<= -uixj -ujxi +uiuj  (for i< j)*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->u[j]/2);
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,(double)qp->u[i]/2);
            fprintf(fsdqp,"%d 1 %d %d -0.5\n",compt_cont,i+2,j+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*for i = j and x non binary variable */
    /* Xii<= (ui +li)xi -uili (for non binary variables)*/
    for (i=0; i<qp->n; i++)
        if  (!((qp->u[i]==1) && (i < qp->nb_int)))
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)-(qp->u[i]/2+qp->l[i]/2));
            fprintf(fsdqp,"%d 1 %d %d 1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /* -Xii <= - 2lixi + li� (for non binary variables)*/
    for (i=0; i<qp->n; i++)
        if  (!((qp->u[i]==1) && (i < qp->nb_int)))
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->l[i]);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*- Xii <=  - 2uixi + ui� (for non binary variables)*/
    for (i=0; i<qp->n; i++)
        if (!((qp->u[i]==1) && (i < qp->nb_int)))
        {
            fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,i+2,(double)qp->u[i]);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*- Xii <=  - xi (for integer variables)*/
    for (i=0; i<qp->nb_int; i++)
        if  (qp->u[i]!=1)
        {
            fprintf(fsdqp,"%d 1 1 %d 0.5\n",compt_cont,i+2);
            fprintf(fsdqp,"%d 1 %d %d -1\n",compt_cont,i+2,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }

    /*X_ii = x_i (for binary variables) */
    for (i=0; i<qp->nb_int; i++)
        if (qp->u[i]==1)
        {
            fprintf(fsdqp,"%d 1 1 %d -0.5\n",compt_cont,i+2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,i+2,i+2);
            compt_cont++;
        }

    /*Ax =b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->a[ij2k(i,j,qp->n)]/2);
            compt_cont++;
        }

    /*Dx +s = e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"%d 1 1 %d %lf\n",compt_cont,j+2,qp->d[ij2k(i,j,qp->n+qp->p)]/2);
            fprintf(fsdqp,"%d 2 %d %d 1\n",compt_cont,compt_ecart,compt_ecart);
            compt_ecart++;
            compt_cont++;
        }


    /*X_11 = 1*/
    fprintf(fsdqp,"%d 1 1 1 1\n",compt_cont);


    fclose(fsdqp);
}

