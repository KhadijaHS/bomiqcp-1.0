/* -*-c-*-
 *
 *    Source     $RCSfile: in_out_scilab.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"in_out_scilab.h"
#include"utilities.h"
#include"in_out.h"


/******************************************************************************/
/******************write data files for Scilab ********************************/
/******************************************************************************/

void write_matrix_scilab_d(double * M,int n,int m, FILE * temp)
{
  int i,j;
  fprintf(temp,"[");
  for(i=0;i<n;i++)
    {
      for(j=0;j<m;j++)
	{
	  fprintf(temp,"%lf",M[ij2k(i,j,n)]);
	  if(j!=m-1)
	    fprintf(temp,",");
	}
      if(i!=n-1)
	fprintf(temp,";\n");
    }
  fprintf(temp,"]");
}

void write_matrix_scilab_cont_d(double * M,int ind,int n,int p,FILE * temp)
{
  int i,j,k;
  fprintf(temp,"[");
  for(i=0;i<n+p-1;i++)
    {
      for(j=0;j<n+p-1;j++)
	{
	  fprintf(temp,"%lf ",M[ijk2l(ind,i+1,j+1,n+p,n+p)]);
	  if(j!=n+p-2)
	    fprintf(temp,",");
	}
      if(i!=n+p-2)
	fprintf(temp,";\n");
    }
  fprintf(temp,"]");
}
  
void write_file_scilab(double * M, int u )	       
{ 
  FILE * file_scilab = fopen(data_scilab,"w");
  fprintf(file_scilab,"v=");
  write_matrix_scilab_d(M,u,u,file_scilab);
  fprintf(file_scilab,";\n");
  fclose(file_scilab);
}

void write_file_scilab_quad(double * M, int ind, int n, int p)	       
{ 
  FILE * file_scilab = fopen(data_scilab_quad,"w");
  fprintf(file_scilab,"v=");
  write_matrix_scilab_cont_d(M,ind,n,p,file_scilab);
  fprintf(file_scilab,";\n");
  fclose(file_scilab);
}


/**************************************************************************/
/******************* Write matlab file ************************************/
/*************************************************************************/
void write_matrix_matlab(double * M,int n, FILE * temp)
{
  int i,j;
  for(i=0;i<n;i++)
    {
      for(j=0;j<n;j++)
	fprintf(temp,"\t%lf",M[ij2k(i,j,n)]);
      fprintf(temp,"\n");
    }
}

void write_matrix_matlab_entropy(double * M,int n, FILE * temp)
{
  int i,j;
  for(i=0;i<n;i++)
    {
      for(j=0;j<n;j++)
	fprintf(temp,"%lf\n",M[ij2k(i,j,n)]);
    }

}
void write_vector_matlab_entropy(double * M,int n, FILE * temp)
{
  int i,j;
  for(i=0;i<n;i++)
    {
      fprintf(temp,"%lf\n",M[i]);
    }
}


void write_matrix_matlab_3d(double * M,int n, FILE * temp)
{
  int i,j,k;
  for(i=0;i<n;i++)
    {
      for(j=0;j<n;j++){
	for(k=0;k<n;k++)
	  fprintf(temp,"%lf\n",M[ijk2l(i,j,k,n,n)]);
      }
    }
}


void write_file_matlab(C_MIQCP cqp)	       
{ 
  FILE * file_matlab = fopen(data_matlab,"w");
  fprintf(file_matlab,"m=[");
  write_matrix_matlab(cqp->q,cqp->n,file_matlab);
  fprintf(file_matlab,"];\n");
  fprintf(file_matlab,"v=sort(eig(m));\n file_instance = fopen('../data/eigenValue.dat', 'w');\n fprintf(file_instance,'%%3.4f\\n',v(1));\n   fclose(file_instance);\n");
  fclose(file_matlab);
}

/*****write_file_matlab_triangle()*****/
void write_file_matlab_triangle(SDP psdp)	       
{
  double * q_beta;
  double * c_beta;
  double l_beta;
  FILE * file_matlab = fopen(data_entropy,"w");
  dualized_objective_function_entropy_mc(psdp, &q_beta,&c_beta,&l_beta);
  //dualized_objective_function_entropy(psdp, &q_beta,&c_beta,&l_beta);
  fprintf(file_matlab,"%d\n%d\n%lf\n",psdp->n,psdp->s,alpha_mixed[0]);
  write_matrix_matlab_entropy(psdp->q,psdp->n,file_matlab);
  write_matrix_matlab_entropy(q_beta,psdp->n,file_matlab);
  write_vector_matlab_entropy(c_beta,psdp->n,file_matlab);
  fprintf(file_matlab,"%lf\n",l_beta);
  fclose(file_matlab);
}
