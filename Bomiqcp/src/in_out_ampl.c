/* -*-c-*-
 *
 *    Source     $RCSfile: in_out_ampl.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"in_out_ampl.h"
#include"in_out.h"
#include"utilities.h"


/******************************************************************************/
/*************** Write vectors and matrices in ampl format ********************/
/******************************************************************************/

void write_vector_ampl(int *v, int n, FILE * temp, char * name)
{
    int i;
    fprintf(temp,"param %s :=\n",name);
    for(i=0; i<n; i++)
        fprintf(temp,"%d %d\n",i+1,v[i]);
    fprintf(temp,";\n");
}

void write_vector_ampl_d(double *v, int n, FILE * temp, char * name)
{
    int i;
    fprintf(temp,"param %s :=\n",name);
    for(i=0; i<n; i++)
        fprintf(temp,"%d %lf\n",i+1,v[i]);
    fprintf(temp,";\n");
}

void write_matrix_ampl(int * M, int m,int n, FILE * temp, char * name)
{
    int i;
    int j;
    fprintf(temp,"param %s : ",name);
    for(i=0; i<n; i++)
        fprintf(temp,"%d ",i+1);
    fprintf(temp,":=\n");
    for(i=0; i<m; i++)
    {
        fprintf(temp,"%d ",i+1);
        for(j=0; j<n; j++)
            fprintf(temp,"%d ",M[ij2k(i,j,n)]);
        if(i==m-1)
            fprintf(temp,";\n");
        else
            fprintf(temp,"\n");
    }

}

void write_matrix_ampl_d(double * M, int m,int n, FILE * temp, char * name)
{
    int i;
    int j;
    fprintf(temp,"param %s : ",name);
    for(i=0; i<n; i++)
        fprintf(temp,"%d ",i+1);
    fprintf(temp,":=\n");
    for(i=0; i<m; i++)
    {
        fprintf(temp,"%d ",i+1);
        for(j=0; j<n; j++)
            fprintf(temp,"%lf ",M[ij2k(i,j,n)]);
        if(i==m-1)
            fprintf(temp,";\n");
        else
            fprintf(temp,"\n");
    }

}

void write_matrix_ampl_3d(double * M, int m,int n, FILE * temp, char * name)
{
    int i,j,k;

    fprintf(temp,"param %s :=\n",name);
    for(i=0; i<m; i++)
        for(j=0; j<n; j++)
            for(k=0; k<n; k++)
            {
                fprintf(temp,"%d ",i+1);
                fprintf(temp,"%d ",j+1);
                fprintf(temp,"%d ",k+1);
                fprintf(temp,"%lf ",M[ijk2l(i,j,k,n,n)]);
            }

    fprintf(temp,";\n");

}

void write_set_ampl(int *v, int* lg,int n, FILE * temp, char * name)
{
    int i,j;
    fprintf(temp,"set %s :=",name);
    fprintf(temp,"\{");
    while(i<n)
    {
        fprintf(temp,"(");
        for(j=0; j<lg[i]; j++)
        {
            if (j!=lg[i]-1)
                fprintf(temp,"%d,",v[i]);
            else
                fprintf(temp,"%d",v[i]);
        }
        i++;
        if (i!=n)
            fprintf(temp,"),");
    }
    printf("sorti");

}

/******************************************************************************/
/*************** Write data files in ampl format ******************************/
/******************************************************************************/

void write_file_ampl(MIQCP qp)
{

    FILE * file_ampl = fopen(data_local,"w");
    fprintf(file_ampl,"data;\n");
    fprintf(file_ampl,"param n:= %d;\n",qp->n);
    fprintf(file_ampl,"param n_i:= %d;\n",qp->nb_int);
    fprintf(file_ampl,"param m:= %d;\n",qp->m);
    fprintf(file_ampl,"param p:= %d;\n",qp->p);
    if (Positive(qp->m))
        qp->mq--;
    fprintf(file_ampl,"param mq:= %d;\n",qp->mq);
    if (Positive(qp->p))
        qp->pq -= 2*qp->n*qp->p;
    fprintf(file_ampl,"param pq:= %d;\n",qp->pq);
    write_vector_ampl_d(qp->u,qp->n,file_ampl,"u");
    write_matrix_ampl_d(qp->q,qp->n,qp->n,file_ampl,"Q");
    write_vector_ampl_d(qp->c,qp->n,file_ampl,"c");

    if(!Zero(qp->m))
    {
        write_matrix_ampl_d(qp->a,qp->m,qp->n,file_ampl,"A");
        write_vector_ampl_d(qp->b,qp->m,file_ampl,"b");
    }

    if(!Zero(qp->p))
    {
        write_matrix_ampl_d(qp->d,qp->p,qp->n,file_ampl,"D");
        write_vector_ampl_d(qp->e,qp->p,file_ampl,"e");
    }


    if(!Zero(qp->mq))
    {
        write_matrix_ampl_3d(qp->aq,qp->mq,qp->n+1,file_ampl,"Aq");
        write_vector_ampl_d(qp->bq,qp->mq,file_ampl,"bq");
    }

    if(!Zero(qp->pq))
    {
        write_matrix_ampl_3d(qp->dq,qp->pq,qp->n+1,file_ampl,"Dq");
        write_vector_ampl_d(qp->eq,qp->pq,file_ampl,"eq");
    }

    fprintf(file_ampl,"end;\n");
    fclose(file_ampl);
}

void write_file_reformulated_ampl(C_MIQCP qp)
{
    FILE * file_ampl = fopen(buf_out,"w");
    fprintf(file_ampl,"data;\n");
    fprintf(file_ampl,"param n:= %d;\n",qp->n);
    fprintf(file_ampl,"param n_i:= %d;\n",qp->nb_int);
    fprintf(file_ampl,"param m:= %d;\n",qp->m);
    fprintf(file_ampl,"param p:= %d;\n",qp->p);
    fprintf(file_ampl,"param mq:= %d;\n",qp->mq);
    fprintf(file_ampl,"param pq:= %d;\n",qp->pq);
    write_vector_ampl_d(qp->u,qp->n,file_ampl,"u");
    write_matrix_ampl_d(qp->new_q,qp->n,qp->n,file_ampl,"Q");
    write_vector_ampl_d(qp->c,qp->n,file_ampl,"c");
    write_matrix_ampl_d(qp->beta,qp->n,qp->n,file_ampl,"beta");
    write_matrix_ampl(qp->nb_var_y,qp->n,qp->n,file_ampl,"nb_var_y");
    if(!Zero(qp->m))
    {
        write_matrix_ampl_d(qp->a,qp->m,qp->n,file_ampl,"A");
        write_vector_ampl_d(qp->b,qp->m,file_ampl,"b");
    }

    if(!Zero(qp->p))
    {
        write_matrix_ampl_d(qp->d,qp->p,qp->n,file_ampl,"D");
        write_vector_ampl_d(qp->e,qp->p,file_ampl,"e");
    }


    if(!Zero(qp->mq))
    {
        write_matrix_ampl_3d(qp->aq,qp->mq,qp->n+1,file_ampl,"Aq");
        write_vector_ampl_d(qp->bq,qp->mq,file_ampl,"bq");
    }

    if(!Zero(qp->pq))
    {
        write_matrix_ampl_3d(qp->dq,qp->pq,qp->n+1,file_ampl,"Dq");
        write_vector_ampl_d(qp->eq,qp->pq,file_ampl,"eq");
    }

    fprintf(file_ampl,"end;\n");
    fclose(file_ampl);
}


void write_file_ampl_bb(C_MIQCP qp, double ** l, double ** u, double b_sup)
{

    FILE * file_ampl = fopen(data_local,"w");

    fprintf(file_ampl,"data;\n");
    fprintf(file_ampl,"param n:= %d;\n",qp->n);
    fprintf(file_ampl,"param n_i:= %d;\n",qp->nb_int);
    fprintf(file_ampl,"param m:= %d;\n",qp->m);
    fprintf(file_ampl,"param p:= %d;\n",qp->p);
    fprintf(file_ampl,"param b_sup:= %lf;\n",b_sup);
    int mq=qp->mq;
    int pq=qp->pq;
    if (Positive(qp->m))
        mq = qp->mq-1;
    fprintf(file_ampl,"param mq:= %d;\n",mq);
    if (Positive(qp->p))
        pq = qp->pq - (2*qp->n*qp->p);
    fprintf(file_ampl,"param pq:= %d;\n",pq);
    write_vector_ampl_d(*u,qp->n,file_ampl,"u");
    write_vector_ampl_d(*l,qp->n,file_ampl,"l");
    write_matrix_ampl_d(qp->q,qp->n,qp->n,file_ampl,"Q");
    write_vector_ampl_d(qp->c,qp->n,file_ampl,"c");

    if(!Zero(qp->m))
    {
        write_matrix_ampl_d(qp->a,qp->m,qp->n,file_ampl,"A");
        write_vector_ampl_d(qp->b,qp->m,file_ampl,"b");
    }

    if(!Zero(qp->p))
    {
        write_matrix_ampl_d(qp->d,qp->p,qp->n,file_ampl,"D");
        write_vector_ampl_d(qp->e,qp->p,file_ampl,"e");
    }


    if(!Zero(mq))
    {
        write_matrix_ampl_3d(qp->aq,mq,qp->n+1,file_ampl,"Aq");
        write_vector_ampl_d(qp->bq,mq,file_ampl,"bq");
    }

    if(!Zero(pq))
    {
        write_matrix_ampl_3d(qp->dq,pq,qp->n+1,file_ampl,"Dq");
        write_vector_ampl_d(qp->eq,pq,file_ampl,"eq");
    }

    fprintf(file_ampl,"end;\n");
    fclose(file_ampl);
}
