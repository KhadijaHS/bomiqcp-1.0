/* -*-c-*-
 *
 *    Source     $RCSfile: p_miqcrq.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/


#ifndef P_MIQCRQ_H_INCLUDED
#define P_MIQCRQ_H_INCLUDED

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"quad_prog.h"


/*********************************************************************/
/****************************** Variable *****************************/
/*********************************************************************/
C_MIQCP cqp;
double best_sol_adm_miqcp;
/*double best_sol_adm=0;*/

/*********************************************************************/
/***************************** Functions *****************************/
/*********************************************************************/
void  compute_alpha_beta_p_miqcrq( MIQCP qp,  double ** beta, double ** alphaq,double ** alphabisq, SDP psdp) ;
void compute_new_q_bb_quad(C_MIQCP cqp);
void compute_new_lambda_min_bb_quad(C_MIQCP cqp) ;
void p_miqcrq( struct miqcp_bab bab, FILE * aa);
void compute_new_q_p_miqcrq(C_MIQCP cqp);
void compute_new_q_linearization(C_MIQCP cqp);
int setqpproblemdata_p_miqcrq (struct miqcp_bab bab, char **probname_p, int *numcols_p, int *numrows_p, int *objsen_p, double **obj_p, double **rhs_p, char **sense_p, int **matbeg_p, int **matcnt_p, int **matind_p, double **matval_p, int **qmatbeg_p, int **qmatcnt_p, int **qmatind_p, double **qmatval_p, double **lb_p, double **ub_p, char ** ctype_p);

#endif //P_MIQCRQ_H_INCLUDED
