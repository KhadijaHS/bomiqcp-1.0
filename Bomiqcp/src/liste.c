/* -*-c-*-
 *
 *    Source     $RCSfile: liste.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

#include"liste.h"
#include"utilities.h"
#include"s_miqcrq_bb.h"

/*****new_liste()*****/
Liste new_liste()
{
    return  (Liste)malloc(sizeof(struct liste));
}

/*****new_node()*****/
Node new_node(double value, double * sol, struct miqcp_bab new_bab, int nb_col)
{
    Node node = (Node)malloc(sizeof(struct node));
    if (node == NULL)
    {
        printf ("\nnew_node : Could not create node.\n");
        return NULL;
    }

    node->inf_bound_value = value;
    node->sol_x =sol;
    node->bab =realloc_mbab_liste(new_bab,nb_col);
    node->previous = NULL;
    node->next =NULL;
    return node;
}

/*****init_liste()*****/
Liste init_liste()
{
    Liste liste = new_liste();
    if (liste == NULL)
    {
        printf ("\ninit_liste : Could not create liste.\n");
        return NULL;
    }

    liste->first =NULL;
    liste->size=0;
    return liste;
}

/*****insert_liste()*****/
void insert_liste(Liste liste, double value, double * sol, struct miqcp_bab bab, int nb_col)
{
    Node tmp=liste->first;
    int i=0;
    /*init new node*/
    Node node = new_node(value,sol,bab,nb_col);
    if (node == NULL)
    {
        printf ("\n insert :Could not create node.\n");
        return;
    }

    if (liste->size==1)
    {
        if (tmp->inf_bound_value < value)
        {
            tmp->next=node;
            node->previous=tmp;
            node->next=NULL;
            liste->size++;
        }
        else
        {
            node->next=tmp;
            tmp->previous=node;
            node->previous=NULL;
            liste->size++;
            liste->first=node;
            borne_inf_bb=node-> inf_bound_value ;
        }
    }
    else if (liste->size>1)
    {
        while(i<liste->size-1)
        {
            if (tmp->inf_bound_value > value)
                break;

            tmp=tmp->next;
            i++;
        }
        if (i==liste->size-1)
        {
            if (tmp->inf_bound_value > value)
            {
                node->previous=tmp->previous;
                node->next=tmp;
                tmp->previous->next = node;
                tmp->previous=node;
                liste->size++;
            }
            else
            {
                tmp->next=node;
                node->previous=tmp;
                node->next=NULL;
                liste->size++;
            }
        }
        else
        {

            if (i==0)
            {
                node->next=tmp;
                node->previous=NULL;
                tmp->previous=node;
                liste->size++;
                liste->first=node;
                borne_inf_bb=node-> inf_bound_value ;
            }
            else
            {
                node->next=tmp;
                node->previous=tmp->previous;
                tmp->previous->next=node;
                tmp->previous=node;
                liste->size++;
            }
        }
    }
    else if(liste->size==0)
    {
        node->next = NULL;
        node->previous= NULL;
        liste->first = node;
        borne_inf_bb=node-> inf_bound_value ;
        liste->size++;
    }
}

/*****suppress_first_liste()*****/
void suppress_first_liste(Liste liste)
{
    if (liste->size == 0)
    {
        printf ("\n suppress :Could not supress node : NULL liste.\n");
    }
    if (liste->size == 1)
    {
        liste->first=NULL;
        liste->size=0;
    }
    else
    {
        Node to_delete = liste->first;
        liste->first = liste->first->next;
        liste->first->previous=NULL;
        liste->size--;
        /*free_node(to_delete);*/
    }
}

/*****clean_liste()*****/
void clean_liste(Liste liste, double value)
{
    Node tmp=liste->first;

    int i=0;

    if (liste->size ==1)
    {
        if (tmp->inf_bound_value > value)
        {
            liste->first=NULL;
            liste->size=0;
        }
    }
    if (liste->size>1)
    {
        while (i< liste->size)
        {
            if (tmp->inf_bound_value > value)
                if (i==0)
                {
                    liste->first=NULL;
                    liste->size=0;
                }
                else
                {
                    tmp->previous->next=NULL;
                    liste->size=i;
                    break;
                }
            tmp=tmp->next;
            i++;
        }
    }
}

/*****print_liste()*****/
void print_liste(Liste liste)
{
    if (liste->size == 0)
    {
        printf ("\n print_liste :Could not print liste : NULL liste.\n");
    }
    else
    {
        Node current=liste->first;
        int i =0;

        while (i< liste->size)
        {
            printf("\nliste elem %d \n",i);
            printf("valeur borne : %lf\n",current->inf_bound_value);
            current=current->next;
            i++;
        }
    }
}
