/* -*-c-*-
 *
 *    Source     $RCSfile: utilities.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors     Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License

//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#ifndef UTILITIES_H_INCLUDED
#define UTILITIES_H_INCLUDED

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"quad_prog.h"

/*********************************************************************/
/****************************** Variable *****************************/
/*********************************************************************/
/* parameter for entropy*/
double * alpha_mixed;

/* parameters for algorithms (default quadratic objective function and linear constraints)*/
static int cont_lin=1;
static int cont_quad=0;
static int obj_quad=1;
static int obj_lin=0;

/*max upper bound on variable*/
#define MAXU 10000
#define MAX_TIME_SOL_INIT 120
#define MAX_TIME_SOL_LOCAL 3
#define EPS_LOCAL_SOL 1e-4
#define NB_PASS_OBBT 1
#define NB_PASS_OBBT_INIT 1
#define EPS_OBBT 1e-2

/* parameters for equality constraints*/
#define FACTOR_ALPHA 1

/* parameters for CPLEX */
#define ABS_GAP 0.999
#define OPT_TOL 1e-6
#define CONV_TOL 1e-6
#define MEMORY 64000
#define BARRIER_TOL 1e-6
#define OBJDIF 0.999
#define EPRHS 1e-6
#define EPRELAX 1e-6
#define HEURFREQ 0 /* 0  default value :  cplex choose*/
#define SYMBREAK -1 /* -1  default value :  cplex choose*/
#define PRESOLVENODE  0 /* 0  default value :  cplex choose*/

int TIME_LIMIT_CPLEX;
double REL_GAP;
int THREAD;
int VARSEL; /*-1 or 1 or 2 or 3 or 4 default 0*/

/* parameters for SB */
char * TIME_LIMIT_SB;
char * EPS_SB;
char * FILE_SB;

/* parameters for CONIC BUNDLE */
#define MAX_SOL_SDP 1e12
#define EPS_BETA 1e-6
#define EPS_LM 1e-6
#define EPS_VIOL_CB 1e-3
#define EPS_TERM_CB 1e-2
#define ACT_BOUND 1
#define EVAL_LIMIT 1000
#define UPDATE_LIMIT 500
#define FACTOR 1
#define UPPER_BOUND 1e5
#define MAX_SUBG 1 /* a ne pas changer sinon incohérence*/
#define NB_MAX_ITER 30 /*parametre perso*/
#define TIME_LIMIT_CB 1000
double PREC_STEP;
double PREC;
int NB_DESCENT_STEP;

/* parameters for BB */
#define _5BRANCHES 0
#define MAX_SOL_BB 1e10
#define MIN_SOL_BB -1e10
#define GAMMA 0
#define EPS_INT 1e-2
#define EPS_ABS_GAP 0.99
#define NB_NODE_COMPUTE_LOCAL 10

double EPS_BB;
double EPS_BRANCH;
int TIME_LIMIT_BB;

/* parameters for CSDP */
double AXTOL;
double AYTOL;
double OBJTOL;
double PINFTOL;
double DINFTOL;
double MAXITER;
double MINSF;
double MAXSF;
double MINSTEPP;
double MINSTEPD;
int USEXZGAP;
int TWEAKGAP;
int AFFINE;
int PERTURBOBJ;
int FASTMODE;

/* for initilization of the avl tree*/
int NB_MAX_CONT;

/* Macros */
#define EPS 1e-6
#define EPS_LAMBDA_MIN 1e-12
#define Round(a) (a>=0?(int) (a+0.5) :(int) (a-0.5) )
#define Positive(a) (a> EPS)
#define Negative(a) (a<-EPS)
#define Zero(a) (!Positive(a) && !Negative(a))
#define Positive_BB(a) (a> EPS_BETA)
#define Negative_BB(a) (a<- EPS_BETA)
#define Positive_VIOL_CB(a) (a> EPS_VIOL_CB)
#define Negative_VIOL_CB(a) (a< - EPS_VIOL_CB)
#define Zero_VIOL_CB(a) (!Positive_VIOL_CB(a) && !Negative_VIOL_CB(a))
#define Zero_BB(a) (!Positive_BB(a) && !Negative_BB(a))
#define Positive_LM(a) (a> EPS_LM)
#define Negative_LM(a) (a<- EPS_LM)
#define Zero_LM(a) (!Positive_LM(a) && !Negative_LM(a))
#define Positive_LAMBDA_MIN(a) (a> EPS_LAMBDA_MIN)
#define Negative_LAMBDA_MIN(a) (a<-EPS_LAMBDA_MIN)
#define Zero_LAMBDA_MIN(a) (!Positive(a) && !Negative(a))

/*indices from an ** to an *, and reciprocally*/
#define ij2k(i,j,n) ((i)*(n)+(j))
#define k2i(k,n) ((int)(k)/(n))
#define k2j(k,n) ((k)%(n))

/*indices from an *** to an *, and reciprocally*/
#define ijk2l(i,j,k,n,p) ((i)*(n)*(p)+(j)*(p) +(k))

/* global variables */
int nb_int_node;
int nodecount;

double start_time;
double sol_sdp;
double best_sol_adm;
double upper_bound;

char TYPE_T; /* if relax 'C' else 'B' */
char TYPE_X; /* if relax 'C' else 'I' */
char TYPE_Y; /* if relax 'C' else 'I' */
int DEBUG; /*DEBUG == 0, i.e. no debug*/

/*********************************************************************/
/****************************** Functions ****************************/
/*********************************************************************/
/*  print tables */
void print_vec_d(double * tab, int n);
void print_mat_d(double * tab, int n, int m);
void print_vec(int * tab, int n);
void print_mat(int * tab, int n, int m);
void print_tab(char * tab, int n);
void print_mat_3d(double * tab, int n, int m,int l);

/* allocate memory */
int* alloc_vector (int dimension);
int * alloc_matrix (int ligne, int colonne);
double* alloc_vector_d (int dimension);
double * alloc_matrix_d (int ligne, int colonne);
double * alloc_matrix_3d (int n, int m, int p);
char* alloc_string (int dimension);
char* alloc_tab (int dim1,int dim2);
MIQCP_BAB realloc_mbab_liste(struct miqcp_bab bab, int nb_col);
void free_string(char * chaine);
void free_vector(int * v);
void free_vector_d(double * v);
void free_and_null (char **ptr);

/* basic operations on vectors*/
double v_abs(double a);
void v_abs_ref(double *a);
int sum_vector(int * a, int j,int n);
double sum_vector_d(double * a, int j, int n);
int maximum(int * vector,int n);
double maximum_d(double * vector,int n);
double maximum_d_abs(double * vector,int n);
double minimum_d(double * vector,int n);
int minimum(int * vector,int n);
int * copy_vector(int *v, int n);
double * copy_vector_d(double *v, int n);
double * copy_matrix_d(double *matrix, int m,int n);
double * copy_matrix_3d(double *matrix, int m,int n,int p);
char * copy_string(char *s);
void copy_line(double * B, double * v, int i,int t);
int nb_non_zero_matrix_sym(double * a, int n);
int nb_non_zero_matrix(double * a, int n, int m);
int nb_non_zero_matrix_i(int * a, int n, int m);
int nb_non_zero_matrix_sup_i(int *a, int n, int m);
int nb_non_zero_matrix_without_diag(double *a, int n, int m);
int nb_non_zero_matrix_without_diag_i(int *a, int n, int m);
int nb_non_zero_vector(double * v, int n);
int nb_non_zero_vector_int(int * v, int n);
int nb_non_zero_matrix_3d(double * a, int m, int n, int p);
double * add_two_vector(double * v1, int * v2,int n);
double * opposite_matrix_3d(double * M, int m, int n, int p);

/*management of bounds*/
int log_base_2 (int x);
int vector_length_base_2(double * u, int n);
int * create_u_base2_cumulated(double * u, int n);
int * create_u_base2(double * u, int n);

/*change an int into a char*/
char * intochar(int val);
double random_double(double min, double max);

/* basic operations on matrices*/
double * multiplication_matrices( double * a, double * b, int n);
double * multiplie_d(double * M, double * P, int n,int m);
double * transpose_d(double * M, int n, int m);
double * matrice_par_scal(double * M, int n, int m, double scal);
double * addition_mat_d(double* M,double * P,int n);
double * carre_cont(double * M, int m, int n);
double * vect_c(double* M,double * v, int m, int n);
double * vect_par_scal(double * v, int n, double scal);
double * addition_vect(double * v,double * w, int n);
double constante(double * v, int n,int scal);
int is_symetric(double * a, int n);
int is_symetric_3d(double * a,int k, int n);
double * transpose_matrix( double * a, int n);
double * multiplication_matrices( double * a, double * b, int n);
double * transpose_matrix_mn( double * a, int m, int n);

/* utilities for cplex*/
double sum_r_ari_b_r(double * a, double * b,int m,int n, int i);
double sum_r_ari_arj(double * a, int m,int n, int i, int j);
double sum_r_b_r(double * b,int m);
double sum_r_alphaq_bq_r(double * b,double* alphaq,int m);

/* utilities for the sdp solver*/
void dualized_objective_function(SDP psdp, double ** q_beta, double ** c_beta, double * l);
void evaluate_objective(double * objective_value,SDP psdp, double ** q_beta, double ** c_beta, double *l_beta);
void compute_subgradient(double ** subgrad,SDP psdp);
void compute_new_subgradient(double ** check_violated,SDP psdp,int new_length, int ** variable_indices,double **X);
void purge_constraints_not_violated(SDP psdp, double ** slacks, int * n_assign, int ** assign_new_from_old);
void add_constraints_violated(SDP psdp, double ** check_violated,int * n_append);
void initialize_constraints_violated(SDP psdp, double ** check_violated, int * new_length);
int check_constraints_violated(SDP psdp, double ** check_violated, double ** x);
void update_beta_value(SDP psdp, double * zbeta);
int nb_max_cont(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4);
int nb_cont_Xij(int n, int nb_int, int p);
int nb_max_cont_cont(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4);
int nb_cont_Xij_cont(int n);
int nb_max_cont_mixed(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4);
int nb_max_cont_triangle(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4,int * ind_X5,int * ind_X6,int *ind_X7,int*ind_X8,int * ind_X9,int * ind_X10,int *ind_X11,int*ind_X12,int*ind_X13,int*ind_X14,int*ind_X15,int*ind_X16);
int nb_max_cont_mkc_r2(SDP psdp, int * ind_X1,int * ind_X2);
int nb_max_cont_mkc_r4(SDP psdp, int * ind_X1,int * ind_X2);

/* utilities for the sdp 0 1 solver*/
void dualized_objective_function_0_1(SDP psdp, double ** q_beta, double ** c_beta, double * l);
void compute_subgradient_0_1(double ** subgrad,SDP psdp);
void compute_new_subgradient_0_1(double ** check_violated,SDP psdp,int new_length, int ** variable_indices,double **X);
int check_constraints_violated_0_1(SDP psdp, double ** check_violated, double ** x);
void update_beta_value_0_1(SDP psdp, double * zbeta);
int nb_cont_Xij_0_1(int n);
int ijk2l_triangular(int i,int j,int k, int n);
int nb_max_cont_0_1(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4, int * ind_X5,int * ind_X6,int *ind_X7,int*ind_X8);

/* utilities for the sdp qap solver*/
void dualized_objective_function_qap(SDP psdp, double ** q_beta, double ** c_beta, double * l);
void compute_subgradient_qap(double ** subgrad,SDP psdp);
void compute_new_subgradient_qap(double ** check_violated,SDP psdp,int new_length, int ** variable_indices,double **X);
int check_constraints_violated_qap(SDP psdp, double ** check_violated, double ** x);
void update_beta_value_qap(SDP psdp, double * zbeta);
int nb_cont_Xij_qap(SDP psdp);
int nb_max_cont_qap(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3);

/*utilities for the branch and bound*/
/* utilities for the sdp qap v2 solver*/
void dualized_objective_function_qapv2(SDP psdp, double ** q_beta, double ** c_beta, double * l);
void compute_subgradient_qapv2(double ** subgrad,SDP psdp);
void compute_new_subgradient_qapv2(double ** check_violated,SDP psdp,int new_length, int ** variable_indices,double **X);
int check_constraints_violated_qapv2(SDP psdp, double ** check_violated, double ** x);
void update_beta_value_qapv2(SDP psdp, double * zbeta);
int nb_max_cont_qapv2(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4);

/*utilities for the branch and bound*/
/* utilities for the sdp qap v3 solver*/
void dualized_objective_function_qapv3(SDP psdp, double ** q_beta, double ** c_beta, double * l);
void compute_subgradient_qapv3(double ** subgrad,SDP psdp);
void compute_new_subgradient_qapv3(double ** check_violated,SDP psdp,int new_length, int ** variable_indices,double **X);
int check_constraints_violated_qapv3(SDP psdp, double ** check_violated, double ** x);
void update_beta_value_qapv3(SDP psdp, double * zbeta);
int nb_max_cont_qapv3(SDP psdp, int * ind_X1);

/* utilities for the sdp  solver mkc_r4*/
void dualized_objective_function_mkc_r4(SDP psdp, double ** q_beta, double ** c_beta, double * l);
void compute_subgradient_mkc_r4(double ** subgrad,SDP psdp);
void compute_new_subgradient_mkc_r4(double ** check_violated,SDP psdp,int new_length, int ** variable_indices,double **X);
int check_constraints_violated_mkc_r4(SDP psdp, double ** check_violated, double ** x);
void update_beta_value_mkc_r4(SDP psdp, double * zbeta);

/* utilities for the sdp  solver mkc_r2*/
void dualized_objective_function_mkc_r2(SDP psdp, double ** q_beta, double ** c_beta, double * l);
void compute_subgradient_mkc_r2(double ** subgrad,SDP psdp);
void compute_new_subgradient_mkc_r2(double ** check_violated,SDP psdp,int new_length, int ** variable_indices,double **X);
int check_constraints_violated_mkc_r2(SDP psdp, double ** check_violated, double ** x);
void update_beta_value_mkc_r2(SDP psdp, double * zbeta);

/* utilities for the sdp  solver triangles*/
void dualized_objective_function_triangle(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta);
void compute_subgradient_triangle(double ** check_violated,SDP psdp);
void compute_new_subgradient_triangle(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X);
int check_constraints_violated_triangle(SDP psdp, double ** check_violated, double ** x);
void update_beta_value_triangle(SDP psdp, double * zbeta);

/* utilities for the sdp  solver entropy*/
void evaluate_objective_entropy(double * objective_value,SDP psdp, double ** q_beta, double ** c_beta, double *l_beta);
void dualized_objective_function_entropy(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta);
void dualized_objective_function_entropy_mc(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta);
void refine_bounds(struct miqcp_bab bab, C_MIQCP cqp);
int compare_solution_miqcp(double sol_adm, double * best_sol_adm,double * x_y, FILE * f, C_MIQCP cqp);
int feasible_for_constraints(double * x_y, C_MIQCP cqp);
int update_boud_f_x_miqcp(double fx);
int is_feasible_miqcp( double * x_y, C_MIQCP cqp);
int select_i_j_miqcrq(double * x_y, struct miqcp_bab bab, int *i, int *j, C_MIQCP cqp);
int select_i_j_miqcrq_bis(double * x_y, struct miqcp_bab bab, int *i, int *j, C_MIQCP cqp);
struct miqcp_bab copy_bab(struct miqcp_bab bab);
struct miqcp_bab update_variables_int_branch_1(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_int_branch_1_diag(struct miqcp_bab bab, double * x_y, int* i,int *j, C_MIQCP cqp);
struct miqcp_bab  update_variables_int_branch_2(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp);
struct miqcp_bab  update_variables_int_branch_3(struct miqcp_bab bab, double * x_y, int* i,int * j, C_MIQCP cqp);
struct miqcp_bab update_variables_int_branch_4(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_int_branch_5(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_int_branch_6(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_int_branch_7(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_cont_branch_1(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_cont_branch_1_diag(struct miqcp_bab bab, double * x_y, int* i,int *j, C_MIQCP cqp);
struct miqcp_bab  update_variables_cont_branch_2(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp);
struct miqcp_bab  update_variables_cont_branch_3(struct miqcp_bab bab, double * x_y, int* i,int * j, C_MIQCP cqp);
struct miqcp_bab update_variables_cont_branch_4(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_cont_branch_5(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp);
struct miqcp_bab  update_variables_mixed_branch_1(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp);
struct miqcp_bab  update_variables_mixed_branch_2(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp);
struct miqcp_bab  update_variables_mixed_branch_3(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqpa);
struct miqcp_bab update_variables_spatial_branch_1(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_spatial_branch_2(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_cont_branch_1bis(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_cont_branch_2bis(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_cont_branch_3bis(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp);
struct miqcp_bab update_variables_cont_branch_4bis(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp);
int test_bound_miqcp(struct miqcp_bab bab, int nb_col);
int is_integer(double a);
double check_value(double a);
double sum_ij_qi_qj_x_ij(double * q, double * x, int n);
double sum_i_ci_x_i(double * c, double * x, int n);

/* Utilities for unconstrained programs */
MIQCP reformulate_iqp(MIQCP qp);
void binary_to_integer(MIQCP qp, FILE * file_save);

/* compute nb_eigenvalues */
void compute_number_neg_eigenvalues(MIQCP qp,double ** vspectre,int * nb_neg);


#endif // UTILITIES_H_INCLUDED
