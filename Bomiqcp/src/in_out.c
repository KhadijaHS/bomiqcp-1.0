/* -*-c-*-
 *
 *    Source     $RCSfile: in_out.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"in_out.h"
#include"quad_prog.h"
#include"utilities.h"
//#include"obbt.h"

/*********************************************************************************/
/*************** write vectors and matrix in the save file ***********************/
/*********************************************************************************/
/*write_vector_d_save()*/
void write_vector_d_save(double * v, int n,FILE * temp)
{
    int i;
    for (i=0; i<n; i++)
        fprintf(temp,"%.2lf ",v[i]);
}

/*write_vector_d_save_opp()*/
void write_vector_d_save_opp(double * v, int n,FILE * temp)
{
    int i;
    for (i=0; i<n; i++)
        fprintf(temp,"%.2lf ",-v[i]);
}

/*write_matrix_d_save()*/
void write_matrix_d_save(double * M, int m, int n,FILE *temp)
{
    int i,j,nb_non_zero;
    for (i=0; i<m; i++)
        for(j=0; j<n; j++)
            if( (ij2k(i,j,n) +1 ) % (n) ==0)
                fprintf(temp,"%.6lf\n ",M[ij2k(i,j,n)]);
            else
                fprintf(temp,"%.6lf ",M[ij2k(i,j,n)]);

}

/*********************************************************************************/
/******************* write vectors and matrix in à file **************************/
/*********************************************************************************/
/*write_vector()*/
void write_vector(int * v, int n,FILE * temp)
{
    int i;
    for (i=0; i<n; i++)
        fprintf(temp,"%d ",v[i]);
    fprintf(temp,"\n");
}

/*write_vector_sol()*/
void write_vector_sol(double * v, int n,FILE * temp)
{
    int i;
    for (i=0; i<n; i++)
        fprintf(temp,"%d ",(int)v[i]);
}

/*write_vector_d()*/
void write_vector_d(double * v, int n,FILE * temp)
{
    int i,nb_non_zero;
    nb_non_zero= nb_non_zero_vector(v,n);
    fprintf(temp,"%d\n",nb_non_zero);
    for (i=0; i<n; i++)
        if(!Zero(v[i]))
            fprintf(temp,"%d %lf\n",i,v[i]);
}

/*write_vector_u()*/
void write_vector_u(double * v, int n,FILE * temp)
{
    int i,nb_non_zero;
    for (i=0; i<n; i++)
        fprintf(temp,"%lf ",v[i]);
    fprintf(temp,"\n");
}

/*write_matrix()*/
void write_matrix(int * M, int m, int n,FILE *temp)
{
    int i,j;
    for (i=0; i<m; i++)
        for(j=0; j<n; j++)
            if(!Zero(M[ij2k(i,j,n)]))
                fprintf(temp,"%d %d %d\n",i,j,M[ij2k(i,j,n)]);
}

/*write_matrix_d()*/
void write_matrix_d(double * M, int m, int n,FILE *temp)
{
    int i,j,nb_non_zero;
    nb_non_zero= nb_non_zero_matrix(M,n,m);
    fprintf(temp,"%d\n",nb_non_zero);
    for (i=0; i<m; i++)
        for(j=0; j<n; j++)
            if(!Zero(M[ij2k(i,j,n)]))
                fprintf(temp,"%d %d %lf\n",i,j,M[ij2k(i,j,n)]);
}

/*write_matrix_Qd()*/
void write_matrix_Qd(double * M, int m, int n,FILE *temp)
{
    int i,j,nb_non_zero;
    nb_non_zero= nb_non_zero_matrix(M,n,m);
    fprintf(temp,"%d\n",nb_non_zero);
    for (i=0; i<m; i++)
        for(j=0; j<n; j++)
            if(!Zero(M[ij2k(i,j,n)]))
                fprintf(temp,"%d %d %lf\n",i,j,M[ij2k(i,j,n)]);
}
/*write_matrix_3d()*/
void write_matrix_3d(double * M, int m, int n,int p,FILE *temp)
{
    int i,j,k,nb_non_zero;
    nb_non_zero= nb_non_zero_matrix_3d(M,m,n+1,n+1);
    fprintf(temp,"%d\n",nb_non_zero);
    for (i=0; i<m; i++)
        for(j=0; j<n+1; j++)
            for(k=0; k<p+1; k++)
                if(!Zero(M[ijk2l(i,j,k,n+1,p+1)]))
                    fprintf(temp,"%d %d %d %lf\n",i,j,k,M[ijk2l(i,j,k,n+1,p+1)]);
}

/**********************************************************************************/
/****************************** write save file ***********************************/
/**********************************************************************************/
/*write_file_instance()*/
void write_file_instance(MIQCP qp)
{
    FILE * file_instance = fopen(buf_out,"a");
    fprintf(file_instance,"%d %d %d %d %d %d\n",qp->n,qp->nb_int,qp->m,qp->p,qp->mq,qp->pq);
    fprintf(file_instance,"u\n");
    write_vector_u(qp->u,qp->n,file_instance);
    fprintf(file_instance,"Q\n");
    write_matrix_Qd(qp->q,qp->n,qp->n,file_instance);
    fprintf(file_instance,"c\n");
    write_vector_d(qp->c,qp->n,file_instance);
    /*fprintf(file_instance,"l\n%lf\n",qp->cons);*/

    /*Ax =b*/
    if(qp->m!=0)
    {
        fprintf(file_instance,"A\n");
        write_matrix_d(qp->a,qp->m,qp->n,file_instance);
        fprintf(file_instance,"b\n");
        write_vector_d(qp->b,qp->m,file_instance);
    }
    /*Dx<=e*/
    if(qp->p!=0)
    {
        fprintf(file_instance,"D\n");
        write_matrix_d(qp->d,qp->p,qp->n,file_instance);
        fprintf(file_instance,"e\n");
        write_vector_d(qp->e,qp->p,file_instance);
    }

    /*x^T Aq x = bq*/
    if(qp->mq!=0)
    {
        fprintf(file_instance,"Aq\n");
        write_matrix_3d(qp->aq,qp->mq,qp->n,qp->n,file_instance);
        fprintf(file_instance,"bq\n");
        write_vector_d(qp->bq,qp->mq,file_instance);
    }

    /*x^T Dq x <= eq*/
    if(qp->pq!=0)
    {
        fprintf(file_instance,"Dq\n");
        write_matrix_3d(qp->dq,qp->pq,qp->n,qp->n,file_instance);
        fprintf(file_instance,"eq\n");
        write_vector_d(qp->eq,qp->pq,file_instance);
    }

    fclose(file_instance);
}

/*cat_save(): copy a file in another one*/
void cat_save(FILE * file_save)
{
    FILE * file_res;
    char c;
    if (DEBUG == 0)
    {
        file_res=fopen(res_solver,"r");
        fprintf(file_save,"Convex solver\n\n");
        c = fgetc(file_res);

        while( c != -1)
        {
            fputc(c,file_save);

            c = fgetc(file_res);
        }
        fclose(file_res);
    }
}

/*cat_save_sdp()*/
void cat_save_sdp(FILE * file_save)
{
    FILE * file_res;
    char c;

    file_res=fopen(res_sdp,"r");
    fprintf(file_save,"SDP solver\n\n");
    c = fgetc(file_res);

    while( c != -1)
    {
        fputc(c,file_save);

        c = fgetc(file_res);
    }

    fprintf(file_save,"\n\n");
    fclose(file_res);

}

/*cat_save_cb()*/
void cat_save_cb(FILE * file_save)
{
    FILE * file_res;
    char c;
    int in_if=0;
    file_res=fopen(res_sdp,"r");
    fprintf(file_save,"SDP solver\n\n");
    c = fgetc(file_res);

    while( c != -1)
    {
        if (c=='*')
        {
            while( c != '\n')
                c = fgetc(file_res);
            in_if =1;
        }

        if (c=='I')
        {
            while( c != '\n')
                c = fgetc(file_res);
            in_if =1;
        }

        if (c=='S')
        {
            while( c != '\n')
                c = fgetc(file_res);
            in_if =1;
        }

        if (c=='P')
        {
            while( c != '\n')
                c = fgetc(file_res);
            in_if =1;
        }

        if (c=='D')
        {
            while( c != '\n')
                c = fgetc(file_res);
            in_if =1;
        }

        if (c=='R')
        {
            while( c != '\n')
                c = fgetc(file_res);
            in_if =1;
        }

        if (c=='X')
        {
            while( c != '\n')
                c = fgetc(file_res);
            in_if =1;
        }

        if (in_if == 0)
            fputc(c,file_save);
        in_if=0;
        c = fgetc(file_res);
    }

    fprintf(file_save,"\n\n");
    fclose(file_res);
}

/**********************************************************************************/
/************** take feasible solution frome local cplex **************************/
/**********************************************************************************/
/*initialize_local_sol()*/
double  initialize_local_sol(char * file_name)
{
    double best_sol_admissible;
    FILE * temp = fopen(file_name,"r");
    fscanf(temp,"obj = %lf\n", &best_sol_admissible);
    fclose(temp);
    return best_sol_admissible;
}

/**********************************************************************************/
/***************************  Read the instances **********************************/
/**********************************************************************************/

double * scan_vector_u(FILE * temp, int t)
{
    int i,j,k;
    double * v= alloc_vector_d(t + (t+1)*t/2);
    for(i=0; i<t; i++)
        fscanf(temp,"%lf",&v[i]);

    return v;
}

double * scan_vector_d(FILE * temp, int t)
{
    int i,nb_non_zero,k;
    double * v= alloc_vector_d(t);
    for(i=0; i<t; i++)
        v[i]=0;
    fscanf(temp,"%d",&nb_non_zero);
    for(i=0; i<nb_non_zero; i++)
    {
        fscanf(temp,"%d",&k);
        fscanf(temp,"%lf",&v[k]);
    }
    return v;
}

double * scan_matrix_d(FILE * temp,int t1, int t2)
{
    int i,j,k;
    long nb_non_zero;
    double * M= alloc_matrix_d(t1,t2);
    for(i=0; i<t1; i++)
        for(j=0; j<t2; j++)
            M[ij2k(i,j,t2)]=0;
    fscanf(temp,"%ld",&nb_non_zero);

    for(i=0; i<nb_non_zero; i++)
    {
        fscanf(temp,"%d",&j);
        fscanf(temp,"%d",&k);
        fscanf(temp,"%lf",&M[ij2k(j,k,t2)]);
    }
    return M;
}

double * scan_matrix_3d(FILE * temp,int t1, int t2)
{
    int i,j,k,l;
    long nb_non_zero;
    double * M= alloc_matrix_3d(t1,t2,t2);
    for(i=0; i<t1; i++)
        for(j=0; j<t2; j++)
            for(l=0; l<t2; l++)
                M[ijk2l(i,j,l,t2,t2)]=0;

    fscanf(temp,"%ld",&nb_non_zero);

    for(i=0; i<nb_non_zero; i++)
    {
        fscanf(temp,"%d",&j);
        fscanf(temp,"%d",&k);
        fscanf(temp,"%d",&l);
        fscanf(temp,"%lf",&M[ijk2l(j,k,l,t2,t2)]);
    }
    return M;
}

/* Change bounds to 0 and 1*/
void change_bounds(MIQCP qp)
{

    int i,j,k;
    double * zq = copy_matrix_d(qp->q, qp->n,qp->n);
    double * zc = copy_vector_d(qp->c,qp->n);
    double *zb,*ze,*zbq,*zeq,* za,*zd,*zaq,*zdq;

    double * zcq = alloc_vector_d(qp->n);
    double consq=0;

    if(!Zero(qp->m))
    {
        za = copy_matrix_d(qp->a, qp->m,qp->n);
        zb = copy_vector_d(qp->b,qp->m);
    }
    else
    {
        za=alloc_matrix_d(1,1);
        zb = copy_vector_d(qp->b,qp->m);
    }

    if(!Zero(qp->p))
    {
        zd= copy_matrix_d(qp->d, qp->p,qp->n);
        ze = copy_vector_d(qp->e,qp->p);
    }
    else
    {
        zd=alloc_matrix_d(1,1);
        ze = copy_vector_d(qp->e,qp->p);
    }

    if(!Zero(qp->mq))
    {
        zaq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
        zbq = copy_vector_d(qp->bq,qp->mq);
    }
    else
    {
        zaq=alloc_matrix_d(1,1);
        zbq = copy_vector_d(qp->bq,qp->mq);
    }

    if(!Zero(qp->pq))
    {
        zdq= copy_matrix_3d(qp->dq, qp->pq,qp->n+1,qp->n+1);
        zeq = copy_vector_d(qp->eq,qp->pq);
    }
    else
    {
        zdq=alloc_matrix_d(1,1);
        zeq = copy_vector_d(qp->eq,qp->pq);
    }

    qp->cons= 0;
    /*Objective function*/
    for(i=0; i<qp->n; i++)
    {
        qp->cons= qp->cons + qp->c[i]*qp->l[i];
        qp->c[i]=qp->c[i]*(qp->u[i] - qp->l[i]);
        for(j=0; j<qp->n; j++)
        {
            qp->cons= qp->cons + zq[ij2k(i,j,qp->n)]*qp->l[i]*qp->l[j];
            qp->c[i]=qp->c[i] + 2*zq[ij2k(i,j,qp->n)]*(qp->u[i] - qp->l[i])*qp->l[j]  ;
            qp->q[ij2k(i,j,qp->n)] = zq[ij2k(i,j,qp->n)]*(qp->u[i] - qp->l[i])*(qp->u[j] - qp->l[j]);
        }
    }

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            qp->q[ij2k(i,j,qp->n)] = (qp->q[ij2k(i,j,qp->n)] + qp->q[ij2k(i,j,qp->n)])/2 ;
            qp->q[ij2k(j,i,qp->n)] = qp->q[ij2k(i,j,qp->n)] ;

        }

    /*equality Constraints */
    for(k=0; k<qp->mq; k++)
    {
        consq = 0;
        for(i=0; i<qp->n; i++)
            zcq[i]=0;


        for(i=0; i<qp->n; i++)
        {
            consq =  consq +  qp->aq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]*qp->l[i]*2;
            zcq[i]= zcq[i] + qp->aq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]*( qp->u[i] - qp->l[i]);
            for(j=0; j<qp->n; j++)
            {
                consq= consq + zaq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]*qp->l[i]*qp->l[j];
                zcq[i]=zcq[i] + zaq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]*( qp->u[i] - qp->l[i])*qp->l[j] ;
                qp->aq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=zaq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]*(qp->u[i] - qp->l[i])*(qp->u[j] - qp->l[j]);
            }
        }

        qp->bq[k] = qp->bq[k] -consq;
        for(i=0; i<qp->n; i++)
        {
            qp->aq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]= zcq[i];
            qp->aq[ijk2l(k,i+1,0,qp->n+1,qp->n+1)]=zcq[i];
        }
    }

    for(k=0; k<qp->mq; k++)
        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
            {
                qp->aq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=(qp->aq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)] +  qp->aq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)])/2;
                qp->aq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)] = qp->aq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)];
            }

    /*inequality Constraints */
    for(k=0; k<qp->pq; k++)
    {
        consq=0;
        for(i=0; i<qp->n; i++)
        {
            zcq[i]= 0;
            consq = 0;
        }

        for(i=0; i<qp->n; i++)
        {
            consq =  consq +  qp->dq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]*qp->l[i]*2;
            zcq[i]= zcq[i] + qp->dq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]*( qp->u[i] - qp->l[i]);
            for(j=0; j<qp->n; j++)
            {
                consq= consq + zdq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]*qp->l[i]*qp->l[j];
                zcq[i]=zcq[i] + zdq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]* (qp->u[i] - qp->l[i])*qp->l[j] ;
                qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=zdq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]*(qp->u[i] - qp->l[i])*(qp->u[j] - qp->l[j]);
            }
        }

        qp->eq[k] = qp->eq[k] -consq;
        for(i=0; i<qp->n; i++)
        {
            qp->dq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]= zcq[i];
            qp->dq[ijk2l(k,i+1,0,qp->n+1,qp->n+1)]= zcq[i];
        }
    }

    for(k=0; k<qp->pq; k++)
        for(i=0; i<qp->n; i++)
        {
            for(j=i+1; j<qp->n; j++)
            {
                qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=(qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)] +  qp->dq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)])/2;
                qp->dq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)] = qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)];
            }
        }

    /*upper and lower bounds*/
    for(i=0; i<qp->n; i++)
    {
        if (qp->u[i] == qp->l[i])
        {
            qp->u[i]=qp->l[i];
            qp->l[i]=qp->l[i];
        }
        else
        {
            qp->u[i]=1;
            qp->l[i]=0;
        }
    }

    for(j=0; j<qp->n; j++)
        for(k=j; k<qp->n; k++)
        {
            if ((k==j) && (qp->u[k] == qp->l[k]))
            {
                qp->u[i]=qp->l[k]*qp->l[k];
                qp->l[i]=qp->l[k]* qp->l[k];
                i++;
            }
            else
            {
                qp->u[i]=qp->u[j]*qp->u[k];
                qp->l[i]=qp->l[j]*qp->l[k];
                i++;
            }
        }
}

/*Read a standard file*/
void read_file(MIQCP qp)
{
    int i,j,k,l;
    FILE *file_inst;
    char tmp;

    /* Gestion des cont. lin.*/
    double * a_t_a;
    double * a_t;
    double b_q;


    double * zaq;
    double * zbq;
    double * zdq;
    double * zeq;

    file_inst = fopen(buf_in,"r");

    fscanf(file_inst,"%d",&qp->n);
    fscanf(file_inst,"%d",&qp->nb_int);
    fscanf(file_inst,"%d",&qp->m);
    fscanf(file_inst,"%d",&qp->p);
    fscanf(file_inst,"%d",&qp->mq);
    fscanf(file_inst,"%d",&qp->pq);

    int nbcols=qp->n+(qp->n+1)*qp->n/2;
    /*fscanf(file_inst,"%c",&tmp);*/
    fscanf(file_inst,"%c",&tmp);
    while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
        fscanf(file_inst,"%c",&tmp);
    fscanf(file_inst,"%c",&tmp);
    qp->u=scan_vector_u(file_inst,qp->n);

    fscanf(file_inst,"%c",&tmp);
    while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
        fscanf(file_inst,"%c",&tmp);
    fscanf(file_inst,"%c",&tmp);
    qp->l=scan_vector_u(file_inst,qp->n);

    i=qp->n;
    for(j=0; j<qp->n; j++)
        for(k=j; k<qp->n; k++)
        {
            if (k==j)
            {
                if ((Positive_BB(qp->u[j])||Zero_BB(qp->u[j])) && (Positive_BB(qp->l[j]) || Zero_BB(qp->l[j])))
                {
                    qp->u[i]=qp->u[j]*qp->u[j];
                    qp->l[i]=0;
                    i++;
                }
                else
                {
                    if ((Positive_BB(qp->u[j])|| Zero_BB(qp->u[j])) && Negative_BB(qp->l[j]))
                    {
                        if (qp->u[j]*qp->u[j] >qp->l[j]*qp->l[j])
                            qp->u[i]=qp->u[j]*qp->u[j];
                        else
                            qp->u[i]=qp->l[j]*qp->l[j];
                        qp->l[i]=0;
                        i++;
                    }
                    else if (Negative_BB(qp->u[j]) && Negative_BB(qp->l[j]))
                    {
                        qp->u[i]=qp->l[j]*qp->l[j];
                        qp->l[i]=0;
                        i++;
                    }
                }
            }
            else
            {
                if ((Positive_BB(qp->u[j])||Zero_BB(qp->u[j])) && (Positive_BB(qp->u[k]) || Zero_BB(qp->u[k])))
                {
                    if (Negative_BB(qp->l[j]) && Negative_BB(qp->l[k]))
                    {
                        if (qp->u[j]*qp->u[k] > qp->l[j]*qp->l[k])
                            qp->u[i]=qp->u[j]*qp->u[k];
                        else
                            qp->u[i]=qp->l[j]*qp->l[k];
                        if  (qp->l[j]*qp->u[k] > qp->l[k]*qp->u[j])
                            qp->l[i]=qp->l[j]*qp->u[k];
                        else
                            qp->l[i]=qp->l[k]*qp->u[j];
                        i++;
                    }

                    else
                    {
                        if (Negative_BB(qp->l[j]))
                        {
                            qp->u[i]=qp->u[j]*qp->u[k];
                            qp->l[i]=qp->l[j]*qp->u[k];
                            i++;
                        }
                        else
                        {
                            if (Negative_BB(qp->l[k]))
                            {
                                qp->u[i]=qp->u[j]*qp->u[k];
                                qp->l[i]=qp->u[j]*qp->l[k];
                                i++;
                            }
                            else
                            {
                                qp->u[i]=qp->u[j]*qp->u[k];
                                qp->l[i]=qp->l[j]*qp->l[k];
                                i++;
                            }
                        }
                    }
                }
                else
                {
                    if (Negative_BB(qp->u[j]) && Negative_BB(qp->u[k]) )
                    {
                        qp->u[i]=qp->l[k]*qp->l[j];
                        qp->l[i]=qp->u[k]*qp->u[j];
                        i++;
                    }
                    else
                    {
                        if (Negative_BB(qp->u[j]))
                        {
                            qp->u[i]=qp->l[k]*qp->l[j];
                            qp->l[i]=qp->u[k]*qp->l[j];
                            i++;
                        }
                        else if (Negative_BB(qp->u[k]))
                        {
                            qp->u[i]=qp->l[k]*qp->l[j];
                            qp->l[i]=qp->u[j]*qp->l[k];
                            i++;
                        }

                    }
                }

            }

        }


    fscanf(file_inst,"%c",&tmp);
    while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
        fscanf(file_inst,"%c",&tmp);

    fscanf(file_inst,"%c",&tmp);

    qp->q = scan_matrix_d(file_inst,qp->n,qp->n);
    /*check if Q is symetric : if not make it symetric */

    if (!is_symetric(qp->q,qp->n))
    {
        for(i=0; i<qp->n; i++)
        {
            for(j=i+1; j<qp->n; j++)
            {
                qp->q[ij2k(i,j,qp->n)] = (qp->q[ij2k(i,j,qp->n)] +qp->q[ij2k(j,i,qp->n)])/2;
                qp->q[ij2k(j,i,qp->n)] = qp->q[ij2k(i,j,qp->n)];
            }
        }
    }

    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\nq (read_file)\n"); */
    /*     print_mat_d(qp->q, qp->n,qp->n); */
    /*     printf("\n"); */
    /*   } */
    fscanf(file_inst,"%c",&tmp);
    while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
        fscanf(file_inst,"%c",&tmp);

    fscanf(file_inst,"%c",&tmp);

    qp->c = scan_vector_d(file_inst,qp->n);

    qp->cons=0;
    if (qp->flag_const)
    {
        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);

        fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%lf",&qp->cons);
    }

    /*Ax=b*/
    if(Positive(qp->m))
    {
        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        qp->a= scan_matrix_d(file_inst,qp->m,qp->n);

        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        qp->b= scan_vector_d(file_inst,qp->m);

        /* creation of Matrix A^t A*/
        a_t_a = alloc_matrix_d(qp->n,qp->n);
        a_t = transpose_matrix_mn(qp->a, qp->m, qp->n);

        for (i=0; i<qp->n; i++)
            for (j=0; j<qp->n; j++)
                a_t_a[ij2k(i,j,qp->n)]=0;


        for (i=0; i<qp->m; i++)
            for (j=0; j<qp->n; j++)
                for(k=j; k<qp->n; k++)
                    a_t_a[ij2k(j,k,qp->n)]=a_t_a[ij2k(j,k,qp->n)] + a_t[ij2k(j,i,qp->m)]*qp->a[ij2k(i,k,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                a_t_a[ij2k(k,j,qp->n)]=a_t_a[ij2k(j,k,qp->n)];

        b_q=0;
        for(i=0; i<qp->m; i++)
            b_q = b_q + qp->b[i]*qp->b[i];


    }
    else
    {
        qp->a=alloc_matrix_d(1,1);
        qp->b=alloc_vector_d(1);
    }

    /*Dx <= e*/
    if(Positive(qp->p))
    {
        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        qp->d= scan_matrix_d(file_inst,qp->p,qp->n);

        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        qp->e= scan_vector_d(file_inst,qp->p);

    }
    else
    {
        qp->d=alloc_matrix_d(1,1);
        qp->e=alloc_vector_d(1);
    }

    /* x^T Aq x = bq */
    if(Positive(qp->mq))
    {
        fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);

        zaq= scan_matrix_3d(file_inst,qp->mq,qp->n+1);

        for(k=0; k<qp->mq; k++)
            if (!is_symetric_3d(zaq,k,qp->n+1))
            {
                for(i=0; i<qp->n+1; i++)
                {
                    for(j=i+1; j<qp->n+1; j++)
                    {
                        zaq[ijk2l(k,i,j,qp->n+1,qp->n+1)] = (zaq[ijk2l(k,i,j,qp->n+1,qp->n+1)] + zaq[ijk2l(k,j,i,qp->n+1,qp->n+1)])/2;
                        zaq[ijk2l(k,j,i,qp->n+1,qp->n+1)] = zaq[ijk2l(k,i,j,qp->n+1,qp->n+1)];
                    }
                }
            }

        fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        zbq= scan_vector_d(file_inst,qp->mq);

        if (Positive(qp->m))
        {
            qp->mq++;

            qp->aq=alloc_matrix_3d(qp->mq,qp->n+1,qp->n+1);
            qp->bq=alloc_vector_d(qp->mq);

            for (i=0; i<qp->mq-1; i++)
                for (j=0; j<qp->n+1; j++)
                    for(k=0; k<qp->n+1; k++)
                        qp->aq[ijk2l(i,j,k,qp->n+1,qp->n+1)]=zaq[ijk2l(i,j,k,qp->n+1,qp->n+1)];


            /* cont lin */
            i=qp->mq-1;
            qp->aq[ijk2l(i,0,0,qp->n+1,qp->n+1)]=0;
            for (j=0; j<qp->n; j++)
            {
                qp->aq[ijk2l(i,0,j+1,qp->n+1,qp->n+1)]=0;
                qp->aq[ijk2l(i,j+1,0,qp->n+1,qp->n+1)]=0;
                for(k=0; k<qp->n; k++)
                    qp->aq[ijk2l(i,j+1,k+1,qp->n+1,qp->n+1)]=a_t_a[ij2k(j,k,qp->n)];
            }
            for (i=0; i<qp->mq-1; i++)
                qp->bq[i] = zbq[i];
            i=qp->mq-1;
            qp->bq[i]= b_q;



        }
        else
        {
            qp->aq=alloc_matrix_3d(qp->mq,qp->n+1,qp->n+1);
            qp->bq=alloc_vector_d(qp->mq);

            for (i=0; i<qp->mq; i++)
                for (j=0; j<qp->n+1; j++)
                    for(k=0; k<qp->n+1; k++)
                        qp->aq[ijk2l(i,j,k,qp->n+1,qp->n+1)]=zaq[ijk2l(i,j,k,qp->n+1,qp->n+1)];

            for (i=0; i<qp->mq; i++)
                qp->bq[i] = zbq[i];
        }
        free_vector_d(zaq);
        free_vector_d(zbq);

    }
    else
    {
        if (Positive(qp->m))
        {
            qp->mq++;

            qp->aq=alloc_matrix_3d(qp->mq,qp->n+1,qp->n+1);
            qp->bq=alloc_vector_d(qp->mq);

            /* cont lin */
            i=qp->mq-1;
            qp->aq[ijk2l(i,0,0,qp->n+1,qp->n+1)]=0;
            for (j=0; j<qp->n; j++)
            {
                qp->aq[ijk2l(i,0,j+1,qp->n+1,qp->n+1)]=0;
                qp->aq[ijk2l(i,j+1,0,qp->n+1,qp->n+1)]=0;
                for(k=0; k<qp->n; k++)
                    qp->aq[ijk2l(i,j+1,k+1,qp->n+1,qp->n+1)]=a_t_a[ij2k(j,k,qp->n)];
            }
            for (i=0; i<qp->mq-1; i++)
                qp->bq[i] = zbq[i];
            i=qp->mq-1;
            qp->bq[i]= b_q;


        }
        else
        {
            qp->aq=alloc_matrix_3d(1,1,1);
            qp->bq=alloc_vector_d(1);
        }
    }

    /* x^T Dq x <= eq */
    if(Positive(qp->pq))
    {
        fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        /* fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);*/
        zdq= scan_matrix_3d(file_inst,qp->pq,qp->n+1);

        for(k=0; k<qp->pq; k++)
            if (!is_symetric_3d(zdq,k,qp->n+1))
            {
                printf("not symmetric\n");
                for(i=0; i<qp->n+1; i++)
                {
                    for(j=i+1; j<qp->n+1; j++)
                    {
                        zdq[ijk2l(k,i,j,qp->n+1,qp->n+1)] = (zdq[ijk2l(k,i,j,qp->n+1,qp->n+1)] + zdq[ijk2l(k,j,i,qp->n+1,qp->n+1)])/2;
                        zdq[ijk2l(k,j,i,qp->n+1,qp->n+1)] = zdq[ijk2l(k,i,j,qp->n+1,qp->n+1)];
                    }
                }
            }


        fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        while((tmp==' ' ) || (tmp =='\r') || (tmp== '\t'))
            fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        fscanf(file_inst,"%c",&tmp);
        zeq=scan_vector_d(file_inst,qp->pq);

        if (Positive(qp->p))
        {
            qp->pq=qp->pq+2*qp->n*qp->p;

            qp->dq=alloc_matrix_3d(qp->pq,qp->n+1,qp->n+1);
            qp->eq=alloc_vector_d(qp->pq);

            for (i=0; i<qp->pq-1; i++)
                for (j=0; j<qp->n+1; j++)
                    for(k=0; k<qp->n+1; k++)
                        qp->dq[ijk2l(i,j,k,qp->n+1,qp->n+1)]=zdq[ijk2l(i,j,k,qp->n+1,qp->n+1)];

            /* cont lin */
            k=qp->pq-2*qp->n*qp->p;
            for(l=0; l<qp->p; l++)
            {
                for(i=0; i<qp->n; i++)
                {
                    k=k+1;
                    qp->dq[ijk2l(k,0,0,qp->n+1,qp->n+1)]=0;
                    qp->dq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]=-qp->e[l]/2;
                    qp->dq[ijk2l(k,i+1,0,qp->n+1,qp->n+1)]=-qp->e[l]/2;
                    for (j=0; j<qp->n; j++)
                    {
                        if (i==j)
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)];
                        else
                        {
                            qp->dq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)]/2;
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)]/2;
                        }
                    }
                }
            }

            k=qp->pq-qp->n*qp->p;
            for(l=0; l<qp->p; l++)
            {
                for(i=0; i<qp->n; i++)
                {
                    k=k+1;
                    qp->dq[ijk2l(k,0,0,qp->n+1,qp->n+1)]=0;

                    for (j=0; j<qp->n; j++)
                    {

                        if (i==j)
                        {
                            qp->dq[ijk2l(k,0,j+1,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)]+ qp->e[l])/2;
                            qp->dq[ijk2l(k,j+1,0,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)]+ qp->e[l])/2;
                            qp->dq[ijk2l(k,j+1,j+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)];
                        }
                        else
                        {
                            qp->dq[ijk2l(k,0,j+1,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)])/2;
                            qp->dq[ijk2l(k,j+1,0,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)])/2;
                            qp->dq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)]/2;
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)]/2;
                        }
                    }
                }
            }


            for (i=0; i<qp->pq-2*qp->n*qp->p; i++)
                qp->eq[i] = zeq[i];
            for(i; i<qp->pq-qp->n*qp->p; i++)
                qp->eq[i]= 0;
            for(j=0; j<qp->p; j++)
                for(k=0; k<qp->n; k++)
                {
                    qp->eq[i]= qp->u[k]*qp->e[j];
                    i++;
                }
        }
        else
        {
            qp->dq=alloc_matrix_3d(qp->pq,qp->n+1,qp->n+1);
            qp->eq=alloc_vector_d(qp->pq);

            for (i=0; i<qp->pq; i++)
                for (j=0; j<qp->n+1; j++)
                    for(k=0; k<qp->n+1; k++)
                        qp->dq[ijk2l(i,j,k,qp->n+1,qp->n+1)]=zdq[ijk2l(i,j,k,qp->n+1,qp->n+1)];
            for (i=0; i<qp->pq; i++)
                qp->eq[i] = zeq[i];

        }

        free_vector_d(zdq);
        free_vector_d(zeq);

    }
    else
    {
        if (Positive(qp->p))
        {
            qp->pq=qp->pq+2*qp->n*qp->p;

            qp->dq=alloc_matrix_3d(qp->pq,qp->n+1,qp->n+1);
            qp->eq=alloc_vector_d(qp->pq);

            /* cont lin */
            k=qp->pq-2*qp->n*qp->p;
            for(l=0; l<qp->p; l++)
            {
                for(i=0; i<qp->n; i++)
                {

                    qp->dq[ijk2l(k,0,0,qp->n+1,qp->n+1)]=0;
                    qp->dq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]=-qp->e[l]/2;
                    qp->dq[ijk2l(k,i+1,0,qp->n+1,qp->n+1)]=-qp->e[l]/2;
                    for (j=0; j<qp->n; j++)
                    {
                        if (i==j)
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)];
                        else
                        {
                            qp->dq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)]/2;
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)]/2;
                        }
                    }
                    k++;

                }
            }

            k=qp->pq-qp->n*qp->p;

            for(l=0; l<qp->p; l++)
            {
                for(i=0; i<qp->n; i++)
                {

                    qp->dq[ijk2l(k,0,0,qp->n+1,qp->n+1)]=0;

                    for (j=0; j<qp->n; j++)
                    {

                        if (i==j)
                        {
                            qp->dq[ijk2l(k,0,j+1,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)]+ qp->e[l])/2;
                            qp->dq[ijk2l(k,j+1,0,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)]+ qp->e[l])/2;
                            qp->dq[ijk2l(k,j+1,j+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)];
                        }
                        else
                        {
                            qp->dq[ijk2l(k,0,j+1,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)])/2;
                            qp->dq[ijk2l(k,j+1,0,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)])/2;
                            qp->dq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)]/2;
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)]/2;
                        }
                    }
                    k++;
                }
            }

            for(i=0; i<qp->pq-qp->n*qp->p; i++)
                qp->eq[i]= 0;
            for(j=0; j<qp->p; j++)
                for(k=0; k<qp->n; k++)
                {
                    qp->eq[i]= qp->u[k]*qp->e[j];
                    i++;
                }
        }
        else
        {
            qp->dq=alloc_matrix_3d(1,1,1);
            qp->eq=alloc_vector_d(1);
        }
    }

    qp->N = vector_length_base_2(qp->u, qp->nb_int);
    qp->lgc = create_u_base2_cumulated(qp->u,qp->n);
    qp->lg = create_u_base2(qp->u,qp->n);

    fclose(file_inst);
}

/*Read a MaxCut file*/
void read_max_cut_file( MIQCP qp)
{
    int i,j,k;
    FILE *file_inst;
    int nb_non_zero;

    file_inst = fopen(buf_in,"r");

    fscanf(file_inst,"%d",&qp->n);
    fscanf(file_inst,"%d",&nb_non_zero);

    qp->q = alloc_matrix_d(qp->n,qp->n);

    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            qp->q[ij2k(i,j,qp->n)]=0;

    qp->c=alloc_vector_d(qp->n);
    for(j=0; j<qp->n; j++)
        qp->c[j]=0;

    for(k=0; k<nb_non_zero; k++)
    {
        fscanf(file_inst,"%d",&i);
        fscanf(file_inst,"%d",&j);
        fscanf(file_inst,"%lf",&qp->q[ij2k(i-1,j-1,qp->n)]);
    }

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            qp->q[ij2k(j,i,qp->n)]=qp->q[ij2k(i,j,qp->n)];

    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            qp->q[ij2k(i,j,qp->n)]=-qp->q[ij2k(i,j,qp->n)];

    qp->u = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->u[i]=1;

    qp->l = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->l[i]=0;

    qp->a=alloc_matrix_d(1,1);
    qp->b=alloc_vector_d(1);

    qp->aq=alloc_matrix_3d(1,1,1);
    qp->bq=alloc_vector_d(1);

    qp->lgc = alloc_vector(1);
    qp->lg = alloc_vector(1);

    qp->d=alloc_matrix_d(1,1);
    qp->e=alloc_vector_d(1);

    qp->dq=alloc_matrix_3d(1,1,1);
    qp->eq=alloc_vector_d(1);

    qp->N =0;

    fclose(file_inst);
    write_file_instance(qp);
}

/*Read a k-Cluster file*/
void read_k_cluster_file( MIQCP qp, int valk)
{
    int i,j,k;
    FILE *file_inst;
    int nb_non_zero;

    file_inst = fopen(buf_in,"r");

    fscanf(file_inst,"%d",&qp->n);
    fscanf(file_inst,"%d",&nb_non_zero);

    qp->q = alloc_matrix_d(qp->n,qp->n);
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            qp->q[ij2k(i,j,qp->n)]=0;

    for(k=0; k<nb_non_zero; k++)
    {
        fscanf(file_inst,"%d",&i);
        fscanf(file_inst,"%d",&j);
        fscanf(file_inst,"%lf",&qp->q[ij2k(i-1,j-1,qp->n)]);
    }

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            qp->q[ij2k(j,i,qp->n)]=qp->q[ij2k(i,j,qp->n)];

    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            qp->q[ij2k(i,j,qp->n)]=-qp->q[ij2k(i,j,qp->n)];

    qp->c=alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->c[i]=0;
    qp->u = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->u[i]=1;
    qp->l = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->l[i]=0;


    qp->m=1;
    qp->a=alloc_matrix_d(1,qp->n);
    for(i=0; i<qp->n; i++)
        qp->a[ij2k(0,i,qp->n)]=1;

    qp->b=alloc_vector_d(1);
    qp->b[0]=valk;

    qp->lgc = alloc_vector(qp->n);
    qp->lg = alloc_vector(qp->n);

    for(i=0; i<qp->n; i++)
    {
        qp->lg[i]=1;
        qp->lgc[i]=1;
    }

    qp->d=alloc_matrix_d(1,1);
    qp->e=alloc_vector_d(1);

    qp->aq=alloc_matrix_3d(1,1,1);
    qp->bq=alloc_vector_d(1);
    qp->dq=alloc_matrix_3d(1,1,1);
    qp->eq=alloc_vector_d(1);

    fclose(file_inst);
    write_file_instance(qp);
}

/*Read a Task allocation problem file*/
void read_task_allocation_problem( MIQCP qp)
{
    int i,j,k,l;
    FILE *file_inst;
    int nb_non_zero;

    int m = 0;
    int n=0;

    int T;
    int P;

    file_inst = fopen(buf_in,"r");

    fscanf(file_inst,"%d",&T);
    fscanf(file_inst,"%d",&P);

    qp->n= T*P;


    qp->c=alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        fscanf(file_inst,"%lf",&qp->c[i]);

    qp->q = alloc_matrix_d(qp->n,qp->n);

    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            qp->q[ij2k(i,j,qp->n)]=0;

    for(i=1; i<=T-1; i++)
        for(k=1; k<=P; k++)
            for(j=i+1; j<=T; j++)
                for(l=1; l<=P; l++)
                    fscanf(file_inst,"%lf",&qp->q[ij2k((i-1)*P+k-1,(j-1)*P+l-1,qp->n)]);

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            qp->q[ij2k(j,i,qp->n)]=qp->q[ij2k(i,j,qp->n)];

    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            qp->q[ij2k(i,j,qp->n)]=qp->q[ij2k(i,j,qp->n)]/2;

    qp->u = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->u[i]=1;
    qp->l = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->l[i]=0;

    qp->m=T;
    qp->a=alloc_matrix_d(qp->m,qp->n);

    for(i=0; i<qp->m; i++)
        for(k=1; k<=P; k++)
            qp->a[ij2k(i,i*P+k-1,qp->n)]=1;

    qp->b=alloc_vector_d(qp->m);
    for(j=0; j<qp->m; j++)
        qp->b[j]=1;

    qp->lgc = alloc_vector(qp->n);
    qp->lg = alloc_vector(qp->n);

    for(i=0; i<qp->n; i++)
    {
        qp->lg[i]=1;
        qp->lgc[i]=1;
    }

    qp->d=alloc_matrix_d(1,1);
    qp->e=alloc_vector_d(1);

    qp->aq=alloc_matrix_3d(1,1,1);
    qp->bq=alloc_vector_d(1);
    qp->dq=alloc_matrix_3d(1,1,1);
    qp->eq=alloc_vector_d(1);

    fclose(file_inst);
    write_file_instance(qp);
}

/**********************************************************************************/
/**********************  Read for the sdp solver **********************************/
/**********************************************************************************/
/*read_solution_x_from_file()*/
void read_solution_x_from_file(SDP psdp,double **x)
{
    int i,j,k;
    double tmp;

    int dim_x= (psdp->n+2)*(psdp->n+1)/2;

    FILE * sol_file;
    char c,d;
    char * str=alloc_string(8);

    if ((strcmp(psdp->solver_sdp,"SB\0") == 0) || (strcmp(psdp->solver_sdp,"sb\0") == 0))
    {
        sol_file=fopen(res,"r");
        fscanf(sol_file, "%d", &i);

        for(i=0; i<dim_x; i++)
        {
            fscanf(sol_file, "%lf", &x[0][i]);
        }
        fclose(sol_file);
    }

    else
    {
        sol_file=fopen(au,"r");
        c=fgetc(sol_file);
        while (c!= '\n')
            c=fgetc(sol_file);

        i=0;
        while (i!=2)
        {
            c='\0';
            while (c!= '\n')
                c=fgetc(sol_file);
            fscanf(sol_file, "%d", &i);
        }

        for(i=0; i<7; i++)
            c=fgetc(sol_file);
        fscanf(sol_file, "%lf", &x[0][0]);
        c=fgetc(sol_file);
        c=fgetc(sol_file);

        for(i=1; i<dim_x; i++)
        {
            for(j=0; j<4; j++)
                fscanf(sol_file, "%d", &k);
            fscanf(sol_file, "%lf", &x[0][i]);
            c=fgetc(sol_file);
            c=fgetc(sol_file);
        }
        fclose(sol_file);
    }
}

/*read_min_eigenvalue()*/
void read_min_eigenvalue(double * min)
{
    FILE * file_scilab;
    int a;

    file_scilab = fopen(spectre,"r");
    fscanf(file_scilab,"%lf",min);
    a=fclose(file_scilab);

}

/**********************************************************************************/
/***************************  Read a MPS file *************************************/
/**********************************************************************************/
/*strucures for reading mps files*/
struct table
{
    double *value;
    char * constraint;
    double b;
    char letter;
};

struct tablebis
{
    char * variable;
};

typedef struct table * table_p;

typedef struct tablebis * tablebis_p;

char * get_row(char ** tab, int indice)
{
    int i=0;
    char * result= alloc_string (10);
    char c=tab[indice][i];
    while(c!=' ')
    {
        result[i]=c;
        i++;
        c=tab[indice][i];
    }
    return result;
}


char get_type(char ** tab, int indice)
{
    int i=0;
    char result;
    char c=tab[indice][i];
    while(c!=' ')
    {
        i++;
        c=tab[indice][i];
    }
    result=tab[indice][i+1];
    return result;
}


int indice_mps(char ** tab, int dim,char * test)
{
    int i;
    for(i=0; i<dim; i++)
    {


        if(strcmp(test,tab[i])==0)
        {
            return i;
        }
    }
    return -1;

}

table_p create_table(int m,int n)
{
    int i,j;
    table_p t = (table_p) malloc(m*sizeof(struct table));

    if (m==0)
        m=1;

    for (i=0; i<m; i++)
    {
        t[i].constraint=malloc(12*sizeof(char));
        t[i].constraint="\0";

        t[i].value=malloc(n*sizeof(double));
        for(j=0; j<n; j++)
            t[i].value[j]=0;

    }


    return t;
}

tablebis_p  create_tablebis(int n)
{
    int i,j;
    tablebis_p t= (tablebis_p) malloc(n*sizeof(struct tablebis));


    if (n==0)
        n=1;

    for (i=0; i<n; i++)
    {
        t[i].variable=malloc(12*sizeof(char));
        t[i].variable="\0";

    }


    return t;
}


void add_to_table(table_p * t, char * constraint, char letter, int n)
{
    int i=0;

    while((t[0][i].constraint!="\0") &&(i<n))
        i++;

    t[0][i].constraint= copy_string(constraint);
    t[0][i].letter=letter;

}

void add_to_tablebis(tablebis_p * t, char * variable, int n)
{
    int i=0;

    while((t[0][i].variable!="\0")  &&(i<n))
        i++;

    t[0][i].variable= copy_string(variable);

}

void modify_table(table_p * t, int i, int j, double value)
{

    t[0][j].value[i]=value;

}


void modify_table_b(table_p * t, int i, double g)
{
    t[0][i].b=g;
}

int search_into_table(table_p * t,char * constraint, int n)
{
    int i=0;

    if (t[0][0].constraint=="\0")
        return -1;

    while((t[0][i].constraint!="\0") && (i<n))
    {
        if(strcmp(t[0][i].constraint,constraint)==0)
            return i;
        i++;
    }
    return -1;

}

int search_into_tablebis(tablebis_p * t,char * variable, int n)
{
    int i=0;

    if (t[0][0].variable=="\0")
        return -1;

    while(t[0][i].variable!="\0")
    {
        if((strcmp(t[0][i].variable,variable)==0) && (i<n))
            return i;
        i++;
    }
    return -1;

}

char ** add_to_char(char ** t, int n, char * name)
{
    char ** temp=t;
    int i=0;
    while((t[i]!="\0") && (i<n))
        i++;

    if(i<n)
    {

        t[i]=  copy_string(name);

    }

    return temp;
}

void compute_a(table_p * t,MIQCP qp)
{
    int i=0;
    int j;
    qp->a = alloc_matrix_d(qp->m,qp->n);
    while(t[0][i].constraint != "\0")
    {
        for(j=0; j<qp->n; j++)
            qp->a[ij2k(i,j,qp->n)] = t[0][i].value[j];
        i++;
    }
}

void compute_b(table_p * t,MIQCP qp)
{
    int i=0;
    int j;
    qp->b = alloc_vector_d(qp->m);
    while(t[0][i].constraint != "\0")
    {
        qp->b[i] = t[0][i].b;
        i++;
    }
}



void compute_c(table_p * t,MIQCP qp)
{
    int i=0;
    int j;

    qp->c = alloc_vector_d(qp->n);
    for(j=0; j<qp->n; j++)
        qp->c[j] = t[0][0].value[j];
}

void compute_d(table_p * t,MIQCP qp)
{
    int i=0;
    int j;
    qp->d = alloc_matrix_d(qp->p,qp->n);
    while(t[0][i].constraint != "\0")
    {
        if(t[0][i].letter == 'L')
            for(j=0; j<qp->n; j++)
                qp->d[ij2k(i,j,qp->n)] = t[0][i].value[j];
        else
            for(j=0; j<qp->n; j++)
                qp->d[ij2k(i,j,qp->n)] = - t[0][i].value[j];
        i++;
    }
}

void compute_e(table_p * t,MIQCP qp)
{
    int i=0;
    int j;
    qp->d = alloc_vector_d(qp->p);
    while(t[0][i].constraint != "\0")
    {
        if(t[0][i].letter == 'L')
            qp->d[i] = t[0][i].b;
        else
            qp->d[i] = - t[0][i].b;
        i++;
    }
}

/*scan_string()*/
char scan_string(FILE * temp, char * s)
{
    char c=fgetc(temp);
    int i=0;

    while ((c==' ') || (c== '\n') || (c== '\0'))
        c=fgetc(temp);

    while((c!=' ') && (c!= EOF) && (c!= '\n') && (c!= '\0'))
    {
        s[i]=c;
        c=fgetc(temp);
        i++;
    }

    s[i]='\0';
    return c;
}


/*scan_line()*/
int scan_line(FILE* temp,char * field1,char*field2,char * field3,char*field4, char * field5)
{
    char c;
    int item=0;
    int i=0;

    c=scan_string(temp,field1);
    item ++;
    if(c=='\n')
        return item;
    c=scan_string(temp,field2);
    item ++;
    if(c=='\n')
        return item;
    c=scan_string(temp,field3);
    item ++;
    if(c=='\n')
        return item;
    c=scan_string(temp,field4);
    item ++;
    if(c=='\n')
        return item;
    c=scan_string(temp,field5);
    item ++;
    if(c=='\n')
        return item;
}

table_p table_eq;
table_p  table_ineq;
table_p  table_obj;
tablebis_p table_var;
tablebis_p table_cont;

/*copy2_string()*/
void copy2_string(char*a,char *s)
{
    int i =0;
    while (s[i] != '\0')
    {
        a[i] = s[i];
        i++;
    }
    a[i]='\0';

}

/*copy3_string()*/
void copy3_string(char a[],char s[])
{
    int i =0;
    while (s[i] != '\0')
    {
        a[i] = s[i];
        i++;
    }
    a[i]='\0';

}

/*read_mps_file()*/
void read_mps_file(MIQCP qp)
{
    FILE * temp= fopen(buf_in,"r");

    /*char ** table_var;*/
    char field1[9];
    char field2[9];
    char field3[9];
    char field4[9];
    char field5[9];
    char field6[9];
    char car,tp;
    int nb_rows=0;
    int i,j,k,ind,ind1,ind2,item;
    double g,g2;
    FILE * file_instance;

    i=0;




    /* count the number of constraints */
    qp->m=0;
    qp->p=0;
    qp->n=0;
    item = scan_line(temp,field1,field2,field3,field4,field5);

    while (strcmp(field1,"ROWS")!=0)
    {

        item = scan_line(temp,field1,field2,field3,field4,field5);
    }

    while (strcmp(field1,"COLUMNS")!=0)
    {


        item = scan_line(temp,field1,field2,field3,field4,field5);

        if(strcmp(field1,"E")==0)
        {
            qp->m++;

        }
        if (strcmp(field1,"L")==0 || strcmp(field1,"G")==0)
        {
            qp->p++;
        }

    }

    fclose(temp);
    table_cont=create_tablebis(qp->m+qp->p+1);
    temp= fopen(buf_in,"r");

    while ( strcmp(field1,"ROWS")!=0)
    {


        item = scan_line(temp,field1,field2,field3,field4,field5);

    }



    while (strcmp(field1,"COLUMNS")!=0)
    {


        item = scan_line(temp,field1,field2,field3,field4,field5);

        if(strcmp(field1,"N")==0)
        {
            add_to_tablebis(&table_cont,field2,qp->m+qp->p+1);


        }
        if(strcmp(field1,"E")==0)
        {
            add_to_tablebis(&table_cont,field2,qp->m+qp->p+1);

        }

        if(strcmp(field1,"L")==0)
        {
            add_to_tablebis(&table_cont,field2,qp->m+qp->p+1);

        }
        if(strcmp(field1,"G")==0)
        {
            add_to_tablebis(&table_cont,field2,qp->m+qp->p+1);


        }
    }


    fclose(temp);

    temp= fopen(buf_in,"r");


    /*count the number of variables*/

    while (strcmp(field2,"'MARKER'")!=0)
    {


        item = scan_line(temp,field1,field2,field3,field4,field5);
    }



    item = scan_line(temp,field1,field2,field3,field4,field5);


    while (strcmp(field2,"'MARKER'")!=0)
    {
        if(strcmp(field1,field6) !=0)
        {
            qp->n++;
        }



        item = scan_line(temp,field1,field2,field3,field4,field5);

    }


    fclose(temp);

    /*recover data */

    table_eq=create_table(qp->m+1,qp->n);
    table_ineq=create_table(qp->p+1,qp->n);
    table_obj=create_table(2,2);




    i=0;
    temp= fopen(buf_in,"r");

    while ( strcmp(field1,"ROWS")!=0)
    {


        item = scan_line(temp,field1,field2,field3,field4,field5);
    }



    while ( strcmp(field1,"COLUMNS")!=0)
    {


        item = scan_line(temp,field1,field2,field3,field4,field5);

        if(strcmp(field1,"N")==0)
        {
            add_to_table( &table_obj,field2, 'N', 2);
        }

        if(strcmp(field1,"E")==0)
        {
            add_to_table( &table_eq,field2,  'E',qp->m);
        }

        if (strcmp(field1,"L")==0)
        {
            add_to_table( &table_ineq, field2,  'L',qp->p);
        }

        if(strcmp(field1,"G")==0)
        {
            add_to_table( &table_ineq, field2, 'G',qp->p);
        }
    }


    printf("cont bis\n");
    printf("%s\n",table_obj[0].constraint);

    while ( strcmp(field2,"'MARKER'")!=0)
    {


        item = scan_line(temp,field1,field2,field3,field4,field5);
    }


    k=0;


    item = scan_line(temp,field1,field2,field3,field4,field5);


    table_var=create_tablebis(qp->n);
    while (strcmp(field2,"'MARKER'")!=0)
    {

        if(strcmp(field1,field6) !=0)
        {
            add_to_tablebis( &table_var,field1,qp->n);
            k++;
        }

        g=atof(field3);

        i= search_into_tablebis( &table_var,field1,qp->n);


        if(i!=-1)
        {
            j= search_into_table( &table_obj,field2,2);
            if(j!=-1)
                modify_table( &table_obj, i,j, g);
            else if(j==-1)
            {
                j= search_into_table(& table_eq,field2,qp->m);
                if(j!=-1)
                    modify_table(&table_eq, i,j, g);
                else if(j==-1)
                {
                    j= search_into_table( &table_ineq,field2,qp->p);
                    if(j!=-1)
                        modify_table(&table_ineq, i,j, g);
                }
            }
        }
        if(item==5)
        {
            g=atof(field5);
            if(i!=-1)
            {
                j= search_into_table( &table_obj,field4,2);
                if(j!=-1)
                    modify_table( &table_obj, i,j, g);
                else if(j==-1)
                {
                    j= search_into_table( &table_eq,field4,qp->m);
                    if(j!=-1)
                        modify_table( &table_eq, i,j, g);
                    else if(j==-1)
                    {
                        j= search_into_table( &table_ineq,field4,qp->p);
                        if(j!=-1)
                            modify_table( &table_ineq,i, j, g);

                    }
                }
            }
        }


        copy3_string(field6,field1);



        item = scan_line(temp,field1,field2,field3,field4,field5);
    }





    printf("table_var\n");
    for(i=0; i<qp->n; i++)
        printf("%s\n",table_var[i].variable);

    printf("values\n");
    for(j=0; j<qp->m; j++)
        for(i=0; i<qp->n; i++)
            printf("%lf\n",table_eq[j].value[i]);


    if (!Zero_BB(qp->m))
        compute_a(&table_eq,qp);

    if (!Zero_BB(qp->p))
        compute_d(&table_ineq,qp);

    compute_c(&table_obj,qp);



    i=0;
    while ( (strcmp(field1,"RANGES") != 0 ) && (strcmp(field1,"BOUNDS") != 0))
    {

        printf("i:%d\n",i);
        item = scan_line(temp,field1,field2,field3,field4,field5);

        g=atof(field3);


        j= search_into_table(&table_obj,field2,2);
        if(j!=-1)
            modify_table_b(&table_obj,j,g);
        else if(j==-1)
        {
            j= search_into_table(&table_eq,field2,qp->m);
            if(j!=-1)
                modify_table_b(&table_eq,j, g);
            else if(j==-1)
            {
                j= search_into_table(&table_ineq,field2,qp->p);
                if(j!=-1)
                    modify_table_b(&table_ineq, j, g);
            }
        }



    }
    /*free_tab(table_cont,qp->m+qp->p+1);*/

    if(!Zero_BB(qp->m))
        compute_b(&table_eq,qp);
    if(!Zero_BB(qp->p))
        compute_e(&table_ineq,qp);

    if (strcmp(field1,"RANGES") == 0 )
        item = scan_line(temp,field1,field2,field3,field4,field5);


//a changer
    qp->u = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->u[i]= 150;
    qp->l = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
        qp->l[i]= 0;

    while  (strcmp(field1,"QUADOBJ") != 0 )
    {



        item = scan_line(temp,field1,field2,field3,field4,field5);


        if(strcmp(field1,"FR") != 0)
        {

            if(strcmp(field1,"UP") == 0)
            {
                ind=search_into_tablebis(&table_var,field3,qp->n);
                g=atof(field4);

                qp->u[ind]=(int)g;
            }
        }
    }


    qp->q=alloc_matrix_d(qp->n,qp->n);
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            qp->q[ij2k(i,j,qp->n)]=0;

    item = scan_line(temp,field1,field2,field3,field4,field5);
    while  (strcmp(field1,"ENDATA") != 0 )
    {




        ind1= search_into_tablebis(&table_var,field1,qp->n);

        ind2= search_into_tablebis(&table_var,field2,qp->n);

        g=atof(field3);

        qp->q[ij2k(ind1,ind2,qp->n)]=g;
        qp->q[ij2k(ind2,ind1,qp->n)]=g;
        item = scan_line(temp,field1,field2,field3,field4,field5);
    }



    /*  free_string(s); */
    /*  free_string(s2); */
    /*  free_string(s3); */
    /*  free_string(s4); */
    /*  free_tab(tab_eq,qp->p); */
    /*  free_tab(tab_in,qp->m); */
    /*  free_tab(tab_obj,2); */
    /*  free_tab(tab_var,qp->n); */

    fclose(temp);

    qp->lgc = create_u_base2_cumulated(qp->u,qp->n);
    qp->lg = create_u_base2(qp->u,qp->n);
    qp->N = vector_length_base_2(qp->u,qp->n);
    ind = nb_non_zero_matrix_sym(qp->q,qp->n);


    file_instance = fopen(buf_out,"a");

    fprintf(file_instance,"%d %d %d %d %d %d\n",qp->n,qp->nb_int,qp->m,qp->p,qp->mq,qp->pq);
    fprintf(file_instance,"u\n");
    write_vector_u(qp->u,qp->n,file_instance);
    fprintf(file_instance,"Q\n");
    write_matrix_Qd(qp->q,qp->n,qp->n,file_instance);
    fprintf(file_instance,"c\n");
    write_vector_d(qp->c,qp->n,file_instance);
    fprintf(file_instance,"l\n%lf",qp->cons);

    /*Ax =b*/
    if(qp->m!=0)
    {
        fprintf(file_instance,"A\n");
        write_matrix_d(qp->a,qp->m,qp->n,file_instance);
        fprintf(file_instance,"b\n");
        write_vector_d(qp->b,qp->m,file_instance);
    }
    /*Dx<=e*/
    if(qp->p!=0)
    {
        fprintf(file_instance,"D\n");
        write_matrix_d(qp->d,qp->p,qp->n,file_instance);
        fprintf(file_instance,"e\n");
        write_vector_d(qp->e,qp->p,file_instance);
    }

    /*x^T Aq x = bq*/
    if(qp->mq!=0)
    {
        fprintf(file_instance,"Aq\n");
        write_matrix_3d(qp->aq,qp->mq,qp->n,qp->n,file_instance);
        fprintf(file_instance,"bq\n");
        write_vector_d(qp->bq,qp->mq,file_instance);
    }

    /*x^T Dq x <= eq*/
    if(qp->pq!=0)
    {
        fprintf(file_instance,"Dq\n");
        write_matrix_3d(qp->dq,qp->pq,qp->n,qp->n,file_instance);
        fprintf(file_instance,"eq\n");
        write_vector_d(qp->eq,qp->pq,file_instance);
    }

    fclose(file_instance);
}

/**********************************************************************************/
/*********************************  Read a nl file ********************************/
/**********************************************************************************/
/*read_sum()*/
double * read_sum(FILE * file_in,int nb_sum,int n)
{
    int j,ind_j;
    char tmp;
    int tp;
    double val;
    double * d=alloc_vector_d(n);
    for (j=0; j<n; j++)
        d[j]=0;

    for (j=0; j<nb_sum; j++)
    {
        fscanf(file_in,"%c%d\n",&tmp,&tp);
        switch(tmp)
        {
        case 'o':
            fscanf(file_in,"n%lf\n",&val);
            fscanf(file_in,"v%d\n",&ind_j);
            d[ind_j]=val;
            break;
        case 'v':
            d[tp]=1;
            break;
        default:
            printf("\nerror format q matrix\n");
        }
    }

    return d;
}

/*read_nl_file()*/
void read_nl_file( MIQCP qp)
{

    FILE * file_in = fopen(buf_in,"r");
    char tmp;

    int sense;

    int i,j,r,l,k;
    int tot_cont,nb_nl_c,nb_nl_o, nb_l_c;
    int nb_eq,nb_range;
    int tp,tp2;
    int nb_i,nb_b;
    int nnz_q,nnz_c,nb_sum,nb_sum_var;
    int ind_eg,ind_ineg,ind_i,ind_j;
    double val;
    double factor;
    int neg;

    double *dq,*d;
    double *u_q,*l_q,*u_l,*l_l;
    int * tab_cont;

    /*for linear constraints*/
    double * a_t_a;
    double * a_t;
    double b_q;

    /*  read first line ascii code file*/
    fscanf(file_in,"g3");
    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);

    /*  vars, constraints, objectives, ranges, eqns*/
    fscanf(file_in," %d %d 1 %d %d",&qp->n,&tot_cont,&nb_range,&nb_eq);
    qp->m=0;
    qp->p=0;
    qp->mq=0;
    qp->pq=0;
    qp->cons=0;
    /*allocation and initialization to 0 of the coeff of the objective function*/
    qp->q=alloc_matrix_d(qp->n,qp->n);
    qp->c=alloc_vector_d(qp->n);
    qp->u=alloc_vector_d(qp->n);
    qp->l=alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
    {
        qp->c[i]=0;
        qp->u[i]=MAXU;
        qp->l[i]=0;
        for(j=i; j<qp->n; j++)
        {
            qp->q[ij2k(i,j,qp->n)]=0;
            qp->q[ij2k(j,i,qp->n)]=0;
        }
    }


    if (Positive(tot_cont))
        tab_cont=alloc_vector(tot_cont);

    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);

    /* nonlinear constraints, objectives*/
    fscanf(file_in," %d %d\n",&nb_nl_c,&nb_nl_o);

    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);

    /*network constraints: nonlinear, linear*/
    fscanf(file_in," %d %d",&tp,&tp);

    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);
    /* nonlinear vars in constraints, objectives, both*/
    fscanf(file_in," %d %d %d",&tp,&tp,&tp);

    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);
    /* linear network variables; functions; arith, flags*/
    fscanf(file_in," %d %d %d %",&tp,&tp,&tp,&tp);

    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);
    /* discrete variables: binary, integer, nonlinear (b,c,o)*/
    fscanf(file_in," %d %d %d %d %d\n",&nb_b,&nb_i,&tp,&tp,&qp->nb_int);


    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);
    /* nonzeros in Jacobian, gradients*/
    fscanf(file_in," %d %d",&nnz_q,&nnz_c);

    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);
    /*max name lengths: constraints, variables*/
    fscanf(file_in," %d %d",&tp,&tp);

    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);
    /* common exprs: b,c,o,c1,o1*/
    fscanf(file_in," %d %d %d %d %d",&tp,&tp,&tp,&tp,&tp);
    fscanf(file_in,"%c",&tmp);
    while(tmp!='\n')
        fscanf(file_in,"%c",&tmp);


    /*allocation and initialization of matrix and vectors*/
    if (Positive(tot_cont))
        dq=alloc_matrix_3d(nb_nl_c,qp->n+1,qp->n+1);


    for (r=0; r<nb_nl_c; r++)
    {

        for(i=0; i<qp->n+1; i++)
            for(j=0; j<qp->n+1; j++)
                dq[ijk2l(r,i,j,qp->n+1,qp->n+1)]=0;
    }


    /*quadbratic part of the constraints*/
    nb_l_c=tot_cont-nb_nl_c;
    for (r=0; r<tot_cont; r++)
    {
        factor=1;
        neg=1;
        fscanf(file_in,"C%d\n",&tp);
        fscanf(file_in,"%c%d\n",&tmp,&tp);
        switch(tmp)
        {
        case 'o':
            if (tp == 16)
            {
                neg =-1;
                fscanf(file_in,"%c%d\n",&tmp,&tp);
            }
            switch(tp)
            {
            case 54:
                fscanf(file_in,"%d\n",&nb_sum);
                for (i=0; i<nb_sum; i++)
                {
                    factor=1;
                    neg=1;
                    fscanf(file_in,"%c%d\n",&tmp,&tp);
                    switch(tmp)
                    {
                    case 'o':
                        if (tp == 16)
                        {
                            neg =-1;
                            fscanf(file_in,"%c%d\n",&tmp,&tp);
                        }
                        switch(tp)
                        {
                        case 54:
                            fscanf(file_in,"%d\n",&nb_sum_var);
                            d=read_sum(file_in,nb_sum_var,qp->n);
                            for(j=0; j<qp->n; j++)
                                dq[ijk2l(r,ind_i+1,j+1,qp->n+1,qp->n+1)]=d[j]*factor*neg;
                            break;
                        case 0:
                            d=read_sum(file_in,2,qp->n);
                            for(j=0; j<qp->n; j++)
                                dq[ijk2l(r,ind_i+1,j+1,qp->n+1,qp->n+1)]=d[j]*factor*neg;
                            break;
                        case 2:
                            fscanf(file_in,"%c",&tmp);
                            switch(tmp)
                            {
                            case 'n':
                                fscanf(file_in,"%lf\n",&val);
                                fscanf(file_in,"%c%d\n",&tmp,&tp);
                                switch(tmp)
                                {
                                case 'o':
                                    switch(tp)
                                    {
                                    case 2:
                                        factor=val;
                                        fscanf(file_in,"%c%d\n",&tmp,&tp);
                                        ind_i=tp;
                                        fscanf(file_in,"%c",&tmp);
                                        switch(tmp)
                                        {
                                        case 'o':
                                            fscanf(file_in,"%d\n",&tp);
                                            switch(tp)
                                            {
                                            case 54:
                                                fscanf(file_in,"%d\n",&nb_sum_var);
                                                d=read_sum(file_in,nb_sum_var,qp->n);
                                                for(j=0; j<qp->n; j++)
                                                    dq[ijk2l(r,ind_i+1,j+1,qp->n+1,qp->n+1)]=d[j]*factor*neg;
                                                break;
                                            case 2:
                                                fscanf(file_in,"n%lf\n",&val);
                                                fscanf(file_in,"v%d\n",&ind_j);
                                                dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=val*factor*neg;
                                                break;
                                            case 0:
                                                d=read_sum(file_in,2,qp->n);
                                                for(j=0; j<qp->n; j++)
                                                    dq[ijk2l(r,ind_i+1,j+1,qp->n+1,qp->n+1)]=d[j]*factor*neg;
                                                break;

                                            default:
                                                printf("\n error dq matrix\n");

                                            }
                                            break;
                                        case 'v':
                                            fscanf(file_in,"%d\n",&ind_j);
                                            dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=val*factor*neg;
                                            break;

                                        default:
                                            printf("\n error dq matrix\n");

                                        }
                                        break;
                                    case 5:
                                        /*only the square case for us*/
                                        factor=val;
                                        fscanf(file_in,"v%d\n",&ind_i);
                                        fscanf(file_in,"n%lf\n",&val);
                                        dq[ijk2l(r,ind_i+1,ind_i+1,qp->n+1,qp->n+1)]=factor*neg;
                                        break;
                                    default:
                                        printf("\n error dq matrix\n");
                                    }

                                    break;
                                case 'v':
                                    ind_j=tp;
                                    dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=val*factor*neg;
                                    break;
                                default:
                                    printf("\n error format dq matrix o2 n_val o2 v_ind\n");
                                }
                                break;
                            case 'o':
                                fscanf(file_in,"%d\n",&tp);
                                fscanf(file_in,"n%lf\n",&val);
                                fscanf(file_in,"v%d\n",&ind_i);
                                fscanf(file_in,"v%d\n",&ind_j);
                                dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=val*factor*neg;
                                break;
                            case 'v':
                                fscanf(file_in,"%d\n",&ind_i);
                                fscanf(file_in,"v%d\n",&ind_j);
                                dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=factor*neg;
                                break;

                            default:
                                printf("\n error format dq matrix\n");
                            }
                            break;
                        case 5:
                            /*only the square case for us*/
                            factor=1;
                            fscanf(file_in,"v%d\n",&ind_i);
                            fscanf(file_in,"n%lf\n",&val);
                            dq[ijk2l(r,ind_i+1,ind_i+1,qp->n+1,qp->n+1)]=factor*neg;
                            break;
                        default:
                            printf("\n error format dq matrix\n");

                        }
                        break;
                    case 'v':
                        ind_j=tp;
                        dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=1*factor*neg;
                        break;
                    default:
                        printf("\n error format dq matrix\n");
                    }

                }
                /*nb_nl_c++;*/
                break;
            case 0:
                d=read_sum(file_in,2,qp->n);
                for(j=0; j<qp->n; j++)
                    dq[ijk2l(r,ind_i+1,j+1,qp->n+1,qp->n+1)]=d[j]*factor*neg;
                /*nb_nl_c++;*/
                break;
            case 2:
                fscanf(file_in,"%c\n",&tmp);
                switch(tmp)
                {
                case 'n':
                    fscanf(file_in,"%lf\n",&val);
                    fscanf(file_in,"%c%d\n",&tmp,&tp);
                    switch(tmp)
                    {
                    case 'o':
                        factor=val;
                        fscanf(file_in,"%c%d\n",&tmp,&tp);
                        switch(tmp)
                        {
                        case 'v':
                            ind_i=tp;
                            fscanf(file_in,"%c",&tmp);
                            switch(tmp)
                            {
                            case 'o':
                                switch(tp)
                                {
                                case 54:
                                    fscanf(file_in,"%d\n",&nb_sum_var);
                                    d=read_sum(file_in,nb_sum_var,qp->n);
                                    for(j=0; j<qp->n; j++)
                                        dq[ijk2l(r,ind_i+1,j+1,qp->n+1,qp->n+1)]=d[j]*factor*neg;
                                    break;
                                case 2:
                                    fscanf(file_in,"n%lf\n",&val);
                                    fscanf(file_in,"v%d\n",&ind_j);
                                    dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=val*factor*neg;
                                    break;
                                case 0:
                                    d=read_sum(file_in,2,qp->n);
                                    for(j=0; j<qp->n; j++)
                                        dq[ijk2l(r,ind_i+1,j+1,qp->n+1,qp->n+1)]=d[j]*factor*neg;
                                    break;
                                default:
                                    printf("\n error dq matrix\n");
                                }
                                break;
                            default:
                                printf("\n error dq matrix\n");
                            }
                            break;
                        case 'o':
                            switch(tp)
                            {
                            case 5:
                                /*only the square case for us*/
                                factor=val;
                                fscanf(file_in,"v%d\n",&ind_i);
                                fscanf(file_in,"n%lf\n",&val);
                                dq[ijk2l(r,ind_i+1,ind_i+1,qp->n+1,qp->n+1)]=factor*neg;
                                break;
                            default:
                                printf("\n error dq matrix pow \n");
                            }
                            break;
                        default:
                            printf("\n error dq matrix pow \n");
                        }
                        break;
                    case 'v':
                        fscanf(file_in,"%d\n",&ind_j);
                        dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=val*factor*neg;
                        break;
                    default:
                        printf("\n error format dq matrix o2 n_val o2 v_ind\n");
                    }
                    break;
                case 'o':
                    fscanf(file_in,"%d\n",&tp);
                    fscanf(file_in,"n%lf\n",&val);
                    fscanf(file_in,"v%d\n",&ind_i);
                    fscanf(file_in,"v%d\n",&ind_j);
                    dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=val*factor*neg;
                    break;
                case 'v':
                    fscanf(file_in,"%d\n",&ind_i);
                    fscanf(file_in,"v%d\n",&ind_j);
                    dq[ijk2l(r,ind_i+1,ind_j+1,qp->n+1,qp->n+1)]=factor*neg;
                    break;

                default:
                    printf("\n error format dq matrix\n");
                }
                /*nb_nl_c++;*/
                break;
            case 5:
                /*only the square case for us*/
                factor=val;
                fscanf(file_in,"v%d\n",&ind_i);
                fscanf(file_in,"n%lf\n",&val);
                dq[ijk2l(r,ind_i+1,ind_i+1,qp->n+1,qp->n+1)]=factor*neg;
                break;
            default:
                printf("\n error format read dq matrix\n");
            }
            break;
        case 'n':
            /*nb_l_c++;*/
            break;
        default:
            printf("\n error format read dq matrix\n");
        }
    }



    for(k=0; k<nb_nl_c; k++)
        if (!is_symetric_3d(dq,k,qp->n+1))
        {
            for(i=0; i<qp->n+1; i++)
            {
                for(j=i+1; j<qp->n+1; j++)
                {
                    dq[ijk2l(k,i,j,qp->n+1,qp->n+1)] = (dq[ijk2l(k,i,j,qp->n+1,qp->n+1)] + dq[ijk2l(k,j,i,qp->n+1,qp->n+1)])/2;
                    dq[ijk2l(k,j,i,qp->n+1,qp->n+1)] = dq[ijk2l(k,i,j,qp->n+1,qp->n+1)];
                }
            }
        }

    /*allocation of vector and constraints*/
    u_q=alloc_vector_d(nb_nl_c);
    l_q=alloc_vector_d(nb_nl_c);
    u_l=alloc_vector_d(nb_l_c);
    l_l=alloc_vector_d(nb_l_c);



    for (r=0; r<nb_nl_c; r++)
    {
        u_q[r]=0;
        l_q[r]=0;
    }
    for (r=0; r<nb_l_c; r++)
    {
        u_l[r]=0;
        l_l[r]=0;
    }




    /* Objective function */
    fscanf(file_in,"O0 %d\n",&sense);
    /*if sense ==0 minimization, else ==1 maximization*/

    /*quadratic part of the objective function*/

    fscanf(file_in,"%c%d\n",&tmp,&tp);
    switch(tmp)
    {
    case 'o':
        switch(tp)
        {
        case 54:
            fscanf(file_in,"%d\n",&nb_sum);
            for (i=0; i<nb_sum; i++)
            {
                factor=1;
                fscanf(file_in,"%c%d\n",&tmp,&tp);
                switch(tmp)
                {
                case 'o':
                    switch(tp)
                    {
                    case 54:
                        fscanf(file_in,"%d\n",&nb_sum_var);
                        d=read_sum(file_in,nb_sum_var,qp->n);
                        for(j=0; j<qp->n; j++)
                            qp->q[ij2k(ind_i,j,qp->n)]=d[j]*factor;
                        break;
                    case 0:
                        d=read_sum(file_in,2,qp->n);
                        for(j=0; j<qp->n; j++)
                            qp->q[ij2k(ind_i,j,qp->n)]=d[j]*factor;
                        break;
                    case 2:
                        fscanf(file_in,"%c",&tmp);
                        switch(tmp)
                        {
                        case 'n':
                            fscanf(file_in,"%lf\n",&val);
                            fscanf(file_in,"%c%d\n",&tmp,&tp);
                            switch(tmp)
                            {
                            case 'o':
                                factor=val;
                                fscanf(file_in,"v%d\n",&ind_i);
                                fscanf(file_in,"%c",&tmp);
                                switch(tmp)
                                {
                                case 'o':
                                    fscanf(file_in,"%d\n",&tp);
                                    switch(tp)
                                    {
                                    case 54:
                                        fscanf(file_in,"%d\n",&nb_sum_var);
                                        d=read_sum(file_in,nb_sum_var,qp->n);
                                        for(j=0; j<qp->n; j++)
                                            qp->q[ij2k(ind_i,j,qp->n)]=d[j]*factor;
                                        break;
                                    case 2:
                                        fscanf(file_in,"n%lf\n",&val);
                                        fscanf(file_in,"v%d\n",&ind_j);
                                        qp->q[ij2k(ind_i,ind_j,qp->n)]=val*factor;
                                        break;
                                    case 0:
                                        d=read_sum(file_in,2,qp->n);
                                        for(j=0; j<qp->n; j++)
                                            qp->q[ij2k(ind_i,j,qp->n)]=d[j]*factor;
                                        break;
                                    default:
                                        printf("\n error q matrix\n");

                                    }
                                    break;
                                case 'v':
                                    fscanf(file_in,"%d\n",&ind_j);
                                    qp->q[ij2k(ind_i,ind_j,qp->n)]=val*factor;
                                    break;

                                default:
                                    printf("\n error q matrix\n");

                                }

                                break;
                            case 'v':
                                ind_j=tp;
                                qp->q[ij2k(ind_i,ind_j,qp->n)]=val*factor;
                                break;
                            default:
                                printf("\n error format q matrix o2 n_val o2 v_ind\n");
                            }
                            break;
                        case 'o':
                            fscanf(file_in,"%d\n",&tp);
                            fscanf(file_in,"n%lf\n",&val);
                            fscanf(file_in,"v%d\n",&ind_i);
                            fscanf(file_in,"v%d\n",&ind_j);
                            qp->q[ij2k(ind_i,ind_j,qp->n)]=val*factor;
                            break;
                        case 'v':
                            fscanf(file_in,"%d\n",&ind_i);
                            fscanf(file_in,"v%d\n",&ind_j);
                            qp->q[ij2k(ind_i,ind_j,qp->n)]=factor;
                            break;

                        default:
                            printf("\n error format q matrix\n");
                        }
                        break;
                    default:
                        printf("\n error format q matrix\n");
                    }
                    break;
                case 'v':
                    ind_j=tp;
                    qp->q[ij2k(ind_i,ind_j,qp->n)]=1*factor;
                    break;
                default:
                    printf("\n error format q matrix\n");
                }
            }
            break;
        case 0:
            d=read_sum(file_in,2,qp->n);
            for(j=0; j<qp->n; j++)
                qp->q[ij2k(ind_i,j,qp->n)]=d[j]*factor;
            break;
        case 2:
            fscanf(file_in,"%c\n",&tmp);
            switch(tmp)
            {
            case 'n':
                fscanf(file_in,"%lf\n",&val);
                fscanf(file_in,"%c%d\n",&tmp,&tp);
                switch(tmp)
                {
                case 'o':
                    factor=val;
                    fscanf(file_in,"v%d\n",&ind_i);
                    fscanf(file_in,"%c%d\n",&tmp,&tp);
                    switch(tmp)
                    {
                    case 'o':
                        switch(tp)
                        {
                        case 54:
                            fscanf(file_in,"%d\n",&nb_sum_var);
                            d=read_sum(file_in,nb_sum_var,qp->n);
                            for(j=0; j<qp->n; j++)
                                qp->q[ij2k(ind_i,j,qp->n)]=d[j]*factor;
                            break;
                        case 2:
                            fscanf(file_in,"n%lf\n",&val);
                            fscanf(file_in,"v%d\n",&ind_j);
                            qp->q[ij2k(ind_i,ind_j,qp->n)]=val*factor;
                            break;
                        case 0:
                            d=read_sum(file_in,2,qp->n);
                            for(j=0; j<qp->n; j++)
                                qp->q[ij2k(ind_i,j,qp->n)]=d[j]*factor;
                            break;
                        default:
                            printf("\error q matrix\n");
                        }
                        break;
                    default:
                        printf("\error q matrix\n");
                    }
                    break;
                case 'v':
                    fscanf(file_in,"%d\n",&ind_j);
                    qp->q[ij2k(ind_i,ind_j,qp->n)]=val*factor;
                    break;
                default:
                    printf("\nerror format q matrix o2 n_val o2 v_ind\n");
                }
                break;
            case 'o':
                fscanf(file_in,"%d\n",&tp);
                fscanf(file_in,"n%lf\n",&val);
                fscanf(file_in,"v%d\n",&ind_i);
                fscanf(file_in,"v%d\n",&ind_j);
                qp->q[ij2k(ind_i,ind_j,qp->n)]=val*factor;
                break;
            case 'v':
                fscanf(file_in,"%d\n",&ind_i);
                fscanf(file_in,"v%d\n",&ind_j);
                qp->q[ij2k(ind_i,ind_j,qp->n)]=factor;
                break;

            default:
                printf("\nerror format q matrix\n");
            }
            break;

        default:
            printf("\nerror format read q matrix\n");
        }
        break;
    case 'n':
        break;
    default:
        printf("\nerror format read q matrix\n");
    }



    //change the sign of Q matrix if sense ==1
    if (sense==1)
        for(i=0; i<qp->n; i++)
            for(j=0; j<qp->n; j++)
                qp->q[ij2k(i,j,qp->n)] = -qp->q[ij2k(i,j,qp->n)];

    /*check if Q is symetric : if not make it symetric */
    if (!is_symetric(qp->q,qp->n))
    {
        for(i=0; i<qp->n; i++)
        {
            for(j=i+1; j<qp->n; j++)
            {
                qp->q[ij2k(i,j,qp->n)] = (qp->q[ij2k(i,j,qp->n)] +qp->q[ij2k(j,i,qp->n)])/2;
                qp->q[ij2k(j,i,qp->n)] = qp->q[ij2k(i,j,qp->n)];
            }
        }
    }

    /* rhs attention pb quand egalites*/
    fscanf(file_in,"r\n");
    for (r=0; r<nb_nl_c; r++)
    {
        fscanf(file_in,"%d ",&tp);
        switch (tp)
        {
        case 0:
            qp->pq=qp->pq+2;
            tab_cont[r]=0;
            fscanf(file_in,"%lf %lf\n",&l_q[r],&u_q[r]);
            break;
        case 1:
            qp->pq++;
            tab_cont[r]=1;
            fscanf(file_in,"%lf\n",&u_q[r]);
            break;
        case 2:
            qp->pq++;
            tab_cont[r]=2;
            fscanf(file_in,"%lf\n",&l_q[r]);
            break;
        case 4:
            qp->mq++;
            tab_cont[r]=4;
            fscanf(file_in,"%lf\n",&u_q[r]);
            break;
        default:
            printf("\n error format rhs \n");
        }
    }

    for (r=0; r<nb_l_c; r++)
    {
        fscanf(file_in,"%d ",&tp);
        switch (tp)
        {
        case 0:
            qp->p=qp->p+2;
            qp->pq = qp->pq + 4*qp->n;
            tab_cont[r+nb_nl_c]=0;
            fscanf(file_in,"%lf %lf\n",&l_l[r],&u_l[r]);
            break;
        case 1:
            qp->p++;
            qp->pq = qp->pq + 2*qp->n;
            tab_cont[r+nb_nl_c]=1;
            fscanf(file_in,"%lf\n",&u_l[r]);
            break;
        case 2:
            qp->p++;
            qp->pq = qp->pq + 2*qp->n;
            tab_cont[r+nb_nl_c]=2;
            fscanf(file_in,"%lf\n",&l_l[r]);
            break;
        case 4:
            qp->m++;
            tab_cont[r+nb_nl_c]=4;
            fscanf(file_in,"%lf\n",&u_l[r]);
            break;
        default:
            printf("\n error format rhs \n");
        }
    }
    /*Add the quadratic constraints for linear equalities*/
    if (Positive(qp->m))
        qp->mq++;


    /*allocation and initialization to 0 of the coeff of the constraints*/

    if (Positive(qp->mq))
    {
        qp->aq=alloc_matrix_3d(qp->mq,qp->n+1,qp->n+1);
        qp->bq= alloc_vector_d(qp->mq);
    }
    else
    {
        qp->aq=alloc_matrix_3d(1,1,1);
        qp->bq=alloc_vector_d(1);
    }

    if (Positive(qp->pq))
    {
        qp->dq=alloc_matrix_3d(qp->pq,qp->n+1,qp->n+1);
        qp->eq= alloc_vector_d(qp->pq);
    }
    else
    {
        qp->dq=alloc_matrix_3d(1,1,1);
        qp->eq=alloc_vector_d(1);
    }

    if  (Positive(qp->m))
    {
        qp->a= alloc_matrix_d(qp->m,qp->n);
        qp->b= alloc_vector_d(qp->m);
    }
    else
    {
        qp->a=alloc_matrix_d(1,1);
        qp->b= alloc_vector_d(1);
    }

    if  (Positive(qp->p))
    {
        qp->d= alloc_matrix_d(qp->p,qp->n);
        qp->e= alloc_vector_d(qp->p);
    }
    else
    {
        qp->d=alloc_matrix_d(1,1);
        qp->e= alloc_vector_d(1);
    }


    /*quadratic equalities*/
    for (r=0; r<qp->mq; r++)
    {
        qp->bq[r]=0;
        for(i=0; i<qp->n+1; i++)
        {
            for(j=i; j<qp->n+1; j++)
            {
                qp->aq[ijk2l(r,i,j,qp->n+1,qp->n+1)]=0;
                qp->aq[ijk2l(r,j,i,qp->n+1,qp->n+1)]=0;
            }
        }
    }
    /*quadratic inequalities*/
    for (r=0; r<qp->pq; r++)
    {
        qp->eq[r]=0;
        for(i=0; i<qp->n+1; i++)
        {
            for(j=i; j<qp->n+1; j++)
            {
                qp->dq[ijk2l(r,i,j,qp->n+1,qp->n+1)]=0;
                qp->dq[ijk2l(r,j,i,qp->n+1,qp->n+1)]=0;
            }
        }
    }

    /*linear equalities*/
    for (r=0; r<qp->m; r++)
    {
        qp->b[r]=0;
        for(i=0; i<qp->n; i++)
            qp->a[ij2k(r,i,qp->n)]=0;
    }

    /*linear inequalities*/
    for (r=0; r<qp->p; r++)
    {
        qp->e[r]=0;
        for(i=0; i<qp->n; i++)
            qp->d[ij2k(r,i,qp->n)]=0;
    }


    /*bounds*/
    fscanf(file_in,"b\n");
    for (i=0; i<qp->n; i++)
    {
        fscanf(file_in,"%d ",&tp);
        switch (tp)
        {
        case 0:
            fscanf(file_in,"%lf %lf\n",&qp->l[i],&qp->u[i]);
            break;
        case 1:
            fscanf(file_in,"%lf\n",&qp->u[i]);
            break;
        case 2:
            fscanf(file_in,"%lf\n",&qp->l[i]);
            break;
        case 4:
            fscanf(file_in,"%lf\n",&qp->u[i]);
            qp->l[i]=qp->u[i];
            break;
        case 3:
            if (DEBUG ==1)
                printf("\n no bounds on variables %d\n",i);
            break;
        default:
            printf("\n error format bounds \n");
        }

    }

    if (Positive(tot_cont))
    {
        fscanf(file_in,"%c",&tmp);
        while(tmp !='J')
            fscanf(file_in,"%c",&tmp);

        /*Building matrix aq,dq,a and d from */
        d=alloc_vector_d(qp->n);
        ind_ineg=0;
        ind_eg=0;
        for(r=0; r<nb_nl_c; r++)
        {
            for (i=0; i<qp->n; i++)
                d[i]=0;

            fscanf(file_in,"%d %d\n",&tp,&nnz_c);
            for (i=0; i<nnz_c; i++)
            {
                fscanf(file_in,"%d %lf\n",&ind_j,&val);
                d[ind_j]=val;
            }
            switch (tab_cont[r])
            {
            case 0:
                /*range case*/
                for (j=0; j<qp->n; j++)
                {
                    qp->dq[ijk2l(ind_ineg,0,j+1,qp->n+1,qp->n+1)]=-d[j]/2;
                    qp->dq[ijk2l(ind_ineg+1,i,j+1,qp->n+1,qp->n+1)]=d[j]/2;
                    qp->dq[ijk2l(ind_ineg,j+1,0,qp->n+1,qp->n+1)]=-d[j]/2;
                    qp->dq[ijk2l(ind_ineg+1,j+1,0,qp->n,1+qp->n+1)]=d[j]/2;
                }

                for (i=1; i<qp->n+1; i++)
                    for (j=1; j<qp->n+1; j++)
                    {
                        qp->dq[ijk2l(ind_ineg,i,j,qp->n+1,qp->n+1)]=-dq[ijk2l(r,i,j,qp->n+1,qp->n+1)];
                        qp->dq[ijk2l(ind_ineg+1,i,j,qp->n+1,qp->n+1)]=dq[ijk2l(r,i,j,qp->n+1,qp->n+1)];
                    }

                qp->eq[ind_ineg]=-l_q[r];
                qp->eq[ind_ineg+1]=u_q[r];
                ind_ineg=ind_ineg+2;
                break;
            case 1:
                /*leq case */
                for (j=0; j<qp->n; j++)
                {
                    qp->dq[ijk2l(ind_ineg,0,j+1,qp->n+1,qp->n+1)]=d[j]/2;
                    qp->dq[ijk2l(ind_ineg,j+1,0,qp->n+1,qp->n+1)]=d[i]/2;
                }

                for (i=1; i<qp->n+1; i++)
                    for (j=1; j<qp->n+1; j++)
                        qp->dq[ijk2l(ind_ineg,i,j,qp->n+1,qp->n+1)]=dq[ijk2l(r,i,j,qp->n+1,qp->n+1)];

                qp->eq[ind_ineg]=u_q[r];
                ind_ineg++;
                break;

            case 2:
                /*geq case*/
                for (j=0; j<qp->n; j++)
                {
                    qp->dq[ijk2l(ind_ineg,0,j+1,qp->n+1,qp->n+1)]=-d[j]/2;
                    qp->dq[ijk2l(ind_ineg,j+1,0,qp->n+1,qp->n+1)]=-d[i]/2;
                }
                for (i=1; i<qp->n+1; i++)
                    for (j=1; j<qp->n+1; j++)
                        qp->dq[ijk2l(ind_ineg,i,j,qp->n+1,qp->n+1)]=-dq[ijk2l(r,i,j,qp->n+1,qp->n+1)];

                qp->eq[r]=-l_q[r];
                ind_ineg++;
                break;

            case 4:
                /*equality case*/
                for (j=0; j<qp->n; j++)
                {
                    qp->aq[ijk2l(ind_eg,i,j+1,qp->n+1,qp->n+1)]=-d[j]/2;
                    qp->aq[ijk2l(ind_eg,i+1,0,qp->n+1,qp->n+1)]=-d[i]/2;
                }
                for (i=1; i<qp->n+1; i++)
                    for (j=1; j<qp->n+1; j++)
                        qp->aq[ijk2l(ind_eg,i,j,qp->n+1,qp->n+1)]=dq[ijk2l(r,i,j,qp->n+1,qp->n+1)];

                qp->bq[ind_eg]=u_l[r];
                ind_eg++;
                break;

            default:
                printf("\nerror building non linear constraints matrix\n");

            }
            fscanf(file_in,"%c",&tmp);
        }




        ind_eg=0;
        ind_ineg=0;
        for(r=0; r<nb_l_c; r++)
        {
            for (i=0; i<qp->n; i++)
                d[i]=0;

            fscanf(file_in,"%d %d\n",&tp,&nnz_c);
            for (i=0; i<nnz_c; i++)
            {
                fscanf(file_in,"%d %lf\n",&ind_j,&val);
                d[ind_j]=val;
            }



            switch (tab_cont[r+nb_nl_c])
            {
            case 0:
                /*range case*/
                for (i=0; i<qp->n; i++)
                {
                    qp->d[ij2k(ind_ineg,i,qp->n)]=-d[i];
                    qp->d[ij2k(ind_ineg+1,i,qp->n)]=d[i];
                }

                qp->e[ind_ineg]=-l_l[r];
                qp->e[ind_ineg+1]=u_l[r];
                ind_ineg=ind_ineg+2;

                if(nnz_c==1)
                {
                    qp->u[ind_j]=u_l[r];
                    qp->l[ind_j]=l_l[r];
                }
                break;

            case 1:
                /*leq case */
                for (i=0; i<qp->n; i++)
                    qp->d[ij2k(ind_ineg,i,qp->n)]=d[i];

                qp->e[ind_ineg]=u_l[r];
                ind_ineg++;
                break;

            case 2:
                /*geq case*/
                for (i=0; i<qp->n; i++)
                    qp->d[ij2k(ind_ineg,i,qp->n)]=-d[i];

                qp->e[ind_ineg]=-l_l[r];
                ind_ineg++;
                break;

            case 4:
                for (i=0; i<qp->n; i++)
                    qp->a[ij2k(ind_eg,i,qp->n)]=d[i];

                qp->b[ind_eg]=u_l[r];
                ind_eg++;
                break;

            default:
                printf("\nerror building linear constraints matrix\n");

            }
            fscanf(file_in,"%c",&tmp);
        }



        /*Add quadratic constraints from linear constraints*/
        if (Positive(qp->m))
        {
            /* creation of Matrix A^t A*/
            a_t_a = alloc_matrix_d(qp->n,qp->n);
            a_t = transpose_matrix_mn(qp->a, qp->m, qp->n);

            for (i=0; i<qp->n; i++)
                for (j=0; j<qp->n; j++)
                    a_t_a[ij2k(i,j,qp->n)]=0;


            for (i=0; i<qp->m; i++)
                for (j=0; j<qp->n; j++)
                    for(k=j; k<qp->n; k++)
                        a_t_a[ij2k(j,k,qp->n)]=a_t_a[ij2k(j,k,qp->n)] + a_t[ij2k(j,i,qp->m)]*qp->a[ij2k(i,k,qp->n)];

            for (j=0; j<qp->n; j++)
                for(k=j+1; k<qp->n; k++)
                    a_t_a[ij2k(k,j,qp->n)]=a_t_a[ij2k(j,k,qp->n)];

            b_q=0;
            for(i=0; i<qp->m; i++)
                b_q = b_q + qp->b[i]*qp->b[i];


            i=qp->mq-1;
            qp->aq[ijk2l(i,0,0,qp->n+1,qp->n+1)]=0;
            for (j=0; j<qp->n; j++)
            {
                qp->aq[ijk2l(i,0,j+1,qp->n+1,qp->n+1)]=0;
                qp->aq[ijk2l(i,j+1,0,qp->n+1,qp->n+1)]=0;
                for(k=0; k<qp->n; k++)
                    qp->aq[ijk2l(i,j+1,k+1,qp->n+1,qp->n+1)]=a_t_a[ij2k(j,k,qp->n)];
            }

            qp->bq[qp->mq-1]= b_q;
        }


        if (Positive(qp->p))
        {
            /* cont lin */
            k=qp->pq-2*qp->n*qp->p;
            for(l=0; l<qp->p; l++)
            {
                for(i=0; i<qp->n; i++)
                {

                    qp->dq[ijk2l(k,0,0,qp->n+1,qp->n+1)]=0;
                    qp->dq[ijk2l(k,0,i+1,qp->n+1,qp->n+1)]=-qp->e[l]/2;
                    qp->dq[ijk2l(k,i+1,0,qp->n+1,qp->n+1)]=-qp->e[l]/2;
                    for (j=0; j<qp->n; j++)
                    {
                        if (i==j)
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)];
                        else
                        {
                            qp->dq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)]/2;
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=qp->d[ij2k(l,j,qp->n)]/2;
                        }
                    }
                    k++;
                }
            }

            k=qp->pq-qp->n*qp->p;
            for(l=0; l<qp->p; l++)
            {
                for(i=0; i<qp->n; i++)
                {

                    qp->dq[ijk2l(k,0,0,qp->n+1,qp->n+1)]=0;

                    for (j=0; j<qp->n; j++)
                    {

                        if (i==j)
                        {
                            qp->dq[ijk2l(k,0,j+1,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)]+ qp->e[l])/2;
                            qp->dq[ijk2l(k,j+1,0,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)]+ qp->e[l])/2;
                            qp->dq[ijk2l(k,j+1,j+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)];
                        }
                        else
                        {
                            qp->dq[ijk2l(k,0,j+1,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)])/2;
                            qp->dq[ijk2l(k,j+1,0,qp->n+1,qp->n+1)]=(qp->u[i]*qp->d[ij2k(l,j,qp->n)])/2;
                            qp->dq[ijk2l(k,j+1,i+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)]/2;
                            qp->dq[ijk2l(k,i+1,j+1,qp->n+1,qp->n+1)]=-qp->d[ij2k(l,j,qp->n)]/2;
                        }
                    }
                    k++;
                }
            }


            for(i=qp->pq-2*qp->n*qp->p; i<qp->pq-qp->n*qp->p; i++)
                qp->eq[i]= 0;
            for(j=0; j<qp->p; j++)
                for(k=0; k<qp->n; k++)
                {
                    qp->eq[i]= qp->u[k]*qp->e[j];
                    i++;
                }
        }

    }

    /*linear coeff of the objective function vector */
    while(tmp!='G' )
        fscanf(file_in,"%c",&tmp);

    fscanf(file_in,"0 %d\n",&nnz_c);
    for (i=0; i<nnz_c; i++)
    {
        fscanf(file_in,"%d %lf\n",&j,&val);
        qp->c[j]=val;
    }


    fclose(file_in);

    qp->N = vector_length_base_2(qp->u, qp->nb_int);
    qp->lgc = create_u_base2_cumulated(qp->u,qp->n);
    qp->lg = create_u_base2(qp->u,qp->n);

}

