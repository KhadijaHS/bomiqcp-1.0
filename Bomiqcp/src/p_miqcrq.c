/* -*-c-*-
 *
 *    Source     $RCSfile: p_miqcrq.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>

#include<unistd.h>
#include<fcntl.h>

#include<ilcplex/cplex.h>

#include"p_miqcrq.h"
//#include"s_miqcrq.h"
#include"solver_sdp_mixed.h"
#include"utilities.h"
#include"quad_prog.h"
#include"in_out.h"
#include"in_out_ampl.h"
#include"local_sol.h"
#include"local_sol_ipopt.h"
#include"in_out_csdp.h"
#include"in_out_scilab.h"

double best_sol_adm_miqcp;

/*****compute_alpha_beta_p_miqcrq()*****/
void  compute_alpha_beta_p_miqcrq( MIQCP qp,  double ** beta, double ** alphaq,double ** alphabisq, SDP psdp) 
{ 
  best_sol_adm_miqcp = MAX_SOL_BB;
 
  int file_sol;
  pid_t pid_fils;

  FILE * file_save;
  FILE * file_au;
  double obj_val;

  int i,j;
  double  *beta_bis;
  double  *zbeta;
  double * zalphaq;
  double * zalphabisq;
  double zalpha=0;
  double zalphabis=0;
  double tp;
       
  zbeta = alloc_matrix_d(qp->n,qp->n);
  zalphaq= alloc_vector_d(qp->mq);
  zalphabisq= alloc_vector_d(qp->pq);
  
      
  if (DEBUG == 0  )
    {
      file_sol = open(res_sdp, O_RDWR | O_CREAT | O_TRUNC ,S_IRWXU | S_IRWXG | S_IRWXO);
      dup2(file_sol,STDOUT_FILENO);
      close(file_sol);
    }
  run_conic_bundle_mixed(psdp, &obj_val);
  file_save = fopen(res_sdp,"a");
     
  fprintf(file_save,"\nObjective function value sdp : %lf\n",obj_val);
  fclose(file_save);
      
  for(i=0;i<psdp->n;i++)
    for(j=i;j<psdp->n;j++)
      if (i==j) 
	zbeta[ij2k(i,i,qp->n)]= psdp->beta_diag[i];
      else
	zbeta[ij2k(i,j,qp->n)]= psdp->beta_1[ij2k(i,j,qp->n)] + psdp->beta_2[ij2k(i,j,qp->n)] - psdp->beta_3[ij2k(i,j,qp->n)] - psdp->beta_4[ij2k(i,j,qp->n)];
      
  for(i=0;i<psdp->n;i++)
    for(j=i+1;j<psdp->n;j++)
      zbeta[ij2k(j,i,qp->n)]=zbeta[ij2k(i,j,qp->n)];
      
  for(i=0;i<qp->n ;i++)
    for(j=0;j<qp->n ;j++)
      if (((zbeta[ij2k(i,j,qp->n )] < EPS_BETA) && (zbeta[ij2k(i,j,qp->n )] > 0)) || ((zbeta[ij2k(i,j,qp->n )] > -EPS_BETA) && (zbeta[ij2k(i,j,qp->n )] < 0)))
	zbeta[ij2k(i,j,qp->n )]=0;
      
  zalphaq=psdp->alphaq;
  zalphabisq=psdp->alphabisq;
      

  /* if (DEBUG == 1 ) */
  /*   { */
  /*     printf("\n zbeta (compute_alpha_beta_p_miqcrq\n"); */
  /*     print_mat_d(zbeta,psdp->n,psdp->n); */
  /*     printf("\n zalphaq \n"); */
  /*     print_vec_d(zalphaq,psdp->mq); */
  /*     printf("\n zalphabisq \n"); */
  /*     print_vec_d(zalphabisq,psdp->pq); */
  /*      printf("\n \n"); */
      
  /*   } */
  *beta = zbeta;
  
  *alphaq=zalphaq;
  *alphabisq=zalphabisq;

}

/*****compute_new_q_p_miqcrq()*****/
void compute_new_q_p_miqcrq(C_MIQCP cqp)
{
  double * a_t;
  double * d_t;
  int i,j,k;

  cqp->new_q = alloc_matrix_d(cqp->n,cqp->n);
  

  /*x^T Aq x = bq */
  if (!Zero(cqp->mq))
    {
      cqp->sum_r_aq_alphaq=alloc_matrix_d(cqp->n,cqp->n);
      for(i=0;i<cqp->n;i++)
	for(j=0;j<cqp->n;j++)
	  {
	    cqp->sum_r_aq_alphaq[ij2k(i,j,cqp->n)]=0;
	    for(k=0;k<cqp->mq;k++)
	     
		cqp->sum_r_aq_alphaq[ij2k(i,j,cqp->n)] = cqp->sum_r_aq_alphaq[ij2k(i,j,cqp->n)] + cqp->aq[ijk2l(k,i+1,j+1, cqp->n+1,cqp->n+1)]*cqp->alphaq[k];
	
	  }

      cqp->sum_r_bq=alloc_vector_d(cqp->n);
      for(i=0;i<cqp->n;i++)
	{
	  cqp->sum_r_bq[i]=0;
	  for(j=0;j<cqp->mq;j++)
	    cqp->sum_r_bq[i] =  cqp->sum_r_bq[i] + cqp->aq[ijk2l(j,0,i+1,cqp->n+1,cqp->n+1)]* cqp->alphaq[j];
	}
       
    }
   
  if (!Zero(cqp->pq))
    {
      cqp->sum_r_dq_alphabisq=alloc_matrix_d(cqp->n,cqp->n);
      for(i=0;i<cqp->n;i++)
	for(j=0;j<cqp->n;j++)
	  {
	    cqp->sum_r_dq_alphabisq[ij2k(i,j,cqp->n)]=0;
	    for(k=0;k<cqp->pq;k++)
	      cqp->sum_r_dq_alphabisq[ij2k(i,j,cqp->n)] = cqp->sum_r_dq_alphabisq[ij2k(i,j,cqp->n)] + cqp->dq[ijk2l(k,i+1,j+1, cqp->n+1,cqp->n+1)]*cqp->alphabisq[k];
	  }

      cqp->sum_r_eq=alloc_vector_d(cqp->n);
      for(i=0;i<cqp->n;i++)
	{
	  cqp->sum_r_eq[i]=0;
	  for(j=0;j<cqp->pq;j++)
	    cqp->sum_r_eq[i] =  cqp->sum_r_eq[i] + cqp->dq[ijk2l(j,0,i+1,cqp->n+1,cqp->n+1)]* cqp->alphabisq[j];
       
	}
    }
      
    /* } */
  for(i=0;i<cqp->n;i++)
    for(j=0;j<cqp->n;j++)
      { 
	cqp->new_q[ij2k(i,j,cqp->n)] = 2*cqp->q[ij2k(i,j,cqp->n)]+ 2*cqp->beta[ij2k(i,j,cqp->n) ];
		
	if (!Zero(cqp->mq))
	  cqp->new_q[ij2k(i,j,cqp->n)] = cqp->new_q[ij2k(i,j,cqp->n)] + 2*cqp->sum_r_aq_alphaq[ij2k(i,j,cqp->n)];
	
	if (!Zero(cqp->pq))
	  cqp->new_q[ij2k(i,j,cqp->n)]= cqp->new_q[ij2k(i,j,cqp->n)] + 2*cqp->sum_r_dq_alphabisq[ij2k(i,j,cqp->n)];

      }
  
}

/*****compute_new_q_linearization()*****/
void compute_new_q_linearization(C_MIQCP cqp)
{
  double * a_t;
  double * d_t;
  int i,j,k;
  cqp->new_q = alloc_matrix_d(cqp->n,cqp->n);
    for(i=0;i<cqp->n;i++)
    for(j=0;j<cqp->n;j++)
	cqp->new_q[ij2k(i,j,cqp->n)] = 0;  
}

/*****compute_new_lambda_min_bb_quad()*****/
void compute_new_lambda_min_bb_quad(C_MIQCP cqp) 
{
  int file_sol;
  pid_t pid_fils;
  FILE * file_scilab;
  
  write_file_scilab(cqp->new_q, cqp->n);
  system("scilab -nw -f ../scilab/spectre.sce");
    
  file_scilab = fopen(spectre,"r");
  fscanf(file_scilab,"%lf",&cqp->new_lambda_min);
  fclose(file_scilab);
  cqp->new_lambda_min_print=cqp->new_lambda_min;
  if (Negative_LM(cqp->new_lambda_min))
    cqp->new_lambda_min=  cqp->new_lambda_min - EPS_LM;
  else
    cqp->new_lambda_min= - EPS_LM;    
}

/*****setqpproblemdata_p_miqcrq()*****/
int setqpproblemdata_p_miqcrq (struct miqcp_bab bab, char **probname_p, int *numcols_p, int *numrows_p, int *objsen_p, double **obj_p, double **rhs_p, char **sense_p, int **matbeg_p, int **matcnt_p, int **matind_p, double **matval_p, int **qmatbeg_p, int **qmatcnt_p, int **qmatind_p, double **qmatval_p, double **lb_p, double **ub_p, char ** ctype_p)
{
  char     *zprobname = NULL;     /* Problem name <= 16 characters */        
  double   *zobj = NULL;
  double   *zrhs = NULL;
  char     *zsense = NULL;
  int      *zmatbeg = NULL;
  int      *zmatcnt = NULL;
  int      *zmatind = NULL;
  double   *zmatval = NULL;
  double   *zlb = NULL;
  double   *zub = NULL;
  int      *zqmatbeg = NULL;
  int      *zqmatcnt = NULL;
  int      *zqmatind = NULL;
  double   *zqmatval = NULL;
  int      status = 0;
  char *zctype = NULL;
   
  int i,j,k,l,s,lim,lim_bis,lim_ter, s_bis;

  double * new_q;

  zprobname = alloc_string(16); 
  
  /***********************************************************************************/
  /*********************************** Variables *************************************/
  /***********************************************************************************/
  
  zmatbeg   = alloc_vector(cqp->nb_col);   
  zmatcnt   = alloc_vector(cqp->nb_col); 

 
  /* x */
  zmatbeg[0]= 0;
  for(i=1;i<=cqp->ind_max_x;i++)
    zmatbeg[i]= zmatbeg[i-1] + cqp->x_cont[i-1]+ cqp->m+cqp->p +cqp->mq +cqp->pq;

    
  /* y  */
  for(j=0;j<cqp->ind_max_x;j++)
    for(k=j;k<cqp->ind_max_x;k++)
      {
	if (!Zero_BB(cqp->nb_var_y[ij2k(j,k,cqp->n)]))// && !(j==cqp->ind_max_x-1 && k==cqp->ind_max_x-1))
	  {
	    zmatbeg[i]= zmatbeg[i-1]; 
	    if ((j==k) && (j<cqp->nb_int))
	      zmatbeg[i]= zmatbeg[i] + 4;
	    else
	      if (j==k)
		zmatbeg[i]= zmatbeg[i] +3;
	      else
		zmatbeg[i]= zmatbeg[i] + 4;

	    if (!Zero_BB(cqp->mq))
	       zmatbeg[i]= zmatbeg[i] + cqp->mq;
	    if (!Zero_BB(cqp->pq))
	      zmatbeg[i]= zmatbeg[i] + cqp->pq;
	    i++;
	  }
      }
 
  int numnz;
  /* if (cqp->n==cqp->nb_int)   */
  /*   numnz = zmatbeg[cqp->nb_col-1]  + 4 + cqp->mq+ cqp->pq; */

  /* else */
  /*   numnz = zmatbeg[cqp->nb_col-1]  + 4  + cqp->mq+  cqp->pq; */
 
  
  /* for(i=0;i<cqp->nb_col-1;i++) */
  /*   zmatcnt[i] = zmatbeg[i+1]- zmatbeg[i]; */
  
  

  /* zmatcnt[cqp->nb_col - 1] = numnz - zmatbeg[cqp->nb_col-1]; */

  /* + 1 for ocnstant term*/
  numnz = zmatbeg[cqp->nb_col-1] ;
  
  for(i=0;i<cqp->nb_col-1;i++)
    zmatcnt[i] = zmatbeg[i+1]- zmatbeg[i];
  zmatcnt[cqp->nb_col - 1] = 0;
  

  
  zmatind   = alloc_vector(numnz);    
  zmatval   = alloc_vector_d(numnz);

  s=0;
   
  
  /***********************************************************************************/
  /*********************************** Constraints ***********************************/
  /***********************************************************************************/
   
  /* Contraints on x in order x_1 x_2 ... x_n */
  for(i=0;i<cqp->ind_max_x;i++)
    {
      
      /*Ax = b*/
      for(j= 0;j<cqp->ind_eg;j++)
	{ 
	  zmatval[s]=cqp->a[ij2k(j,i,cqp->n)];
	  zmatind[s]=cqp->cont[j];
	  s++;
	}
       
      /* Dx =e*/
      for(j=cqp->ind_eg;j<cqp->ind_ineg;j++)
	{
	  zmatval[s]=cqp->d[ij2k(j-cqp->ind_eg,i,cqp->n)];
	  zmatind[s]=cqp->cont[j];
	  s++;
	}
      
      /*Aq,y + Aq_0 x = bq*/
      for(j=cqp->ind_ineg;j<cqp->ind_eg_q;j++)
      	{
      	  zmatval[s]=2*cqp->aq[ijk2l(j-cqp->ind_ineg,0,i+1,cqp->n+1,cqp->n+1)];
      	  zmatind[s]=j;
      	  s++;
      	}
      
      /*Dq,y + Dq_0 x <= eq*/
      for(j=cqp->ind_eg_q;j<cqp->ind_ineg_q;j++)
	{
	  zmatval[s]=2*cqp->dq[ijk2l(j- cqp->ind_eg_q,0,i+1,cqp->n+1,cqp->n+1)];
	  zmatind[s]=j;
	  s++;
	} 
        
      /* y_ij <= l_ix_j + u_jx_i - l_iu_j*/
      for (j=0;j<cqp->ind_max_x;j++)
	if ((!Zero_BB(cqp->nb_var_y[ij2k(i,j,cqp->n)])) || (!Zero_BB(cqp->nb_var_y[ij2k(j,i,cqp->n)])))
	  {
	    if(j>i)
	      { 
		zmatind[s]= cqp->cont[cqp->ind_ineg_q + i* (2*(cqp->n) -i +1)/2 + j-i];
		zmatval[s]=-(double)bab.u[j];
		s++;
	      }
	    if(i>j)
	      { 
		zmatind[s]=cqp->cont[cqp->ind_ineg_q + j* (2*(cqp->n) -j +1)/2  + i-j];
		zmatval[s]=-(double)bab.l[j];
		s++;
	      } 
	    if(i==j)
	      {
		zmatind[s]=cqp->cont[cqp->ind_ineg_q + j* (2*(cqp->n) -j +1)/2  + i-j];
		zmatval[s]=-((double)bab.l[i]+(double)bab.u[j]);
		s++;
	      }
	  }
    
	        
      /* y_ij <= l_jx_i + u_ix_j - l_ju_j*/
      for (j=0;j<cqp->ind_max_x;j++)
	if ((!Zero_BB(cqp->nb_var_y[ij2k(i,j,cqp->n)]))|| (!Zero_BB(cqp->nb_var_y[ij2k(j,i,cqp->n)])))
	  {
	  if(j>i)
	    { 
	      zmatind[s]= cqp->cont[cqp->ind_y_1 + i* (2*(cqp->n) - i -1)/2 + j-i-1];
	      zmatval[s]=-(double)bab.l[j];
	      s++;
	    }
	  if(i>j)
	    { 
	      zmatind[s]=cqp->cont[cqp->ind_y_1 + j* (2*(cqp->n) -j+ -1)/2  + i-j-1];
	      zmatval[s]=-(double)bab.u[j];
	      s++;
	    } 
	  }
      
      /* y_ij >= u_ix_j + u_jx_i - u_iu_j*/
      for (j=0;j<cqp->ind_max_x;j++)
	if ((!Zero_BB(cqp->nb_var_y[ij2k(i,j,cqp->n)]))|| (!Zero_BB(cqp->nb_var_y[ij2k(j,i,cqp->n)])))
	  {
	    if(j>i)
	      { 
		zmatind[s] =cqp->cont[cqp->ind_y_2+ i* (2*(cqp->n) -i +1)/2 + j-i];
		zmatval[s]=(double)bab.u[j]; 
		s++;
	      }
	    if(i>j)
	      { 
		zmatind[s]=cqp->cont[cqp->ind_y_2 + j* (2*(cqp->n) -j +1)/2  + i-j];
		zmatval[s]=(double)bab.u[j];
	      s++;
	      }   
	    if(i==j)
	      {
		zmatind[s]=cqp->cont[cqp->ind_y_2  + j* (2*(cqp->n) -j +1)/2];
		zmatval[s]=2*(double)bab.u[i];
		s++;
	      }
	  }
      
      /* y_ij >= l_ix_j + l_jx_i - l_il_j*/
      for (j=0;j<cqp->ind_max_x;j++)
	if ((!Zero_BB(cqp->nb_var_y[ij2k(i,j,cqp->n)]))|| (!Zero_BB(cqp->nb_var_y[ij2k(j,i,cqp->n)])))
	  {
	    if(j>i)
	      { 
		zmatind[s] =cqp->cont[cqp->ind_y_3+ i* (2*(cqp->n) -i +1)/2 + j-i];
		zmatval[s]=(double)bab.l[j]; 
		s++;
	      }
	    if(i>j)
	    { 
	      zmatind[s]=cqp->cont[cqp->ind_y_3 + j* (2*(cqp->n) -j +1)/2  + i-j];
	      zmatval[s]=(double)bab.l[j];
	      s++;
	    }   
	    if(i==j)
	      {
		zmatind[s]=cqp->cont[cqp->ind_y_3  + j* (2*(cqp->n) -j +1)/2];
		zmatval[s]=2*(double)bab.l[i];
		s++;
	      }
	  }
      
      /* y_ii >= x_i*/
      if ((!Zero_BB(cqp->nb_var_y[ij2k(i,i,cqp->n)])) && (i < cqp->nb_int))
	{
	  zmatind[s]=cqp->cont[cqp->ind_y_4 + i];
	  zmatval[s]=1;
	  s++;
	}
    }

      
  /* Constraints on y in order y_11 y_12 .. y_1n , ... ,y_n1 .. y_nn */   
  
  for(i=0;i<cqp->ind_max_x;i++)
    for(j=i;j<cqp->ind_max_x;j++)
      if (!Zero_BB(cqp->nb_var_y[ij2k(i,j,cqp->n)]))
	{
	  
	  /*Aq,y + Aq_0 x = bq*/
	  for(k=cqp->ind_ineg;k<cqp->ind_eg_q;k++)
	    {
	      if (i==j)
	  	{
	  	  zmatval[s]=cqp->aq[ijk2l(k-cqp->ind_ineg,i+1,j+1,cqp->n+1,cqp->n+1)];
	  	  zmatind[s]=k;
	  	  s++;
	  	}
	      else
	  	{
	  	  zmatval[s]=2*cqp->aq[ijk2l(k-cqp->ind_ineg,i+1,j+1,cqp->n+1,cqp->n+1)];
	  	  zmatind[s]=k;
	  	  s++;
	  	}
	    }
	  
	  /* Dq,y + Dq_0 x <=eq*/
	  for(k=cqp->ind_eg_q;k<cqp->ind_ineg_q;k++)
	    {
	      if (i==j)
		{
		  zmatval[s]=cqp->dq[ijk2l(k-cqp->ind_eg_q,i+1,j+1,cqp->n+1,cqp->n+1)];
		  zmatind[s]=k;
		  s++;
		}
	      else
		{
		  zmatval[s]=2*cqp->dq[ijk2l(k-cqp->ind_eg_q,i+1,j+1,cqp->n+1,cqp->n+1)];
		  zmatind[s]=k;
		  s++;
		}
	    }

 
	  if(i==j)
	    {	  
	      /*y_ii <= l_ix_i+u_ix_i - l_iu_i*/ 
	      zmatind[s] = cqp->cont[cqp->ind_ineg_q + i* (2*(cqp->n) -i +1)/2];
	      zmatval[s]=1; 
	      s++;   
	      
	      /*y_ii >= 2u_ix_i - u_i^2*/ 
	      zmatind[s] =cqp->cont[cqp->ind_y_2 + i* (2*(cqp->n) -i +1)/2];
	      zmatval[s]=-1; 
	      s++;   
	      
	      /*y_ii >= 2l_ix_i - l_i^2*/ 
	      zmatind[s] =cqp->cont[cqp->ind_y_3 + i* (2*(cqp->n) -i +1)/2];
	      zmatval[s]=-1; 
	      s++;   
	      
	      if(i<cqp->nb_int)
		{
		  /*y_ii >= x_i*/ 
		  zmatind[s] =cqp->cont[cqp->ind_y_4+i];
		  zmatval[s]=-1; 
		  s++;
		}
	    }
	
	  if(j>i)
	    {	  
	      /* y_ij <= l_ix_j + u_jx_i - l_iu_j*/
	      zmatind[s] = cqp->cont[cqp->ind_ineg_q + i* (2*(cqp->n) -i +1)/2 + j-i];
	      zmatval[s]=1; 
	      s++;   
	      
	      /* y_ij <= u_ix_j + l_jx_i - u_il_j*/
	      zmatind[s] =cqp->cont[cqp->ind_y_1+ i* (2*(cqp->n) -i -1)/2 + j-i-1];
	      zmatval[s]=1; 
	      s++;   
	      
	      /*y_ij >= u_ix_j + u_jx_i - u_iu_j*/ 
	      zmatind[s] =cqp->cont[cqp->ind_y_2+ i* (2*(cqp->n) -i +1)/2 + j-i];
	      zmatval[s]=-1; 
	      s++;
	      
	      /*y_ij >= l_ix_j + l_jx_i - l_il_j*/ 
	      zmatind[s] =cqp->cont[cqp->ind_y_3+ i* (2*(cqp->n) -i +1)/2 + j-i];
	      zmatval[s]=-1; 
	      s++;
	      
	    }
	}
 
  
  /* if (DEBUG == 1) */
  /*   { */
      
  /*     printf("\ncqp->cont\n"); */
  /*     print_vec(cqp->cont,cqp->nrow); */
  /*     printf("\n"); */
  /*     printf("\ncqp->nb_var_y\n"); */
  /*     print_mat(cqp->nb_var_y,cqp->n,cqp->n); */
  /*     printf("\n"); */
  /*     printf("\nzmatbeg\n"); */
  /*     print_vec(zmatbeg,cqp->nb_col); */
  /*     printf("\n"); */
  /*     printf("\nzmatcnt\n"); */
  /*     print_vec(zmatcnt,cqp->nb_col); */
  /*     printf("\n"); */
      
  /*     printf("\nzmatind\n"); */
  /*     print_vec(zmatind,numnz); */
  /*     printf("\n"); */
  /*     printf("\nzmatval\n"); */
  /*     print_vec_d(zmatval,numnz); */
  /*     printf("\n"); */
  /*   } */
   /* Second member of constrains */
   zrhs = alloc_vector_d (cqp->nb_row);
  i=0;
   
  /* Ax = b */
  for(j=0;j<cqp->m;j++)
    {
      zrhs[i]=cqp->b[j];
      i++;
    }
  /* Dx <= e*/
  for(j=0;j<cqp->p;j++)
    {
      zrhs[i]=cqp->e[j];
      i++;
    }
   
  /*Aq,y + Aq_0 x = bq*/
  for(j=0;j<cqp->mq;j++)
    {
      zrhs[i]=cqp->bq[j];
      i++;
    }
  
  /*Dq,y + Dq_0 x <= eq*/  
  for(j=0;j<cqp->pq;j++)
    {
      zrhs[i]=cqp->eq[j];
      i++;
    }

  
  for(j=0;j<cqp->ind_max_x;j++)
    for(k=j;k<cqp->ind_max_x;k++)
      if (!Zero_BB(cqp->nb_var_y[ij2k(j,k,cqp->n)]))
	{	  
	  zrhs[i]=-(double)bab.u[k] *(double)bab.l[j] ;
	  i++;
	}
  
  for(j=0;j<cqp->ind_max_x;j++)
    for(k=j+1;k<cqp->ind_max_x;k++)
      if (!Zero_BB(cqp->nb_var_y[ij2k(j,k,cqp->n)]))
	{	  
	  zrhs[i]=-(double)bab.u[j] *(double)bab.l[k] ;
	  i++;
	}
   
  for(j=0;j<cqp->ind_max_x;j++)
    for(k=j;k<cqp->ind_max_x;k++)
      if (!Zero_BB(cqp->nb_var_y[ij2k(j,k,cqp->n)]))
	{	  
	  zrhs[i]=(double)bab.u[k] *(double)bab.u[j] ;
	  i++;
	}
      
  for(j=0;j<cqp->ind_max_x;j++)
    for(k=j;k<cqp->ind_max_x;k++)
      if (!Zero_BB(cqp->nb_var_y[ij2k(j,k,cqp->n)]))
	{	  
	  zrhs[i]=(double)bab.l[k] *(double)bab.l[j] ;
	  i++;
	}
  
  for(j=0;j<cqp->ind_max_x;j++)
    if (!Zero_BB(cqp->nb_var_y[ij2k(j,j,cqp->n)]) && (j<cqp->nb_int))
      {	  
	zrhs[i]=0;
	i++;
      }
  
  /* Sense of constraints */
  zsense    = alloc_string(cqp->nb_row);
   
  for(i=0;i<cqp->ind_eg;i++)
    zsense[i]= 'E';
  for(i;i<cqp->ind_ineg;i++)
    zsense[i]= 'L';
  for(i;i<cqp->ind_eg_q;i++)
    zsense[i]= 'E';
  for(;i<cqp->nb_row;i++)
    zsense[i]= 'L';
 
   
  /***********************************************************************************/
  /*********************************** Bounds*****************************************/
  /***********************************************************************************/
  zlb= alloc_vector_d(cqp->nb_col);
  for(i=0;i<cqp->nb_col-1;i++)
    zlb[i]=(double)bab.l[i];
  zlb[cqp->nb_col-1] = cqp->cons;
  
  zub = alloc_vector_d(cqp->nb_col);
  for(i=0;i<cqp->nb_col-1;i++)
    zub[i]=(double)bab.u[i];
  zub[cqp->nb_col-1] = cqp->cons;
  /**************************************************************************************/
  /******************************* Objective function ***********************************/
  /**************************************************************************************/
   
   
  /* Be sure that the new matrix is SDP, compute new lambda_min and add it to diagonal terms*/
  
 
  
  zobj = alloc_vector_d (cqp->nb_col);
  for(i=0;i<cqp->nb_col;i++)
    {
      if(i<cqp->n)
	zobj[i]= cqp->c[i];
      else
	zobj[i]=0;
    }

   
  i=cqp->ind_max_x;
  for(j=0;j<cqp->ind_max_x;j++)
    for(k=j;k<cqp->ind_max_x ;k++)
      if (!Zero_BB(cqp->nb_var_y[ij2k(j,k,cqp->n)])){
	if (j==k) 
	  {
	    zobj[i]= cqp->q[ij2k(j,k,cqp->n)] - cqp->new_q[ij2k(j,k,cqp->n)]/2 + cqp->new_lambda_min; 
	    i++;
	  }
	else
	  {
	    zobj[i]= 2*(cqp->q[ij2k(j,k,cqp->n)] - (cqp->new_q[ij2k(j,k,cqp->n)])/2);
	    i++;
	  }
      }
  
  zobj[cqp->nb_col-1]=1;

  int numqnz = nb_non_zero_matrix(cqp->new_q, cqp->n,cqp->n);

  zqmatbeg  = alloc_vector(cqp->nb_col);
  zqmatbeg[0]=0;
  for (i=1;i<=cqp->ind_max_x;i++)
    zqmatbeg[i]=zqmatbeg[i-1]+ cqp->ind_max_x;
   
  for (i;i<cqp->nb_col;i++)
    zqmatbeg[i]=zqmatbeg[cqp->ind_max_x];
   
  zqmatcnt   = alloc_vector(cqp->nb_col);
  for (i=0;i<cqp->ind_max_x;i++)
    zqmatcnt[i] = zqmatbeg[i+1] - zqmatbeg[i];
   
  for (i;i<cqp->nb_col;i++)
    zqmatcnt[i]=0; 

  zqmatind   = alloc_vector(cqp->ind_max_x*cqp->ind_max_x);
  zqmatval   = alloc_vector_d(cqp->ind_max_x*cqp->ind_max_x);
   
  k=0;
  for(i=0;i<cqp->ind_max_x;i++)
    for(j=0;j<cqp->ind_max_x;j++)
      { 
	if (i==j)
	  zqmatval[k] = cqp->new_q[ij2k(i,j,cqp->n)] - ((cqp->new_lambda_min)*2) ;
	else
	  zqmatval[k] = cqp->new_q[ij2k(i,j,cqp->n)];
	zqmatind[k] = j;
	k++;
      }
  
  /* printf("\nzqmatval\n"); */
  /* print_mat_d(zqmatval,cqp->n,cqp->n); */
  /* printf("\n"); */
  /* printf("\nzqmatind\n"); */
  /* print_mat(zqmatind,cqp->n,cqp->n); */
  /* printf("\n"); */
  /* printf("\nzqmatbeg\n"); */
  /* print_vec(zqmatbeg,cqp->nb_col); */
  /* printf("\n"); */
  /* printf("\nzqmatcnt\n"); */
  /* print_vec(zqmatcnt,cqp->nb_col); */
  /* printf("\n"); */
  /**************************************************************************************/
  /******************************** Type of variables ***********************************/
  /**************************************************************************************/
   
  zctype = alloc_string(cqp->nb_col); 
   
  for(i=0;i<cqp->nb_int;i++)
    if (_5BRANCHES==1)
      zctype[i]= 'I';
    else
      zctype[i]= 'C';
  
  for(i;i<cqp->nb_col;i++)
    zctype[i]= 'C';
   
  strcpy (zprobname, "p_miqcrq");
   
  *numcols_p   = cqp->nb_col;
  *numrows_p   = cqp->nb_row;
  *objsen_p    = CPX_MIN;   /* The problem is minimization */
   
  *probname_p  = zprobname;
  *obj_p       = zobj;
  *rhs_p       = zrhs;
  *sense_p     = zsense;
  *matbeg_p    = zmatbeg;
  *matcnt_p    = zmatcnt;
  *matind_p    = zmatind;
  *matval_p    = zmatval;
  *qmatbeg_p    = zqmatbeg;
  *qmatcnt_p    = zqmatcnt;
  *qmatind_p    = zqmatind;
  *qmatval_p    = zqmatval;
  *lb_p        = zlb;
  *ub_p        = zub;
  *ctype_p = zctype; 


  return (status);
   
}  /* END setproblemdata */

void p_miqcrq( struct miqcp_bab bab, FILE * file_save)
{
  char     *probname = NULL;  
  int      numcols;
  int      numrows;
  int      objsen;
  double   *obj = NULL;
  double   *rhs = NULL;
  char     *sense = NULL;
  int      *matbeg = NULL;
  int      *matcnt = NULL;
  int      *matind = NULL;
  double   *matval = NULL;
  double   *lb = NULL;
  double   *ub = NULL;
  char     *ctype = NULL;
  int      *qmatbeg = NULL;
  int      *qmatcnt = NULL;
  int      *qmatind = NULL;
  double   *qmatval = NULL;
  
  int      solstat;
  double   sol_admissible;
   
  double fx;
 
  
  nb_int_node++;

  double * x_y = alloc_vector_d (cqp->nb_col);

  CPXENVptr    env = NULL;
  CPXLPptr      lp = NULL;
  int           status;
   
  int           cur_numrows, cur_numcols;

  /* Initialize the CPLEX environment */
  int k,l,temp;
     
  int *i =  alloc_vector(1);
  int *j = alloc_vector(1);

  struct miqcp_bab new_bab; 

  int test_time = time(NULL);
  if (test_time - start_time > TIME_LIMIT_BB)
    {
      printf("time limit exeeded");
      return;
    }
   
  /* load program into cplex*/
  env = CPXopenCPLEX (&status);
  char  errmsg[1024];
  if ( env == NULL ) 
    {
      fprintf (stderr, "Could not open CPLEX environment.\n");
      CPXgeterrorstring (env, status, errmsg);
      fprintf (stderr, "%s", errmsg);
      return;
    }
      
  status = CPXsetdblparam (env, CPX_PARAM_EPGAP,REL_GAP);
  if ( status ) 
    {
      printf ("Failure to change relative gap, error %d.\n", status);
      return;
    }

  status = CPXsetdblparam (env, CPX_PARAM_EPAGAP,ABS_GAP);
  if ( status ) 
    {
      printf ("Failure to change absolute gap, error %d.\n", status);
      return;
    }

  status = CPXsetdblparam (env, CPX_PARAM_TILIM,TIME_LIMIT_CPLEX);
  if ( status ) 
    {
      printf ("Failure to change time limit, error %d.\n", status);
      return;
    }
  
  status = CPXsetintparam (env, CPX_PARAM_NODEFILEIND,2);
  if ( status )
    {
      printf ("Failure to change node storage, error %d.\n", status);
      return;
    }

  /* Set memory parameter*/
  status = CPXsetdblparam (env, CPX_PARAM_WORKMEM,MEMORY);
  if ( status )
    {
      printf ("Failure to change memory, error %d.\n", status);
      return;
    }

  /* Set threads parameter*/
  status = CPXsetintparam (env, CPX_PARAM_THREADS,THREAD);
  if ( status )
    {
      printf ("Failure to change thread, error %d.\n", status);
      return;
    }
  /* Fill in the data for the problem.  */
  
  /*var selection strategie*/
  status = CPXsetintparam (env, CPX_PARAM_VARSEL, VARSEL);
  if ( status ) 
    {
      printf ("Failure to change var selection strategie, error %d.\n", status);
      return;
    }

   status = CPXsetintparam (env, 4012, 0);
  if ( status )
    {
      printf ("Failure to turn on screen indicator, error %d.\n", status);
      return;
    }


  status = setqpproblemdata_p_miqcrq (bab,&probname, &numcols, &numrows, &objsen, &obj, 
				     &rhs, &sense, &matbeg, &matcnt, &matind, &matval,
				     &qmatbeg, &qmatcnt, &qmatind, &qmatval, 
				     &lb, &ub, &ctype);
   
  if ( status ) 
    {
      printf ( "Failed to build problem data arrays.\n");
      return;
    }

  /* Create the problem. */
  lp = CPXcreateprob (env, &status, probname);

  if ( lp == NULL ) 
    {
      printf ("Failed to create LP.\n");
      return;
    }
   
  /* Now copy the problem data into the lp */
  status = CPXcopylp (env, lp, numcols, numrows, objsen, obj, rhs, 
		      sense, matbeg, matcnt, matind, matval,
		      lb, ub, NULL);
   
  if ( status )
    {
      printf ( "Failed to copy problem data.\n");
      return ;
    }

  /* Now copy the ctype array */
  status = CPXcopyctype (env, lp, ctype);
  if ( status )
    {
      printf ( "Failed to copy ctype\n");
      return;
    }

  status = CPXcopyquad (env, lp, qmatbeg, qmatcnt, qmatind, qmatval);
  if ( status ) 
    {
      printf ("Failed to copy quadratic matrix.\n");
      return;
    }
  
  /* /\*write  a copy of the problem*\/ */
  /*  status = CPXwriteprob (env, lp, p_miqcrq_lp, NULL); */
 
  /* Optimize the problem and obtain solution. */
  status = CPXmipopt (env, lp);
  if ( status ) 
    {
      printf ( "Failed to optimize MIQP.\n");
      return;
    }
    
  solstat = CPXgetstat (env, lp);

  /* Write the output to the screen. */
  status = CPXgetmipobjval (env, lp, &sol_admissible);
  if ( status ) 
    {
      printf ("No MIP objective value available.  Exiting...\n");
      return;
    }
   
  
  cur_numrows = CPXgetnumrows (env, lp);
  cur_numcols = CPXgetnumcols (env, lp);

  status = CPXgetmipx (env, lp, x_y, 0, cur_numcols-1);
  if ( status ) 
    {
      printf ( "Failed to get optimal integer x.\n");
      return;
    }
   
  
  /* if (feasible_for_constraints(x_y,cqp)) */
  /*   { */
  /*     fx =sum_ij_qi_qj_x_ij(cqp->q, x_y, cqp->n); */
  /*     fx = fx + sum_i_ci_x_i(cqp->c, x_y, cqp->n); */
  /*     status = compare_solution_miqcp(fx,&best_sol_adm_miqcp,x_y, file_save,cqp); */
  /*   } */

   compute_local_sol(cqp, &bab.l, &bab.u,file_save);
      
      if (cqp->local_sol_adm < best_sol_adm_miqcp)
	{
	  best_sol_adm_miqcp = cqp->local_sol_adm ;
	  fprintf(file_save,"\n node %d local solution, update best sol adm : %6lf \n",nb_int_node,best_sol_adm_miqcp);
	  
	}
  
  if (DEBUG==1)
    {
      printf("\n best_sol_adm : %6lf \n feasible for const : %d \n",best_sol_adm_miqcp,feasible_for_constraints(x_y,cqp));
      
    }
  if (Zero_BB(cqp->nb_int)) 
    nodecount = nodecount ;
  else
    nodecount = nodecount+ CPXgetnodecnt (env, lp); 

  if (DEBUG == 1)
    {
      printf ("Integer solution value: %lf \n\n", sol_admissible );
      if (Zero_BB(cqp->nb_int))
	printf ("Number of branch-and-bound nodes: %d \n\n", nb_int_node);
      else
	printf ("Number of branch-and-bound nodes: %d \n\n", nodecount+nb_int_node);
      int m,p,ind=cqp->n;
      for(m=0;m<cqp->n;m++)
	printf("x[%d] : %.2lf ",m+1,x_y[m]);
      printf("\n");
      for(m=0;m<cqp->n;m++)
	{
	  for(p=0;p<cqp->n;p++)
	  if (!Zero_BB(cqp->nb_var_y[ij2k(m,p,cqp->n)]))
	    {
	      printf("y[%d,%d] : %.2lf ",m+1,p+1,x_y[ind]);
	      ind++;
	    }
	  printf("\n");
	}
      printf("\n");
    
      printf("\nl\n");
      print_vec_d(bab.l,bab.n);
      printf("\n");

      printf("\nu\n");
      print_vec_d(bab.u,bab.n);
      printf("\n");      
    }
  
  /* Free up the problem as allocated by CPXcreateprob, if necessary */
  if ( lp != NULL )  
    {
      status = CPXfreeprob (env, &lp);
      if ( status )
	{
	  printf ( "CPXfreeprob failed, error code %d.\n", status);
	  return;
	}
    }
      
  /* Free up the CPLEX environment, if necessary */
  if ( env != NULL ) 
    {
      status = CPXcloseCPLEX (&env);
      if ( status ) 
	{
	  fprintf (stderr, "Could not close CPLEX environment.\n");
	  CPXgeterrorstring (env, status, errmsg);
	  fprintf (stderr, "%s", errmsg);
	  return;
	}
    }
   
  /* Free up the problem data arrays, if necessary. */
  free_and_null ((char **) &probname);
  free_and_null ((char **) &obj);
  free_and_null ((char **) &rhs);
  free_and_null ((char **) &sense);
  free_and_null ((char **) &matbeg);
  free_and_null ((char **) &matcnt);
  free_and_null ((char **) &matind);
  free_and_null ((char **) &matval);
  free_and_null ((char **) &lb);
  free_and_null ((char **) &ub);
  free_and_null ((char **) &ctype);

 
  int is_not_feasible = select_i_j_miqcrq(x_y, bab , i, j,cqp);

  if (!is_not_feasible && compare_solution_miqcp(sol_admissible,&best_sol_adm_miqcp,x_y, file_save,cqp))
    if (Zero(cqp->nb_int))
      fprintf (file_save,"Number of branch-and-bound nodes: %d \n\n", nb_int_node);
    else
      fprintf (file_save,"Number of branch-and-bound nodes: %d \n\n", nb_int_node+nodecount);


  
  
 if (DEBUG==1)
    {
      printf("\n best_sol_adm : %6lf \n",best_sol_adm_miqcp);
      printf("\n sol_cour : %6lf \n",sol_admissible);
    }

 if (sol_admissible > best_sol_adm_miqcp - EPS_BB)
    {
      if (DEBUG==1)
	printf("\n\n on coupe (best sol_adm)\n\n");
      return;
    }

  

  if ((TYPE_X=='I') || (TYPE_Y=='I') || (TYPE_T=='B'))
    {
      if(is_not_feasible)
	if ((*i == *j) && (cqp->nb_int > *i)  && (*j > cqp->nb_int))
	  {
	    new_bab = update_variables_int_branch_1_diag(bab, x_y, i,j,cqp);
	    if (test_bound_miqcp(new_bab,cqp->nb_col))
	      p_miqcrq(new_bab,file_save);
	    
	    new_bab = update_variables_int_branch_4(bab, x_y, i,j,cqp);
	    if (test_bound_miqcp(new_bab,cqp->nb_col))
	      p_miqcrq(new_bab,file_save);
	    
	    new_bab = update_variables_int_branch_5(bab, x_y,i,j,cqp);
	    if (test_bound_miqcp(new_bab,cqp->nb_col))
	      p_miqcrq(new_bab,file_save);
	  }
	else
	  {
	    if ((cqp->nb_int > *i)  && (cqp->nb_int > *j))
	      {
		new_bab = update_variables_int_branch_1(bab, x_y, i,j,cqp);
		if (test_bound_miqcp(new_bab,cqp->nb_col))
		  p_miqcrq(new_bab,file_save);
		
		new_bab = update_variables_int_branch_2(bab, x_y, i,j,cqp);
		if (test_bound_miqcp(new_bab,cqp->nb_col))
		  p_miqcrq(new_bab,file_save);
		
		new_bab = update_variables_int_branch_3(bab, x_y,i,j,cqp);
		if (test_bound_miqcp(new_bab,cqp->nb_col))
		  p_miqcrq(new_bab,file_save);
		
		new_bab = update_variables_int_branch_4(bab, x_y,i,j,cqp);
		if (test_bound_miqcp(new_bab,cqp->nb_col))
		  p_miqcrq(new_bab,file_save);
		
		new_bab = update_variables_int_branch_5(bab, x_y,i,j,cqp);
		if (test_bound_miqcp(new_bab,cqp->nb_col))
		  p_miqcrq(new_bab,file_save);
	      }
	    else
	      if (((cqp->nb_int > *i)  && (cqp->nb_int < *j)) || ((cqp->nb_int < *i)  && (cqp->nb_int > *j)))
		{
		  new_bab = update_variables_mixed_branch_1(bab, x_y, i,j,cqp);
		  if (test_bound_miqcp(new_bab,cqp->nb_col))
		    p_miqcrq(new_bab,file_save);
		  
		  new_bab = update_variables_mixed_branch_2(bab, x_y, i,j,cqp);
		  if (test_bound_miqcp(new_bab,cqp->nb_col))
		    p_miqcrq(new_bab,file_save);
		  
		  new_bab = update_variables_mixed_branch_3(bab, x_y, i,j,cqp);
		  if (test_bound_miqcp(new_bab,cqp->nb_col))
		    p_miqcrq(new_bab,file_save);
		}
	      else
		if (*i==*j)
		  {
		      
		    new_bab = update_variables_cont_branch_1_diag(bab, x_y, i,j,cqp);
		    if (test_bound_miqcp(new_bab,cqp->nb_col))
		      p_miqcrq(new_bab,file_save);
		
		    new_bab = update_variables_cont_branch_4(bab, x_y, i,j,cqp);
		    if (test_bound_miqcp(new_bab,cqp->nb_col))
		      p_miqcrq(new_bab,file_save);
		    
		    new_bab = update_variables_cont_branch_5(bab, x_y,i,j,cqp);
		    if (test_bound_miqcp(new_bab,cqp->nb_col))
		      p_miqcrq(new_bab,file_save);

		   
		  }
		else
		  {
		    new_bab = update_variables_cont_branch_1(bab, x_y, i,j,cqp);
		    if (test_bound_miqcp(new_bab,cqp->nb_col))
		      p_miqcrq(new_bab,file_save);
		    
		    new_bab = update_variables_cont_branch_2(bab, x_y, i,j,cqp);
		    if (test_bound_miqcp(new_bab,cqp->nb_col))
		      p_miqcrq(new_bab,file_save);
		    
		    new_bab = update_variables_cont_branch_3(bab, x_y,i,j,cqp);
		    if (test_bound_miqcp(new_bab,cqp->nb_col))
		      p_miqcrq(new_bab,file_save);
		    
		    new_bab = update_variables_cont_branch_4(bab, x_y,i,j,cqp);
		    if (test_bound_miqcp(new_bab,cqp->nb_col))
		      p_miqcrq(new_bab,file_save);
		    
		    new_bab = update_variables_cont_branch_5(bab, x_y,i,j,cqp);
		    if (test_bound_miqcp(new_bab,cqp->nb_col))
		      p_miqcrq(new_bab,file_save);

		  
		 
		  }
	  }
    }
}

  


