/* -*-c-*-
 *
 *    Source     $RCSfile: smiqcp.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

#include<config.h>

#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<sys/timeb.h>

#include<getopt.h>
#include<assert.h>

#include<unistd.h>
#include<fcntl.h>

#include<ilcplex/cplex.h>

#include"quad_prog.h"
#include"liste.h"
#include"utilities.h"
#include"in_out.h"
#include"in_out_ampl.h"
#include"in_out_csdp.h"
#include"in_out_sb.h"
#include"in_out_scilab.h"
#include"obbt.h"

/******************** Branch & Bound */
#include"p_miqcrq.h"
#include"s_miqcrq.h"
#include"s_miqcrq_bb.h"
#include"local_sol.h"
#include"local_sol_ipopt.h"
/******************** Solver SDP */
#include"solver_sdp.h"
#include"solver_sdp_mixed.h"
#include "cb_cinterface.h"
#include "declarations.h"

/******************** needed fct 1: usage() */
void usage()
{
    printf("Usage: smiqcp_bb -f [sub-options] (read problems in a file with a standard format)\n");
    printf("You can only call one of the options\n\n");
    printf(" Algorithms for solving integer quadratically constrained quadratic programming\n");
    printf(" Convexifications \n");
    printf("             --p_miqcrq (solve the problem by the Mixed Integer Quadratic Convex Reformulation where the quadratic constraints are linearized) + branch-and-bound (5 branches)\n ");
    printf("             --n_miqcrq (solve the problem by the Mixed Integer Quadratic Convex Reformulation where the quadratic constraints are linearized) + branch-and-bound (4 branches)\n ");
    printf("             --s_miqcrq (solve the problem by the Mixed Integer Quadratic Convex Reformulation where the quadratic constraints are linearized) + spatial branch-and-bound\n ");
    printf(" For the sub-options(to seperate by comas):\n");
    printf(" Sub-options for all options:\n");
    printf(" Solver_sdp : CSDP, or CB default CB\n");
    printf("    -g       n: number of total variables\n");
    printf("             n_int: number of integer variables\n");
    printf("             m: number of linear equality constraints\n");
    printf("             p: number of linear inequality constraints\n");
    printf("             mq: number of quadratic equality constraints\n");
    printf("             pq: number of quadratic inequality constraints\n");
    printf("             min_obj : default -100 \n");
    printf("             max_obj: default 100\n");
    printf("             min_cont: default 1\n");
    printf("             max_cont: default 100\n");
    printf("             mult_cont: default 50\n");
    printf("             min_u: default 1\n");
    printf("             max_u: default 20\n");
    printf("             dens: default 1\n");
    printf("             The terms of q and c are choosen randomly between min_obj and max_obj\n");
    printf("             The right terms of constraints are choosen randomly between min_cont and max_cont with a densty dens\n");
    printf("             The terms b_r are equal to the mult_cont *(sum on i of the a_ri)\n");
    printf("             The terms u_i are equal choosen between min_u and max_u\n");
    printf("    -k       n: number of binary variables\n");
    printf("             dens: density of the graph\n");
    printf("             valk: value of k\n");
    printf("    -f       file: name of the file to read\n");
    //printf("    -c       file: name of the max-cut file to read\n");
    printf(" Examples:\n");
    printf("smiqcp_bb -f file_in=\"../saves/instance.dat\",file_out=\"../saves/instance.sol\",solver_sdp=\"csdp\" --p_miqcrq\n");
}

/******************** needed fct 2: init_param() */
void init_param(char * file_param, double local_sol, FILE * file_save, int flag)
{
    FILE * param;
    int prec = 0;
    double epsilon = 1;
    double toto = local_sol - MAX_SOL_BB;

    /* if ((file_param == NULL) && (local_sol == MAX_SOL_BB ))  */
    /*{/*default parameter*/
    fprintf(file_save,"Parameters\n");

    /* parameters for cplex */
    TIME_LIMIT_CPLEX = 3600;
    REL_GAP = 1e-4;
    VARSEL = 4; /*-1 or 1; or 2 or 3 or 4 default 0*/
    THREAD = 48;
    
    /* parameters for SB */
    TIME_LIMIT_SB = alloc_string(10);
    strcpy(TIME_LIMIT_SB, "700");
    EPS_SB = alloc_string(10);
    strcpy(EPS_SB,"1e-4");
    FILE_SB = alloc_string(40);
    strcpy(FILE_SB,"../saves/res.txt");

    /* parameters for CONIC BUNDLE */
    PREC = 1e-6;
    PREC_STEP = 1e-6;
    NB_DESCENT_STEP = 1;

    /* parameters for BB */
    if (flag == 0)
        EPS_BB = 1e-2;
    else
        EPS_BB = 1e-2;
    EPS_BRANCH = 1e-2;
    TIME_LIMIT_BB = 3600;

    /* parameters for CSDP */
    AXTOL= 1e-5;
    AYTOL= 1e-5;
    OBJTOL= 1e-5;
    PINFTOL= 1e3;
    DINFTOL= 1e3;
    MAXITER= 100;
    MINSF= 0.90;
    MAXSF= 0.97;
    MINSTEPP= 1e-5;
    MINSTEPD= 1e-5;
    USEXZGAP= 1;
    TWEAKGAP= 0;
    AFFINE= 0;
    PERTURBOBJ= 0;
    FASTMODE= 1;

    fprintf(file_save,"Parameters for cplex \n TIME_LIMIT_CPLEX= %d s \n REL_GAP : %lf\nTHREAD= %d \n VARSEL= %d\n ABS_GAP= %lf\n OPT_TOL= %lf\n CONV_TOL= %lf\n MEMORY= %d\n BARRIER_TOL= %lf\nOBJDIF=%lf\n EPRHS= %lf\n EPRELAX= %lf\n HEURFREQ= %d\n SYMBREAK %d\n PRSOLVENODE %d\n\nParameters for CONIC BUNDLE \n PREC= %lf\n PREC_STEP= %lf\n NB_DESCENT_STEP= %d \n EPS_BETA= %lf\n EPS_LM= %lf\n EPS_TERM_CB= %lf\n ACT_BOUND= %d\n EVAL_LIMIT =%d\n UPDATE_LIMIT= %d\n FACTOR=%lf\n UPPER_BOUND=%lf\n MAX_SUBG=%d \n MAX_SUBG=%d\n NB_MAX_ITER=%d\nTIME_LIMIT_CB=%d\n Parameters for BB\n EPS_BB= %lf \n EPS_BRANCH= %lf \n TIME_LIMIT_BB= %d \n\nParameters for CSDP \nAXTOL= %lf\n AYTOL=%lf\n OBJTOL= %lf\n PINFTOL= %lf\n DINFTOL= %lf\n MAXITER= %d\n MINSF= %lf\n MAXSF= %lf\n MINSTEPP= %lf\n MINSTEPD= %lf\n USEXZGAP= %d\n TWEAKGAP= %d\n AFFINE= %d\n PERTURBOBJ= %d\n FASTMODE= %d\n\n",TIME_LIMIT_CPLEX,REL_GAP,THREAD,VARSEL,ABS_GAP,OPT_TOL,CONV_TOL,MEMORY,BARRIER_TOL,OBJDIF,EPRHS,EPRELAX,HEURFREQ,SYMBREAK,PRESOLVENODE,PREC,PREC_STEP,NB_DESCENT_STEP,EPS_BETA, EPS_LM, EPS_TERM_CB,ACT_BOUND,EVAL_LIMIT,UPDATE_LIMIT,FACTOR,UPPER_BOUND,MAX_SUBG,MAX_SUBG,NB_MAX_ITER,TIME_LIMIT_CB,EPS_BB,EPS_BRANCH,TIME_LIMIT_BB,AXTOL,AYTOL,OBJTOL,PINFTOL,DINFTOL,MAXITER,MINSF,MAXSF,MINSTEPP,MINSTEPD,USEXZGAP,TWEAKGAP,AFFINE,PERTURBOBJ,FASTMODE);
}// end of init_param()

/******************** needed fct 3: compute_time() */
/*return the time spended between n.a - m.b */
double compute_time(long n, int a, long m, int b)
{
    return n-m + (a-b)/1000;
}

/*********************************************************************************/
/*********************************  MAIN *****************************************/
/*********************************************************************************/
int main (int argc, char **argv)
{
    FILE * fd;
    char *subopts, *value;
    int car,i;
    
    buf_in = alloc_string(200);
    strcpy(buf_in,save_inst);
    buf_out = alloc_string(200);
    strcpy(buf_out,save);

    MIQCP qp = new_miqcp();
    C_MIQCP cqp_obbt;
    MIQCP new_qp;
    SDP psdp;
    Q_MIQCP qqp;

    /*parameters*/
    qp->n=0;
    qp->nb_int=0;
    qp->m=0;
    qp->p=0;
    qp->mq=0;
    qp->pq=0;

    struct miqcp_bab bab;
    struct miqcp_bab mbab;

    double * beta;
    double * beta_1;
    double * beta_2;
    double * beta_3;
    double * beta_4;
    double * beta_c;
    double alpha;
    double * lambda;
    double alphabis;
    double * alphaq;
    double * alphabisq;
    double * delta;
    double * delta_c;
    double delta_l=0;
    double * delta_1;
    double * delta_2;
    double * delta_3;
    double * delta_4;
    double * delta_5;
    double * delta_6;
    double * delta_7;
    double * delta_8;
    double * delta_9;
    double * delta_10;
    double * delta_11;
    double * delta_12;

    double lambda_min;
    double * lambda_min_ineg;
    double * lambda_max_ineg;
    double * lambda_min_eg;
    double * lambda_max_eg;
    double new_lambda_min;
    double new_lambda_min_print;

    double * vspectre;
    int nb_neg;
    double percent;

    double * lower_bound;

    best_sol_adm_miqcp_s_bb = MAX_SOL_BB;
    borne_inf = -MAX_SOL_BB;

    if(argc == 1)
    {
        usage();
        exit(-1);
    }
    
    if (argc >= 2)
      strcpy(buf_in, argv[1]);
    if (argc >= 3)
      strcpy(buf_out, argv[2]);

    DEBUG = 0;
    if (argc >= 4)
      DEBUG = atoi(argv[3]);
    
    FILE * file_save = fopen(buf_out,"a");

    /* Integer or continuous solution and choice of wich variable to branch*/
    /* determine if there is continuous variables*/
    if (qp->nb_int != qp->n)
        qp->miqp = 1;

    /*inf bound*/
    qp->flag_alg = 1;
    
    /* Variables for time, node and sol adm bb*/
    start_time = 0;
    double total_time;
    double end_time = 0;
    double pre_processing_time = 0;

    nb_int_node = 0;
    nodecount = 0;
    //best_sol_adm = 0;
    upper_bound = MAX_SOL_BB;

    /* Check option */
    int file_sol;
    fd = fopen( buf_in, "r" );
    if (fd == NULL)
      {
	printf("erreur ouverture\n");
	usage();
	return 1;
      }
    fclose(fd);
    read_file(qp);

    /*compute_number_neg_eigenvalues(qp,&vspectre,&nb_neg);*/
    int cont_lin = 0;
    int unconstrained = 0;
    
    if ((qp->m>0) && (qp->mq - 1 == 0))
        {
	  if (qp->pq == 0 && qp->p==0)
	    cont_lin = 1;
	  else if ((qp->p>0) && (qp->pq -(2*qp->p*qp->n) ==0))
	    cont_lin = 1;
        }
    else
      {
	if ((qp->p>0) && (qp->pq -(2*qp->p*qp->n) ==0) && ((qp->mq -1 == 0) || qp->mq==0))
	  cont_lin=1;
	if ((qp->m==0) && (qp->p==0) && (qp->mq  == 0) && (qp->pq ==0))
	  unconstrained = 1;
      }
    
    int is_0_1 = 0;
    for (i=0; i<qp->n; i++)
      if (qp->u[i] != 1 && qp->l[i]!=0)
	{
	  is_0_1 =1;
	  break;
	}
    
    /*change bounds of x_i to 0 and 1*/
    if (qp->nb_int==0 && (is_0_1==1))
      {
	fprintf(file_save,"\nPresolve : Change bounds to 0 and 1\n");
	change_bounds(qp);
      }
    
    best_x = alloc_vector_d(qp->n);
    qp_init = copy_miqcp(qp);
    
    if (cont_lin && qp->nb_int == qp->n)
      {
	compute_local_sol_init(qp_init,file_save);
	if ( qp_init->local_sol != NULL)
	  {
	    best_sol_adm_miqcp_s_bb = qp_init->sol_adm ;
	    qp->local_sol = copy_vector_d(qp_init->local_sol,qp->n);
	    qp->sol_adm = qp_init->sol_adm;
	    fprintf(file_save,"\nCompute local sol succeeded \n");
	    fprintf(file_save,"\nLocal solution value : %lf\nSolution :\n",qp_init->sol_adm);
	    for(i=0; i<qp_init->n; i++)
	      fprintf(file_save,"x[%d]=%lf\n",i,qp_init->local_sol[i]);
	    fprintf(file_save,"\n\n");
	  }
      }
    else
      {
	if (qp->nb_int==0)
	  {
	    compute_local_sol_ipopt_init();
	    if ( qp_init->local_sol != NULL)
	      {
		best_sol_adm_miqcp_s_bb = qp_init->sol_adm ;
		qp->local_sol = copy_vector_d(qp_init->local_sol,qp->n);
		qp->sol_adm = qp_init->sol_adm;
		fprintf(file_save,"\nCompute local sol succeeded \n");
		fprintf(file_save,"\nLocal solution value : %lf\nSolution :\n",qp_init->sol_adm);
		for(i=0; i<qp_init->n; i++)
		  fprintf(file_save,"x[%d]=%lf\n",i,qp_init->local_sol[i]);
		fprintf(file_save,"\n\n");
	      }
	  }
	else	  
	  {
	    compute_local_sol_scip_init(qp_init);
	    if ( qp_init->local_sol != NULL)
	      {
		best_sol_adm_miqcp_s_bb= qp_init->sol_adm ;
		qp->local_sol=copy_vector_d(qp_init->local_sol,qp->n);
		qp->sol_adm=qp_init->sol_adm;
		fprintf(file_save,"\nCompute local sol succeeded \n");
		fprintf(file_save,"\nLocal solution value : %lf\nSolution :\n",qp_init->sol_adm);
		for(i=0; i<qp_init->n; i++)
		  fprintf(file_save,"x[%d]=%lf\n",i,qp_init->local_sol[i]);
		fprintf(file_save,"\n\n");
	      }
	  }
      }
    /* end Compute local sol*/


    /*******************************************************************************/
    /********* Algorithms for binary linearly constrained quadratic programs *******/
    /*******************************************************************************/
    fprintf(file_save,"############################################################################\n############################################################################\n\nStart solving.\n\n");
    fprintf(file_save,"Solution by MIQCRQ_BB_BN (spatial)\n\nInstance %d\n\n %d integer variables\n %d continuous variables\n  %d variables\n %d linear equality constraints\n %d linear inequality constraints\n %d quadratic equality constraints\n %d quadratic inequality constraints\n\n",i,qp->nb_int, qp->n - qp->nb_int, qp->n, qp->m,qp->p,qp->mq,qp->pq);
	    
    start_time = time(NULL);
    
    /*******************************************************************************/
    /********* Solve the sdp problem with conic bundle *****************************/
    /*******************************************************************************/
    psdp = create_sdp_mixed(qp);

    lower_bound = alloc_vector_d(qp->n);
    for(i=0; i<qp->n; i++)
      lower_bound[i]=0;
    
    /*start from a local solution*/
    best_sol_adm_miqcp_s_bb = qp_init->sol_adm;
    if (qp_init->local_sol != NULL)
      best_x = copy_vector_d(qp_init->local_sol,qp->n);

    if (qp->n ==qp->nb_int)
      init_param(NULL,best_sol_adm_miqcp_s,file_save,0);
    else
      init_param(NULL,best_sol_adm_miqcp_s,file_save,1);

    compute_alpha_beta_p_miqcrq(qp,&beta,&alphaq,&alphabisq,psdp);
    pre_processing_time = time(NULL);

    cat_save_cb(file_save);
    
    fprintf(file_save,"\nPre-processing time (solver sdp for obbt): %.2lf \n", pre_processing_time - start_time);

    /*******************************************************************************/
    /********* Update data and structures for OBBT and B&B  ************************/
    /*******************************************************************************/

    /*******************************************************************************/
    /************************** Reduce bounds with obbt ****************************/
    /*******************************************************************************/
    int proceed_obbt = 0;
    if ((Positive_BB(qp->m) ||Positive_BB(qp->p) || Positive_BB(qp->mq) || Positive_BB(qp->pq)) && (qp->nb_int==0))
      proceed_obbt = 1;
    int nb_pass_var, nb_pass_cont;

    if (proceed_obbt == 1)
      {
	cqp_obbt = create_c_miqcp(qp,beta,alpha,alphabis,alphaq,alphabisq,lambda_min_ineg,lambda_max_ineg,lambda_min_eg,lambda_max_eg,2);

	compute_new_q_p_miqcrq(cqp_obbt);
	cqp_obbt->new_lambda_min = -0.01;
	fprintf(file_save,"\nStarting Presolve with OBBT and branch and Reduce : nb_pass_obbt_init max : %d\n",NB_PASS_OBBT_INIT);
	presolve_with_obbt(cqp_obbt,&nb_pass_var, &nb_pass_cont, best_sol_adm_miqcp_s_bb,NB_PASS_OBBT_INIT);
	fprintf(file_save,"\n nb iterations for variable bounds reduction : %d\n iterations for constraint bounds reduction : %d\n",nb_pass_var,nb_pass_cont);
        
	pre_processing_time = time(NULL);
	fprintf(file_save,"\nPre-processing time (OBBT): %.2lf \n", pre_processing_time - start_time);
		
	/*************************************************************************************/
	/****************remove all non necessary constraints deduced from obbt***************/
	/*************************************************************************************/
	printf("\n Before update \n cqp_obbt->dq \n");
	print_mat_3d(cqp_obbt->dq, cqp_obbt->pq, cqp_obbt->n+1, cqp_obbt->n+1);
	printf("\n cqp_obbt->eq \n");
	print_vec_d(cqp_obbt->eq, cqp_obbt->pq);
	printf("\n \n");
	printf(" \n qp->dq \n");
	print_mat_3d(qp->dq, qp->pq, qp->n+1, qp->n+1);
	printf("\n qp->eq \n");
	print_vec_d(qp->eq, qp->pq);
	printf("\n \n");

	update_qp(qp,cqp_obbt);

	printf("\n After update \n cqp_obbt->dq \n");
	print_mat_3d(cqp_obbt->dq, cqp_obbt->pq, cqp_obbt->n+1, cqp_obbt->n+1);
	printf("\n cqp_obbt->eq \n");
	print_vec_d(cqp_obbt->eq, cqp_obbt->pq);
	printf("\n \n");

	printf("\n qp->pq : %d \n qp->dq \n", qp->pq);
	print_mat_3d(qp->dq, qp->pq, qp->n+1, qp->n+1);
	printf("\n qp->eq \n");
	print_vec_d(qp->eq, qp->pq);
	printf("\n \n");
		
	/*************************************************************************************/
	/******************  solve again the sdp if necessary  *******************************/
	/**************************************************************************************/
	psdp = create_sdp_mixed(qp);
	compute_alpha_beta_p_miqcrq(qp,&beta,&alphaq,&alphabisq,psdp);
	pre_processing_time = time(NULL);

	if (strcmp(qp->solver_sdp,"cb\0") == 0)
	  cat_save_cb(file_save);
	else
	  cat_save_sdp(file_save);

	fprintf(file_save,"\nPre-processing time (solver sdp for miqcrq): %.2lf \n", pre_processing_time - start_time);
      }
    else
      fprintf(file_save,"\nNo Presolve with OBBT\n");
	    
    /*************************************************************************************/
    /******************  proceed to branch-and-bound  ************************************/
    /**************************************************************************************/
    fprintf(file_save,"\nStarting Branch-and-bound process\n");
    cqp = create_c_miqcp(qp,beta,alpha,alphabis,alphaq,alphabisq,lambda_min_ineg,lambda_max_ineg,lambda_min_eg,lambda_max_eg,2);
    liste = init_liste();
    fprintf(file_save,"number of created variables y : %d\n",cqp->nb_y);
    mbab = create_mbab(qp,cqp,2);

    nodecount = 0;
    compute_new_q_p_miqcrq(cqp);
    compute_new_lambda_min_bb_quad(cqp);
    
    s_miqcrq_bb(mbab,file_save);

    end_time = time(NULL);

    fprintf(file_save,"\nBest lowerbound: %.5lf\n", borne_inf_bb);
    fprintf(file_save,"\nBest feasible solution: %.5lf\n", best_sol_adm_miqcp_s_bb);
    fprintf(file_save,"\nBranch-and-bound time: %.2lf \n", end_time - pre_processing_time);
    fprintf(file_save,"Total time: %.2lf \n",end_time - start_time);
    if (!Zero_BB(qp->nb_int))
      {
	fprintf(file_save,"Number of integer nodes: %d \n",nb_int_node);
	fprintf (file_save,"Total number of nodes: %d \n\n", nodecount+nb_int_node);
      }
    else
      fprintf (file_save,"Total number of nodes: %d \n\n", nb_int_node);
    fprintf(file_save,"Solving complete.\n\n############################################################################\n############################################################################\n\n");
    fclose(file_save);
    return 1;
}
