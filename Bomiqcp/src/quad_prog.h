/* -*-c-*-
 *
 *    Source     $RCSfile: quad_prog.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "Bomiqcp",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#ifndef QUAD_PROG_H_INCLUDED
#define QUAD_PROG_H_INCLUDED

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

/*********************************************************************/
/****************************** Variable *****************************/
/*********************************************************************/

/* structures for miqcp */
struct miqcp
{
    int n;
    int nb_int;
    int m;
    int p;
    int mq;
    int pq;
    int N;
    /* flag_const = 1 if there is a constant term to consider, cons is the value of the constant term*/
    double cons;
    double flag_const;
    /* miqp = 1 if there is continuous variables*/
    int miqp;
    /*flag_alg = 1 => algorithms s_miqcrq or p_miqcrq or s_miqcrq2 or n_miqcrq */
    int flag_alg;
    double *q;
    double *a;
    double *d;
    double *c;
    double *u;
    double *l;
    double *b;
    double * e;
    double * aq;
    double * dq;
    double *bq;
    double *eq;
    int *lg;
    int *lgc;
    char * solver;
    char * solver_sdp;
    double * sol;
    double sol_adm;
    double * local_sol;
};
typedef struct miqcp * MIQCP;

/* structures for q_miqcp */
struct q_miqcp
{
    int n;
    int nb_int;
    int m;
    int p;
    int mq;
    int pq;
    int N;
    int miqp;
    double cons;
    double *q;
    double * new_q;
    double *a;
    double * a_t_a;
    double *d;
    double * d_t_d;
    double *c;
    double *u;
    double *l;
    double *b;
    double * e;
    double * aq;
    double * sum_r_aq_alphaq;
    double * dq;
    double * sum_r_dq_alphabisq;
    double *bq;
    double * sum_r_bq;
    double *eq;
    double * sum_r_eq;
    int *lg;
    int *lgc;
    char * solver;
    char * solver_sdp;
    double alpha;
    double alphabis;
    double *alphaq;
    double *alphabisq;
    double * beta;
    double * beta_1;
    double * beta_2;
    double * beta_3;
    double * beta_4;
    double * beta_c;
    double * delta;
    double * delta_1;
    double * delta_2;
    double * delta_3;
    double * delta_4;
    double * delta_5;
    double * delta_6;
    double * delta_7;
    double * delta_8;
    double * delta_9;
    double * delta_10;
    double * delta_11;
    double * delta_12;
    double * delta_c;
    double delta_l;
    double new_lambda_min;
    double new_lambda_min_print;
    double *lambda_min_eg;
    double *lambda_max_eg;
    double *lambda_min_ineg;
    double *lambda_max_ineg;
    int flag_nb_var;
    int * cont;
    int * x_cont;
    int * nb_var_y;
    int * var;
    double * sol;
    double sol_adm;
    int nb_init_var;
  
    /*number of constraints of each type after reduction of the problem*/
    /* Ax=b */
    int nb_eg;
    /* Dx = e */
    int nb_ineg;
    /*x^TAqx = bq*/
    int nb_eg_q;
    /*x^TDqx = eq*/
    int nb_ineg_q;
    /*Dq,y + Dq_0 x + sq = eq*/
    int nb_ineg_quad;

    /* y_ii = x_i */
    int nb_y_1;
    /* y_ij <= x_i i <j */
    int nb_y_2;
    /* y_ij <= x_j i <j */
    int nb_y_3;
    /* y_ij >= x_i + x_j -1 i <j */
    int nb_y_4;
    /* y_ij >= 0 i <j */
    int nb_y_5;
    /* y_ik + y_jk -y_ij -x_k <= 0*/
    int nb_y_6;
    /* y_ij + y_ik -y_jk -x_i <= 0*/
    int nb_y_7;
    /* y_ij + y_jk -y_ik -x_j <= 0*/
    int nb_y_8;
    /* x_i+x_j+x_k -1 - y_ij - y_jk -y_ik <= 0*/
    int nb_y_9;
    int nb_y_10;
    int nb_y_11;
    int nb_y_12;
    int nb_y_13;
    int nb_y_14;
    int nb_y_15;
    int nb_y_16;
    int nb_y_17;

    int nb_row;
    int nb_cut;
    int nb_cont;

    /* for integer case */
    /* x = sum_k 2^k t_ik*/
    int nb_dec_x;
    /* y_ij = sum_k 2^k z_ijk*/
    int nb_dec_y;
    /*z_ijk <= u_jt_ik*/
    int nb_z_1;
    /*z_ijk <= x_j*/
    int nb_z_2;
    /*z_ijk >= x_j - u_j(1 - t_ik)*/
    int nb_z_3;
    /*z_ijk >= 0*/
    int nb_z_4;

    /* new number of variables in  the problem*/
    int nb_z;
    int nb_y;
    int nb_col;
    int diag;

    /*number of constraints of each type initialy in the problem*/
    /* Ax=b */
    int ind_eg;
    /* Dx = e */
    int ind_ineg;
    /*x^TAqx = bq*/
    int ind_eg_q;
    /*x^TDqx = eq*/
    int ind_ineg_q;
    /* y_ii = x_i */
    int ind_y_1;
    /* y_ij <= x_i i<j*/
    int ind_y_2;
    /* y_ij <= x_j i<j*/
    int ind_y_3;
    /* y_ij >= x_i + x_j -1 */
    int ind_y_4;
    /* y_ij >= 0 */
    int ind_y_5;
    /* y_ik + y_jk -y_ij -x_k <= 0*/
    int ind_y_6;
    /* y_ij + y_ik -y_jk -x_i <= 0*/
    int ind_y_7;
    /* y_ij + y_jk -y_ik -x_j <= 0*/
    int ind_y_8;
    /* x_i+x_j+x_k -1 - y_ij - y_jk -y_ik <= 0*/
    int ind_y_9;
    int ind_y_10;
    int ind_y_11;
    int ind_y_12;
    int ind_y_13;
    int ind_y_14;
    int ind_y_15;
    int ind_y_16;
    int nrow;

    /* for the general integer case*/
    /* x = sum_k 2^k t_ik*/
    int ind_dec_x;
    /* y_ij = sum_k 2^k z_ijk*/
    int ind_dec_y;
    /*z_ijk <= u_jt_ik*/
    int ind_z_1;
    /*z_ijk <= x_j*/
    int ind_z_2;
    /*z_ijk >= x_j - u_j(1 - t_ik)*/
    int ind_z_3;
    /*z_ijk >= 0*/
    int ind_z_4;

    /* number of variables initialy in  the problem*/
    int ind_max_int;;
    int ind_max_x;
    int ind_max_y;
    int ind_max_z;
    int ncol;
};
typedef struct q_miqcp * Q_MIQCP;

/* structures for branch and bound */
struct miqcp_bab
{
    int n;
    int p;
    int pq;
    int mq;
    int nb_int;
    double * u;
    double * l;
    int i;
    int j;
};

typedef struct miqcp_bab * MIQCP_BAB;

struct c_miqcp
{
    int n;
    int nb_int;
    int m;
    int p;
    int mq;
    int pq;
    int N;
    int miqp;
    double cons;
    double local_sol_adm;
    double * local_sol;
    double *q;
    double * new_q;
    double *a;
    double * a_t_a;
    double *d;
    double * d_t_d;
    double *c;
    double *l;
    double *u;
    double *b;
    double * e;
    double * aq;
    double * sum_r_aq_alphaq;
    double * dq;
    double * sum_r_dq_alphabisq;
    double *bq;
    double * sum_r_bq;
    double *eq;
    double * sum_r_eq;
    int *lg;
    int *lgc;
    char * solver;
    char * solver_sdp;
    double alpha;
    double alphabis;
    double *alphaq;
    double *alphabisq;
    double * beta;
    double * beta_1;
    double * beta_2;
    double * beta_3;
    double * beta_4;
    double * beta_c;
    double * delta;
    double * delta_1;
    double * delta_2;
    double * delta_3;
    double * delta_4;
    double * delta_5;
    double * delta_6;
    double * delta_7;
    double * delta_8;
    double * delta_9;
    double * delta_10;
    double * delta_11;
    double * delta_12;
    double * delta_c;
    double delta_l;
    double new_lambda_min;
    double new_lambda_min_print;
    double *lambda_min_eg;
    double *lambda_max_eg;
    double *lambda_min_ineg;
    double *lambda_max_ineg;
    int flag_nb_var;
    int * cont;
    int * x_cont;

    int * var;
    int * nb_var_y;
    /*number of constraints of each type after reduction of the problem*/
    /* Ax=b */
    int nb_eg;
    /* Dx = e */
    int nb_ineg;
    /*x^TAqx = bq*/
    int nb_eg_q;
    /*x^TDqx = eq*/
    int nb_ineg_q;
    /* y_ij <= u_jx_i + l_ix_j - u_jl_j*/
    int nb_y_1;
    /* y_ij <= u_ix_j + l_jx_i - u_il_j*/
    int nb_y_2;
    /* y_ij >= u_ix_j + u_jx_i - u_iu_j */
    int nb_y_3;
    /* y_ij >= l_jx_i + l_ix_j - l_il_j */
    int nb_y_4;
    /* y_ii >= x_i */
    int nb_y_5;
    int nb_y_6;
    int nb_y_7;
    int nb_y_8;
    int nb_y_9;
    int nb_y_10;
    int nb_y_11;
    int nb_y_12;
    int nb_y_13;
    int nb_y_14;
    int nb_y_15;
    int nb_y_16;
    int nb_y_17;

    int nb_row;
    int nb_cut;
    int nb_cont;

    /* for integer case */
    /* x = sum_k 2^k t_ik*/
    int nb_dec_x;
    /* y_ij = sum_k 2^k z_ijk*/
    int nb_dec_y;
    /*z_ijk <= u_jt_ik*/
    int nb_z_1;
    /*z_ijk <= x_j*/
    int nb_z_2;
    /*z_ijk >= x_j - u_j(1 - t_ik)*/
    int nb_z_3;
    /*z_ijk >= 0*/
    int nb_z_4;

    /* new number of variables in  the problem*/
    int nb_z;
    int nb_y;
    int nb_col;
    int diag;

    /*number of constraints of each type initialy in the problem*/
    /* Ax=b */
    int ind_eg;
    /* Dx = e */
    int ind_ineg;
    /*x^TAqx = bq*/
    int ind_eg_q;
    /*x^TDqx = eq*/
    int ind_ineg_q;
    /* y_ii = x_i */
    int ind_y_1;
    /* y_ij <= x_i i<j*/
    int ind_y_2;
    /* y_ij <= x_j i<j*/
    int ind_y_3;
    /* y_ij >= x_i + x_j -1 */
    int ind_y_4;
    /* y_ij >= 0 */
    int ind_y_5;
    /* y_ik + y_jk -y_ij -x_k <= 0*/
    int ind_y_6;
    /* y_ij + y_ik -y_jk -x_i <= 0*/
    int ind_y_7;
    /* y_ij + y_jk -y_ik -x_j <= 0*/
    int ind_y_8;
    /* x_i+x_j+x_k -1 - y_ij - y_jk -y_ik <= 0*/
    int ind_y_9;
    int ind_y_10;
    int ind_y_11;
    int ind_y_12;
    int ind_y_13;
    int ind_y_14;
    int ind_y_15;
    int ind_y_16;
    int nrow;

    /* for the general integer case*/
    /* x = sum_k 2^k t_ik*/
    int ind_dec_x;
    /* y_ij = sum_k 2^k z_ijk*/
    int ind_dec_y;
    /*z_ijk <= u_jt_ik*/
    int ind_z_1;
    /*z_ijk <= x_j*/
    int ind_z_2;
    /*z_ijk >= x_j - u_j(1 - t_ik)*/
    int ind_z_3;
    /*z_ijk >= 0*/
    int ind_z_4;

    /* number of variables initialy in  the problem*/
    int ind_max_int;;
    int ind_max_x;
    int ind_max_y;
    int ind_max_z;
    int ncol;

};


typedef struct c_miqcp * C_MIQCP;

/*  structure for the semi-definite program */
typedef int ** TAB_CONT;

struct sdp
{
    int n;
    int nb_int;
    int m;
    int p;
    int mq;
    int pq;
    int miqp;
    int length;
    int nb_cont;
    int nb_max_cont;
    int i1;
    int i2;
    int i3;
    int i4;
    int i5;
    int i6;
    int i7;
    int i8;
    int i9;
    int i10;
    int i11;
    int i12;
    int i13;
    int i14;
    int i15;
    int i16;
    int nb_init_var;
    double cons;
    int s; // for entropy problem
    double *q;
    double *a;
    double *d;
    double *c;
    double *u;
    double *l;
    double *b;
    double * e;
    double * aq;
    double * dq;
    double *bq;
    double *eq;
    double * x;
    double * beta_1;
    double * beta_2;
    double * beta_3;
    double * beta_4;
    double * beta_5;
    double * beta_diag;
    double * delta_1;
    double * delta_2;
    double * delta_3;
    double * delta_4;
    double * delta_5;
    double * delta_6;
    double * delta_7;
    double * delta_8;
    double * delta_9;
    double * delta_10;
    double * delta_11;
    double * delta_12;
    double alpha;
    double alphabis;
    double *alphaq;
    double *alphabisq;
    int * constraints;
    int * constraints_old;
    TAB_CONT tab;
    char * solver_sdp;

};

typedef struct sdp * SDP;


/*********************************************************************/
/***************************** Functions *****************************/
/*********************************************************************/
MIQCP new_miqcp();
C_MIQCP new_c_miqcp();
Q_MIQCP new_q_miqcp();

C_MIQCP create_c_miqcp(MIQCP qp, double * beta, double alpha, double alphabis, double * alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag  );
Q_MIQCP create_q_miqcp(MIQCP qp, double * beta, double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag );
Q_MIQCP create_g_miqcp(MIQCP qp, double * beta, double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag );
Q_MIQCP create_0_1_miqcp(MIQCP qp, double * beta,double * beta_1,double * beta_2,double * beta_3,double * beta_4,double * beta_c, double *delta,double* delta_c, double delta_l,double * delta_1,  double * delta_2,  double * delta_3,  double * delta_4,double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag );
C_MIQCP create_triangular_miqcp(MIQCP qp, double * beta, double * beta_1,double * beta_2,double * beta_3,double * beta_4,double * beta_c,double * delta, double * delta_c, double delta_l, double * delta_1, double * delta_2, double * delta_3, double * delta_4,double * delta_5, double * delta_6, double * delta_7, double * delta_8,double * delta_9, double * delta_10, double * delta_11, double * delta_12,double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag );
Q_MIQCP create_qap_miqcp(MIQCP qp, double * beta, SDP psdp, int flag );
Q_MIQCP create_mkc_miqcp(MIQCP qp, double * beta, SDP psdp, int flag);

SDP create_sdp(MIQCP qp);
SDP create_sdp_mixed(MIQCP qp);
SDP create_sdp_0_1(MIQCP qp);
SDP create_sdp_qap(MIQCP qp, int flag);
SDP create_sdp_mkc(MIQCP qp, int flag, int k);
SDP create_sdp_triangle(MIQCP qp);
struct miqcp_bab create_mbab(MIQCP qp,C_MIQCP cqp,int flag);

void partial_free_miqcp(MIQCP qp);
void free_miqcp(MIQCP qp);
void free_c_miqcp(C_MIQCP qp);
void free_sdp(SDP psdp);
void free_tab_cont(TAB_CONT tab, int n);

MIQCP copy_miqcp(MIQCP qp);
void update_qp(MIQCP qp, C_MIQCP cqp);
void reduce_c_miqcp_obbt(C_MIQCP qp, int num_ineg);


#endif //QUAD_PROG_H_INCLUDED
