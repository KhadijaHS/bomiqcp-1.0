/* -*-c-*-
 *
 *    Source     $RCSfile: s_miqcrq.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>

#include<unistd.h>
#include<fcntl.h>

#include<ilcplex/cplex.h>

#include"s_miqcrq.h"
#include"p_miqcrq.h"
#include"quad_prog.h"
#include"utilities.h"
#include"in_out.h"
#include"in_out_ampl.h"
#include"local_sol.h"
#include"local_sol_ipopt.h"

//#include"local_sol_bonmin.h"

/***** compute_local_sol_couenne()*****/
void  compute_local_sol_couenne( C_MIQCP qp, double ** l, double ** u, double b_sup )
{
    int file_sol;
    pid_t pid_fils;

    FILE * file_local;
    FILE * file_x_local;
    int i;

    write_file_ampl_bb(qp,l,u,b_sup);

    char sol[50], x[50];

    strcpy(sol,  "rm ");
    strcpy(x, "rm ");

    strcat(sol,local_sol);
    strcat(x,x_local_sol);

    system("rm ../data/res_local.sol");
    system("rm ../data/local.sol");
    system("rm ../data/x_local.sol");

    pid_fils=fork();
    if (pid_fils==0)
    {
        if (DEBUG == 0)
        {
            file_sol = open(res_sdp, O_RDWR | O_CREAT | O_TRUNC,S_IRWXU | S_IRWXG | S_IRWXO);
            dup2(file_sol,STDOUT_FILENO);
            close(file_sol);
        }

        if (qp->nb_int==0)
            execlp("ampl","ampl","-f",model_local_couenne,NULL);
        else
            execlp("ampl","ampl","-f",model_local_int_couenne,NULL);
    }
    else
        wait(NULL);

    /*récuperer la valeur des x*/

    system("grep \"Stats\" ../data/res_local.sol | awk '{print $(NF-3)}' >> ../data/local.sol");

    file_local = fopen(local_sol,"r");
    fscanf(file_local,"%lf",&qp->local_sol_adm);
    fclose(file_local);
    qp->local_sol=alloc_vector_d(qp->n);
    file_x_local = fopen(x_local_sol,"r");
    fscanf(file_x_local,"x [*] :=\n");
    for (i=0; i<qp->n; i++)
    {
        fscanf(file_x_local,"%lf\n",&qp->local_sol[i]);
    }
    fclose(file_x_local);

}

/*****compute_local_sol_scip()*****/
void  compute_local_sol_scip( C_MIQCP qp, double ** l, double ** u)
{
    int file_sol;
    pid_t pid_fils;
    double loc_sol;

    FILE * file_local;
    FILE * file_x_local;
    int i;

    write_file_ampl_bb(qp,l,u,MAX_SOL_BB);

    char sol[50], x[50];

    strcpy(sol,  "rm ");
    strcpy(x, "rm ");

    strcat(sol,local_sol);
    strcat(x,x_local_sol);

    system("rm ../data/res_local.sol");
    system("rm ../data/local.sol");
    system("rm ../data/x_local.sol");

    pid_fils=fork();
    if (pid_fils==0)
    {
        execlp("ampl","ampl","-f",model_local_int_scip,NULL);
    }
    else
        wait(NULL);

    /*récuperer la valeur des x*/
    if (qp->nb_int==0)
        system(" grep \"Objective.........\" ../data/res_local.sol | awk  '{print $(NF-1)}' >> ../data/local.sol");
    else
        system(" grep \"best objective\" ../data/res_local.sol | awk  '{print $(NF-8)}' >> ../data/local.sol");

    qp->local_sol=alloc_vector_d(qp->n);

    file_x_local = fopen(x_local_sol,"r");

    fscanf(file_x_local,"%lf",&loc_sol);

    qp->local_sol_adm=loc_sol;

    for (i=0; i<qp->n; i++)
    {
        fscanf(file_x_local,"%lf\n",&qp->local_sol[i]);
    }
    fclose(file_x_local);
}

/*****compute_local_sol_scip_init()*****/
void  compute_local_sol_scip_init(MIQCP qp)
{
    int file_sol;
    pid_t pid_fils;
    double loc_sol;

    FILE * file_local;
    FILE * file_x_local;
    int i;

    write_file_ampl(qp);

    char sol[50], x[50];

    strcpy(sol,  "rm ");
    strcpy(x, "rm ");

    strcat(sol,local_sol);
    strcat(x,x_local_sol);

    system("rm ../data/res_local.sol");
    system("rm ../data/local.sol");
    system("rm ../data/x_local.sol");

    pid_fils=fork();
    if (pid_fils==0)
    {
        execlp("ampl","ampl","-f",model_local_int_scip,NULL);
    }
    else
        wait(NULL);

    /*récuperer la valeur des x*/
    file_local = fopen(x_local_sol,"r");
    fscanf(file_local,"%lf\n",&qp->sol_adm);
    qp->local_sol=alloc_vector_d(qp->n);
    for (i=0; i<qp->n; i++)
    {
        fscanf(file_local,"%lf\n",&qp->local_sol[i]);
    }
    fclose(file_local);

    if (DEBUG==1)
    {
        printf("\nlocal_sol_init : %lf\nvect x\n",qp->sol_adm);
        print_vec_d(qp->local_sol,qp->n);
        printf("\n\n");
    }
}

/*****compute_local_sol_ipopt_ampl()*****/
void  compute_local_sol_ipopt_ampl( C_MIQCP qp, double ** l, double ** u)
{
    int file_sol;
    pid_t pid_fils;

    FILE * file_local;
    FILE * file_x_local;
    int i;

    write_file_ampl_bb(qp,l,u,1000000);

    system("rm ../data/local.sol");
    system("rm ../data/x_local.sol");

    pid_fils=fork();
    if (pid_fils==0)
    {
        if (DEBUG == 0)
        {
            file_sol = open(res_sdp, O_RDWR | O_CREAT | O_TRUNC,S_IRWXU | S_IRWXG | S_IRWXO);
            dup2(file_sol,STDOUT_FILENO);
            close(file_sol);
        }

        if (qp->nb_int==0)
            execlp("ampl","ampl","-f",model_local_ipopt,NULL);
        else
            execlp("ampl","ampl","-f",model_local_int_ipopt,NULL);
    }
    else
        wait(NULL);

    /*récuperer la valeur des x*/
    file_local = fopen(local_sol,"r");
    fscanf(file_local,"obj = %lf\n",&qp->local_sol_adm);
    fclose(file_local);
    qp->local_sol=alloc_vector_d(qp->n);
    file_x_local = fopen(x_local_sol,"r");
    fscanf(file_x_local,"x [*] :=\n");
    for (i=0; i<qp->n; i++)
    {
        fscanf(file_x_local,"%lf\n",&qp->local_sol[i]);

    }
    fclose(file_x_local);
}

/*****s_miqcrq()*****/
void s_miqcrq( struct miqcp_bab bab, FILE * file_save)
{
    char     *probname = NULL;
    int      numcols;
    int      numrows;
    int      objsen;
    double   *obj = NULL;
    double   *rhs = NULL;
    char     *sense = NULL;
    int      *matbeg = NULL;
    int      *matcnt = NULL;
    int      *matind = NULL;
    double   *matval = NULL;
    double   *lb = NULL;
    double   *ub = NULL;
    char     *ctype = NULL;
    int      *qmatbeg = NULL;
    int      *qmatcnt = NULL;
    int      *qmatind = NULL;
    double   *qmatval = NULL;

    int      solstat;
    double   sol_admissible;

    double fx;

    nb_int_node++;

    double * x_y = alloc_vector_d (cqp->nb_col);

    CPXENVptr    env = NULL;
    CPXLPptr      lp = NULL;
    int           status;

    int           cur_numrows, cur_numcols;

    /* Initialize the CPLEX environment */
    int k,l,temp;

    int *i =  alloc_vector(1);
    int *j = alloc_vector(1);

    struct miqcp_bab new_bab;

    int test_time = time(NULL);
    if (test_time - start_time > TIME_LIMIT_BB)
    {
        printf("time limit exeeded");
        return;
    }

    int cont_lin=0;
    int unconstrained=0;
    int compare_sol=1;
    int feasible_for_const=1;

    if ((cqp->m>0) && (cqp->mq - 1 == 0))
    {
        if (cqp->pq == 0 && cqp->p==0)
            cont_lin = 1;
        else if ((cqp->p>0) && (cqp->pq -(2*cqp->p*cqp->n) ==0))
            cont_lin=1;
    }
    else
    {
        if ((cqp->p>0) && (cqp->pq -(2*cqp->p*cqp->n) ==0) && ((cqp->mq -1 == 0) || cqp->mq==0))
            cont_lin=1;

        if ((cqp->m==0) && (cqp->p==0) && (cqp->mq  == 0) && (cqp->pq ==0))
            unconstrained=1;
    }
    if (DEBUG==1)
        printf("\n cont_lin : %d \n",cont_lin);

    /* update upper bounds*/
    if ((cont_lin ==0) &&  (unconstrained==0))
        refine_bounds(bab, cqp);

    /* load program into cplex*/
    env = CPXopenCPLEX (&status);
    char  errmsg[1024];
    if ( env == NULL )
    {
        fprintf (stderr, "Could not open CPLEX environment.\n");
        CPXgeterrorstring (env, status, errmsg);
        fprintf (stderr, "%s", errmsg);
        return;
    }

    /* Turn on output to the screen */
    if (DEBUG==1)
    {
        status = CPXsetintparam (env, CPXPARAM_ScreenOutput, CPX_ON);
        if ( status )
        {
            fprintf (stderr,
                     "Failure to turn on screen indicator, error %d.\n", status);
            return;
        }
    }

    status = CPXsetintparam (env, 4012, 0);
    if ( status )
    {
        printf ("Failure to change param 4012, error %d.\n", status);
        return;
    }

    status = CPXsetdblparam (env, CPX_PARAM_EPGAP,REL_GAP);
    if ( status )
    {
        printf ("Failure to change relative gap, error %d.\n", status);
        return;
    }

    status = CPXsetdblparam (env, CPX_PARAM_EPAGAP,ABS_GAP);
    if ( status )
    {
        printf ("Failure to change absolute gap, error %d.\n", status);
        return;
    }

    status = CPXsetdblparam (env, CPX_PARAM_TILIM,TIME_LIMIT_CPLEX);
    if ( status )
    {
        printf ("Failure to change time limit, error %d.\n", status);
        return;
    }

    status = CPXsetintparam (env, CPX_PARAM_NODEFILEIND,2);
    if ( status )
    {
        printf ("Failure to change node storage, error %d.\n", status);
        return;
    }

    /* Set memory parameter*/
    status = CPXsetdblparam (env, CPX_PARAM_WORKMEM, MEMORY);
    if ( status )
    {
        printf ("Failure to change memory, error %d.\n", status);
        return;
    }

    /* Set threads parameter*/
    status = CPXsetintparam (env, CPX_PARAM_THREADS,THREAD);
    if ( status )
    {
        printf ("Failure to change thread, error %d.\n", status);
        return;
    }

    /*var selection strategie*/
    status = CPXsetintparam (env, CPX_PARAM_VARSEL, VARSEL);
    if ( status )
    {
        printf ("Failure to change var selection strategie, error %d.\n", status);
        return;
    }

    /* Fill in the data for the problem.  */
    status = setqpproblemdata_p_miqcrq (bab,&probname, &numcols, &numrows, &objsen, &obj,
                                        &rhs, &sense, &matbeg, &matcnt, &matind, &matval,
                                        &qmatbeg, &qmatcnt, &qmatind, &qmatval,
                                        &lb, &ub, &ctype);

    if ( status )
    {
        printf ( "Failed to build problem data arrays.\n");
        return;
    }

    /* Create the problem. */
    lp = CPXcreateprob (env, &status, probname);

    if ( lp == NULL )
    {
        printf ("Failed to create LP.\n");
        return;
    }

    /* Now copy the problem data into the lp */
    status = CPXcopylp (env, lp, numcols, numrows, objsen, obj, rhs,
                        sense, matbeg, matcnt, matind, matval,
                        lb, ub, NULL);

    if ( status )
    {
        printf ( "Failed to copy problem data.\n");
        return ;
    }

    /* Now copy the ctype array */
    if (!Zero_BB(cqp->nb_int) && (_5BRANCHES == 1))
    {
        status = CPXcopyctype (env, lp, ctype);
        if ( status )
        {
            printf ( "Failed to copy ctype\n");
            return;
        }
    }

    status = CPXcopyquad (env, lp, qmatbeg, qmatcnt, qmatind, qmatval);
    if ( status )
    {
        printf ("Failed to copy quadratic matrix.\n");
        return;
    }

    /* write  a copy of the problem */
    if (DEBUG==1)
        status = CPXwriteprob (env, lp, s_miqcrq_lp, NULL);



    /* Optimize the problem and obtain solution. */
    if (!Zero_BB(cqp->nb_int) && (_5BRANCHES == 1))
        status = CPXmipopt (env, lp);
    else
        status = CPXqpopt (env, lp);

    if ( status )
    {
        printf ( "Failed to optimize MIQP.\n");
        return;
    }
    solstat = CPXgetstat (env, lp);

    if (solstat !=   CPX_STAT_OPTIMAL && solstat !=   CPXMIP_OPTIMAL && solstat!=CPX_STAT_NUM_BEST && solstat !=CPXMIP_OPTIMAL_TOL )
    {
        printf ( "\n\nnot optimal, exit ...\n\nsolstat : %d \n\n",solstat);
        return;
    }

    if  (!Zero_BB(cqp->nb_int)&& (_5BRANCHES == 1))
        status = CPXgetmipobjval (env, lp, &sol_admissible);
    else
        status = CPXgetobjval (env, lp, &sol_admissible);
    if ( status )
    {
        printf ("No MIP objective value available.  Exiting...\n");
        sol_admissible = MAX_SOL_BB;
        return;
    }

    if  (cqp->nb_int == cqp->n)
        sol_admissible = (int) sol_admissible +1;

    cur_numrows = CPXgetnumrows (env, lp);
    cur_numcols = CPXgetnumcols (env, lp);

    if  (!Zero_BB(cqp->nb_int)&& (_5BRANCHES == 1))
        status = CPXgetmipx (env, lp, x_y, 0, cur_numcols-1);
    else
        status = CPXgetx (env, lp, x_y, 0, cur_numcols-1);
    if ( status )
    {
        printf ( "Failed to get optimal integer x.\n");
        return;
    }

    if (sol_admissible - best_sol_adm_miqcp_s  > -EPS_BB )
    {
        if (DEBUG == 1)
            printf("\n\n on coupe (best sol_adm)\n\n");
        return;
    }

    /*Compute local sol*/
    if (cont_lin && cqp->nb_int==0)
    {
        compute_local_sol(cqp,&bab.l,&bab.u,file_save);

        fx =sum_ij_qi_qj_x_ij(cqp->q, x_y, cqp->n);
        fx =fx + sum_i_ci_x_i(cqp->c, x_y, cqp->n);
        if (fx - cqp->local_sol_adm < -EPS_BB)
        {
            cqp->local_sol_adm = fx;
            for(k=0; k<cqp->n; k++)
                cqp->local_sol[k]= x_y[k];
        }

        if (cqp->local_sol_adm  - best_sol_adm_miqcp_s < -EPS_BB)
        {
            best_sol_adm_miqcp_s = cqp->local_sol_adm ;
            fprintf(file_save,"\n node %d : local solution, update best sol adm : %6lf \n",nodecount,best_sol_adm_miqcp_s);
            for(k=0; k<cqp->n; k++)
                fprintf (file_save,"x_local[%d] : %.2lf ",k+1,cqp->local_sol[k]);
            fprintf (file_save,"\n");
        }
    }
    else
    {
        if (cqp->nb_int==0)
        {
            compute_local_sol_ipopt(x_y,&bab.l,&bab.u);
            feasible_for_const= feasible_for_constraints(x_y,cqp);
            if (feasible_for_const)
            {
                fx =sum_ij_qi_qj_x_ij(cqp->q, x_y, cqp->n);
                fx =fx + sum_i_ci_x_i(cqp->c, x_y, cqp->n);
                if (fx - cqp->local_sol_adm < -EPS_BB)
                {
                    cqp->local_sol_adm = fx;
                    for(k=0; k<cqp->n; k++)
                        cqp->local_sol[k]= x_y[k];
                }
            }
        }
        else
        {
            compute_local_sol_scip(cqp,&bab.l,&bab.u);
            feasible_for_const= feasible_for_constraints(x_y,cqp);
            if (feasible_for_const)
            {
                fx =sum_ij_qi_qj_x_ij(cqp->q, x_y, cqp->n);
                fx =fx + sum_i_ci_x_i(cqp->c, x_y, cqp->n);
                if (fx - cqp->local_sol_adm < -EPS_BB)
                {
                    cqp->local_sol_adm = fx;
                    for(k=0; k<cqp->n; k++)
                        cqp->local_sol[k]= x_y[k];
                }
            }
        }

        if (cqp->local_sol_adm   - best_sol_adm_miqcp_s < - EPS_BB)
        {
            best_sol_adm_miqcp_s = cqp->local_sol_adm ;
            fprintf(file_save,"\n node %d : local solution, update best sol adm : %6lf \n",nb_int_node,best_sol_adm_miqcp_s);

            for(k=0; k<cqp->n; k++)
                fprintf (file_save,"x_local[%d] : %.2lf ",k+1,cqp->local_sol[k]);
            fprintf (file_save,"\n\n\n");

            if (DEBUG ==1)
            {
                for(k=0; k<cqp->n; k++)
                    fprintf (file_save,"x_y[%d] : %.2lf ",k+1,x_y[k]);
                fprintf (file_save,"\n");
            }
        }
    }

    if (DEBUG==1)
    {
        printf("\n local sol : %lf\n", cqp->local_sol_adm);
        printf("\n sol courante: %lf\n", sol_admissible);
        printf("\n best sol adm : %6lf \n feasible for const : %d \n",best_sol_adm_miqcp_s,feasible_for_constraints(x_y,cqp));
        printf("\n nb int node: %d\n", nb_int_node);
    }
    /*end Compute local sol*/

    if (Zero_BB(cqp->nb_int))
        nodecount = nodecount +1 ;
    else if (_5BRANCHES ==1)
        nodecount = nodecount +1 + CPXgetnodecnt (env, lp);

    if (DEBUG == 1)
    {
        fprintf (file_save,"Sol courante : %lf \n\n", sol_admissible );
        if (Zero_BB(cqp->nb_int))
            fprintf (file_save,"Number of branch-and-bound nodes: %d \n\n", nb_int_node);
        else
            fprintf (file_save,"Number of branch-and-bound nodes: %d \n\n", nb_int_node);
        int m,p,ind=cqp->n;
        for(m=0; m<cqp->n; m++)
            fprintf (file_save,"x[%d] : %.2lf ",m+1,x_y[m]);
        fprintf (file_save,"\n");
        for(m=0; m<cqp->n; m++)
        {
            for(p=m; p<cqp->n; p++)
                if (!Zero_BB(cqp->nb_var_y[ij2k(m,p,cqp->n)]))
                {
                    fprintf (file_save,"y[%d,%d] : %.2lf ",m+1,p+1,x_y[ind]);
                    ind++;
                }
            fprintf (file_save,"\n");
        }
        fprintf (file_save,"\n");

        fprintf (file_save,"\nl\n");
        for(m=0; m<cqp->n; m++)
        {
            fprintf(file_save,"%lf ",bab.l[m]);
        }
        fprintf (file_save,"\n");

        fprintf (file_save,"\nu\n");
        for(m=0; m<cqp->n; m++)
        {
            fprintf(file_save,"%lf ",bab.u[m]);
        }
        fprintf (file_save,"\n");

    }

    /* Free up the problem as allocated by CPXcreateprob, if necessary */
    if ( lp != NULL )
    {
        status = CPXfreeprob (env, &lp);
        if ( status )
        {
            printf ( "CPXfreeprob failed, error code %d.\n", status);
            return;
        }
    }

    /* Free up the CPLEX environment, if necessary */
    if ( env != NULL )
    {
        status = CPXcloseCPLEX (&env);
        if ( status )
        {
            fprintf (stderr, "Could not close CPLEX environment.\n");
            CPXgeterrorstring (env, status, errmsg);
            fprintf (stderr, "%s", errmsg);
            return;
        }
    }

    /* Free up the problem data arrays, if necessary. */
    free_and_null ((char **) &probname);
    free_and_null ((char **) &obj);
    free_and_null ((char **) &rhs);
    free_and_null ((char **) &sense);
    free_and_null ((char **) &matbeg);
    free_and_null ((char **) &matcnt);
    free_and_null ((char **) &matind);
    free_and_null ((char **) &matval);
    free_and_null ((char **) &lb);
    free_and_null ((char **) &ub);
    free_and_null ((char **) &ctype);

    int is_not_feasible = select_i_j_miqcrq(x_y, bab, i, j,cqp);

    if( is_not_feasible)
    {
        if (_5BRANCHES==1)
        {

            if ((*i == *j) && (cqp->nb_int > *i)  && (*j > cqp->nb_int))
            {
                new_bab = update_variables_int_branch_1_diag(bab, x_y, i,j,cqp);
                if (test_bound_miqcp(new_bab,cqp->nb_col))
                    s_miqcrq(new_bab,file_save);

                new_bab = update_variables_int_branch_4(bab, x_y, i,j,cqp);
                if (test_bound_miqcp(new_bab,cqp->nb_col))
                    s_miqcrq(new_bab,file_save);

                new_bab = update_variables_int_branch_5(bab, x_y,i,j,cqp);
                if (test_bound_miqcp(new_bab,cqp->nb_col))
                    s_miqcrq(new_bab,file_save);
            }
            else
            {
                if ((cqp->nb_int > *i) && (cqp->nb_int > *j))
                {

                    new_bab = update_variables_int_branch_1(bab, x_y, i,j,cqp);
                    if (test_bound_miqcp(new_bab,cqp->nb_col))
                        s_miqcrq(new_bab,file_save);

                    new_bab = update_variables_int_branch_2(bab, x_y, i,j,cqp);
                    if (test_bound_miqcp(new_bab,cqp->nb_col))
                        s_miqcrq(new_bab,file_save);

                    new_bab = update_variables_int_branch_3(bab, x_y,i,j,cqp);
                    if (test_bound_miqcp(new_bab,cqp->nb_col))
                        s_miqcrq(new_bab,file_save);

                    new_bab = update_variables_int_branch_4(bab, x_y,i,j,cqp);
                    if (test_bound_miqcp(new_bab,cqp->nb_col))
                        s_miqcrq(new_bab,file_save);

                    new_bab = update_variables_int_branch_5(bab, x_y,i,j,cqp);
                    if (test_bound_miqcp(new_bab,cqp->nb_col))
                        s_miqcrq(new_bab,file_save);
                }

                else
                {
                    if (((cqp->nb_int > *i)  && (cqp->nb_int < *j)) || ((cqp->nb_int < *i)  && (cqp->nb_int > *j)))
                    {
                        new_bab = update_variables_mixed_branch_1(bab, x_y, i,j,cqp);
                        if (test_bound_miqcp(new_bab,cqp->nb_col))
                            s_miqcrq(new_bab,file_save);

                        new_bab = update_variables_mixed_branch_2(bab, x_y, i,j,cqp);
                        if (test_bound_miqcp(new_bab,cqp->nb_col))
                            s_miqcrq(new_bab,file_save);

                        new_bab = update_variables_mixed_branch_3(bab, x_y, i,j,cqp);
                        if (test_bound_miqcp(new_bab,cqp->nb_col))
                            s_miqcrq(new_bab,file_save);
                    }

                    else
                    {

                        new_bab = update_variables_cont_branch_4(bab, x_y,i,j,cqp);
                        if (test_bound_miqcp(new_bab,cqp->nb_col))
                            s_miqcrq(new_bab,file_save);

                        new_bab = update_variables_cont_branch_5(bab, x_y,i,j,cqp);
                        if (test_bound_miqcp(new_bab,cqp->nb_col))
                            s_miqcrq(new_bab,file_save);
                    }
                }
            }
        }
        else
        {
            if (cqp->nb_int > *i)
            {
                new_bab = update_variables_int_branch_6(bab, x_y,i,j,cqp);
                if (test_bound_miqcp(new_bab,cqp->nb_col))
                    s_miqcrq(new_bab,file_save);

                new_bab = update_variables_int_branch_7(bab, x_y,i,j,cqp);
                if (test_bound_miqcp(new_bab,cqp->nb_col))
                    s_miqcrq(new_bab,file_save);
            }
            else
            {

                new_bab = update_variables_cont_branch_4(bab, x_y,i,j,cqp);
                if (test_bound_miqcp(new_bab,cqp->nb_col))
                    s_miqcrq(new_bab,file_save);

                new_bab = update_variables_cont_branch_5(bab, x_y,i,j,cqp);
                if (test_bound_miqcp(new_bab,cqp->nb_col))
                    s_miqcrq(new_bab,file_save);
            }

        }
    }
}




