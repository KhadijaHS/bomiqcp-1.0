/* -*-c-*-
 *
 *    Source     $RCSfile: in_out.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#ifndef IN_OUT_H_INCLUDED
#define IN_OUT_H_INCLUDED

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"quad_prog.h"

/*********************************************************************/
/****************************** Variable *****************************/
/*********************************************************************/
/* file names */
/* data */
static char data_ampl[80] = "/home/khadija/smiqcp_bb/data/ampl.dat";
static char data_local[80] = "/home/khadija/smiqcp_bb/data/model_quad.dat";
static char local_sol[80] = "/home/khadija/smiqcp_bb/data/local.sol";
static char x_local_sol[80] = "/home/khadija/smiqcp_bb/data/x_local.sol";
static char model_local_couenne[80] = "/home/khadija/smiqcp_bb/data/quad_cont_couenne.run";
static char model_local_int_couenne[80] = "/home/khadija/smiqcp_bb/data/quad_int_couenne.run";
static char model_local_bonmin[80] = "/home/khadija/smiqcp_bb/data/quad_cont_bonmin.run";
static char model_local_int_bonmin[80] = "/home/khadija/smiqcp_bb/data/quad_int_bonmin.run";
static char model_local_scip[80] = "/home/khadija/smiqcp_bb/data/quad_cont_scip.run";
static char model_local_int_scip[80] = "/home/khadija/smiqcp_bb/data/quad_int_scip.run";
static char model_local_ipopt[80] = "/home/khadija/smiqcp_bb/data/quad_cont_ipopt.run";
static char model_local_int_ipopt[80] = "/home/khadija/smiqcp_bb/data/quad_int_ipopt.run";
static char data_a_sb[80] = "/home/khadija/smiqcp_bb/data/a_sb.txt";
static char data_solver[80] = "/home/khadija/smiqcp_bb/data/data_c.dat";
static char data_lambda[80] = "/home/khadija/smiqcp_bb/data/lambda.dat";
static char data_au[80] = "/home/khadija/smiqcp_bb/data/au.dat";
static char data_scilab[80] = "/home/khadija/smiqcp_bb/data/lambda.sce";
static char data_matlab[80] = "/home/khadija/smiqcp_bb/data/matrixQ.m";
static char data_entropy[80] = "/home/khadija/smiqcp_bb/entropy/data.m";
static char sol_x_entropy[80] = "/home/khadija/smiqcp_bb/entropy/sol_x.dat";
static char eigenValue[80] = "/home/khadija/smiqcp_bb/data/eigenValue.dat";
static char data_sb[80] = "/home/khadija/smiqcp_bb/data/file_c.sb";
static char data_csdp[80] = "/home/khadija/smiqcp_bb/data/file_c.dat";
static char data_lambda_cont[80] = "/home/khadija/smiqcp_bb/data/lambda_cont.dat";
static char data_scilab_quad[80] = "/home/khadija/smiqcp_bb/data/lambda_quad.sce";
static char data_local_sol[80] = "/home/khadija/smiqcp_bb/data/init_sol_ub.sol";
/* saves */
static char au[80] = "/home/khadija/smiqcp_bb/saves/au.txt";
static char spectre[80] = "/home/khadija/smiqcp_bb/saves/spectre.txt";
static char res_solver[80]="/home/khadija/smiqcp_bb/saves/result_solver.txt";
static char res_sdp[80]="/home/khadija/smiqcp_bb/saves/result_sdp.txt";
static char res_sdp_bis[80]="/home/khadija/smiqcp_bb/saves/result_csdp.txt";
static char res[80]="/home/khadija/smiqcp_bb/saves/res.txt";
static char save[80] = "/home/khadija/smiqcp_bb/saves/instance.sol";
static char save_inst[80] = "/home/khadija/smiqcp_bb/saves/instance.dat";
static char lambda_eg[80] = "/home/khadija/smiqcp_bb/saves/lambda_eg.txt";
static char lambda_ineg[80] = "/home/khadija/smiqcp_bb/saves/lambda_ineg.txt";
static char prob_csdp[80] = "/home/khadija/smiqcp_bb/saves/prob.dat";
/* lp */
static char qap_lp[80] = "/home/khadija/smiqcp_bb/saves/qap.lp";
static char qap1_lp[80] = "/home/khadija/smiqcp_bb/saves/qap1.lp";
static char qap1bis_lp[80] = "/home/khadija/smiqcp_bb/saves/qap1bis.lp";
static char mkc_r4_lp[80] = "/home/khadija/smiqcp_bb/saves/mkc_r4.lp";
static char qap_cplex_lp[80] = "/home/khadija/smiqcp_bb/saves/qap_cplex.lp";
static char qapv2_lp[80] = "/home/khadija/smiqcp_bb/saves/qapv2.lp";
static char qcr_lp[80] = "/home/khadija/smiqcp_bb/saves/qcr.lp";
static char cqcr_lp[80] = "/home/khadija/smiqcp_bb/saves/cqcr.lp";
static char bilrq_lp[80] = "/home/khadija/smiqcp_bb/saves/bilrq.lp";
static char bilrq_g_lp[80] = "/home/khadija/smiqcp_bb/saves/bilrq_g.lp";
static char bilrq_gl_lp[80] = "/home/khadija/smiqcp_bb/saves/bilrq_gl.lp";
static char p_miqcrq_lp[80] = "/home/khadija/smiqcp_bb/saves/p_miqcrq.lp";
static char s_miqcrq_bc_lp[80] = "/home/khadija/smiqcp_bb/saves/s_miqcrq_bc.lp";
static char s_miqcrq_triang_lp[80] = "/home/khadija/smiqcp_bb/saves/s_miqcrq_t.lp";
static char s_miqcrq_lp[80] = "/home/khadija/smiqcp_bb/saves/s_miqcrq.lp";
static char s_miqcrq_bb_lp[80] = "/home/khadija/smiqcp_bb/saves/s_miqcrq_bb.lp";
static char s_miqcrq2_lp[80] = "/home/khadija/smiqcp_bb/saves/s_miqcrq2.lp";
static char boxqp_lp[80] = "/home/khadija/smiqcp_bb/saves/boxqp.lp";
static char boxqp_bb_lp[80] = "/home/khadija/smiqcp_bb/saves/boxqp_bb.lp";
static char boxqp_01_lp[80] = "/home/khadija/smiqcp_bb/saves/boxqp_01.lp";
static char n_miqcrq_lp[80] = "/home/khadija/smiqcp_bb/saves/n_miqcrq.lp";
static char miqcrq_g_lp[80] = "/home/khadija/smiqcp_bb/saves/miqcrq_g.lp";
static char miqcrq_g_01_lp[80] = "/home/khadija/smiqcp_bb/saves/miqcrq_g_01.lp";
static char miqcrq_01_bc_lp[80] = "/home/khadija/smiqcp_bb/saves/miqcrq_01_bc.lp";
static char miqcrq_gl_lp[80] = "/home/khadija/smiqcp_bb/saves/miqcrq_gl.lp";
static char miqcrq_ev_g_lp[80] = "/home/khadija/smiqcp_bb/saves/miqcrq_ev_g.lp";
static char local_sol_lp[80] = "/home/khadija/smiqcp_bb/saves/local_sol.lp";
static char obbt_lp[80] = "/home/khadija/smiqcp_bb/saves/obbt.lp";

/*scilab*/
static char spectre_sce[80] = "/home/khadija/smiqcp_bb/scilab/spectre.sce";

/*variables globales */
char *buf_in;
char *buf_out;
char *buf_local_sol;

/*********************************************************************/
/****************************** Functions ****************************/
/*********************************************************************/
/*write de vectors dans un fichier*/
void write_vector(int * v, int n,FILE * temp);
void write_vector_d(double * v, int n,FILE * temp);
void write_vector_d_save(double * v, int n,FILE * temp);
void write_vector_d_save_opp(double * v, int n,FILE * temp);
void write_matrix_d_save(double * M, int m, int n,FILE *temp);

/*initialiser sol locale*/
double  initialize_local_sol(char * file_name);

/*write des files de save et de donnees*/
void write_file_instance( MIQCP qp);

/*copie d'un file dans un autre*/
void cat_save(FILE *  file_save);
void cat_save_sdp(FILE *  file_save);
void cat_save_cb(FILE * file_save);

/*lecture du file d'instance*/
void read_file(MIQCP qp);       
void read_k_cluster_file( MIQCP qp, int valk);
void read_max_cut_file( MIQCP qp);
void read_task_allocation_problem( MIQCP qp);
void read_nl_file( MIQCP qp);
void read_mps_file(MIQCP qp);
void read_solution_x_from_file(SDP psdp,double ** x);
void read_min_eigenvalue(double * min);

/*changer bornes*/
void change_bounds(MIQCP qp);


#endif // IN_OUT_H_INCLUDED
