/* -*-c-*-
 *
 *    Source     $RCSfile: liste.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#ifndef LISTE_H_INCLUDED
#define LISTE_H_INCLUDED

#include"quad_prog.h"

/*********************************************************************/
/****************************** Variable *****************************/
/*********************************************************************/
typedef struct node * Node;
struct node
{
    double inf_bound_value;
    double *sol_x;
    MIQCP_BAB bab;
    Node next;
    Node previous;

};

typedef struct liste * Liste;
struct liste
{
    Node first;
    int size;
};

/*********************************************************************/
/***************************** Functions *****************************/
/*********************************************************************/
Liste init_liste();
void insert_liste(Liste liste, double value, double * sol, struct miqcp_bab bab, int nb_col);
void suppress_first_liste(Liste liste);
void print_liste(Liste liste);
void clean_liste(Liste liste, double value);

#endif //LISTE_H_INCLUDED
