/* -*-c-*-
 *
 *    Source     $RCSfile: local_sol_ipopt.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors     Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<ctype.h>
#include<assert.h>

#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<time.h>

#include<unistd.h>
#include<fcntl.h>

#include"IpStdCInterface.h"

#include"local_sol_ipopt.h"
#include"quad_prog.h"
#include"utilities.h"
#include"in_out.h"
#include"in_out_ampl.h"
#include"in_out_csdp.h"
#include"in_out_sb.h"
#include"solver_sdp.h"
#include"solver_sdp_mixed.h"
#include"s_miqcrq_bb.h"
#include "p_miqcrq.h"


/* Function Declarations */
Bool eval_f(Index n, Number* x, Bool new_x,
            Number* obj_value, UserDataPtr user_data);

Bool eval_grad_f(Index n, Number* x, Bool new_x,
                 Number* grad_f, UserDataPtr user_data);

Bool eval_g(Index n, Number* x, Bool new_x,
            Index m, Number* g, UserDataPtr user_data);

Bool eval_jac_g(Index n, Number *x, Bool new_x,
                Index m, Index nele_jac,
                Index *iRow, Index *jCol, Number *values,
                UserDataPtr user_data);

Bool eval_h(Index n, Number *x, Bool new_x, Number obj_factor,
            Index m, Number *lambda, Bool new_lambda,
            Index nele_hess, Index *iRow, Index *jCol,
            Number *values, UserDataPtr user_data);

/* Main Program */
void  compute_local_sol_ipopt(double * x_y, double ** l, double ** u)
{
    Index n=cqp->n;                          /* number of variables */
    Index m=cqp->m+cqp->p+cqp->mq+cqp->pq;                          /* number of constraints  */
    Number* x_L = NULL;                  /* lower bounds on x */
    Number* x_U = NULL;                  /* upper bounds on x */
    Number* g_L = NULL;                  /* lower bounds on g */
    Number* g_U = NULL;                  /* upper bounds on g */
    IpoptProblem nlp = NULL;             /* IpoptProblem */
    enum ApplicationReturnStatus status; /* Solve return code */
    Number* x = NULL;                    /* starting point and solution vector */
    Number* mult_g = NULL;               /* constraint multipliers     at the solution */
    Number* mult_x_L = NULL;             /* lower bound multipliers
					  at the solution */
    Number* mult_x_U = NULL;             /* upper bound multipliers
					  at the solution */
    Number obj;                          /* objective value */
    int i,j;                             /* generic counter */

    /* Number of nonzeros in the Jacobian of the constraints */
    Index nele_jac = cqp->n*m;
    /* Number of nonzeros in the Hessian of the Lagrangian (lower or
       upper triangual part only) */
    Index nele_hess = cqp->n*(cqp->n+1)/2;
    /* indexing style for matrices */
    Index index_style = 0; /* C-style; start counting of rows and column
			    indices at 0 */

    /* set the number of variables and allocate space for the bounds */
    x_L = (Number*)malloc(sizeof(Number)*n);
    x_U = (Number*)malloc(sizeof(Number)*n);
    /* set the values for the variable bounds */
    for (i=0; i<n; i++)
    {
        x_L[i] = l[0][i];
        x_U[i] = u[0][i];
    }

    /* set the number of constraints and allocate space for the bounds */
    g_L = (Number*)malloc(sizeof(Number)*m);
    g_U = (Number*)malloc(sizeof(Number)*m);
    /* set the values of the constraint bounds */
    for (i=0; i<cqp->m; i++)
    {
        g_L[i] = cqp->b[i];
        g_U[i] = cqp->b[i];
    }

    for (; i<cqp->m+cqp->p; i++)
    {
        g_L[i] = -2e19;
        g_U[i] = cqp->e[i-cqp->m];
    }

    for (; i<cqp->m+cqp->p+cqp->mq; i++)
    {
        g_L[i] = cqp->bq[i - cqp->m- cqp->p];
        g_U[i] = cqp->bq[i- cqp->m-cqp->p];
    }

    for (; i<cqp->m+cqp->p+cqp->mq+cqp->pq; i++)
    {
        g_L[i] = -2e19;
        g_U[i] = cqp->eq[i - cqp->m- cqp->p-cqp->mq];
    }


    /* create the IpoptProblem */
    nlp = CreateIpoptProblem(n, x_L, x_U, m, g_L, g_U, nele_jac, nele_hess,
                             index_style, &eval_f, &eval_g, &eval_grad_f,
                             &eval_jac_g, &eval_h);

    /* We can free the memory now - the values for the bounds have been
       copied internally in CreateIpoptProblem */

    /* Set some options.  Note the following ones are only examples,
       they might not be suitable for your problem. */
    AddIpoptNumOption(nlp, "tol", 1e-3);

    /*AddIpoptStrOption(nlp, "mu_strategy", "adaptive");*/
    /*No output*/
    AddIpoptIntOption(nlp, "print_level", 1);

    /*Max cpu time*/
    AddIpoptNumOption(nlp, "max_cpu_time", MAX_TIME_SOL_LOCAL);
    /*AddIpoptStrOption(nlp, "output_file", "../data/local.sol");*/

    /* allocate space for the initial point and set the values */
    x = (Number*)malloc(sizeof(Number)*n);

    /* possibility to start from a point (for instance the best solution found)*/
    if (MAX_SOL_BB > cqp->local_sol_adm)
        for (i=0; i<n; i++)
            x[i] = x_y[i];
    else
        for (i=0; i<n; i++)
            x[i] = random_double(x_L[i],x_U[i]);

    /* allocate space to store the bound multipliers at the solution */
    mult_x_L = (Number*)malloc(sizeof(Number)*n);
    mult_x_U = (Number*)malloc(sizeof(Number)*n);
    mult_g = (Number*)malloc(sizeof(Number)*m);
    /* solve the problem */
    status = IpoptSolve(nlp, x, NULL, &obj, mult_g, mult_x_L, mult_x_U, NULL);

    if (status == Solve_Succeeded)
    {
        /*copy local sol */

        for (i=0; i<n; i++)
            cqp->local_sol[i] = x[i];
        cqp->local_sol_adm=obj;
    }

    /* free allocated memory */
    FreeIpoptProblem(nlp);
    free(x);
    free(mult_x_L);
    free(mult_x_U);
    free(x_L);
    free(x_U);
    free(g_L);
    free(g_U);

}

/* Function Implementations */
Bool eval_f(Index n, Number* x, Bool new_x,
            Number* obj_value, UserDataPtr user_data)
{
    assert (n == cqp->n);

    double fx=0;
    int i,j;
    for(i=0; i<cqp->n; i++)
    {
        fx=fx + cqp->c[i]*x[i];
        for(j=i; j<cqp->n; j++)
            if (i==j)
                fx = fx + cqp->q[ij2k(i,j,cqp->n)]*x[i]*x[j];
            else
                fx = fx + 2*cqp->q[ij2k(i,j,cqp->n)]*x[i]*x[j];
    }

    fx = fx + cqp->cons;
    *obj_value = fx;

    return TRUE;
}

Bool eval_grad_f(Index n, Number* x, Bool new_x,
                 Number* grad_f, UserDataPtr user_data)
{
    assert (n == cqp->n);
    int i,j;

    for(i=0; i<cqp->n; i++)
    {
        grad_f[i]=cqp->c[i];
        for(j=0; j<cqp->n; j++)
            grad_f[i]=grad_f[i] + 2*cqp->q[ij2k(i,j,cqp->n)]*x[j];
    }

    return TRUE;
}

Bool eval_g(Index n, Number* x, Bool new_x,
            Index m, Number* g, UserDataPtr user_data)
{
    assert(n == cqp->n);
    int tot_cont = cqp->m+cqp->p+cqp->mq+cqp->pq;
    assert(m == tot_cont);

    int k,i,j;


    for (k=0; k<cqp->m; k++)
    {
        g[k]=0;
        for (i=0; i<cqp->n; i++)
            g[k] = g[k] + cqp->a[ij2k(k,i,cqp->n)]*x[i];
    }

    for (; k<cqp->m+cqp->p; k++)
    {
        g[k]=0;
        for (i=0; i<cqp->n; i++)
            g[k] = g[k] + cqp->d[ij2k(k-cqp->p,i,cqp->n)]*x[i];
    }

    for (k; k<cqp->m+cqp->p+cqp->mq; k++)
    {
        g[k]=0;
        for (i=0; i<cqp->n; i++)
        {
            g[k] = g[k] + 2*cqp->aq[ijk2l(k-(cqp->m+cqp->p),0,i+1,cqp->n+1,cqp->n+1)]*x[i];
            for (j=i; j<cqp->n; j++)
            {
                if (i==j)
                    g[k] = g[k] + cqp->aq[ijk2l(k-(cqp->m+cqp->p),i+1,j+1,cqp->n+1,cqp->n+1)]*x[i]*x[j];
                else
                    g[k] = g[k] + 2*cqp->aq[ijk2l(k-(cqp->m+cqp->p),i+1,j+1,cqp->n+1,cqp->n+1)]*x[i]*x[j];
            }
        }
    }

    for (k; k<cqp->m+cqp->p+cqp->mq+cqp->pq; k++)
    {
        g[k] = 0;
        for (i=0; i<cqp->n; i++)
        {
            g[k] = g[k] + 2*cqp->dq[ijk2l(k-(cqp->m+cqp->p+cqp->mq),0,i+1,cqp->n+1,cqp->n+1)]*x[i];
            for (j=i; j<cqp->n; j++)
            {
                if (i==j)
                    g[k] = g[k] + cqp->dq[ijk2l(k-(cqp->m+cqp->p+cqp->mq),i+1,j+1,cqp->n+1,cqp->n+1)]*x[i]*x[j];
                else
                    g[k] = g[k] + 2*cqp->dq[ijk2l(k-(cqp->m+cqp->p+cqp->mq),i+1,j+1,cqp->n+1,cqp->n+1)]*x[i]*x[j];
            }
        }
    }

    return TRUE;
}

Bool eval_jac_g(Index n, Number *x, Bool new_x,
                Index m, Index nele_jac,
                Index *iRow, Index *jCol, Number *values,
                UserDataPtr user_data)
{
    int i,j,k;
    int l=0;
    if (values == NULL)
    {
        /* return the structure of the jacobian */

        /* this particular jacobian is dense */
        for (k=0; k<cqp->m+cqp->p+cqp->mq+cqp->pq; k++)
        {
            for (i=0; i<cqp->n; i++)
            {
                iRow[l] = k;
                jCol[l] = i;
                l++;
            }
        }


    }
    else
    {
        /* return the values of the jacobian of the constraints */

        for (k=0; k<cqp->m; k++)
        {
            for (i=0; i<cqp->n; i++)
            {
                values[l] = cqp->a[k];
                l++;
            }
        }

        for (k; k<cqp->m+cqp->p; k++)
        {
            for (i=0; i<cqp->n; i++)
            {
                values[l] = cqp->d[k-cqp->p];
                l++;
            }
        }

        for (k; k<cqp->m+cqp->p+cqp->mq; k++)
        {
            for (i=0; i<cqp->n; i++)
            {
                values[l] = 2*cqp->aq[ijk2l(k-(cqp->m+cqp->p),0,i+1,cqp->n+1,cqp->n+1)];
                for(j=0; j<cqp->n; j++)
                {
                    values[l] =values[l] + 2*cqp->aq[ijk2l(k-(cqp->m+cqp->p),i+1,j+1,cqp->n+1,cqp->n+1)]*x[j];
                }
                l++;
            }

        }

        for (k; k<cqp->m+cqp->p+cqp->mq+cqp->pq; k++)
        {
            for (i=0; i<cqp->n; i++)
            {
                values[l] = 2*cqp->dq[ijk2l(k-(cqp->m+cqp->p+cqp->mq),0,i+1,cqp->n+1,cqp->n+1)];
                for(j=0; j<cqp->n; j++)
                {
                    values[l] =values[l] + 2*cqp->dq[ijk2l(k-(cqp->m+cqp->p+cqp->mq),i+1,j+1,cqp->n+1,cqp->n+1)]*x[j];

                }
                l++;
            }

        }



    }
    return TRUE;
}

Bool eval_h(Index n, Number *x, Bool new_x, Number obj_factor,
            Index m, Number *lambda, Bool new_lambda,
            Index nele_hess, Index *iRow, Index *jCol,
            Number *values, UserDataPtr user_data)
{
    Index idx = 0; /* nonzero element counter */
    Index row = 0; /* row counter for loop */
    Index col = 0; /* col counter for loop */
    if (values == NULL)
    {
        /* return the structure. This is a symmetric matrix, fill the lower left
         * triangle only. */

        /* the hessian for this problem is actually dense */
        idx=0;
        for (row = 0; row < cqp->n; row++)
        {
            for (col = row; col < cqp->n; col++)
            {
                iRow[idx] = row;

                jCol[idx] = col;

                idx++;
            }
        }


        assert(idx == nele_hess);
    }
    else
    {
        /* return the values. This is a symmetric matrix, fill the lower left
         * triangle only */

        /* fill the objective portion */
        int i,j;
        int l=0;
        for (i = 0; i < cqp->n; i++)
        {
            for (j = i; j < cqp->n; j++)
            {
                values[l] = 2*cqp->q[ij2k(i,j,cqp->n)];
                l++;
            }
        }



        /* add the portion for the quadratic constraints */

        int k=0;
        for(k=0; k<cqp->mq; k++)
        {
            l=0;
            for (i = 0; i < cqp->n; i++)
            {
                for (j = i; j < cqp->n; j++)
                {
                    values[l] = values[l]+ lambda[k+cqp->m+cqp->p]*2*cqp->aq[ijk2l(k,i+1,j+1,cqp->n+1,cqp->n+1)];
                    l++;
                }
            }
        }

        for(k=0; k<cqp->pq; k++)
        {
            l=0;
            for (i = 0; i < cqp->n; i++)
            {
                for (j = i; j < cqp->n; j++)
                {
                    values[l] = values[l]+ lambda[k+cqp->m+cqp->p+cqp->mq]*2*cqp->dq[ijk2l(k,i+1,j+1,cqp->n+1,cqp->n+1)];
                    l++;
                }
            }
        }
    }
    return TRUE;
}

/************************************************************************************************/
/**************  Compute local sol initialisation ***********************************************/
/************************************************************************************************/
/* Function Declarations */
Bool eval_f_init(Index n, Number* x, Bool new_x,
                 Number* obj_value, UserDataPtr user_data);

Bool eval_grad_f_init(Index n, Number* x, Bool new_x,
                      Number* grad_f, UserDataPtr user_data);

Bool eval_g_init(Index n, Number* x, Bool new_x,
                 Index m, Number* g, UserDataPtr user_data);

Bool eval_jac_g_init(Index n, Number *x, Bool new_x,
                     Index m, Index nele_jac,
                     Index *iRow, Index *jCol, Number *values,
                     UserDataPtr user_data);

Bool eval_h_init(Index n, Number *x, Bool new_x, Number obj_factor,
                 Index m, Number *lambda, Bool new_lambda,
                 Index nele_hess, Index *iRow, Index *jCol,
                 Number *values, UserDataPtr user_data);

/* Function Implementations */
Bool eval_f_init(Index n, Number* x, Bool new_x,
                 Number* obj_value, UserDataPtr user_data)
{
    assert (n == qp_init->n);

    double fx=0;
    int i,j;
    for(i=0; i<qp_init->n; i++)
    {
        fx=fx + qp_init->c[i]*x[i];
        for(j=i; j<qp_init->n; j++)
            if (i==j)
                fx = fx + qp_init->q[ij2k(i,j,qp_init->n)]*x[i]*x[j];
            else
                fx = fx + 2*qp_init->q[ij2k(i,j,qp_init->n)]*x[i]*x[j];
    }

    fx = fx + qp_init->cons;
    *obj_value = fx;

    return TRUE;
}

Bool eval_grad_f_init(Index n, Number* x, Bool new_x,
                      Number* grad_f, UserDataPtr user_data)
{
    assert (n == qp_init->n);
    int i,j;

    for(i=0; i<qp_init->n; i++)
    {
        grad_f[i]=qp_init->c[i];
        for(j=0; j<qp_init->n; j++)
            grad_f[i]=grad_f[i] + 2*qp_init->q[ij2k(i,j,qp_init->n)]*x[j];

    }



    return TRUE;
}

Bool eval_g_init(Index n, Number* x, Bool new_x,
                 Index m, Number* g, UserDataPtr user_data)
{
    assert(n == qp_init->n);
    int tot_cont = qp_init->m+qp_init->p+qp_init->mq+qp_init->pq;
    assert(m == tot_cont);

    int k,i,j;


    for (k=0; k<qp_init->m; k++)
    {
        g[k]=0;
        for (i=0; i<qp_init->n; i++)
            g[k] = g[k] + qp_init->a[ij2k(k,i,qp_init->n)]*x[i];
    }

    for (; k<qp_init->m+qp_init->p; k++)
    {
        g[k]=0;
        for (i=0; i<qp_init->n; i++)
            g[k] = g[k] + qp_init->d[ij2k(k-qp_init->p,i,qp_init->n)]*x[i];
    }

    for (k; k<qp_init->m+qp_init->p+qp_init->mq; k++)
    {
        g[k]=0;
        for (i=0; i<qp_init->n; i++)
        {
            g[k] = g[k] + 2*qp_init->aq[ijk2l(k-(qp_init->m+qp_init->p),0,i+1,qp_init->n+1,qp_init->n+1)]*x[i];
            for (j=i; j<qp_init->n; j++)
                if (i==j)
                    g[k] = g[k] + qp_init->aq[ijk2l(k-(qp_init->m+qp_init->p),i+1,j+1,qp_init->n+1,qp_init->n+1)]*x[i]*x[j];
                else
                    g[k] = g[k] + 2*qp_init->aq[ijk2l(k-(qp_init->m+qp_init->p),i+1,j+1,qp_init->n+1,qp_init->n+1)]*x[i]*x[j];
        }
    }

    for (k; k<qp_init->m+qp_init->p+qp_init->mq+qp_init->pq; k++)
    {
        g[k] = 0;
        for (i=0; i<qp_init->n; i++)
        {
            g[k] = g[k] + 2*qp_init->dq[ijk2l(k-(qp_init->m+qp_init->p+qp_init->mq),0,i+1,qp_init->n+1,qp_init->n+1)]*x[i];
            for (j=i; j<qp_init->n; j++)
            {
                if (i==j)
                    g[k] = g[k] + qp_init->dq[ijk2l(k-(qp_init->m+qp_init->p+qp_init->mq),i+1,j+1,qp_init->n+1,qp_init->n+1)]*x[i]*x[j];
                else
                    g[k] = g[k] + 2*qp_init->dq[ijk2l(k-(qp_init->m+qp_init->p+qp_init->mq),i+1,j+1,qp_init->n+1,qp_init->n+1)]*x[i]*x[j];
            }
        }
    }


    return TRUE;
}

Bool eval_jac_g_init(Index n, Number *x, Bool new_x,
                     Index m, Index nele_jac,
                     Index *iRow, Index *jCol, Number *values,
                     UserDataPtr user_data)
{
    int i,j,k;
    int l=0;
    if (values == NULL)
    {
        /* return the structure of the jacobian */

        /* this particular jacobian is dense */
        for (k=0; k<qp_init->m+qp_init->p+qp_init->mq+qp_init->pq; k++)
        {
            for (i=0; i<qp_init->n; i++)
            {
                iRow[l] = k;
                jCol[l] = i;
                l++;
            }
        }
    }
    else
    {
        /* return the values of the jacobian of the constraints */

        for (k=0; k<qp_init->m; k++)
        {
            for (i=0; i<qp_init->n; i++)
            {
                values[l] = qp_init->a[k];
                l++;
            }
        }

        for (k; k<qp_init->m+qp_init->p; k++)
        {
            for (i=0; i<qp_init->n; i++)
            {
                values[l] = qp_init->d[k-qp_init->p];
                l++;
            }
        }

        for (k; k<qp_init->m+qp_init->p+qp_init->mq; k++)
        {
            for (i=0; i<qp_init->n; i++)
            {
                values[l] = 2*qp_init->aq[ijk2l(k-(qp_init->m+qp_init->p),0,i+1,qp_init->n+1,qp_init->n+1)];
                for(j=0; j<qp_init->n; j++)
                    values[l] =values[l] + 2*qp_init->aq[ijk2l(k-(qp_init->m+qp_init->p),i+1,j+1,qp_init->n+1,qp_init->n+1)]*x[j];
                l++;
            }
        }

        for (k; k<qp_init->m+qp_init->p+qp_init->mq+qp_init->pq; k++)
        {
            for (i=0; i<qp_init->n; i++)
            {
                values[l] = 2*qp_init->dq[ijk2l(k-(qp_init->m+qp_init->p+qp_init->mq),0,i+1,qp_init->n+1,qp_init->n+1)];
                for(j=0; j<qp_init->n; j++)
                    values[l] =values[l] + 2*qp_init->dq[ijk2l(k-(qp_init->m+qp_init->p+qp_init->mq),i+1,j+1,qp_init->n+1,qp_init->n+1)]*x[j];
                l++;
            }
        }


    }

    return TRUE;
}

Bool eval_h_init(Index n, Number *x, Bool new_x, Number obj_factor,
                 Index m, Number *lambda, Bool new_lambda,
                 Index nele_hess, Index *iRow, Index *jCol,
                 Number *values, UserDataPtr user_data)
{
    Index idx = 0; /* nonzero element counter */
    Index row = 0; /* row counter for loop */
    Index col = 0; /* col counter for loop */
    if (values == NULL)
    {
        /* return the structure. This is a symmetric matrix, fill the lower left
         * triangle only. */

        /* the hessian for this problem is actually dense */
        idx=0;
        for (row = 0; row < qp_init->n; row++)
        {
            for (col = row; col < qp_init->n; col++)
            {
                iRow[idx] = row;
                jCol[idx] = col;
                idx++;
            }
        }


        assert(idx == nele_hess);
    }
    else
    {
        /* return the values. This is a symmetric matrix, fill the lower left
         * triangle only */

        /* fill the objective portion */
        int i,j;
        int l=0;
        for (i = 0; i < qp_init->n; i++)
        {
            for (j = i; j < qp_init->n; j++)
            {
                values[l] = 2*qp_init->q[ij2k(i,j,qp_init->n)];
                l++;
            }
        }



        /* add the portion for the quadratic constraints */

        int k=0;
        for(k=0; k<qp_init->mq; k++)
        {
            l=0;
            for (i = 0; i < qp_init->n; i++)
            {
                for (j = i; j < qp_init->n; j++)
                {
                    values[l] = values[l]+ lambda[k+qp_init->m+qp_init->p]*2*qp_init->aq[ijk2l(k,i+1,j+1,qp_init->n+1,qp_init->n+1)];
                    l++;
                }
            }
        }

        for(k=0; k<qp_init->pq; k++)
        {
            l=0;
            for (i = 0; i < qp_init->n; i++)
            {
                for (j = i; j < qp_init->n; j++)
                {
                    values[l] = values[l]+ lambda[k+qp_init->m+qp_init->p+qp_init->mq]*2*qp_init->dq[ijk2l(k,i+1,j+1,qp_init->n+1,qp_init->n+1)];
                    l++;
                }
            }
        }

    }

    return TRUE;
}



void  compute_local_sol_ipopt_init()
{
    Index n=qp_init->n;                          /* number of variables */
    Index m=qp_init->m+qp_init->p+qp_init->mq+qp_init->pq;                          /* number of constraints  */
    Number* x_L = NULL;                  /* lower bounds on x */
    Number* x_U = NULL;                  /* upper bounds on x */
    Number* g_L = NULL;                  /* lower bounds on g */
    Number* g_U = NULL;                  /* upper bounds on g */
    IpoptProblem nlp = NULL;             /* IpoptProblem */
    enum ApplicationReturnStatus status; /* Solve return code */
    Number* x = NULL;                    /* starting point and solution vector */
    Number* mult_g = NULL;               /* constraint multipliers     at the solution */
    Number* mult_x_L = NULL;             /* lower bound multipliers
					  at the solution */
    Number* mult_x_U = NULL;             /* upper bound multipliers
					  at the solution */
    Number obj;                          /* objective value */
    int i,j;                             /* generic counter */

    /* Number of nonzeros in the Jacobian of the constraints */
    Index nele_jac = qp_init->n*m;
    /* Number of nonzeros in the Hessian of the Lagrangian (lower or
       upper triangual part only) */
    Index nele_hess = qp_init->n*(qp_init->n+1)/2;
    /* indexing style for matrices */
    Index index_style = 0; /* C-style; start counting of rows and column
			    indices at 0 */

    /* set the number of variables and allocate space for the bounds */
    x_L = (Number*)malloc(sizeof(Number)*n);
    x_U = (Number*)malloc(sizeof(Number)*n);
    /* set the values for the variable bounds */
    for (i=0; i<n; i++)
    {
        x_L[i] = qp_init->l[i];
        x_U[i] = qp_init->u[i];
    }

    /* set the number of constraints and allocate space for the bounds */
    g_L = (Number*)malloc(sizeof(Number)*m);
    g_U = (Number*)malloc(sizeof(Number)*m);
    /* set the values of the constraint bounds */
    for (i=0; i<qp_init->m; i++)
    {
        g_L[i] = qp_init->b[i];
        g_U[i] = qp_init->b[i];
    }

    for (; i<qp_init->m+qp_init->p; i++)
    {
        g_L[i] = -2e19;
        g_U[i] = qp_init->e[i-qp_init->m];
    }

    for (; i<qp_init->m+qp_init->p+qp_init->mq; i++)
    {
        g_L[i] = qp_init->bq[i - qp_init->m- qp_init->p];
        g_U[i] = qp_init->bq[i- qp_init->m-qp_init->p];
    }

    for (; i<qp_init->m+qp_init->p+qp_init->mq+qp_init->pq; i++)
    {
        g_L[i] = -2e19;
        g_U[i] = qp_init->eq[i - qp_init->m- qp_init->p-qp_init->mq];
    }

    /* create the IpoptProblem */
    nlp = CreateIpoptProblem(n, x_L, x_U, m, g_L, g_U, nele_jac, nele_hess,
                             index_style, &eval_f_init, &eval_g_init, &eval_grad_f_init,
                             &eval_jac_g_init, &eval_h_init);

    /* We can free the memory now - the values for the bounds have been
       copied internally in CreateIpoptProblem */


    /* Set some options.  Note the following ones are only examples,
       they might not be suitable for your problem. */
    AddIpoptNumOption(nlp, "tol", 1e-3);
    AddIpoptStrOption(nlp, "mu_strategy", "adaptive");
    /*No output*/
    AddIpoptIntOption(nlp, "print_level", 1);
    /*Max cpu time*/
    AddIpoptNumOption(nlp, "max_cpu_time", MAX_TIME_SOL_INIT);
    /*AddIpoptStrOption(nlp, "output_file", "../data/local.sol");*/

    /* allocate space for the initial point and set the values */
    x = (Number*)malloc(sizeof(Number)*n);

    /* possibility to start from a point (for instance the best solution found)*/

    for (i=0; i<n; i++)
        x[i] = random_double(x_L[i],x_U[i]);

    /* allocate space to store the bound multipliers at the solution */
    mult_x_L = (Number*)malloc(sizeof(Number)*n);
    mult_x_U = (Number*)malloc(sizeof(Number)*n);
    mult_g = (Number*)malloc(sizeof(Number)*m);
    /* solve the problem */
    status = IpoptSolve(nlp, x, NULL, &obj, mult_g, mult_x_L, mult_x_U, NULL);

    if (DEBUG==1)
    {
        if (status == Solve_Succeeded)
        {
            printf("\n\nSolution of the primal variables, x\n");
            for (i=0; i<n; i++)
            {
                printf("x[%d] = %e\n", i, x[i]);
            }

            printf("\n\nSolution of the bound multipliers, z_L and z_U\n");
            for (i=0; i<n; i++)
            {
                printf("z_L[%d] = %e\n", i, mult_x_L[i]);
            }
            for (i=0; i<n; i++)
            {
                printf("z_U[%d] = %e\n", i, mult_x_U[i]);
            }

            printf("\n\nSolution of the bound multipliers for  constraints\n");
            for (i=0; i<m; i++)
            {
                printf("g_U[%d] = %e\n", i, mult_g[i]);
            }


            printf("\n\nObjective value\n");
            printf("f(x*) = %e\n", obj);
        }
    }


    if (status == Solve_Succeeded)
    {
        /*copy local sol */
        qp_init->local_sol = alloc_vector_d(qp_init->n);
        for (i=0; i<n; i++)
            qp_init->local_sol[i] = x[i];
        qp_init->sol_adm=obj;

    }
    else
    {
        qp_init->local_sol = alloc_vector_d(qp_init->n);
        for (i=0; i<n; i++)
            qp_init->local_sol[i] = 0;
        qp_init->sol_adm=MAX_SOL_SDP;
    }

    /* free allocated memory */
    FreeIpoptProblem(nlp);
    free(x);
    free(mult_x_L);
    free(mult_x_U);
    free(x_L);
    free(x_U);
    free(g_L);
    free(g_U);
}
