/* -*-c-*-
 *
 *    Source     $RCSfile: s_miqcp_bb.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>

#include<unistd.h>
#include<fcntl.h>

#include<ilcplex/cplex.h>

#include"s_miqcrq_bb.h"
#include"in_out.h"
#include"in_out_ampl.h"
#include"quad_prog.h"
#include"utilities.h"
#include"liste.h"
#include"local_sol.h"
#include"local_sol_ipopt.h"
#include"s_miqcrq.h"
#include"p_miqcrq.h"
/*
#include"obbt.h"
#include "local_sol_bonmin.h"
*/

/*****eval_s_miqcrq_b()*****/
int eval_s_miqcrq_b(struct miqcp_bab bab, FILE * file_save, double ** sol_xy, double * sol_adm, int computeLocalSol)
{
    char     *probname = NULL;
    int      numcols;
    int      numrows;
    int      objsen;
    double   *obj = NULL;
    double   *rhs = NULL;
    char     *sense = NULL;
    int      *matbeg = NULL;
    int      *matcnt = NULL;
    int      *matind = NULL;
    double   *matval = NULL;
    double   *lb = NULL;
    double   *ub = NULL;
    char     *ctype = NULL;
    int      *qmatbeg = NULL;
    int      *qmatcnt = NULL;
    int      *qmatind = NULL;
    double   *qmatval = NULL;
    int      solstat;
    double   sol_admissible;
    double fx;
    nb_int_node++;
    double * x_y = alloc_vector_d (cqp->nb_col);
    CPXENVptr    env = NULL;
    CPXLPptr      lp = NULL;
    int           status;
    int           cur_numrows, cur_numcols;

    /* Initialize the CPLEX environment */
    int k,l,temp;
    int *i =  alloc_vector(1);
    int *j = alloc_vector(1);
    int cont_lin=0;
    int unconstrained=0;
    int compare_sol=1;
    int feasible_for_const=1;

    /*******************************************************************************************/
    /****************  OBBT ********************************************************************/
    /*******************************************************************************************/
    /* update upper bounds*/
    int lowerbound=1;
    for(k=0; k<cqp->n; k++)
        if (cqp->l[k]!=0)
        {
            lowerbound=0;
            break;
        }

    if ((lowerbound==1) && (cont_lin ==0) &&  (unconstrained==0))
        refine_bounds(bab, cqp);

    /* load program into cplex*/
    env = CPXopenCPLEX (&status);
    char  errmsg[1024];
    if ( env == NULL )
    {
        fprintf (stderr, "Could not open CPLEX environment.\n");
        CPXgeterrorstring (env, status, errmsg);
        fprintf (stderr, "%s", errmsg);
        return 0;
    }

    /* Turn on output to the screen */
    if (DEBUG==1)
    {
        status = CPXsetintparam (env, CPXPARAM_ScreenOutput, CPX_ON);
        if ( status )
        {
            fprintf (stderr,
                     "Failure to turn on screen indicator, error %d.\n", status);

            return 0;
        }
    }

    status = CPXsetintparam (env, 4012, 0);
    if ( status )
    {
        printf ("Failure to change param 4012, error %d.\n", status);
        return 0;
    }

    status = CPXsetdblparam (env, CPX_PARAM_EPGAP,REL_GAP);
    if ( status )
    {
        printf ("Failure to change relative gap, error %d.\n", status);
        return 0;
    }

    status = CPXsetdblparam (env, CPX_PARAM_EPAGAP,ABS_GAP);
    if ( status )
    {
        printf ("Failure to change absolute gap, error %d.\n", status);
        return 0;
    }

    status = CPXsetdblparam (env, CPX_PARAM_TILIM,TIME_LIMIT_CPLEX);
    if ( status )
    {
        printf ("Failure to change time limit, error %d.\n", status);
        return 0;
    }

    status = CPXsetintparam (env, CPX_PARAM_NODEFILEIND,2);
    if ( status )
    {
        printf ("Failure to change node storage, error %d.\n", status);
        return 0;
    }

    /* Set memory parameter*/
    status = CPXsetdblparam (env, CPX_PARAM_WORKMEM, MEMORY);
    if ( status )
    {
        printf ("Failure to change memory, error %d.\n", status);
        return 0;
    }

    /* Set threads parameter*/
    status = CPXsetintparam (env, CPX_PARAM_THREADS,THREAD);
    if ( status )
    {
        printf ("Failure to change thread, error %d.\n", status);
        return 0;
    }

    /*var selection strategie*/
    status = CPXsetintparam (env, CPX_PARAM_VARSEL, VARSEL);
    if ( status )
    {
        printf ("Failure to change var selection strategie, error %d.\n", status);
        return 0;
    }

    /* Fill in the data for the problem.  */

    status = setqpproblemdata_p_miqcrq (bab,&probname, &numcols, &numrows, &objsen, &obj,
                                        &rhs, &sense, &matbeg, &matcnt, &matind, &matval,
                                        &qmatbeg, &qmatcnt, &qmatind, &qmatval,
                                        &lb, &ub, &ctype);

    if ( status )
    {
        printf ( "Failed to build problem data arrays.\n");
        return 0;
    }

    /* Create the problem. */
    lp = CPXcreateprob (env, &status, probname);

    if ( lp == NULL )
    {
        printf ("Failed to create LP.\n");
        return 0;
    }

    /* Now copy the problem data into the lp */
    status = CPXcopylp (env, lp, numcols, numrows, objsen, obj, rhs,
                        sense, matbeg, matcnt, matind, matval,
                        lb, ub, NULL);

    if ( status )
    {
        printf ( "Failed to copy problem data.\n");
        return 0;
    }

    /* Now copy the ctype array */
    if (!Zero_BB(cqp->nb_int) && (_5BRANCHES == 1))
    {
        status = CPXcopyctype (env, lp, ctype);
        if ( status )
        {
            printf ( "Failed to copy ctype\n");
            return 0;
        }
    }

    status = CPXcopyquad (env, lp, qmatbeg, qmatcnt, qmatind, qmatval);
    if ( status )
    {
        printf ("Failed to copy quadratic matrix.\n");
        return 0;
    }

    /* write  a copy of the problem */
    if (DEBUG==1)
        status = CPXwriteprob (env, lp, s_miqcrq_bb_lp, NULL);



    /* Optimize the problem and obtain solution. */
    if (!Zero_BB(cqp->nb_int) && (_5BRANCHES == 1))
        status = CPXmipopt (env, lp);
    else
        status = CPXqpopt (env, lp);

    if ( status )
    {
        printf ( "Failed to optimize MIQP.\n");
        return 0;
    }
    
    solstat = CPXgetstat (env, lp);

    if (solstat !=   CPX_STAT_OPTIMAL && solstat !=   CPXMIP_OPTIMAL && solstat!=CPX_STAT_NUM_BEST && solstat !=CPXMIP_OPTIMAL_TOL )
    {
        printf ( "\n\nnot optimal, exit ...\n\nsolstat : %d \n\n",solstat);
        return 0;
    }

    if  (!Zero_BB(cqp->nb_int)&& (_5BRANCHES == 1))
        status = CPXgetmipobjval (env, lp, &sol_admissible);
    else
        status = CPXgetobjval (env, lp, &sol_admissible);
    if ( status )
    {
        printf ("No MIP objective value available.  Exiting...\n");
        sol_admissible = MAX_SOL_BB;
        return 0;
    }

    /* if  (cqp->nb_int == cqp->n) */
    /*   sol_admissible = (int) sol_admissible +1; */

    cur_numrows = CPXgetnumrows (env, lp);
    cur_numcols = CPXgetnumcols (env, lp);

    if  (!Zero_BB(cqp->nb_int)&& (_5BRANCHES == 1))
        status = CPXgetmipx (env, lp, x_y, 0, cur_numcols-1);
    else
        status = CPXgetx (env, lp, x_y, 0, cur_numcols-1);
    if ( status )
    {
        printf ( "Failed to get optimal integer x.\n");
        return 0;
    }

    if (sol_admissible - best_sol_adm_miqcp_s_bb  > -EPS_BB )
    {
        fprintf(file_save,"\nnode %d\t current sol = %6lf > UB = %lf, branch pruned ",nb_int_node, sol_admissible,best_sol_adm_miqcp_s_bb);
        if (DEBUG == 1)
            printf("\n\n on coupe (best sol_adm)\n\n");
        if (nb_int_node==1)
            borne_inf_bb = sol_admissible;
        return 0;
    }

    if (nb_int_node==1)
        borne_inf_bb = sol_admissible;

    double gap;
    /*Compute local sol*/
    if (computeLocalSol)
    {
        if (cont_lin && cqp->nb_int==0)
        {
            compute_local_sol(cqp,&bab.l,&bab.u,file_save);

            fx =sum_ij_qi_qj_x_ij(cqp->q, x_y, cqp->n);
            fx =fx + sum_i_ci_x_i(cqp->c, x_y, cqp->n);
            if (fx - cqp->local_sol_adm < -EPS_BB)
            {
                cqp->local_sol_adm = fx;
                for(k=0; k<cqp->n; k++)
                    cqp->local_sol[k]= x_y[k];
            }


            if (cqp->local_sol_adm  - best_sol_adm_miqcp_s_bb < -EPS_BB)
            {
                best_sol_adm_miqcp_s_bb = cqp->local_sol_adm ;
                for(k=0; k<cqp->n; k++)
                    best_x[k] = cqp->local_sol[k];

                gap = (borne_inf_bb - best_sol_adm_miqcp_s_bb);
                gap = gap/best_sol_adm_miqcp_s_bb;
                v_abs_ref(&gap);

                if (nb_int_node ==1)
                    fprintf(file_save,"\nnode %d\t\tUB* (local solver): %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, sol_admissible,gap,time(NULL)-start_time);
                else
                    fprintf(file_save,"\nnode %d\t\tUB* (local solver): %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, borne_inf_bb,gap,time(NULL)-start_time);
                if (DEBUG ==1)
                {

                    for(k=0; k<cqp->n; k++)
                        fprintf (file_save,"\nx_local[%d] : %.2lf ",k+1,cqp->local_sol[k]);
                    fprintf (file_save,"\n");
                }
            }
            else
            {
                gap = (borne_inf_bb - best_sol_adm_miqcp_s_bb);
                gap = gap/best_sol_adm_miqcp_s_bb;
                v_abs_ref(&gap);

                if (nb_int_node ==1)
                    fprintf(file_save,"\nnode %d\t\tUB\t\t  : %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, sol_admissible,gap,time(NULL)-start_time);
                else
                    fprintf(file_save,"\nnode %d\t\tUB\t\t  : %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, borne_inf_bb,gap,time(NULL)-start_time);

            }
        }
        else
        {
            if (cqp->nb_int==0)
            {
                compute_local_sol_ipopt(x_y,&bab.l,&bab.u);
                feasible_for_const= feasible_for_constraints(x_y,cqp);
                if (feasible_for_const)
                {
                    fx =sum_ij_qi_qj_x_ij(cqp->q, x_y, cqp->n);
                    fx =fx + sum_i_ci_x_i(cqp->c, x_y, cqp->n);
                    if (fx - cqp->local_sol_adm < - EPS_BB)
                    {
                        cqp->local_sol_adm = fx;
                        for(k=0; k<cqp->n; k++)
                            cqp->local_sol[k]= x_y[k];
                    }
                }
            }
            else
            {
                compute_local_sol_scip(cqp,&bab.l,&bab.u);
                feasible_for_const= feasible_for_constraints(x_y,cqp);
                if (feasible_for_const)
                {
                    fx =sum_ij_qi_qj_x_ij(cqp->q, x_y, cqp->n);
                    fx =fx + sum_i_ci_x_i(cqp->c, x_y, cqp->n);
                    if (fx - cqp->local_sol_adm < -EPS_BB)
                    {
                        cqp->local_sol_adm = fx;
                        for(k=0; k<cqp->n; k++)
                            cqp->local_sol[k]= x_y[k];
                    }
                }
            }

            if (cqp->local_sol_adm   - best_sol_adm_miqcp_s_bb < - EPS_BB)
            {
                best_sol_adm_miqcp_s_bb = cqp->local_sol_adm ;
                for(k=0; k<cqp->n; k++)
                    best_x[k] = cqp->local_sol[k];

                gap = (borne_inf_bb - best_sol_adm_miqcp_s_bb);
                gap = gap/best_sol_adm_miqcp_s_bb;
                v_abs_ref(&gap);
                if (nb_int_node ==1)
                    fprintf(file_save,"\nnode %d\t\tUB* (local solver): %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, sol_admissible,gap,time(NULL)-start_time);
                else
                    fprintf(file_save,"\nnode %d\t\tUB* (local solver): %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, borne_inf_bb,gap,time(NULL)-start_time);

                if (DEBUG ==1)
                {
                    for(k=0; k<cqp->n; k++)
                        fprintf (file_save,"x_local[%d] : %.2lf ",k+1,cqp->local_sol[k]);
                    fprintf (file_save,"\n\n\n");
                    for(k=0; k<cqp->n; k++)
                        fprintf (file_save,"x_y[%d] : %.2lf ",k+1,x_y[k]);
                    fprintf (file_save,"\n");
                }
            }
            else
            {
                gap = (borne_inf_bb - best_sol_adm_miqcp_s_bb);
                gap = gap/best_sol_adm_miqcp_s_bb;
                v_abs_ref(&gap);
                if (nb_int_node ==1)
                    fprintf(file_save,"\nnode %d\t\tUB\t\t  : %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, sol_admissible,gap,time(NULL)-start_time);
                else
                    fprintf(file_save,"\nnode %d\t\tUB\t\t  : %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, borne_inf_bb,gap,time(NULL)-start_time);

            }
        }
    }
    else
    {
        gap = (borne_inf_bb - best_sol_adm_miqcp_s_bb);
        gap = gap/best_sol_adm_miqcp_s_bb;
        v_abs_ref(&gap);
        if (nb_int_node ==1)
            fprintf(file_save,"\nnode %d\t\tUB\t\t  : %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, sol_admissible,gap,time(NULL)-start_time);
        else
            fprintf(file_save,"\nnode %d\t\tUB\t\t  : %6lf\t\tLB: %6lf\t\tgap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, borne_inf_bb,gap,time(NULL)-start_time);
    }

    if (DEBUG==1)
    {
        printf("\n local sol : %lf\n", cqp->local_sol_adm);
        printf("\n sol courante: %lf\n", sol_admissible);
        printf("\n best sol adm : %6lf \n feasible for const : %d \n",best_sol_adm_miqcp_s_bb,feasible_for_constraints(x_y,cqp));
        printf("\n nb int node: %d\n", nb_int_node);

    }
    /* clean list of leaves*/
    if (liste != NULL && computeLocalSol)
        clean_liste(liste, best_sol_adm_miqcp_s_bb);

    /*end Compute local sol*/

    if (DEBUG == 1)
    {
        fprintf (file_save,"Sol courante : %lf \n\n", sol_admissible );
        if (Zero_BB(cqp->nb_int))
            fprintf (file_save,"Number of branch-and-bound nodes: %d \n\n", nb_int_node);
        else
            fprintf (file_save,"Number of branch-and-bound nodes: %d \n\n", nb_int_node);
        int m,p,ind=cqp->n;
        for(m=0; m<cqp->n; m++)
            fprintf (file_save,"x[%d] : %.2lf ",m+1,x_y[m]);
        fprintf (file_save,"\n");
        for(m=0; m<cqp->n; m++)
        {
            for(p=m; p<cqp->n; p++)
                if (!Zero_BB(cqp->nb_var_y[ij2k(m,p,cqp->n)]))
                {
                    fprintf (file_save,"y[%d,%d] : %.2lf ",m+1,p+1,x_y[ind]);
                    ind++;
                }
            fprintf (file_save,"\n");
        }
        fprintf (file_save,"\n");

        fprintf (file_save,"\nl\n");
        for(m=0; m<cqp->n; m++)
        {
            fprintf(file_save,"%lf ",bab.l[m]);
        }
        fprintf (file_save,"\n");

        fprintf (file_save,"\nu\n");
        for(m=0; m<cqp->n; m++)
        {
            fprintf(file_save,"%lf ",bab.u[m]);
        }
        fprintf (file_save,"\n");

    }

    /* Free up the problem as allocated by CPXcreateprob, if necessary */
    if ( lp != NULL )
    {
        status = CPXfreeprob (env, &lp);
        if ( status )
        {
            printf ( "CPXfreeprob failed, error code %d.\n", status);
            return 0;
        }
    }

    /* Free up the CPLEX environment, if necessary */
    if ( env != NULL )
    {
        status = CPXcloseCPLEX (&env);
        if ( status )
        {
            fprintf (stderr, "Could not close CPLEX environment.\n");
            CPXgeterrorstring (env, status, errmsg);
            fprintf (stderr, "%s", errmsg);
            return 0;
        }
    }

    /* Free up the problem data arrays, if necessary. */
    free_and_null ((char **) &probname);
    free_and_null ((char **) &obj);
    free_and_null ((char **) &rhs);
    free_and_null ((char **) &sense);
    free_and_null ((char **) &matbeg);
    free_and_null ((char **) &matcnt);
    free_and_null ((char **) &matind);
    free_and_null ((char **) &matval);
    free_and_null ((char **) &lb);
    free_and_null ((char **) &ub);
    free_and_null ((char **) &ctype);

    *sol_adm=sol_admissible;
    *sol_xy = x_y;

    return 1;
}

/*****s_miqcrq_bb()*****/
void s_miqcrq_bb( struct miqcp_bab bab, FILE * file_save)
{
    double sol_admissible,fx;
    double * x_y = alloc_vector_d (cqp->nb_col);
    int *i =  alloc_vector(1);
    int *j = alloc_vector(1);
    int k,m,p,ind;
    struct miqcp_bab new_bab_1;
    struct miqcp_bab new_bab_2;
    pere = -1;
    int test_time;
    borne_inf_bb = MIN_SOL_BB;

    /*last parameter: 1 means that it is necessary to compute a local sol at this node else 0 */
    int status = eval_s_miqcrq_b(bab, file_save, &x_y,&sol_admissible,1);
    if(status ==1)
    {
        insert_liste(liste,sol_admissible, x_y, bab, cqp->nb_col);
        borne_inf_bb = liste->first->inf_bound_value;
    }
    
    int is_not_feasible;
    /*decides if a local sol has to be computed at this node*/
    int x_inf_new_u=0;
    double relative_gap;
    double absolute_gap;
    while(liste->size!=0)
    {
        test_time = time(NULL);
        if (test_time - start_time > TIME_LIMIT_BB)
        {
            fprintf(file_save, "\n Time limit exeeded, time > %d \n",  TIME_LIMIT_BB);
            break;
        }

        absolute_gap = (borne_inf_bb - best_sol_adm_miqcp_s_bb);
        v_abs_ref(&absolute_gap);
        if (cqp->n == cqp->nb_int && absolute_gap < EPS_ABS_GAP)
        {
            fprintf(file_save, "\nnode %d\tBranch pruned, absolute_gap: %lf < %lf \n", nb_int_node,absolute_gap,EPS_ABS_GAP);
            break;

        }

        relative_gap = absolute_gap/best_sol_adm_miqcp_s_bb;
        v_abs_ref(&relative_gap);
        if (relative_gap  < EPS_BB )
        {
            fprintf(file_save, "\nnode %d\tBranch pruned, relative_gap: %lf < %lf \n", nb_int_node,relative_gap,EPS_BB);
            break;
        }

        borne_inf_bb = liste->first->inf_bound_value;

        is_not_feasible = select_i_j_miqcrq(liste->first->sol_x, *liste->first->bab, i, j,cqp);
        if( is_not_feasible)
        {
            if (cqp->nb_int > *i)
            {
                new_bab_1 = update_variables_int_branch_6(*liste->first->bab, x_y,i,j,cqp);

                new_bab_2 = update_variables_int_branch_7(*liste->first->bab, x_y,i,j,cqp);

                suppress_first_liste(liste);

                if (test_bound_miqcp(new_bab_1,cqp->nb_col))
                {
                    status = eval_s_miqcrq_b(new_bab_1, file_save, &x_y, &sol_admissible,1);
                    if (status ==1)
                    {
                        if (best_sol_adm_miqcp_s_bb > sol_admissible)
                        {
                            absolute_gap = (sol_admissible - best_sol_adm_miqcp_s_bb);
                            relative_gap = absolute_gap/best_sol_adm_miqcp_s_bb;
                            v_abs_ref(&relative_gap);
                            if (relative_gap  > EPS_BB )
                                insert_liste(liste,sol_admissible, x_y, new_bab_1, cqp->nb_col);
                        }
                    }
                }

                if (test_bound_miqcp(new_bab_2,cqp->nb_col))
                {
                    if (best_sol_adm_miqcp_s_bb > sol_admissible)
                    {
                        status = eval_s_miqcrq_b(new_bab_2, file_save, &x_y, &sol_admissible,1);
                        if (status ==1)
                        {
                            absolute_gap = (sol_admissible - best_sol_adm_miqcp_s_bb);
                            relative_gap = absolute_gap/best_sol_adm_miqcp_s_bb;
                            v_abs_ref(&relative_gap);
                            if (relative_gap  > EPS_BB )
                                insert_liste(liste,sol_admissible, x_y, new_bab_2, cqp->nb_col);
                        }
                    }
                }

            }
            else
            {
                new_bab_1 = update_variables_cont_branch_4(*liste->first->bab, x_y,i,j,cqp);
                new_bab_2 = update_variables_cont_branch_5(*liste->first->bab, x_y,i,j,cqp);
                suppress_first_liste(liste);
                if (x_y[*i] <  new_bab_1.u[*i])
                    x_inf_new_u=1;
                else
                    x_inf_new_u=0;

                if (test_bound_miqcp(new_bab_1,cqp->nb_col))
                {
                    if (x_inf_new_u)
                        status=eval_s_miqcrq_b(new_bab_1, file_save, &x_y, &sol_admissible,0);
                    else
                        status =eval_s_miqcrq_b(new_bab_1, file_save, &x_y, &sol_admissible,1);
                    if (status ==1)
                    {
                        if (best_sol_adm_miqcp_s_bb > sol_admissible)
                        {
                            absolute_gap = (sol_admissible - best_sol_adm_miqcp_s_bb);
                            relative_gap = absolute_gap/best_sol_adm_miqcp_s_bb;
                            v_abs_ref(&relative_gap);
                            if (relative_gap  > EPS_BB )
                                insert_liste(liste,sol_admissible, x_y, new_bab_1, cqp->nb_col);
                            else if (DEBUG ==1)
                                printf("\n  branche 4 : on insere pas\n");
                        }
                    }
                }

                if (test_bound_miqcp(new_bab_2,cqp->nb_col))
                {
                    if  (x_inf_new_u)
                        status = eval_s_miqcrq_b(new_bab_2, file_save, &x_y,& sol_admissible,1);
                    else
                        status = eval_s_miqcrq_b(new_bab_2, file_save, &x_y,& sol_admissible,0);
                    if (status ==1)
                    {
                        if (best_sol_adm_miqcp_s_bb > sol_admissible)
                        {
                            absolute_gap = (sol_admissible - best_sol_adm_miqcp_s_bb);
                            relative_gap = absolute_gap/best_sol_adm_miqcp_s_bb;
                            v_abs_ref(&relative_gap);
                            if (relative_gap  > EPS_BB )
                                insert_liste(liste,sol_admissible, x_y, new_bab_2, cqp->nb_col);
                            else if (DEBUG ==1)
                                printf("\n branche 5 : on insere pas\n");
                        }
                    }
                }
            }
        }
        else
        {
            fx =sum_ij_qi_qj_x_ij(cqp->q, x_y, cqp->n);
            fx =fx + sum_i_ci_x_i(cqp->c, x_y, cqp->n);

            if ((best_sol_adm_miqcp_s_bb > fx) && (borne_inf_bb < fx))
            {
                best_sol_adm_miqcp_s_bb =  fx;
                for(k=0; k<cqp->n; k++)
                    cqp->local_sol[k]= x_y[k];

                if (nb_int_node ==1)
                    fprintf(file_save,"\nnode %d\t\tUB* (bb leaf): %6lf\t\tLB: %6lf\t\trelative_gap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, sol_admissible,relative_gap,time(NULL)-start_time);
                else
                    fprintf(file_save,"\nnode %d\t\tUB* (bb leaf): %6lf\t\tLB: %6lf\t\trelative_gap: %6lf\t\ttime:%3lf",nb_int_node,best_sol_adm_miqcp_s_bb, borne_inf_bb,relative_gap,time(NULL)-start_time);

                if (DEBUG ==1)
                {
                    printf("\n node %d : local solution (bb leaf) update best sol adm : %6lf \n",nb_int_node,best_sol_adm_miqcp_s_bb);
                    printf ("\n\n\n");
                    printf ("Best inf bound at node %d : %lf \n\n", nb_int_node, borne_inf_bb);
                    ind=cqp->n;
                    for(k=0; k<cqp->n; k++)
                        printf ("x_y[%d] : %.2lf ",k+1,x_y[k]);
                    printf ("\n");
                    for(m=0; m<cqp->n; m++)
                    {
                        for(p=m; p<cqp->n; p++)
                            if (!Zero_BB(cqp->nb_var_y[ij2k(m,p,cqp->n)]))
                            {
                                printf ("y[%d,%d] : %.2lf ",m+1,p+1,x_y[ind]);
                                ind++;
                            }
                        printf ("\n");
                    }
                    printf ("\n");

                    printf ("\nl\n");

                }
            }

            suppress_first_liste(liste);
            if (liste != NULL)
                clean_liste(liste, best_sol_adm_miqcp_s_bb);
        }
    }

    fprintf (file_save,"\n\nEnd of branch-and-bound, relative precision criterion satisfied\n");
}





