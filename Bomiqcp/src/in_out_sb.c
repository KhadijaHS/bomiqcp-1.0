/* -*-c-*-
 *
 *    Source     $RCSfile: in_out_sb.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors     Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>

#include<unistd.h>
#include<fcntl.h>

#include "in_out_sb.h"
#include"utilities.h"
#include"quad_prog.h"


/********************************************************************************/
/**************************** Write data file for Sb ****************************/
/********************************************************************************/

void write_matrix_q_SB(FILE * temp,double * mat,int t)
{
    int i,j;
    for(i=1; i<t+1; i++)
    {
        for(j=i; j<t+1; j++)
            if(mat[ij2k(i-1,j-1,t)]!=0)
                fprintf(temp,"%d %d %lf\n",i,j,(float)-mat[ij2k(i-1,j-1,t)]);
    }
}

void write_vector_c_SB(FILE * temp,double * vect,int t)
{
    int i;
    for(i=1; i<t+1; i++)
        if(vect[i-1]!=0)
            fprintf(temp,"0 %d %lf\n",i,(float)-vect[i-1]/2);
}

void write_matrix_q_sdp_SB(FILE * temp,double * mat,int t)
{
    int i,j;
    for(i=1; i<t+1; i++)
    {
        for(j=i; j<t+1; j++)
            if(mat[ij2k(i-1,j-1,t)]!=0)
                fprintf(temp,"%d %d %lf\n",i,j,mat[ij2k(i-1,j-1,t)]);
    }
}

void write_vector_c_sdp_SB(FILE * temp,double * vect,int t)
{
    int i;
    for(i=1; i<t+1; i++)
        if(vect[i-1]!=0)
            fprintf(temp,"0 %d %lf\n",i,vect[i-1]/2);
}


/***************************************************************************/
/************************ Write data file for QCR **************************/
/***************************************************************************/

void write_file_sb_qcr(MIQCP qp)
{

    FILE *fsdqp;
    int h,hc,hbis,nb_cont,i,j,k,lim, nb_non_zero;
    double * alpha;

    double temp;

    h = nb_non_zero_matrix_sym(qp->q,qp->n);
    hbis = h +  nb_non_zero_vector(qp->c,qp->n);

    fsdqp=fopen(data_sb,"w");
    fprintf(fsdqp,"%d\n",qp->n+3);
    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d ",qp->n+2);
    fprintf(fsdqp,"%d\n",hbis);

    write_matrix_q_SB(fsdqp,qp->q,qp->n);
    write_vector_c_SB(fsdqp,qp->c,qp->n);

    nb_cont=qp->n+1;
    if(!Zero(qp->m))
        nb_cont= nb_cont+1+qp->m;

    if(!Zero(qp->p))
        nb_cont= nb_cont+qp->p;

    fprintf(fsdqp,"%d\n", nb_cont);

    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+2,qp->n+2);

        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];

        for (i=0; i<qp->n+2; i++)
            for (j=0; j<qp->n+2; j++)
                alpha[ij2k(i,j,qp->n+2)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+2)]= alpha[ij2k(0,j+1,qp->n+2)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+2)]=alpha[ij2k(j+1,k+1,qp->n+2)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        nb_non_zero= nb_non_zero_matrix_sym(alpha,qp->n+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+2, nb_non_zero);
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(!Zero(alpha[ij2k(j,k,qp->n+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alpha[ij2k(j,k,qp->n+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }


    /* Xii = xi*/
    for (i=0; i<qp->n; i++)
    {
        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d 2\n", qp->n+2);
        fprintf(fsdqp,"0 %d -0.5\n", i+1);
        fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
        fprintf(fsdqp,"= 0\n");
    }

    /*Ax=b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->a[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->b[i]);
        }

    /*Dx <= e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
            for(j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->d[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"< ");
            fprintf(fsdqp,"%lf\n",qp->e[i]);
        }

    /* X_11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", qp->n+2);
    fprintf(fsdqp,"= 1\n");
    fclose(fsdqp);

    if(!Zero(qp->m))
        free_vector_d(alpha);
}


/***************************************************************************/
/************************ Write data file for CQCR *************************/
/***************************************************************************/
int valeur_ater(int n, double *u)
{
    int i;
    int a_sb=0;
    for(i=0; i<n; i++)
        a_sb+=(int)u[i]*(int)u[i];
    if(n<=10)
        return a_sb+10;
    else
        return 4*a_sb/5+10;

}


void write_file_sb_cqcr(MIQCP qp)
{

    FILE *fsdqp,*a_sb;
    int h,hc,hbis,nb_cont,i,j,k,l,a,nb_non_zero;
    double tp;
    double * alpha;

    double temp;

    h = nb_non_zero_matrix_sym(qp->q,qp->n);
    hc=nb_non_zero_vector(qp->c,qp->n);
    hbis = h + hc;

    fsdqp=fopen(data_sb,"w");

    a= valeur_ater(qp->n,qp->u);

    fprintf(fsdqp,"%d\n",a);

    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d %d\n",qp->n+2,hbis);

    write_matrix_q_SB(fsdqp,qp->q,qp->n);
    write_vector_c_SB(fsdqp,qp->c,qp->n);

    int cpt =0;
    for(i=0; i<qp->n; i++)
        if (qp->u[i] !=1)
            cpt++;

    nb_cont=4*cpt + qp->n-cpt +1;

    if(!Zero(qp->m))
        nb_cont+=1+qp->m;

    if(!Zero(qp->p))
        nb_cont+=qp->p;

    fprintf(fsdqp,"%d\n", nb_cont);



    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+2,qp->n+2);

        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];

        for (i=0; i<qp->n+2; i++)
            for (j=0; j<qp->n+2; j++)
                alpha[ij2k(i,j,qp->n+2)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+2)]= alpha[ij2k(0,j+1,qp->n+2)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+2)]=alpha[ij2k(j+1,k+1,qp->n+2)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        nb_non_zero= nb_non_zero_matrix_sym(alpha,qp->n+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+2, nb_non_zero);
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(!Zero(alpha[ij2k(j,k,qp->n+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alpha[ij2k(j,k,qp->n+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /* Xii<= uixi (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d %d 1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*  - Xii <= 0 (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", qp->n+2);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"< 0\n");
        }

    /* - Xii <= -2uixi + ui� (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 3\n", qp->n+2);
            fprintf(fsdqp,"0 0 %lf\n",- qp->u[i]*qp->u[i]);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)qp->u[i]);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*  - Xii <= -xi (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d 0.5\n",i+1);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*X_ii = x_i (for binary variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]==1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d -0.5\n", i+1);
            fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
            fprintf(fsdqp,"= 0\n");
        }

    /*Ax=b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->a[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->b[i]);
        }

    /*Dx <= e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
            for(j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->d[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"< ");
            fprintf(fsdqp,"%lf\n",qp->e[i]);
        }

    /*X_11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", qp->n+2);
    fprintf(fsdqp,"= 1\n");

    if (!Zero(qp->m))
        free_vector_d(alpha);

    fclose(fsdqp);
}


/***************************************************************************/
/************************ Write data file for IQCR *************************/
/***************************************************************************/

int calc_a(int n,double * u)
{
    int i,a_sb=0;

    int ecart= (int)maximum_d(u,n);
    double ecart2= (int) maximum_d(u,n) + 1;

    for(i=0; i<n; i++)
        a_sb+=ecart2*ecart2;

    return (int)a_sb*ecart/(ecart2*2);

}


void write_file_sb_iqcr(MIQCP qp)
{

    FILE *fsdqp;
    FILE *a_sb;
    int cont,h,hc,hbis,nb_cont,i,j,k,l,lim,nb;
    int tp;
    int a;
    double temp;
    int nb_non_zero;
    double * alpha;

    int is_0_1=1;

    for (i=0; i<qp->n; i++)
        if(qp->u[i]!=1)
        {
            is_0_1=0;
            break;
        }

    h = nb_non_zero_matrix_sym(qp->q,qp->n);
    hc=nb_non_zero_vector(qp->c,qp->n);
    hbis = h + hc;

    fsdqp=fopen(data_sb,"w");

    if (is_0_1 = 1)
        a= qp->n+3;
    else
        a=calc_a(qp->n,qp->u);

    fprintf(fsdqp,"%d\n",a);

    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d %d\n",qp->n+2,hbis);

    write_matrix_q_SB(fsdqp,qp->q,qp->n);
    write_vector_c_SB(fsdqp,qp->c,qp->n);

    int cpt =0;
    for(i=0; i<qp->n; i++)
        if (qp->u[i] !=1)
            cpt++;

    cont=0;
    /*number of constraints without diagonal terms*/
    for(i=qp->n-1; i>=0; i--)
        cont=cont+i;

    nb_cont=4*cont +1+ 4*cpt + qp->n-cpt +qp->n; /* qp->n ajoute pour test k cluster*/

    if(!Zero(qp->m))
        nb_cont=nb_cont+1+qp->m;

    if(!Zero(qp->p))
        nb_cont=nb_cont+qp->p;

    fprintf(fsdqp,"%d\n", nb_cont);


    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+2,qp->n+2);

        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];

        for (i=0; i<qp->n+2; i++)
            for (j=0; j<qp->n+2; j++)
                alpha[ij2k(i,j,qp->n+2)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+2)]= alpha[ij2k(0,j+1,qp->n+2)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+2)]=alpha[ij2k(j+1,k+1,qp->n+2)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        nb_non_zero= nb_non_zero_matrix_sym(alpha,qp->n+2);
        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+2, nb_non_zero);
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(!Zero(alpha[ij2k(j,k,qp->n+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alpha[ij2k(j,k,qp->n+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /* Xij<= ujxi (for i< j) */
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)-qp->u[j]/2);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* Xij<=uixj (for i< j) */
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d %lf\n",j+1,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* -Xij <= -uixj - ujxi +ujui (for i< j) */
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 4\n", qp->n+2);
            fprintf(fsdqp,"0 0 %lf\n",- qp->u[j]*qp->u[i]);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)qp->u[j]/2);
            fprintf(fsdqp,"0 %d %lf\n",j+1,(double)qp->u[i]/2);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"< 0\n");
        }

    /*- Xij <= 0 (for i< j) */
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", qp->n+2);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }
    /*ajout pour le k-cluster*/
    /* sum_i (i<>j) Xij - (k-1) x_j = 0  (for i< j) */
    for (j=0; j<qp->n; j++)
    {
        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
        fprintf(fsdqp,"0 %d %lf\n", j+1, -qp->b[0] +1);
        for(i=0; i<qp->n; i++)
            if (i !=j)
                fprintf(fsdqp,"%d %d 1.0\n",j+1,i+1);
        fprintf(fsdqp,"= 0\n");
    }


    /*for i = j and x integer variable */
    /* Xii<= uixi (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d %d 1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*  - Xii <= 0 (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", qp->n+2);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"< 0\n");
        }

    /* - Xii <= -2uixi + ui� (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 3\n", qp->n+2);
            fprintf(fsdqp,"0 0 %lf\n",- qp->u[i]*qp->u[i]);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)qp->u[i]);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*  - Xii <= -xi (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d 0.5\n",i+1);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*X_ii = x_i (for binary variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]==1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d -0.5\n", i+1);
            fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
            fprintf(fsdqp,"= 0\n");
        }

    /*Ax=b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->a[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->b[i]);
        }

    /*Dx <= e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
            for(j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->d[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"< ");
            fprintf(fsdqp,"%lf\n",qp->e[i]);
        }

    /* X_11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", qp->n+2);
    fprintf(fsdqp,"= 1\n");

    if (!Zero(qp->m))
        free_vector_d(alpha);

    fclose(fsdqp);
}


/***************************************************************************/
/************************ Write data file for MIQCR ************************/
/***************************************************************************/


void write_file_sb_miqcr(MIQCP qp)
{

    FILE *fsdqp;
    FILE *a_sb;
    int cont,h,hc,hbis,nb_cont,i,j,k,l,nb_non_zero;
    int tp;
    int a;
    double * alpha;
    double * alphabis;
    double temp;

    h = nb_non_zero_matrix_sym(qp->q,qp->n+qp->p);
    hc=nb_non_zero_vector(qp->c,qp->n);
    hbis = h + hc;

    fsdqp=fopen(data_sb,"w");

    a=qp->n+qp->p+2;

    fprintf(fsdqp,"%d\n",a);

    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d %d\n",qp->n+qp->p+2,hbis);

    write_matrix_q_SB(fsdqp,qp->q,qp->n);
    write_vector_c_SB(fsdqp,qp->c,qp->n);

    cont=0;

    int cpt =0;
    for(i=0; i<qp->nb_int; i++)
        if (qp->u[i] !=1)
            cpt++;

    /*number of constraints without diagonal terms*/
    for(i=qp->n+qp->p-1; i>=qp->n+qp->p-qp->nb_int; i--)
        cont=cont+i;

    nb_cont=4*cont +1+2*(qp->n - qp->nb_int + qp->p) + 4 *cpt + qp->nb_int-cpt;

    if(!Zero(qp->m))
        nb_cont=nb_cont+1+qp->m;

    if(!Zero(qp->p))
        nb_cont=nb_cont+1+qp->p;

    fprintf(fsdqp,"%d\n", nb_cont);

    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+2,qp->n+2);

        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];

        for (i=0; i<qp->n+2; i++)
            for (j=0; j<qp->n+2; j++)
                alpha[ij2k(i,j,qp->n+2)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+2)]= alpha[ij2k(0,j+1,qp->n+2)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+2)]=alpha[ij2k(j+1,k+1,qp->n+2)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        nb_non_zero= nb_non_zero_matrix_sym(alpha,qp->n+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+2, nb_non_zero);
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(!Zero(alpha[ij2k(j,k,qp->n+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alpha[ij2k(j,k,qp->n+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /*d�X -2dex +e�*/
    if(!Zero(qp->p))
    {
        alphabis = alloc_matrix_d(qp->n+qp->p+2,qp->n+qp->p+2);

        temp=0;
        for (i=0; i<qp->p; i++)
            temp=temp + qp->e[i]*qp->e[i];

        for (i=0; i<qp->n+qp->p+2; i++)
            for (j=0; j<qp->n+qp->p+2; j++)
                alphabis[ij2k(i,j,qp->n+qp->p+2)]=0;

        for (j=0; j<qp->n+qp->p; j++)
            for (i=0; i<qp->p; i++)
                alphabis[ij2k(0,j+1,qp->n+qp->p+2)]= alphabis[ij2k(0,j+1,qp->n+qp->p+2)] - qp->e[i]*qp->d[ij2k(i,j,qp->n+qp->p)];

        for (j=0; j<qp->n+qp->p; j++)
            for(k=j; k<qp->n+qp->p; k++)
                for (i=0; i<qp->p; i++)
                    alphabis[ij2k(j+1,k+1,qp->n+qp->p+2)]=alphabis[ij2k(j+1,k+1,qp->n+qp->p+2)] + qp->d[ij2k(i,j,qp->n+qp->p)]*qp->d[ij2k(i,k,qp->n+qp->p+2)];

        nb_non_zero= nb_non_zero_matrix_sym(alphabis,qp->n+qp->p+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+qp->p+2, nb_non_zero);
        for(j=0; j<qp->n+qp->p+1; j++)
            for(k=j; k<qp->n+qp->p+1; k++)
                if(!Zero(alphabis[ij2k(j,k,qp->n+qp->p+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alphabis[ij2k(j,k,qp->n+qp->p+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /*produits de variables entieres par continues */
    /* Xij<= ujxi (for i< j) */
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n+qp->p; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+qp->p+2);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)-qp->u[j]/2);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* Xij<=uixj (for i< j) */
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n+qp->p; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+qp->p+2);
            fprintf(fsdqp,"0 %d %lf\n",j+1,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* -Xij <= -uixj - ujxi +ujui (for i< j) */
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n+qp->p; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 4\n", qp->n+qp->p+2);
            fprintf(fsdqp,"0 0 %lf\n",- qp->u[j]*qp->u[i]);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)qp->u[j]/2);
            fprintf(fsdqp,"0 %d %lf\n",j+1,(double)qp->u[i]/2);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"< 0\n");
        }

    /*- Xij <= 0 (for i< j) */
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n+qp->p; j++)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", qp->n+qp->p+2);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /*for i = j and x integer variable */
    /* Xii<= uixi (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)-qp->u[i]/2);
            fprintf(fsdqp,"%d %d 1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*  - Xii <= 0 (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", qp->n+2);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"< 0\n");
        }

    /* - Xii <= -2uixi + ui� (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 3\n", qp->n+2);
            fprintf(fsdqp,"0 0 %lf\n",- qp->u[i]*qp->u[i]);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)qp->u[i]);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*  - Xii <= -xi (for integer variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d 0.5\n",i+1);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*X_ii = x_i (for binary variables) */
    for (i=0; i<qp->n; i++)
        if (qp->u[i]==1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d -0.5\n", i+1);
            fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
            fprintf(fsdqp,"= 0\n");
        }

    /*Ax=b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+qp->p+2, qp->n);
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->a[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->b[i]);
        }

    /*Dx <= e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+qp->p+2,qp->n+qp->p);
            for(j=0; j<qp->n+qp->p; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->d[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->e[i]);
        }

    /* X_11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", qp->n+qp->p+2);
    fprintf(fsdqp,"= 1\n");

    if (!Zero(qp->m))
        free_vector_d(alpha);
    if (!Zero(qp->p))
        free_vector_d(alphabis);

    fclose(fsdqp);

}



/***************************************************************************/
/************************ Write data file for EQCR *************************/
/***************************************************************************/


void write_file_sb_eqcr(MIQCP qp)
{

    FILE *fsdqp;
    FILE *a_sb;
    int h,hc,hbis,nb_cont,i,j,k,l,lim,nb,nb_non_zero;
    int tp;
    int a;
    double * alpha;
    double temp;

    h = nb_non_zero_matrix_sym(qp->q,qp->n);
    hc=nb_non_zero_vector(qp->c,qp->n);
    hbis = h + hc;

    fsdqp=fopen(data_sb,"w");

    a= qp->n +2;

    fprintf(fsdqp,"%d\n",a);

    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d %d\n",qp->n+2,h);

    write_matrix_q_SB(fsdqp,qp->q,qp->n);

    nb_cont=4*(qp->n*(qp->n -1)/2) + qp->n +1;

    if (qp->m >0)
        nb_cont = nb_cont + qp->m +1;

    if (qp->m >0)
        nb_cont= nb_cont + qp->p;

    fprintf(fsdqp,"%d\n", nb_cont);

    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+2,qp->n+2);

        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];

        for (i=0; i<qp->n+2; i++)
            for (j=0; j<qp->n+2; j++)
                alpha[ij2k(i,j,qp->n+2)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+2)]= alpha[ij2k(0,j+1,qp->n+2)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n+2; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+2)]=alpha[ij2k(j+1,k+1,qp->n+2)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        nb_non_zero= nb_non_zero_matrix_sym(alpha,qp->n+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+2, nb_non_zero);
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(!Zero(alpha[ij2k(j,k,qp->n+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alpha[ij2k(j,k,qp->n+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /*X_ii = x_i*/
    for (i=0; i<qp->n; i++)
    {
        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d 2\n", qp->n+2);
        fprintf(fsdqp,"0 %d -0.5\n", i+1);
        fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
        fprintf(fsdqp,"= 0\n");

    }

    /* Xij<= xi*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d -0.5\n",i+1);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* Xij<=xj*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d -0.5\n",j+1);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* -Xij <= -xj - xi +1*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 4\n", qp->n+2);
            fprintf(fsdqp,"0 0 -1\n");
            fprintf(fsdqp,"0 %d 0.5\n",i+1);
            fprintf(fsdqp,"0 %d 0.5\n",j+1);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"< 0\n");
        }

    /*- Xij <= 0*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", qp->n+2);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /*Ax=b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->a[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->b[i]);
        }

    /*Dx <= e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+2, qp->n);
            for(j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->d[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"< ");
            fprintf(fsdqp,"%lf\n",qp->e[i]);
        }

    /* X_11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", qp->n+2);
    fprintf(fsdqp,"= 1\n");

    if (!Zero(qp->m))
        free_vector_d(alpha);

    fclose(fsdqp);
}


/***************************************************************************/
/************************ Write data file MAXCUT ***************************/
/***************************************************************************/

void write_file_sb_max_cut(MIQCP qp)
{

    FILE *fsdqp;
    FILE *a_sb;
    int temp,h,hc,hbis,nb_cont,i,j,k,l,lim,nb;
    int tp;
    int a;

    h = nb_non_zero_matrix_sym(qp->q,qp->n);

    fsdqp=fopen(data_sb,"w");

    a= qp->n +1;

    fprintf(fsdqp,"%d\n",a);

    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d %d\n",qp->n+2,h);

    write_matrix_q_SB(fsdqp,qp->q,qp->n);

    nb_cont=4*qp->n*(qp->n -1)/2 + qp->n +1 ;

    fprintf(fsdqp,"%d\n", nb_cont);

    /*X_ii = x_i*/
    for (i=0; i<qp->n; i++)
    {
        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d 2\n", qp->n+2);
        fprintf(fsdqp,"0 %d -0.5\n", i+1);
        fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
        fprintf(fsdqp,"= 0\n");
    }

    /* Xij<= xi*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d -0.5\n",i+1);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* Xij<=xj*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+2);
            fprintf(fsdqp,"0 %d -0.5\n",j+1);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* -Xij <= -xj - xi +1*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 4\n", qp->n+2);
            fprintf(fsdqp,"0 0 -1\n");
            fprintf(fsdqp,"0 %d 0.5\n",i+1);
            fprintf(fsdqp,"0 %d 0.5\n",j+1);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"< 0\n");
        }

    /*- Xij <= 0*/
    for (i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", qp->n+2);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* X_11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", qp->n+2);
    fprintf(fsdqp,"= 1\n");

    fclose(fsdqp);
}


/***************************************************************************/
/************************ Write data file for MEQCR ************************/
/***************************************************************************/

void write_file_sb_meqcr(MIQCP qp)
{

    FILE *fsdqp;
    FILE *a_sb;
    int cont,h,hc,hbis,nb_cont,i,j,k,l,lim,nb,nb_non_zero;
    int tp;
    int a;
    double * alpha;
    double * alphabis;
    double temp;

    h = nb_non_zero_matrix_sym(qp->q,qp->n);
    hc=nb_non_zero_vector(qp->c,qp->n);
    hbis = h + hc;

    fsdqp=fopen(data_sb,"w");

    a=qp->n+qp->p+2;

    fprintf(fsdqp,"%d\n",a);

    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d %d\n",qp->n+qp->p+2,hbis);

    write_matrix_q_SB(fsdqp,qp->q,qp->n);

    write_vector_c_SB(fsdqp,qp->c,qp->n);

    cont=0;
    for(i=qp->n+qp->p-1; i>qp->n+qp->p-1 - qp->nb_int; i--)
        cont=cont+i;

    nb_cont=4*cont +1+qp->nb_int + 2*(qp->n+qp->p -qp->nb_int);

    if(!Zero(qp->m))
        nb_cont=nb_cont+1+qp->m;

    if(!Zero(qp->p))
        nb_cont=nb_cont+1+qp->p;

    fprintf(fsdqp,"%d\n", nb_cont);
    /*a�X -2abx +b�*/
    if(!Zero(qp->m))
    {
        alpha = alloc_matrix_d(qp->n+2,qp->n+2);

        temp=0;
        for (i=0; i<qp->m; i++)
            temp=temp + qp->b[i]*qp->b[i];

        for (i=0; i<qp->n+2; i++)
            for (j=0; j<qp->n+2; j++)
                alpha[ij2k(i,j,qp->n+2)]=0;

        for (j=0; j<qp->n; j++)
            for (i=0; i<qp->m; i++)
                alpha[ij2k(0,j+1,qp->n+2)]= alpha[ij2k(0,j+1,qp->n+2)] - qp->b[i]*qp->a[ij2k(i,j,qp->n)];

        for (j=0; j<qp->n; j++)
            for(k=j; k<qp->n+2; k++)
                for (i=0; i<qp->m; i++)
                    alpha[ij2k(j+1,k+1,qp->n+2)]=alpha[ij2k(j+1,k+1,qp->n+2)] + qp->a[ij2k(i,j,qp->n)]*qp->a[ij2k(i,k,qp->n)];

        nb_non_zero= nb_non_zero_matrix_sym(alpha,qp->n+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+2, nb_non_zero);
        for(j=0; j<qp->n+1; j++)
            for(k=j; k<qp->n+1; k++)
                if(!Zero(alpha[ij2k(j,k,qp->n+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alpha[ij2k(j,k,qp->n+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /*d�X -2dex +e�*/
    if(!Zero(qp->p))
    {
        alphabis = alloc_matrix_d(qp->n+qp->p+2,qp->n+qp->p+2);

        temp=0;
        for (i=0; i<qp->p; i++)
            temp=temp + qp->e[i]*qp->e[i];

        for (i=0; i<qp->n+qp->p+2; i++)
            for (j=0; j<qp->n+qp->p+2; j++)
                alphabis[ij2k(i,j,qp->n+qp->p+2)]=0;

        for (j=0; j<qp->n+qp->p; j++)
            for (i=0; i<qp->p; i++)
                alphabis[ij2k(0,j+1,qp->n+qp->p+2)]= alphabis[ij2k(0,j+1,qp->n+qp->p+2)] - qp->e[i]*qp->d[ij2k(i,j,qp->n+qp->p)];

        for (j=0; j<qp->n+qp->p; j++)
            for(k=j; k<qp->n+qp->p; k++)
                for (i=0; i<qp->p; i++)
                    alphabis[ij2k(j+1,k+1,qp->n+qp->p+2)]=alphabis[ij2k(j+1,k+1,qp->n+qp->p+2)] + qp->d[ij2k(i,j,qp->n+qp->p)]*qp->d[ij2k(i,k,qp->n+qp->p+2)];

        nb_non_zero= nb_non_zero_matrix_sym(alphabis,qp->n+qp->p+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", qp->n+qp->p+2, nb_non_zero);
        for(j=0; j<qp->n+qp->p+1; j++)
            for(k=j; k<qp->n+qp->p+1; k++)
                if(!Zero(alphabis[ij2k(j,k,qp->n+qp->p+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alphabis[ij2k(j,k,qp->n+qp->p+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /*X_ii = x_i*/
    for (i=0; i<qp->nb_int; i++)
    {
        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d 2\n", qp->n+qp->p+2);
        fprintf(fsdqp,"0 %d -0.5\n", i+1);
        fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
        fprintf(fsdqp,"= 0\n");
    }

    /* Xij<= xi*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n+qp->p; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+qp->p+2);
            fprintf(fsdqp,"0 %d -0.5\n",i+1);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* Xij<=xj*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n+qp->p; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", qp->n+qp->p+2);
            fprintf(fsdqp,"0 %d -0.5\n",j+1);
            fprintf(fsdqp,"%d %d 0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /* -Xij <= -xj - xi +1*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n+qp->p; j++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 4\n", qp->n+qp->p+2);
            fprintf(fsdqp,"0 0 %lf\n",- qp->u[j]*qp->u[i]);
            fprintf(fsdqp,"0 %d 0.5\n",i+1);
            fprintf(fsdqp,"0 %d 0.5\n",j+1);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"< 0\n");
        }

    /*- Xij <= 0*/
    for (i=0; i<qp->nb_int; i++)
        for(j=i+1; j<qp->n+qp->p; j++)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", qp->n+qp->p+2);
            fprintf(fsdqp,"%d %d -0.5\n",i+1,j+1);
            fprintf(fsdqp,"<0\n");
        }

    /*Ax=b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+qp->p+2, qp->n);
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->a[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->b[i]);
        }

    /*Dx <= e */
    if(!Zero(qp->p))
        for (i=0; i<qp->p; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", qp->n+qp->p+2, qp->n+qp->p);
            for(j=0; j<qp->n+qp->p; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->d[ij2k(i,j,qp->n+qp->p)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->e[i]);
        }

    /*x_i <= 1 */
    for (i=qp->nb_int; i<qp->n+qp->p; i++)
    {
        fprintf(fsdqp,"SINGLETON\n");
        fprintf(fsdqp,"%d ", qp->n+qp->p+2);
        fprintf(fsdqp,"0 %d 0.5\n",i+1);
        fprintf(fsdqp,"< %lf\n",qp->u[i]);
    }

    /* x_i >= 0 */
    for (i=qp->nb_int; i<qp->n+qp->p; i++)
    {
        fprintf(fsdqp,"SINGLETON\n");
        fprintf(fsdqp,"%d ", qp->n+qp->p+2);
        fprintf(fsdqp,"0 %d 0.5\n",i+1);
        fprintf(fsdqp,">0\n");
    }

    /* X_11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", qp->n+qp->p+2);
    fprintf(fsdqp,"= 1\n");

    fclose(fsdqp);

    if (!Zero(qp->m))
        free_vector_d(alpha);
    if (!Zero(qp->p))
        free_vector_d(alphabis);
}


/***************************************************************************/
/************************ Write data file for QAP   ************************/
/***************************************************************************/
void write_file_sb_qap(MIQCP qp)
{

    FILE *fsdqp;
    FILE *a_sb;
    int i,j,k;
    int dim;
    int nb_non_zero_obj;
    int nb_cont;
    int nb_non_zero;

    nb_non_zero_obj = nb_non_zero_matrix_sym(qp->q,qp->n) + nb_non_zero_vector(qp->c,qp->n);

    fsdqp=fopen(data_sb,"w");

    dim=qp->n+2;

    fprintf(fsdqp,"%d\n",dim);

    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d %d\n", dim,nb_non_zero_obj);

    write_matrix_q_SB(fsdqp,qp->q,qp->n);

    write_vector_c_SB(fsdqp,qp->c,qp->n);

    nb_cont=qp->m +qp->mq + qp->n; //+1;

    fprintf(fsdqp,"%d\n", nb_cont);
    /*x^TAqx = bq*/
    if(!Zero(qp->mq))
    {
        for (i=0; i<qp->mq-1; i++)
        {
            nb_non_zero= nb_non_zero_matrix_sym(&qp->aq[i*(qp->n+1)*(qp->n+1)],qp->n+1);

            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", dim, nb_non_zero);
            for(j=0; j<qp->n+1; j++)
                for(k=j; k<qp->n+1; k++)
                    if(!Zero(qp->aq[ijk2l(i,j,k,qp->n+1,qp->n+1)]))
                        fprintf(fsdqp,"%d %d %lf\n",j,k,qp->aq[ijk2l(i,j,k,qp->n+1,qp->n+1)]);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->bq[i]);
        }
    }

    /*X_ii = x_i*/
    for (i=0; i<qp->n; i++)
    {
        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d 2\n", dim);
        fprintf(fsdqp,"0 %d -0.5\n", i+1);
        fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
        fprintf(fsdqp,"= 0\n");
    }

    /*Ax=b*/
    if(!Zero(qp->m))
        for (i=0; i<qp->m; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", dim, qp->n);
            for (j=0; j<qp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,qp->a[ij2k(i,j,qp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",qp->b[i]);
        }


    /* X_11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", qp->n+qp->p+2);
    fprintf(fsdqp,"= 1\n");

    fclose(fsdqp);

}


/***************************************************************************/
/************************ Write data file for SDP solver *******************/
/***************************************************************************/

void write_file_sb_miqcr_solver(SDP psdp)
{
    FILE *fsdqp;
    FILE *a_sb;
    int cont,h,hc,hbis,nb_cont,i,j,k,l,lim,nb,nb_non_zero;
    int tp;
    int a;
    double * alpha;
    double * alphabis;
    double temp;

    double * q_beta;
    double * c_beta;
    double l_beta;

    dualized_objective_function(psdp, &q_beta, &c_beta, &l_beta);

    h = nb_non_zero_matrix_sym(q_beta,psdp->n+psdp->p);
    hc=nb_non_zero_vector(c_beta,psdp->n+psdp->p);
    hbis = h + hc;

    fsdqp=fopen(data_sb,"w");

    int is_0_1=1;

    for (i=0; i<psdp->n; i++)
        if(psdp->u[i]!=1)
        {
            is_0_1=0;
            break;
        }


    if (is_0_1 = 1)
        a= psdp->n+3;
    else
        a=calc_a(psdp->n +psdp->p,psdp->u);

    fprintf(fsdqp,"500\n");

    fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
    fprintf(fsdqp,"%d %d\n",psdp->n+psdp->p+2,hbis+1);

    write_matrix_q_sdp_SB(fsdqp,q_beta,psdp->n+psdp->p);
    fprintf(fsdqp,"0 0 %lf\n",l_beta);
    write_vector_c_sdp_SB(fsdqp,c_beta,psdp->n+psdp->p);

    int cpt =0;
    for(i=0; i<psdp->nb_int; i++)
        if (psdp->u[i] !=1)
            cpt++;

    nb_cont= 1+ 4*cpt + +2*(psdp->n + psdp->p - psdp->nb_int)+ psdp->nb_int-cpt;

    if(!Zero(psdp->m))
        nb_cont=nb_cont+1+psdp->m;

    if(!Zero(psdp->p))
        nb_cont=nb_cont+1+psdp->p;

    fprintf(fsdqp,"%d\n", nb_cont);

    /*a�X -2abx +b�*/
    if(!Zero(psdp->m))
    {
        alpha = alloc_matrix_d(psdp->n+2,psdp->n+2);

        temp=0;
        for (i=0; i<psdp->m; i++)
            temp=temp + psdp->b[i]*psdp->b[i];

        for (i=0; i<psdp->n+2; i++)
            for (j=0; j<psdp->n+2; j++)
                alpha[ij2k(i,j,psdp->n+2)]=0;

        for (j=0; j<psdp->n; j++)
            for (i=0; i<psdp->m; i++)
                alpha[ij2k(0,j+1,psdp->n+2)]= alpha[ij2k(0,j+1,psdp->n+2)] - psdp->b[i]*psdp->a[ij2k(i,j,psdp->n)];

        for (j=0; j<psdp->n; j++)
            for(k=j; k<psdp->n+2; k++)
                for (i=0; i<psdp->m; i++)
                    alpha[ij2k(j+1,k+1,psdp->n+2)]=alpha[ij2k(j+1,k+1,psdp->n+2)] + psdp->a[ij2k(i,j,psdp->n)]*psdp->a[ij2k(i,k,psdp->n)];

        nb_non_zero= nb_non_zero_matrix_sym(alpha,psdp->n+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", psdp->n+2, nb_non_zero);
        for(j=0; j<psdp->n+1; j++)
            for(k=j; k<psdp->n+1; k++)
                if(!Zero(alpha[ij2k(j,k,psdp->n+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alpha[ij2k(j,k,psdp->n+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /*d�X -2dex +e�*/
    if(!Zero(psdp->p))
    {
        alphabis = alloc_matrix_d(psdp->n+psdp->p+2,psdp->n+psdp->p+2);

        temp=0;
        for (i=0; i<psdp->p; i++)
            temp=temp + psdp->e[i]*psdp->e[i];

        for (i=0; i<psdp->n+psdp->p+2; i++)
            for (j=0; j<psdp->n+psdp->p+2; j++)
                alphabis[ij2k(i,j,psdp->n+psdp->p+2)]=0;

        for (j=0; j<psdp->n+psdp->p; j++)
            for (i=0; i<psdp->p; i++)
                alphabis[ij2k(0,j+1,psdp->n+psdp->p+2)]= alphabis[ij2k(0,j+1,psdp->n+psdp->p+2)] - psdp->e[i]*psdp->d[ij2k(i,j,psdp->n+psdp->p)];

        for (j=0; j<psdp->n+psdp->p; j++)
            for(k=j; k<psdp->n+psdp->p; k++)
                for (i=0; i<psdp->p; i++)
                    alphabis[ij2k(j+1,k+1,psdp->n+psdp->p+2)]=alphabis[ij2k(j+1,k+1,psdp->n+psdp->p+2)] + psdp->d[ij2k(i,j,psdp->n+psdp->p)]*psdp->d[ij2k(i,k,psdp->n+psdp->p+2)];

        nb_non_zero= nb_non_zero_matrix_sym(alpha,psdp->n+psdp->p+2);

        fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
        fprintf(fsdqp,"%d %d\n", psdp->n+psdp->p+2, nb_non_zero);
        for(j=0; j<psdp->n+psdp->p+1; j++)
            for(k=j; k<psdp->n+psdp->p+1; k++)
                if(!Zero(alphabis[ij2k(j,k,psdp->n+psdp->p+2)]))
                    fprintf(fsdqp,"%d %d %lf\n",j,k,alphabis[ij2k(j,k,psdp->n+psdp->p+2)]);
        fprintf(fsdqp,"= ");
        fprintf(fsdqp,"%lf\n",-temp);
    }

    /*Constraints that ensure the existence of a feasible solution*/
    /* Xii<= uixi (for integer variables) */
    for (i=0; i<psdp->n; i++)
        if (psdp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", psdp->n+2);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)-psdp->u[i]/2);
            fprintf(fsdqp,"%d %d 1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*  - Xii <= 0 (for integer variables) */
    for (i=0; i<psdp->n; i++)
        if (psdp->u[i]!=1)
        {
            fprintf(fsdqp,"SINGLETON\n");
            fprintf(fsdqp,"%d ", psdp->n+2);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"< 0\n");
        }

    /* - Xii <= -2uixi + ui� (for integer variables) */
    for (i=0; i<psdp->n; i++)
        if (psdp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 3\n", psdp->n+2);
            fprintf(fsdqp,"0 0 %lf\n",- psdp->u[i]*psdp->u[i]);
            fprintf(fsdqp,"0 %d %lf\n",i+1,(double)psdp->u[i]);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*  - Xii <= -xi (for integer variables) */
    for (i=0; i<psdp->n; i++)
        if (psdp->u[i]!=1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", psdp->n+2);
            fprintf(fsdqp,"0 %d 0.5\n",i+1);
            fprintf(fsdqp,"%d %d -1\n",i+1,i+1);
            fprintf(fsdqp,"<0\n");
        }

    /*X_ii = x_i (for binary variables) */
    for (i=0; i<psdp->n; i++)
        if (psdp->u[i]==1)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d 2\n", psdp->n+2);
            fprintf(fsdqp,"0 %d -0.5\n", i+1);
            fprintf(fsdqp,"%d %d 1.\n", i+1,i+1);
            fprintf(fsdqp,"= 0\n");
        }

    /*Ax=b*/
    if(!Zero(psdp->m))
        for (i=0; i<psdp->m; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", psdp->n+psdp->p+2, psdp->n);
            for (j=0; j<psdp->n; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,psdp->a[ij2k(i,j,psdp->n)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",psdp->b[i]);
        }

    /*Dx <= e */
    if(!Zero(psdp->p))
        for (i=0; i<psdp->p; i++)
        {
            fprintf(fsdqp,"SYMMETRIC_SPARSE\n");
            fprintf(fsdqp,"%d %d\n", psdp->n+psdp->p+2,psdp->n+psdp->p);
            for(j=0; j<psdp->n+psdp->p; j++)
                fprintf(fsdqp,"0 %d %lf\n",j+1,psdp->d[ij2k(i,j,psdp->n+psdp->p)]/2);
            fprintf(fsdqp,"= ");
            fprintf(fsdqp,"%lf\n",psdp->e[i]);
        }

    /*x_i <= u_i */
    for (i=psdp->nb_int; i<psdp->n+psdp->p; i++)
    {
        fprintf(fsdqp,"SINGLETON\n");
        fprintf(fsdqp,"%d ", psdp->n+psdp->p+2);
        fprintf(fsdqp,"0 %d 0.5\n",i+1);
        fprintf(fsdqp,"< %lf\n", (double)psdp->u[i]);
    }

    /* x_i >= 0 */
    for (i=psdp->nb_int; i<psdp->n+psdp->p; i++)
    {
        fprintf(fsdqp,"SINGLETON\n");
        fprintf(fsdqp,"%d ", psdp->n+psdp->p+2);
        fprintf(fsdqp,"0 %d 0.5\n",i+1);
        fprintf(fsdqp,">0\n");
    }

    /* X11 = 1*/
    fprintf(fsdqp,"SINGLETON\n");
    fprintf(fsdqp,"%d 0 0 1\n", psdp->n+psdp->p+2);
    fprintf(fsdqp,"= 1\n");

    if(!Zero(psdp->m))
        free_vector_d(alpha);
    if(!Zero(psdp->p))
        free_vector_d(alphabis);

    fclose(fsdqp);
}

