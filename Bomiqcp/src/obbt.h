/* -*-c-*-
 *
 *    Source     $RCSfile: obbt.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#ifndef OBBT_H_INCLUDED
#define OBBT_H_INCLUDED
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"quad_prog.h"

/*********************************************************************/
/****************************** Variable *****************************/
/*********************************************************************/
/*double best_sol_adm=0;*/

/*********************************************************************/
/****************************** Functions ****************************/
/*********************************************************************/
/*solve_with_obbt()*/
void presolve_with_obbt(C_MIQCP qp,int * nb_pass_var, int * nb_pass_cont,double upperbound, int nb_pass);
/*solve_with_obbt_bb()*/
void presolve_with_obbt_bb(C_MIQCP qp,struct miqcp_bab bab,int * nb_pass_var, int * nb_pass_cont, double upperbound,int nb_pass);

/*int  compute_cplex_obbt_var( C_MIQCP qp,int ind_var,int sense_opt,double upperbound) ;
  int compute_cplex_obbt_cont( C_MIQCP qp,int ind_cont, int sense_opt,double upperbound);*/

#endif // OBBT_H_INCLUDED
