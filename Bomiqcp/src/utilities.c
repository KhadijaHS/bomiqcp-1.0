/* -*-c-*-
 *
 *    Source     $RCSfile: utilities.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<time.h>

#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>

#include<unistd.h>
#include<fcntl.h>

#include"utilities.h"
#include"quad_prog.h"
#include"liste.h"
#include"in_out.h"
#include"in_out_scilab.h"
#include"quicksort.h"
#include"s_miqcrq_bb.h"
//#include"avl.h"

/*****************************************************************************/
/***************************** print tables **********************************/
/*****************************************************************************/
int NB_MAX_CONT;

void print_tab(char * tab, int n)
{
    int i;
    for (i=0; i<n*n; i++)
    {
        printf("%c ",tab[i]);
        if (i %n ==0)
            printf("\n");
    }
}

void print_vec(int * tab, int n)
{
    int i;
    for (i=0; i<n; i++)
        printf("%d ",tab[i]);
}


void print_vec_d(double * tab, int n)
{
    int i;
    for (i=0; i<n; i++)
        printf("%.12lf ",tab[i]);
}

void print_mat_d(double * tab, int n, int m)
{
    int i;
    for (i=0; i<m*n; i++)
    {
        printf("%.7lf ",tab[i]);
        if ((i+1) %n ==0)
            printf("\n");
    }
}

void print_mat_3d(double * tab, int n, int m,int l)
{
    int i,j,k;

    for (i=0; i<n; i++)
    {
        for(j=0; j<m; j++)
        {
            for(k=0; k<l; k++)
                printf("%.7lf ",tab[ijk2l(i,j,k,m,l)]);
            printf("\n");
        }
        printf("\n");
    }
}
void print_mat(int * tab, int n, int m)
{
    int i;
    for (i=0; i<m*n; i++)
    {
        printf("%d ",tab[i]);
        if ((i+1) %n ==0)
            printf("\n");
    }
}

/*****************************************************************************/
/*************************** allocate  memory ********************************/
/*****************************************************************************/
int * alloc_vector (int dimension)
{
    int* vector = (int *)calloc(dimension,sizeof(int));
    if (vector == NULL)
        exit(EXIT_FAILURE);
    return vector;
}

int * alloc_matrix (int line, int column)
{
    int i;
    int* matrix = (int *)calloc(line*column,sizeof(int));
    if (matrix == NULL)
        exit(EXIT_FAILURE);
    return matrix;
}

double* alloc_vector_d (int dimension)
{
    double* vector = (double*) calloc(dimension,sizeof(double));
    if (vector == NULL)
        exit(EXIT_FAILURE);
    return vector;
}

double * alloc_matrix_d (int line, int column)
{
    int i;
    double* matrix = (double*) calloc(line*column,sizeof(double));
    if (matrix == NULL)
        exit(EXIT_FAILURE);
    return matrix;
}

double * alloc_matrix_3d (int n, int m, int p)
{
    int i,j;
    double* matrix = (double *)calloc(n*m*p,sizeof(double));
    if (matrix == NULL)
        exit(EXIT_FAILURE);
    return matrix;
}

char* alloc_string (int dimension)
{
    char* string = (char *)malloc((dimension+1)*sizeof(char));
    if (string == NULL)
        exit(EXIT_FAILURE);
    string[dimension]='\0';
    return string;
}

char* alloc_tab (int dim1,int dim2)
{
    int i;
    char* string = (char *)malloc((dim1)*(dim2+1)*sizeof(char));
    if (string == NULL)
        exit(EXIT_FAILURE);
    for (i=0; i<dim1; i++)
    {
        string[ij2k(i,dim2-1,dim2)]='\0';
    }
    return string;
}

/*free memory*/
void free_vector(int * v)
{
    free(v);
}

void free_vector_d(double * v)
{
    free(v);
}

void free_string(char * string)

{
    free(string);
}


void free_node(Node node)
{
    free_vector_d(node->sol_x);

}

/* This simple routine frees up the pointer *ptr, and sets *ptr to NULL */
/*A passer dans utilities */
void free_and_null (char **ptr)
{
    if ( *ptr != NULL )
    {
        free (*ptr);
        *ptr = NULL;
    }
}

/*utilities*/
void copy_line(double * B, double * v, int i,int t)
{
    int j;
    for(j=0; j<t; j++)
        B[ij2k(i,j,t)] = v[j];
}

int * copy_vector(int *v, int n)
{
    int i;
    int * c = alloc_vector(n);
    for (i=0; i<n; i++)
        c[i] = v[i];
    return c;
}

double * copy_vector_d(double *v, int n)
{
    int i;
    double * c = alloc_vector_d(n);
    if(v!=NULL)
    {
        for (i=0; i<n; i++)
            c[i] = v[i];
    }
    else
    {
        for (i=0; i<n; i++)
            c[i] = 0;
    }
    return c;
}


double * copy_matrix_d(double *matrix, int m,int n)
{
    int i;

    double* new_matrix = (double *)malloc(m*n*sizeof(double));
    for (i=0; i<m*n; i++)
        new_matrix[i] = matrix[i];
    return new_matrix;
}

double * copy_matrix_3d(double *matrix, int m,int n, int p)
{
    int i;

    double* new_matrix = (double *)malloc(m*n*p*sizeof(double));
    for (i=0; i<m*n*p; i++)
        new_matrix[i] = matrix[i];
    return new_matrix;
}

char * copy_string(char *s)
{
    int i =0;
    char * a = alloc_string(25);
    while (s[i] != '\0')
    {
        a[i] = s[i];
        i++;
    }
    a[i]='\0';
    return a;
}

double v_abs(double a)
{
    if (Positive_BB(a))
        return a;
    return -a;

}

void v_abs_ref(double *a)
{
    if (Negative_BB(*a))
        *a = -(*a);
}

int sum_vector(int * a, int j,int n)
{
    int i;
    int b=0;
    for(i=j; i<j+n; i++)
        b+=a[i];
    return b;
}

double sum_vector_d(double * a, int j,int n)
{
    int i;
    double b=0;
    for(i=j; i<j+n; i++)
        b+=a[i];
    return b;
}

double * square_vector(double * v, int n)
{
    int i;
    double * r =alloc_vector_d(n);
    for(i=0; i<n; i++)
        r[i]=v[i]*v[i];
    return r;
}

int random_ind (int a, int b)
{

    return (rand() % (b-a) +a);
}

double random_double(double min, double max)
{
    //used it to generate new number each time
    srand( (unsigned int) time(NULL) );

    double randomNumber, range, tempRan, finalRan;
    //generate random number form 0.0 to 1.0
    randomNumber = (double)rand() / (double)RAND_MAX;
    //total range number from min to max eg. from -2 to 2 is 4
    //range used it to pivot form -2 to 2 -> 0 to 4 for next step
    range = max - min;
    //illustrate randomNumber to range
    //lets say that rand() generate 0.5 number, thats it the half
    //of 0.0 to 1.0, show multiple range with randomNumber we get the
    //half in range. eg 4 * 0.5 = 2
    tempRan = randomNumber * range;
    //add the min to tempRan to get the correct random in ours range
    //so in ours example we have: 2 + (-2) = 0, thats the half in -2 to 2
    finalRan = tempRan + min;
    return finalRan;
}


int maximum(int * vector,int n)
{
    int i;
    int max;
    max = vector[0];
    for(i=1; i<n; i++)
        if(vector[i] > max)
            max = vector[i];
    return max;
}

double maximum_d(double * vector,int n)
{
    int i;
    double max;
    max = vector[0];
    for(i=1; i<n; i++)
        if(vector[i] > max)
            max = vector[i];

    return max;
}

void minimum_d_0(double * vector,int n, int * ind, double *min)
{
    int i;
    *ind=0;
    *min = vector[0];
    for(i=1; i<n; i++)
        if(*min > vector[i])
        {
            *min = vector[i];
            *ind=i;
        }
    vector[*ind]=0;
}

void maximum_d_0(double * vector,int n, int * ind, double *max)
{
    int i;
    *ind=0;
    *max = vector[0];
    for(i=1; i<n; i++)
        if(vector[i]>*max)
        {
            *max = vector[i];
            *ind=i;
        }
    vector[*ind]=0;
}

double maximum_d_abs(double * vector,int n)
{
    int i;
    double max;
    double * v=malloc(n*sizeof(double));
    for(i=0; i<n; i++)
        if(Negative(vector[i]))
            v[i] = -vector[i];
        else
            v[i]=vector[i];
    max=maximum_d(v,n);
    return max;
}

double minimum_d(double * vector,int n)
{
    int i;
    double min;
    min = vector[0];
    for(i=1; i<n; i++)
        if(min > vector[i])
            min = vector[i];
    return min;
}

int minimum(int * vector,int n)
{
    int i;
    int min;
    min = vector[0];
    for(i=1; i<n; i++)
        if(min > vector[i])
            min = vector[i];
    return min;
}

double * sum_two_vector(double * v1, int * v2,int n)
{
    int i;
    for(i=1; i<n; i++)
        v1[i]=v1[i]+(double)v2[i];
    return v1;
}


int nb_non_zero_vector(double * v, int n)
{
    int i;
    int c = 0;
    for(i=0; i<n; i++)
        if(!Zero(v[i]))
            c++;
    return c;
}

int nb_non_zero_vector_int(int * v, int n)
{
    int i;
    int c = 0;
    for(i=0; i<n; i++)
        if(!Zero(v[i]))
            c++;
    return c;
}

int nb_non_zero_matrix_sym(double * a, int n)
{
    int i,j;
    int c = 0;
    for(i=0; i<n; i++)
        for(j=i; j<n; j++)
            if(!Zero(a[ij2k(i,j,n)]))
                c++;
    return c;
}

int nb_non_zero_matrix(double *a, int n, int m)
{
    int i,j;
    int c = 0;
    for(i=0; i<m; i++)
        for(j=0; j<n; j++)
            if(!Zero_BB(a[ij2k(i,j,n)]))
                c++;
    return c;
}

int nb_non_zero_matrix_i(int *a, int n, int m)
{
    int i,j;
    int c = 0;
    for(i=0; i<m; i++)
        for(j=0; j<n; j++)
            if(!Zero_BB(a[ij2k(i,j,n)]))
                c++;
    return c;
}

int nb_non_zero_matrix_sup_i(int *a, int n, int m)
{
    int i,j;
    int c = 0;
    for(i=0; i<m; i++)
        for(j=i; j<n; j++)
            if(!Zero_BB(a[ij2k(i,j,n)]))
                c++;
    return c;
}

int nb_non_zero_matrix_without_diag(double *a, int n, int m)
{
    int i,j;
    int c = 0;
    for(i=0; i<m; i++)
        for(j=0; j<n; j++)
            if (i!=j)
                if(!Zero_BB(a[ij2k(i,j,n)]))
                    c++;
    return c;
}

int nb_non_zero_matrix_without_diag_i(int *a, int n, int m)
{
    int i,j;
    int c = 0;
    for(i=0; i<m; i++)
        for(j=0; j<n; j++)
            if (i!=j)
                if(!Zero_BB(a[ij2k(i,j,n)]))
                    c++;
    return c;
}

int nb_non_zero_matrix_3d(double * a, int m, int n, int p)
{
    int i,j,k;
    int c = 0;
    for(i=0; i<m; i++)
        for(j=0; j<n; j++)
            for(k=0; k<p; k++)
                if(!Zero_BB(a[ijk2l(i,j,k,n,p)]))
                    c++;
    return c;
}

int is_in_vector(int val,int *v,int n)
{
    int i;
    for(i=0; i<n; i++)
        if (val == v[i])
            return 1;
    return 0;
}

/*computing logarithme in basis 2*/
int log_base_2 (int x)
{
    return (log(x)/log(2));
}


int vector_length_base_2(double * u, int n)
{
    int i,borne_u;
    int N = 0;
    for(i=0; i<n; i++)
    {
        borne_u= (int)log_base_2((int)u[i])+1;
        N = N + borne_u;
    }
    return N;
}

int * create_u_base2(double * u, int n)
{
    int * lgbis =alloc_vector(n);
    int i;
    for(i=0; i<n; i++)
        lgbis[i]= log_base_2((int)u[i]) + 1;
    return lgbis;
}


int * create_u_base2_cumulated(double * u, int n)
{
    int * lg = alloc_vector(n);
    int i;
    lg[0]= log_base_2((int)u[0]) + 1;
    for(i=1; i<n; i++)
        lg[i] = lg[i-1] + log_base_2((int)u[i]) + 1;
    return lg;
}

char * intochar(int val)
{
    int tmp1;
    int i=0;
    int tmp=val;
    int k=0;
    /*i=log base 10 de val*/

    while(tmp>=10)
    {
        tmp=(int)(tmp/10);
        i++;
    }
    char *s = alloc_string(i+1);

    tmp=val;
    while(i>=0)
    {
        tmp1=(int)(tmp/pow(10,i));
        s[k]=48+tmp1;
        tmp=tmp-tmp1*pow(10,i);
        i--;
        k++;
    }

    return s;
}


/*operations on  matrices*/

int is_symetric(double * a, int n)
{
    int i,j;
    for(i=0; i<n; i++)
        for(j=i+1; j<n; j++)
            if(a[ij2k(i,j,n)] != a[ij2k(j,i,n)])
                return 0;
    return 1;
}
int is_symetric_3d(double * a,int k, int n)
{
    int i,j;
    for(i=0; i<n; i++)
        for(j=i+1; j<n; j++)
            if(a[ijk2l(k,i,j,n,n)] != a[ijk2l(k,j,i,n,n)])
                return 0;
    return 1;
}
double * transpose_matrix( double * a, int n)
{
    int i,j;
    double * r = alloc_matrix_d(n,n);
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            r[ij2k(i,j,n)] = a[ij2k(j,i,n)];

    return r;
}

double * transpose_matrix_mn( double * a, int m, int n)
{
    int i,j;
    double * r = alloc_matrix_d(n,m);
    for(i=0; i<m; i++)
        for(j=0; j<n; j++)
            r[ij2k(j,i,m)] = a[ij2k(i,j,n)];

    return r;
}

double * multiplication_matrices( double * a, double * b, int n)
{
    int i,j,k;
    double * r = alloc_matrix_d(n,n);
    double s=0;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
        {
            for(k=0; k<n; k++)
                s= s + a[ij2k(i,k,n)] * b[ij2k(k,j,n)];
            r[ij2k(i,j,n)] = s;
            s=0;
        }
    return r;
}

double * opposite_matrix_3d(double * M, int m, int n, int p)
{
    int i,j,k;
    double * new_m = alloc_matrix_3d(m,n,p);
    for (i=0; i<m; i++)
        for(j=0; j<n; j++)
            for(k=0; k<p; k++)
                new_m[ijk2l(i,j,k,n,p)] = -M[ijk2l(i,j,k,n,p)];
    return new_m;

}

/*******************************************************************************************/
/*********************** utilities for the sdp solver***************************************/
/*******************************************************************************************/
int nb_cont_Xij(int n, int nb_int, int p)
{
    int i,cont = 0;
    for(i=n+p-1; i>=n - nb_int; i--)
        cont=cont+i;
    return cont;
}

int nb_max_cont(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4)
{

    int i,j;
    int cont = nb_cont_Xij(psdp->n,psdp->nb_int,0);

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */


    *ind_X1=1;
    *ind_X2=*ind_X1+cont;
    *ind_X3=*ind_X2+cont;
    *ind_X4=*ind_X3+cont;

    return *ind_X4+ cont-1;
}

void make_matrix_with_vector(SDP psdp, double ** m)
{
    int i,j;
    int k=0;

    m[0]=alloc_matrix_d (psdp->n+1,psdp->n+1);
    if (m[0] == NULL)
        exit(EXIT_FAILURE);

    for (i=0; i<1+psdp->n; i++)
        for (j=i; j<1+psdp->n; j++)
        {
            if (i==j)
                m[0][ij2k(i,j,psdp->n+1)]=psdp->x[k];

            if (i !=j)
            {
                m[0][ij2k(i,j,psdp->n+1)]=psdp->x[k];
                m[0][ij2k(j,i,psdp->n+1)]=m[0][ij2k(i,j,psdp->n+1)];
            }
            k++;
        }
}

void make_matrix_with_vector_new_sub(SDP psdp, double ** m, double * X)
{
    int i,j;
    int k=0;

    m[0]=alloc_matrix_d (psdp->n+1,psdp->n+1);
    if (m[0] == NULL)
        exit(EXIT_FAILURE);

    for (i=0; i<1+psdp->n; i++)
        for (j=i; j<1+psdp->n; j++)
        {
            if (i==j)
                m[0][ij2k(i,j,psdp->n+1)]=X[k];

            if (i !=j)
            {
                m[0][ij2k(i,j,psdp->n+1)]=X[k];
                m[0][ij2k(j,i,psdp->n+1)]=m[0][ij2k(i,j,psdp->n+1)];
            }
            k++;
        }
}

void dualized_objective_function(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{

    int i,j,k;

    *q_beta = copy_matrix_d(psdp->q,psdp->n,psdp->n);
    *c_beta = copy_vector_d(psdp->c,psdp->n);
    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = -q_beta[0][ij2k(i,j,psdp->n)];

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = -c_beta[0][i];

    for(k=0; k<psdp->nb_cont; k++)
    {
        if (psdp->constraints[k] >0)
        {
            i = psdp->tab[psdp->constraints[k]-1][0];
            j = psdp->tab[psdp->constraints[k]-1][1];
            switch (psdp->tab[psdp->constraints[k]-1][2])
            {
            case 1:
                /*Dualization of constraints  x_ix_j - u_ix_j - l_jx_i + u_il_j<= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->u[i];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->l[j];
                l_beta[0]=l_beta[0] - 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->u[i]*psdp->l[j];
                break;

            case 2:
                /*Dualization of constraints x_ix_j - u_jx_i -l_ix_j + u_jl_i<= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i] + 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->u[j];
                c_beta[0][j]=c_beta[0][j] + 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->l[i];
                l_beta[0]=l_beta[0] - 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->u[j]*psdp->l[i];
                break;

            case 3:
                /* Dualization of constraints - x_ix_j + u_jx_i + u_ix_j - u_iu_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_3[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i]- 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[j];
                c_beta[0][j]=c_beta[0][j]- 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[i];
                l_beta[0]=l_beta[0] + 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[i]*psdp->u[j];
                break;

            case 4:
                /* Dualization of constraints - x_ix_j+ l_jx_i + l_ix_j - l_il_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_4[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i]- 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[j];
                c_beta[0][j]=c_beta[0][j]- 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[i];
                l_beta[0]=l_beta[0] + 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[i]*psdp->l[j];
                break;

            default:
                printf("\n Error of constraint type (dualize objective function)\n");
                break;
            }
        }
        else
            break;
    }
}


void evaluate_objective(double * objective_value,SDP psdp, double ** q_beta, double ** c_beta, double *l_beta)
{
    /* Objective function value : x^t(- Q - beta^1 - beta^2 + beta^3 + beta^4)x + (- c + 2*beta_1u_i + 2*beta^2u_j - 2*beta^3(u_i+u_j))^tx + 2*beta^3u_iu_j  */

    int i,j;

    double * matrix;
    *objective_value=0;

    make_matrix_with_vector(psdp, &matrix);

    /*f(x) =f(x) + x^t( - Q - beta^1 - beta^2 + beta^3 + beta^4)x*/
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            *objective_value=*objective_value + matrix[ij2k(i+1,j+1,psdp->n+1)]*q_beta[0][ij2k(i,j,psdp->n)];

    /*f(x) =f(x) + (- c + beta_1u_i + beta^2u_j - beta^3(u_i+u_j))^tx*/
    for(i=0; i<psdp->n; i++)
        *objective_value=*objective_value + matrix[ij2k(0,i+1,psdp->n+1)]*c_beta[0][i];

    /*f(x) =f(x) + beta^3u_iu_j */
    *objective_value=*objective_value + *l_beta;

    free_vector_d(matrix);
}
int reduce(double *m, int n, int a, int b, float factor)
{
    int i;
    if(m == NULL)
        return 0;
    if(n < a || n < b)
        return 0;
    for(i = 0; i < n; i++)
    {
        m[ij2k(i,b,n)]  -= m[ij2k(i,a,n)]*factor;
    }

    return 1;
}
double determinant(double *m, int n)
{
    double *copy;
    unsigned int i, j;
    double det, factor;
    if(m == NULL)
        return -1;
    copy = copy_matrix_d(m,n,n);
    det = 1;

    /* reduce each of the rows to get a lower triangle */
    for(i = 0; i < n; i++)
    {
        for(j = i + 1; j < n; j++)
        {
            if(copy[ij2k(i,j,n)] == 0)
                continue;
            factor = copy[ij2k(i,j,n)]/(copy[ij2k(i,j,n)]);
            reduce(copy, n,i, j, factor);
        }
    }
    for(i = 0; i < n; i++)
        det *= copy[ij2k(i,j,n)];
    free(copy);
    return det;
}

void evaluate_objective_entropy(double * objective_value,SDP psdp, double ** q_beta, double ** c_beta, double *l_beta)
{
    /* Objective function value : x^t(- Q - beta^1 - beta^2 + beta^3 + beta^4)x + (- c + 2*beta_1u_i + 2*beta^2u_j - 2*beta^3(u_i+u_j))^tx + 2*beta^3u_iu_j  */

    int i,j;

    double * matrix;
    *objective_value=0;

    make_matrix_with_vector(psdp, &matrix);

    /*f(x) =f(x) + x^t( - Q - beta^1 - beta^2 + beta^3 + beta^4)x*/
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            if (i==j)
                *objective_value=*objective_value - 2*matrix[ij2k(i+1,j+1,psdp->n+1)]*(q_beta[0][ij2k(i,j,psdp->n)] );
            else
                *objective_value=*objective_value - matrix[ij2k(i+1,j+1,psdp->n+1)]*(q_beta[0][ij2k(i,j,psdp->n)] );

    /*f(x) =f(x) + (- c + beta_1u_i + beta^2u_j - beta^3(u_i+u_j))^tx*/
    for(i=0; i<psdp->n; i++)
        *objective_value=*objective_value - matrix[ij2k(0,i+1,psdp->n+1)]*c_beta[0][i]/2;

    /*f(x) =f(x) + beta^3u_iu_j */
    *objective_value=*objective_value - *l_beta;


    /*Evaluate log(det(Cdiag(x)C + I -diag(x)))*/
    double *CMatrix =copy_matrix_d(psdp->q,psdp->n,psdp->n);
    double *diagX = alloc_matrix_d(psdp->n,psdp->n);
    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            if (i==j)
                diagX[ij2k(i,j,psdp->n)] = matrix[ij2k(0,i+1,psdp->n+1)];
            else
                diagX[ij2k(i,j,psdp->n)] = 0;

    double *CdiagX =  multiplication_matrices(CMatrix, diagX, psdp->n);
    double *CdiagXC =  multiplication_matrices(CdiagX, CMatrix, psdp->n);

    print_mat_d(CdiagX,psdp->n,psdp->n);
    print_mat_d(CdiagXC,psdp->n,psdp->n);
    for(i=0; i<psdp->n; i++)
        CdiagXC[ij2k(i,i,psdp->n)] =  CdiagXC[ij2k(i,i,psdp->n)] + 1 -  matrix[ij2k(0,i+1,psdp->n+1)];

    double detCdiagXC = determinant(CdiagXC, psdp->n);
    *objective_value=*objective_value + 0.5*log(detCdiagXC);



}

void compute_subgradient(double ** check_violated,SDP psdp)
{

    double * matrix;

    make_matrix_with_vector(psdp, &matrix);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,k;


    for(k=0; k<psdp->nb_cont; k++)
    {
        if (psdp->constraints[k] >0)
        {
            i = psdp->tab[psdp->constraints[k]-1][0];
            j = psdp->tab[psdp->constraints[k]-1][1];
            switch (psdp->tab[psdp->constraints[k]-1][2])
            {
            case 1:
                /* subgradient =  - x_ix_j + u_ix_j + l_jx_i - u_il_j*/
                check_violated[0][k] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->l[j];
                break;

            case 2:
                /* subgradient = -x_ix_j + u_jx_i+ l_ix_j - u_jl_i*/
                check_violated[0][k] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)]+ psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)]- psdp->u[j]*psdp->l[i];
                break;

            case 3:
                /* subgradient =  x_ix_j - u_jx_i - u_ix_j + u_iu_j */
                check_violated[0][k] = matrix[ij2k(i+1,j+1,psdp->n+1)] - psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j];
                break;

            case 4:
                /* subgradient = x_ix_j- l_jx_i - l_ix_j + l_il_j*/
                check_violated[0][k] = matrix[ij2k(i+1,j+1,psdp->n+1)]- psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j];
                break;

            default:
                printf("\n Erreur de type de contrainte (compute subgradient)\n");
                break;
            }
        }
        else
            break;

    }

    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\npsdp->u (compute subgradient)\n"); */
    /*     print_vec_d(psdp->u,psdp->n); */
    /*     printf("\n"); */
    /*     printf("\npsdp->l (compute subgradient)\n"); */
    /*     print_vec_d(psdp->l,psdp->n); */
    /*     printf("\n"); */
    /*     printf("\nmatrix (compute subgradient\n"); */
    /*     print_mat_d(matrix,psdp->n+1,psdp->n+1); */
    /*     printf("\n"); */

    /*     printf("\ncheck_violated (compute subgradient) \n"); */
    /*     print_vec_d(check_violated[0],psdp->nb_cont); */
    /*     printf("\n"); */
    /*   } */

    free_vector_d(matrix);
}


void compute_new_subgradient(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X)
{

    double * matrix;

    make_matrix_with_vector_new_sub(psdp, &matrix,X[0]);


    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,k;

    int * constraints_new=alloc_vector(new_length);
    int index=variable_indices[0][0];

    for(i=0; i<new_length; i++)
    {
        constraints_new[i]=psdp->constraints[index+i];
    }

    for(k=0; k<new_length; k++)
    {
        if (constraints_new[k] >0)
        {
            i = psdp->tab[constraints_new[k]-1][0];
            j = psdp->tab[constraints_new[k]-1][1];
            switch (psdp->tab[constraints_new[k]-1][2])
            {
            case 1:
                /*  new subgradient =  - x_ix_j + u_ix_j  + l_jx_i - u_il_j*/
                check_violated[0][k] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)]+ psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->l[j];
                break;

            case 2:
                /* new subgradient = -x_ix_j + u_jx_i+ l_ix_j - u_jl_i*/
                check_violated[0][k] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)]+ psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)]- psdp->u[j]*psdp->l[i];
                break;

            case 3:
                /* new subgradient =  x_ix_j - u_jx_i - u_ix_j + u_iu_j */
                check_violated[0][k] = matrix[ij2k(i+1,j+1,psdp->n+1)] - psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j];
                break;

            case 4:
                /* new subgradient = x_ix_j- l_jx_i - l_ix_j + l_il_j*/
                check_violated[0][k] = matrix[ij2k(i+1,j+1,psdp->n+1)]- psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j];
                break;

            default:
                printf("\n Erreur de type de contrainte (compute new subgradient)\n");
                break;
            }
        }
        else
            break;

    }
    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\ncheck_violated (compute new subgradient) \n"); */
    /*     print_vec_d(check_violated[0],psdp->nb_cont); */
    /*     printf("\n"); */
    /*   } */
    free_vector_d(matrix);
}

/* purge or add dualized constraints to the objective function */
int check_constraints_violated(SDP psdp, double ** check_violated, double ** x)
{

    int i,j,k;
    double * zcheck_violated=alloc_vector_d(psdp->length);;
    double * matrix;

    free_vector_d(check_violated[0]);
    make_matrix_with_vector_new_sub(psdp, &matrix,x[0]);

    for(k=0; k<psdp->length; k++)
    {
        i = psdp->tab[k][0];
        j = psdp->tab[k][1];

        switch (psdp->tab[k][2])
        {
        case 1:
            /* check_violated =  - x_ix_j + u_ix_j  + l_jx_i - u_il_j*/
            zcheck_violated[k] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)]+ psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)]- psdp->u[i]*psdp->l[j];
            break;

        case 2:
            /* check_violated = -x_ix_j + u_jx_i+ l_ix_j- u_jl_i*/
            zcheck_violated[k] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)]+ psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)]- psdp->u[j]*psdp->l[i];
            break;

        case 3:
            /* check_violated =  x_ix_j - u_jx_i - u_ix_j + u_iu_j */
            zcheck_violated[k] = matrix[ij2k(i+1,j+1,psdp->n+1)] - psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j];
            break;

        case 4:
            /* check_violated = x_ix_j- l_jx_i - l_ix_j + l_il_j*/
            zcheck_violated[k] = matrix[ij2k(i+1,j+1,psdp->n+1)]- psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j];
            break;

        default:
            printf("\n Erreur de type de contrainte (check violated) \n");
            break;

        }

    }

    check_violated[0]=zcheck_violated;

    free_vector_d(matrix);

    for(i=0; i<psdp->length; i++)
        if (Negative(check_violated[0][i]))
            return 0;
    return 1;
}


void purge_constraints_not_violated(SDP psdp, double ** slacks, int * n_assign, int ** assign_new_from_old)
{
    int i;

    n_assign[0]=0;

    int * zassign_new_from_old=alloc_vector(psdp->nb_cont);
    int * zconstraints=alloc_vector(psdp->nb_cont);


    /* keep constraints that are still violated*/
    for(i=0; i<psdp->nb_cont; i++)
    {
        if ( Zero_VIOL_CB(slacks[0][i]))
        {
            zassign_new_from_old[n_assign[0]]=i;
            zconstraints[n_assign[0]]=psdp->constraints[i];
            n_assign[0]++;
        }
    }
    for(i=0; i<n_assign[0]; i++)
    {
        psdp->constraints[i]=zconstraints[i];
        psdp->constraints_old[i]=zconstraints[i];
    }

    for(i; i<psdp->nb_cont; i++)
    {
        psdp->constraints[i]=0;
        psdp->constraints_old[i]=0;
    }

    free_vector(*assign_new_from_old);
    psdp->nb_cont=n_assign[0];
    *assign_new_from_old=alloc_vector(*n_assign);
    for(i=0; i<*n_assign; i++)
    {
        assign_new_from_old[0][i]=zassign_new_from_old[i];
    }


    free_vector(zassign_new_from_old);
    free_vector(zconstraints);
}



void add_constraints_violated(SDP psdp, double ** check_violated,int * n_append)
{
    int i;
    int ind_min;
    double min;
    n_append[0]=0;


    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\n psdp->constraints (add_const violated before)\n"); */
    /*     print_vec(psdp->constraints,psdp->nb_cont); */
    /*     printf("\n\n"); */
    /*     printf("\n check_violated[0] (add_const violated before)\n"); */
    /*     print_vec_d(check_violated[0],psdp->nb_max_cont); */
    /*     printf("\n\n"); */
    /*   } */

    for(i=0; i<psdp->nb_cont; i++)
        if(psdp->constraints[i]!=0)
            check_violated[0][psdp->constraints[i]-1]=0;

    /* if (DEBUG ==1) */
    /*  { */
    /*    printf("\n check_violated[0] (add_const violated after)\n"); */
    /*    print_vec_d(check_violated[0],psdp->nb_max_cont); */
    /*    printf("\n\n"); */
    /*  } */

    /*add constraints that are the most violated*/
    for(i=psdp->nb_cont; i<psdp->nb_max_cont; i++)
    {
        minimum_d_0(check_violated[0],psdp->length,&ind_min,&min);
        if(Negative_VIOL_CB(min))
        {
            psdp->constraints[i]=ind_min+1;
            n_append[0]++;
        }
        else
            break;
    }

    psdp->nb_cont= psdp->nb_cont+ n_append[0];

    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\n psdp->constraints (add_const violated after)\n"); */
    /*     print_vec(psdp->constraints,psdp->nb_cont); */
    /*     printf("\n\n"); */
    /*   } */

}

void initialize_constraints_violated(SDP psdp, double ** check_violated, int * new_length)
{
    /* int i; */
    /* int nb_cont_violated = 0; */
    /* AVL_NODE tree=NULL;  */

    /* /\*add constraints that are the most violated*\/ */
    /* for(i=0;i<psdp->length;i++) */
    /*   { */
    /*     if(Negative(check_violated[0][i])) */
    /*     	{ */
    /* 	  tree =insert(tree,i,check_violated[0][i]); */
    /* 	  nb_cont_violated++; */
    /* 	} */

    /*     /\*if (nb_cont_violated > psdp->nb_cont) */
    /* 	break;*\/ */
    /*   } */
    /* NB_MAX_CONT = psdp->nb_cont; */
    /* initialize_constraints(tree,psdp->constraints,psdp->constraints_old); */

    /* for (i=nb_cont_violated; i<psdp->nb_cont;i++) */
    /*   psdp->constraints[i]=0; */

    /* if (nb_cont_violated <psdp->nb_cont)  */
    /*   *new_length=nb_cont_violated; */
    /* else   */
    /*   *new_length=psdp->nb_cont;  */
    int i=0;
    int nb_cont_violated = 0;
    TAB_IND * tab =NULL;
    tab = create_tab_ind_from_check_violated(check_violated[0],psdp->length);

    /* if (DEBUG ==1) */
    /*     { */
    /*        printf("\ncheck_violated (initialize constraints violated )\n"); */
    /*       print_vec_d(check_violated[0],psdp->length); */
    /*       printf("\n"); */
    /*     } */

    quickSort(&tab, 0, psdp->length-1);
    /* if (DEBUG ==1) */
    /* { */
    /*    printf("initialize after quick sort \n"); */
    /*    printf("\n tab_ind\n"); */
    /*    for(i=0;i<psdp->nb_max_cont;i++) */
    /* 	{ */
    /* 	  printf("tab->val : %.9lf, tab->ind : %d\n",tab[i]->val,tab[i]->ind); */
    /* 	} */
    /*   printf("\n"); */
    /* } */

    /* if (DEBUG ==1) */
    /* { */
    /*   printf("initialize after quick sort \n"); */

    /*   print_vec_d( check_violated[0],psdp->length); */
    /*   printf("\n\n"); */
    /* } */

    NB_MAX_CONT = psdp->nb_max_cont;


    nb_cont_violated = initialize_constraints_tab(&tab,psdp->constraints,psdp->constraints_old,psdp->nb_max_cont);



    for (i=nb_cont_violated; i<psdp->nb_cont; i++)
        psdp->constraints[i]=0;


    if (nb_cont_violated < psdp->nb_cont)
        *new_length=nb_cont_violated;
    else
        *new_length=psdp->nb_cont;
}


void update_beta_value(SDP psdp, double * zbeta)
{
    int i,j,k;

    /*Attention : on divise par 2 ici car on recopie symetriquement dans la matrice du coup il est necessaire de multiplier par 2 les coefficients des termes lineaires et constants*/

    free_vector_d(psdp->beta_1);
    free_vector_d(psdp->beta_2);
    free_vector_d(psdp->beta_3);
    free_vector_d(psdp->beta_4);

    double * zbeta_1=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_2=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_3=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_4=alloc_matrix_d(psdp->n,psdp->n);

    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
        {
            zbeta_1[ij2k(i,j,psdp->n)]=0;
            zbeta_2[ij2k(i,j,psdp->n)]=0;
            zbeta_3[ij2k(i,j,psdp->n)]=0;
            zbeta_4[ij2k(i,j,psdp->n)]=0;
        }

    for(k=0; k<psdp->nb_cont; k++)
    {
        if (psdp->constraints[k] >0)
        {
            i = psdp->tab[psdp->constraints[k]-1][0];
            j = psdp->tab[psdp->constraints[k]-1][1];
            switch (psdp->tab[psdp->constraints[k]-1][2])
            {
            case 1:
                /*Dual variables associated to constraints  x_ix_j - u_ix_j  - l_jx_i + u_il_j <= 0*/
                zbeta_1[ij2k(i,j,psdp->n)]=zbeta[k]/2;
                zbeta_1[ij2k(j,i,psdp->n)]=zbeta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dual variables associated to constraints x_ix_j - u_jx_i  - l_ix_j + u_jl_i  <= 0*/
                zbeta_2[ij2k(i,j,psdp->n)]=zbeta[k]/2;
                zbeta_2[ij2k(j,i,psdp->n)]=zbeta_2[ij2k(i,j,psdp->n)];
                break;

            case 3:
                /*Dual variables associated to constraints - x_ix_j + u_jx_i + u_ix_j - u_iu_j <= 0*/
                zbeta_3[ij2k(i,j,psdp->n)]=zbeta[k]/2;
                zbeta_3[ij2k(j,i,psdp->n)]=zbeta_3[ij2k(i,j,psdp->n)];
                break;

            case 4:
                /*Dual variables associated to constraints - x_ix_j+ l_jx_i + l_ix_j - l_il_j <= 0*/
                zbeta_4[ij2k(i,j,psdp->n)]=zbeta[k]/2;
                zbeta_4[ij2k(j,i,psdp->n)]=zbeta_4[ij2k(i,j,psdp->n)];
                break;

            default:
                printf("\n Erreur de type de contrainte (update beta) \n");
                break;
            }
        }
        else
            break;
    }

    psdp->beta_1 =zbeta_1;
    psdp->beta_2 =zbeta_2;
    psdp->beta_3 =zbeta_3;
    psdp->beta_4 =zbeta_4;
}


/*******************************************************************************************/
/*********************** utilities for the sdp solver cont ***************************************/
/*******************************************************************************************/
int nb_max_cont_mixed(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4)
{
    int i,j;
    int cont = (psdp->n-1)*psdp->n/2;

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */
    *ind_X1=1;
    *ind_X2=*ind_X1+cont;
    *ind_X3=*ind_X2+cont;
    *ind_X4=*ind_X3+cont;

    return *ind_X4+ cont-1;
}

/*******************************************************************************************/
/*********************** utilities for the sdp solver 0 1 **********************************/
/*******************************************************************************************/
int nb_cont_Xij_triangular(int n)
{
    return (n)*(n-1)*(n-2)/6;
}


int ijk2l_triangular(int i,int j,int k,int n)
{
    int a,b;
    int sum_1=0;
    int sum_2=0;

    for (a=i+1; a<j; a++)
        sum_1=sum_1+n-a-1;

    for (a=0; a<i; a++)
        for (b=a+1; b<n; b++)
            sum_2=sum_2+n-b-1;

    return k-j-1+sum_1+sum_2;
}

int nb_max_cont_0_1(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4,int * ind_X5,int * ind_X6,int *ind_X7,int*ind_X8)
{
    int i,j;
    int cont_cor= nb_cont_Xij(psdp->n,psdp->n,0);
    int cont_triangle = nb_cont_Xij_triangular(psdp->n);

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */
    *ind_X1=1;
    *ind_X2=*ind_X1+cont_cor;
    *ind_X3=*ind_X2+cont_cor;
    *ind_X4=*ind_X3+cont_cor;
    *ind_X5=*ind_X4+cont_cor;
    *ind_X6=*ind_X5+cont_triangle;
    *ind_X7=*ind_X6+cont_triangle;
    *ind_X8=*ind_X7+cont_triangle;
    return *ind_X8+ cont_triangle-1;
}


void dualized_objective_function_0_1(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{
    int i,j,k,l;

    *q_beta = copy_matrix_d(psdp->q,psdp->n,psdp->n);
    *c_beta = copy_vector_d(psdp->c,psdp->n);
    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = -q_beta[0][ij2k(i,j,psdp->n)];

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = -c_beta[0][i];

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            k = psdp->tab[psdp->constraints[l]-1][2];
            switch (psdp->tab[psdp->constraints[l]-1][3])
            {
            case 1:
                /*Dualization of constraints  x_ix_j - x_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->beta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dualization of constraints x_ix_j - x_i <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i] + 2*psdp->beta_2[ij2k(i,j,psdp->n)];
                break;

            case 3:
                /* Dualization of constraints - x_ix_j + x_i + x_j - 1 <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_3[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                l_beta[0]=l_beta[0] + 2*psdp->beta_3[ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i]- 2*psdp->beta_3[ij2k(i,j,psdp->n)];
                c_beta[0][j]=c_beta[0][j]- 2*psdp->beta_3[ij2k(i,j,psdp->n)];
                break;

            case 4:
                /* Dualization of constraints - x_ix_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_4[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 5:
                /*Dualization of constraints  x_ix_k +x_jx_k -x_ix_j -x_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] + psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][k] = c_beta[0][k] + 2*psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 6:
                /*Dualization of constraints x_ix_j +x_ix_k -x_jx_k -x_i <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] + psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 7:
                /* Dualization of constraints x_ix_j +x_jx_k -x_ix_k -x_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] + psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 8:
                /* Dualization of constraints x_i +x_j+x_k - x_ix_j - x_jx_k -x_ix_k -1 <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] + psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] + psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] + psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];

                l_beta[0]=l_beta[0] + 2*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            default:
                printf("\n Error of constraint type (dualize objective function 0 1)\n");
                break;
            }
        }
        else
            break;
    }
    /* if (DEBUG ==1) */
    /*    { */

    /*      int cont_cor= nb_cont_Xij(psdp->n,psdp->n,0); */
    /*      int cont_triangle = nb_cont_Xij_triangular(psdp->n); */
    /*      printf("\n cont_cor : %d cont_triang : %d, \n (dualize obj function)\n",cont_cor, cont_triangle); */

    /*      printf("\n nb_cont : %d length : %d, \n psdp->constraints (dualize obj function)\n",psdp->nb_cont,psdp->length); */
    /*      print_vec(psdp->constraints,psdp->nb_cont); */


    /*      printf("\n psdp->tab (dualize obj function)\n"); */
    /*      for (i=0;i<psdp->nb_cont;i++) */
    /* 	{ */
    /* 	  printf("\n i : %d,psdp->constraints[i]-1 : %d\n", i,psdp->constraints[i]-1); */
    /* 	  if (psdp->constraints[i]-1 >= 0) */
    /* 	    { */
    /* 	      print_vec( psdp->tab[psdp->constraints[i]-1],4); */
    /* 	      printf("\n"); */
    /* 	    } */
    /* 	} */

    /*      printf("\n"); */
    /*      for  (i=0;i<psdp->nb_cont;i++) */
    /* 	{ */
    /* 	  if ( (psdp->constraints[i] > 0) && (psdp->constraints[i]<=cont_cor )) */
    /* 	    printf(" \ncont cor T1 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > cont_cor) && (psdp->constraints[i]<=2*cont_cor )) */
    /* 	    printf(" \ncont cor T2 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 2*cont_cor) && (psdp->constraints[i]<=3*cont_cor )) */
    /* 	    printf(" \ncont cor T3 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 3*cont_cor) && (psdp->constraints[i]<=4*cont_cor )) */
    /* 	    printf(" \ncont cor T4 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor) && (psdp->constraints[i]<=4*cont_cor+cont_triangle )) */
    /* 	    printf(" \ncont triang  T1 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+ 2*cont_triangle)) */
    /* 	    printf(" \ncont triang T2 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+2*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+3*cont_triangle)) */
    /* 	    printf(" \ncont triang T3 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] >4*cont_cor+ 3*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+4*cont_triangle)) */
    /* 	    printf(" \ncont triang T4 violée : %d \n", psdp->constraints[i]); */

    /*      	} */
    /*    } */

}



void dualized_objective_function_entropy_mc(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{
    int i,j,k;

    *q_beta = alloc_matrix_d(psdp->n,psdp->n);
    *c_beta = alloc_vector_d(psdp->n);
    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = 0;

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = 0;

    for(k=0; k<psdp->nb_cont; k++)
    {
        if (psdp->constraints[k] >0)
        {
            i = psdp->tab[psdp->constraints[k]-1][0];
            j = psdp->tab[psdp->constraints[k]-1][1];
            switch (psdp->tab[psdp->constraints[k]-1][2])
            {
            case 1:
                /*Dualization of constraints  x_ix_j - u_ix_j - l_jx_i + u_il_j<= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->u[i];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->l[j];
                l_beta[0]=l_beta[0] - 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->u[i]*psdp->l[j];
                break;

            case 2:
                /*Dualization of constraints x_ix_j - u_jx_i -l_ix_j + u_jl_i<= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i] + 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->u[j];
                c_beta[0][j]=c_beta[0][j] + 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->l[i];
                l_beta[0]=l_beta[0] - 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->u[j]*psdp->l[i];
                break;

            case 3:
                /* Dualization of constraints - x_ix_j + u_jx_i + u_ix_j - u_iu_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_3[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i]- 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[j];
                c_beta[0][j]=c_beta[0][j]- 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[i];
                l_beta[0]=l_beta[0] + 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[i]*psdp->u[j];
                break;

            case 4:
                /* Dualization of constraints - x_ix_j+ l_jx_i + l_ix_j - l_il_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_4[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i]- 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[j];
                c_beta[0][j]=c_beta[0][j]- 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[i];
                l_beta[0]=l_beta[0] + 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[i]*psdp->l[j];
                break;

            default:
                printf("\n Error of constraint type (dualize objective function)\n");
                break;
            }
        }
        else
            break;
    }

}


void dualized_objective_function_entropy(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{
    int i,j,k,l;

    *q_beta = alloc_matrix_d(psdp->n,psdp->n);
    *c_beta = alloc_vector_d(psdp->n);
    *l_beta=0;

    /* printf("\ntab cont \n"); */
    /* print_vec(psdp->constraints,psdp->nb_cont); */
    /* printf("\npsdp->nb_cont : %d\n ", psdp->nb_cont); */

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = 0;

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = 0;

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            k = psdp->tab[psdp->constraints[l]-1][2];
            switch (psdp->tab[psdp->constraints[l]-1][3])
            {
            case 1:
                /*Dualization of constraints  x_ix_j - x_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->beta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dualization of constraints x_ix_j - x_i <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i] + 2*psdp->beta_2[ij2k(i,j,psdp->n)];
                break;

            case 3:
                /* Dualization of constraints - x_ix_j + x_i + x_j - 1 <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_3[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                l_beta[0]=l_beta[0] + 2*psdp->beta_3[ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i]- 2*psdp->beta_3[ij2k(i,j,psdp->n)];
                c_beta[0][j]=c_beta[0][j]- 2*psdp->beta_3[ij2k(i,j,psdp->n)];
                break;

            case 4:
                /* Dualization of constraints - x_ix_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_4[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 5:
                /*Dualization of constraints  x_ix_k +x_jx_k -x_ix_j -x_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] + psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][k] = c_beta[0][k] + 2*psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 6:
                /*Dualization of constraints x_ix_j +x_ix_k -x_jx_k -x_i <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] + psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 7:
                /* Dualization of constraints x_ix_j +x_jx_k -x_ix_k -x_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] + psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 8:
                /* Dualization of constraints x_i +x_j+x_k - x_ix_j - x_jx_k -x_ix_k -1 <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] + psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] + psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] + psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];

                l_beta[0]=l_beta[0] + 2*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            default:
                printf("\n Error of constraint type (dualize objective function 0 1)\n");
                break;
            }
        }
        else
            break;
    }

}

void compute_subgradient_0_1(double ** check_violated,SDP psdp)
{
    double * matrix;

    make_matrix_with_vector(psdp, &matrix);

    /* Attention les indices de la matrice matrix sont décalés de 1*/
    int i,j,k,l;
    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            k = psdp->tab[psdp->constraints[l]-1][2];
            switch (psdp->tab[psdp->constraints[l]-1][3])
            {
            case 1:
                /* subgradient =  - x_ix_j +x_j */
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,j+1,psdp->n+1)];
                break;

            case 2:
                /* subgradient = -x_ix_j + x_i*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,i+1,psdp->n+1)];
                break;

            case 3:
                /* subgradient =  x_ix_j - x_i - x_j + 1 */
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(0,i+1,psdp->n+1)] - matrix[ij2k(0,j+1,psdp->n+1)] + 1;
                break;

            case 4:
                /* subgradient = x_ix_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;

            case 5:
                /* subgradient = -x_ix_k -x_jx_k +x_ix_j +x_k */
                check_violated[0][l] = -matrix[ij2k(i+1,k+1,psdp->n+1)] - matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,k+1,psdp->n+1)];
                break;

            case 6:
                /* subgradient = -x_ix_j -x_ix_k +x_jx_k +x_i*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(i+1,k+1,psdp->n+1)] + matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(0,i+1,psdp->n+1)];
                break;

            case 7:
                /* subgradient =  -x_ix_j -x_jx_k +x_ix_k +x_j */
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,k+1,psdp->n+1)] + matrix[ij2k(0,j+1,psdp->n+1)];
                break;

            case 8:
                /* subgradient = -x_i -x_j-x_k + x_ix_j + x_jx_k +x_ix_k +1*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,k+1,psdp->n+1)] - matrix[ij2k(0,i+1,psdp->n+1)]- matrix[ij2k(0,j+1,psdp->n+1)]- matrix[ij2k(0,k+1,psdp->n+1)]+1;
                break;

            default:
                printf("\n Erreur de type de contrainte (compute subgradient 0 1)\n");
                break;
            }
        }
        else
            break;

    }
    /* if (DEBUG ==1) */
    /*     { */

    /*       printf("\nmatrix (compute subgradient\n"); */
    /*       print_mat_d(matrix,psdp->n+1,psdp->n+1); */
    /*       printf("\n"); */
    /*       printf("\ncheck_violated (compute subgradient) \n"); */
    /*       for(i=0;i<psdp->nb_conmt;i++) */
    /* 	if(check_violated[0][i] < -EPS_VIOL_CB) */
    /* 	  printf("i : %d , val:%.9lf",i,check_violated[0][i]); */
    /*       printf("\n"); */
    /*     } */

    free_vector_d(matrix);
}


void compute_new_subgradient_0_1(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X)
{

    double * matrix;

    make_matrix_with_vector_new_sub(psdp, &matrix,X[0]);


    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,k,l;

    int * constraints_new=alloc_vector(new_length);
    int index=variable_indices[0][0];

    for(i=0; i<new_length; i++)
    {
        constraints_new[i]=psdp->constraints[index+i];
    }

    for(l=0; l<new_length; l++)
    {
        if (constraints_new[l] >0)
        {
            i = psdp->tab[constraints_new[l]-1][0];
            j = psdp->tab[constraints_new[l]-1][1];
            k = psdp->tab[constraints_new[l]-1][2];
            switch (psdp->tab[constraints_new[l]-1][3])
            {
            case 1:
                /*  new subgradient =  - x_ix_j + x_j */
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,j+1,psdp->n+1)];
                break;

            case 2:
                /* new subgradient = -x_ix_j + x_i*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,i+1,psdp->n+1)];
                break;

            case 3:
                /* new subgradient =  x_ix_j - x_i - x_j + 1*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(0,i+1,psdp->n+1)] - matrix[ij2k(0,j+1,psdp->n+1)] + 1;
                break;

            case 4:
                /* new subgradient = x_ix_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;

            case 5:
                /* subgradient = -x_ix_k -x_jx_k +x_ix_j +x_k */
                check_violated[0][l] = -matrix[ij2k(i+1,k+1,psdp->n+1)] - matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,k+1,psdp->n+1)];
                break;

            case 6:
                /* subgradient = -x_ix_j -x_ix_k +x_jx_k +x_i*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(i+1,k+1,psdp->n+1)] + matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(0,i+1,psdp->n+1)];
                break;

            case 7:
                /* subgradient =  -x_ix_j -x_jx_k +x_ix_k +x_j */
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,k+1,psdp->n+1)] + matrix[ij2k(0,j+1,psdp->n+1)];
                break;

            case 8:
                /* subgradient = -x_i -x_j-x_k + x_ix_j + x_jx_k +x_ix_k +1*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,k+1,psdp->n+1)] - matrix[ij2k(0,i+1,psdp->n+1)]- matrix[ij2k(0,j+1,psdp->n+1)]- matrix[ij2k(0,k+1,psdp->n+1)] +1;
                break;

            default:
                printf("\n Erreur de type de contrainte (compute new subgradient 0 1)\n");
                break;

            }
        }
        else
            break;

    }

    free_vector_d(matrix);
}

/* purge or add dualized constraints to the objective function */
int check_constraints_violated_0_1(SDP psdp, double ** check_violated, double ** x)
{
    int i,j,k,l;
    double * zcheck_violated=alloc_vector_d(psdp->length);
    double * matrix;
    int nb_cont_violated = 0;

    free_vector_d(check_violated[0]);
    make_matrix_with_vector_new_sub(psdp, &matrix,x[0]);

    /* if (DEBUG ==1) */
    /*   { */
    /*     /\* printf("\npsdp->u (check_const_violated)\n"); *\/ */
    /*     /\* print_vec_d(psdp->u,psdp->n); *\/ */
    /*     /\* printf("\n"); *\/ */
    /*     /\* printf("\npsdp->l (check_const_violated)\n"); *\/ */
    /*     /\* print_vec_d(psdp->l,psdp->n); *\/ */
    /*     /\* printf("\n"); *\/ */
    /*     printf("\nmatrix (check_const_violated_triangle\n"); */
    /*     print_mat_d(matrix,psdp->n+1,psdp->n+1); */
    /*     printf("\n"); */
    /*   } */

    for(l=0; l<psdp->length; l++)
    {
        i = psdp->tab[l][0];
        j = psdp->tab[l][1];
        k = psdp->tab[l][2];
        switch (psdp->tab[l][3])
        {
        case 1:
            /* check_violated =  - x_ix_j + x_j */
            zcheck_violated[l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,j+1,psdp->n+1)];
            break;

        case 2:
            /* check_violated = -x_ix_j + x_i*/
            zcheck_violated[l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,i+1,psdp->n+1)];
            break;

        case 3:
            /* check_violated =  x_ix_j - x_i -x_j + 1 */
            zcheck_violated[l] = matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(0,i+1,psdp->n+1)] - matrix[ij2k(0,j+1,psdp->n+1)] + 1;
            break;

        case 4:
            /* check_violated = x_ix_j*/
            zcheck_violated[l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        case 5:
            /* subgradient = -x_ix_k -x_jx_k +x_ix_j +x_k */
            zcheck_violated[l] = -matrix[ij2k(i+1,k+1,psdp->n+1)] - matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(0,k+1,psdp->n+1)];
            break;

        case 6:
            /* subgradient = -x_ix_j -x_ix_k +x_jx_k +x_i*/
            zcheck_violated[l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(i+1,k+1,psdp->n+1)] + matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(0,i+1,psdp->n+1)];
            break;

        case 7:
            /* subgradient =  -x_ix_j -x_jx_k +x_ix_k +x_j */
            zcheck_violated[l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] - matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,k+1,psdp->n+1)] + matrix[ij2k(0,j+1,psdp->n+1)];
            break;

        case 8:
            /* subgradient = -x_i -x_j-x_k + x_ix_j + x_jx_k +x_ix_k +1*/
            zcheck_violated[l] = matrix[ij2k(i+1,j+1,psdp->n+1)] + matrix[ij2k(j+1,k+1,psdp->n+1)] + matrix[ij2k(i+1,k+1,psdp->n+1)] - matrix[ij2k(0,i+1,psdp->n+1)]- matrix[ij2k(0,j+1,psdp->n+1)]- matrix[ij2k(0,k+1,psdp->n+1)]+1;
            break;

        default:
            printf("\n Erreur de type de contrainte (check violated 0 1) \n");
            break;

        }
        if (Negative(zcheck_violated[l]))
            nb_cont_violated++;

    }



    check_violated[0]=zcheck_violated;



    /* if (DEBUG ==1) */
    /*   { */
    /*      printf("\ncheck_violated (triangle 01 - check constraints violated)\n"); */
    /*     print_vec_d(check_violated[0],psdp->nb_max_cont); */
    /*     printf("\n"); */
    /*   } */

    free_vector_d(matrix);

    for(i=0; i<psdp->length; i++)
        if (Negative(check_violated[0][i]))
            return 0;
    return 1;
}

void update_beta_value_0_1(SDP psdp, double * zbeta)
{
    int i,j,k,l;



    free_vector_d(psdp->beta_1);
    free_vector_d(psdp->beta_2);
    free_vector_d(psdp->beta_3);
    free_vector_d(psdp->beta_4);
    free_vector_d(psdp->delta_1);
    free_vector_d(psdp->delta_2);
    free_vector_d(psdp->delta_3);
    free_vector_d(psdp->delta_4);

    double * zbeta_1=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_2=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_3=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_4=alloc_matrix_d(psdp->n,psdp->n);
    double * zdelta_1=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_2=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_3=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_4=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);

    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
        {
            zbeta_1[ij2k(i,j,psdp->n)]=0;
            zbeta_2[ij2k(i,j,psdp->n)]=0;
            zbeta_3[ij2k(i,j,psdp->n)]=0;
            zbeta_4[ij2k(i,j,psdp->n)]=0;
        }

    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            for(k=0; k<psdp->n; k++)
            {
                zdelta_1[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_2[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_3[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_4[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
            }

    /* if (DEBUG==1) */
    /*   { */
    /*     printf("\n beta entier (update beta 0 1)\n\n"); */
    /*     print_vec_d(zbeta,psdp->nb_cont); */
    /*     printf("\n\n"); */

    /*   } */
    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            k = psdp->tab[psdp->constraints[l]-1][2];
            switch (psdp->tab[psdp->constraints[l]-1][3])
            {
            case 1:
                /*Dual variables associated to constraints x_ix_j - u_ix_j <= 0*/
                zbeta_1[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_1[ij2k(j,i,psdp->n)]=zbeta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dual variables associated to constraints x_ix_j - u_jx_i <= 0*/
                zbeta_2[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_2[ij2k(j,i,psdp->n)]=zbeta_2[ij2k(i,j,psdp->n)];
                break;

            case 3:
                /*Dual variables associated to constraints - x_ix_j + u_jx_i + u_iu_j - u_iu_j <= 0*/
                zbeta_3[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_3[ij2k(j,i,psdp->n)]=zbeta_3[ij2k(i,j,psdp->n)];
                break;

            case 4:
                /*Dual variables associated to constraints - x_ix_j <= 0*/
                zbeta_4[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_4[ij2k(j,i,psdp->n)]=zbeta_4[ij2k(i,j,psdp->n)];
                break;

            case 5:
                /*Dual variables associated to constraints -x_ix_k -x_jx_k +x_ix_j +x_k >= 0*/
                zdelta_1[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_1[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 6:
                /*Dual variables associated to constraints -x_ix_j -x_ix_k +x_jx_k +x_i >= 0*/
                zdelta_2[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_2[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 7:
                /*Dual variables associated to constraints -x_ix_j -x_jx_k +x_ix_k +x_j >= 0*/
                zdelta_3[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_3[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 8:
                /*Dual variables associated to constraints  -x_i -x_j-x_k + x_ix_j + x_jx_k +x_ix_k +1>= 0*/
                zdelta_4[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_4[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            default:
                printf("\n Erreur de type de contrainte (update delta) \n");
                break;
            }
        }
        else
            break;

    }

    psdp->beta_1 =zbeta_1;
    psdp->beta_2 =zbeta_2;
    psdp->beta_3 =zbeta_3;
    psdp->beta_4 =zbeta_4;
    psdp->delta_1 =zdelta_1;
    psdp->delta_2 =zdelta_2;
    psdp->delta_3 =zdelta_3;
    psdp->delta_4 =zdelta_4;
    /* if (DEBUG == 1) */
    /*    { */

    /*      printf("\n delta1 (update alpha beta 0 1)\n"); */
    /*      print_mat_3d(psdp->delta_1,psdp->n,psdp->n,psdp->n); */
    /*      printf("\n"); */
    /*       printf("\n delta2 (update alpha beta 0 1)\n"); */
    /*      print_mat_3d(psdp->delta_2,psdp->n,psdp->n,psdp->n); */
    /*      printf("\n"); */

    /*       printf("\n delta3 (update alpha beta 0 1)\n"); */
    /*      print_mat_3d(psdp->delta_3,psdp->n,psdp->n,psdp->n); */
    /*      printf("\n"); */
    /*       printf("\n delta4 (update alpha beta 0 1)\n"); */
    /*      print_mat_3d(psdp->delta_4,psdp->n,psdp->n,psdp->n); */
    /*      printf("\n"); */

    /*    } */


}


/*******************************************************************************************/
/*********************** utilities for the sdp solver triangle******************************/
/*******************************************************************************************/
int nb_max_cont_triangle(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int*ind_X4,int * ind_X5,int * ind_X6,int *ind_X7,int*ind_X8,int * ind_X9,int * ind_X10,int *ind_X11,int*ind_X12,int*ind_X13,int*ind_X14,int*ind_X15,int*ind_X16)
{
    int i,j;
    int cont_cor= nb_cont_Xij(psdp->n,psdp->n,0);
    int cont_triangle = nb_cont_Xij_triangular(psdp->n);

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */
    *ind_X1=1;
    *ind_X2=*ind_X1+cont_cor;
    *ind_X3=*ind_X2+cont_cor;
    *ind_X4=*ind_X3+cont_cor;
    *ind_X5=*ind_X4+cont_cor;
    *ind_X6=*ind_X5+cont_triangle;
    *ind_X7=*ind_X6+cont_triangle;
    *ind_X8=*ind_X7+cont_triangle;
    *ind_X9=*ind_X8+cont_triangle;
    *ind_X10=*ind_X9+cont_triangle;
    *ind_X11=*ind_X10+cont_triangle;
    *ind_X12=*ind_X11+cont_triangle;
    *ind_X13=*ind_X12+cont_triangle;
    *ind_X14=*ind_X13+cont_triangle;
    *ind_X15=*ind_X14+cont_triangle;
    *ind_X16=*ind_X15+cont_triangle;
    return *ind_X16+ cont_triangle-1;
}

void dualized_objective_function_triangle(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{
    int i,j,k,l;

    *q_beta = copy_matrix_d(psdp->q,psdp->n,psdp->n);
    *c_beta = copy_vector_d(psdp->c,psdp->n);
    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = -q_beta[0][ij2k(i,j,psdp->n)];

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = -c_beta[0][i];

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            k = psdp->tab[psdp->constraints[l]-1][2];
            switch (psdp->tab[psdp->constraints[l]-1][3])
            {
            case 1:
                /*Dualization of constraints  x_ix_j - u_ix_j - l_jx_i + u_il_j<= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->u[i];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->l[j];
                l_beta[0]=l_beta[0] - 2*psdp->beta_1[ij2k(i,j,psdp->n)]*psdp->u[i]*psdp->l[j];
                break;

            case 2:
                /*Dualization of constraints x_ix_j - u_jx_i -l_ix_j + u_jl_i<= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i] + 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->u[j];
                c_beta[0][j]=c_beta[0][j] + 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->l[i];
                l_beta[0]=l_beta[0] - 2*psdp->beta_2[ij2k(i,j,psdp->n)]*psdp->u[j]*psdp->l[i];
                break;

            case 3:
                /* Dualization of constraints - x_ix_j + u_jx_i + u_ix_j - u_iu_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_3[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i]- 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[j];
                c_beta[0][j]=c_beta[0][j]- 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[i];
                l_beta[0]=l_beta[0] + 2*psdp->beta_3[ij2k(i,j,psdp->n)]*psdp->u[i]*psdp->u[j];
                break;

            case 4:
                /* Dualization of constraints - x_ix_j+ l_jx_i + l_ix_j - l_il_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_4[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                c_beta[0][i]=c_beta[0][i]- 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[j];
                c_beta[0][j]=c_beta[0][j]- 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[i];
                l_beta[0]=l_beta[0] + 2*psdp->beta_4[ij2k(i,j,psdp->n)]*psdp->l[i]*psdp->l[j];
                break;

            case 5:
                /*Dualization of constraints  (l_k - u_k)x_ix_j + (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_kx_j + (u_ju_k - l_jl_k)x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - (psdp->l[k] - psdp->u[k])*psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - (psdp->l[j]- psdp->u[j])*psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] + psdp->u[i]* psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*(psdp->u[j]*psdp->u[k] -psdp->l[j]*psdp->l[k]) *psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*psdp->u[i]*psdp->u[k]*psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*psdp->u[i]*psdp->u[j]*psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] + 2*psdp->u[i]*psdp->u[j]*psdp->u[k]*psdp->delta_1[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 6:
                /*Dualization of constraints  (l_k - u_k)x_ix_j - u_jx_ix_k +(l_i - u_i)x_jx_k + (u_iu_k - l_il_k)x_j + u_ju_k x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - (psdp->l[k] - psdp->u[k])*psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] + psdp->u[j]*psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - (psdp->l[i] - psdp->u[i])* psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*psdp->u[j]*psdp->u[k] *psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*(psdp->u[i]*psdp->u[k] -psdp->l[i]*psdp->l[k]) *psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*psdp->u[i]*psdp->u[j]*psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]=l_beta[0] + 2*psdp->u[i]*psdp->u[j]*psdp->u[k]*psdp->delta_2[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 7:
                /*Dualization of constraints  - u_kx_ix_j + (l_j- u_j)x_ix_k +(l_i - u_i)x_jx_k + u_iu_kx_j + u_ju_k x_i + (u_i_u_j - l_il_j)x_k - u_iu_ju_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] +  psdp->u[k]*psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - (psdp->l[j] - psdp->u[j])*psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - (psdp->l[i] - psdp->u[i])* psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*psdp->u[j]*psdp->u[k] *psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*psdp->u[i]*psdp->u[k] *psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*(psdp->u[i]*psdp->u[j] -psdp->l[i]*psdp->l[j])*psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]=l_beta[0] + 2*psdp->u[i]*psdp->u[j]*psdp->u[k]*psdp->delta_3[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 8:
                /*Dualization of constraints  (l_k - u_k)x_ix_j + (u_j - l_j)x_ix_k + u_ix_jx_k - u_il_kx_j + (l_ju_k - u_jl_k) x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - (psdp->l[k] - psdp->u[k])*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - (psdp->u[j]- psdp->l[j])*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - psdp->u[i]* psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*(psdp->l[j]*psdp->u[k] -psdp->u[j]*psdp->l[k]) *psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->u[i]*psdp->l[k]*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] + 2*psdp->u[i]*psdp->u[j]*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] - 2*psdp->u[i]*psdp->u[j]*psdp->l[k]*psdp->delta_4[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 9:
                /*Dualization of constraints  (l_k - u_k)x_ix_j + u_jx_ix_k + (u_i - l_i)x_jx_k + (l_iu_k- u_il_k) x_j  - u_jl_k x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - (psdp->l[k] - psdp->u[k])*psdp->delta_5[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - psdp->u[j]*psdp->delta_5[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] -( psdp->u[i] - psdp->l[i]) * psdp->delta_5[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->u[j]*psdp->l[k]*psdp->delta_5[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*(psdp->l[i]*psdp->u[k] - psdp->u[i]*psdp->l[k])*psdp->delta_5[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] + 2*psdp->u[i]*psdp->u[j]*psdp->delta_5[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] - 2*psdp->u[i]*psdp->u[j]*psdp->l[k]*psdp->delta_5[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 10:
                /*Dualization of constraints  (u_k - l_k)x_ix_j + (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_k x_j  +( u_jl_k - l_ju_k)x_i - u_il_jx_k + u_il_ju_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - (psdp->u[k] - psdp->l[k])*psdp->delta_6[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - (psdp->l[j]- psdp->u[j])*psdp->delta_6[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - psdp->u[i]* psdp->delta_6[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*(psdp->u[j]*psdp->l[k] -psdp->l[j]*psdp->u[k]) *psdp->delta_6[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->u[i]*psdp->u[k]*psdp->delta_6[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] + 2*psdp->u[i]*psdp->l[j]*psdp->delta_6[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] - 2*psdp->u[i]*psdp->l[j]*psdp->u[k]*psdp->delta_6[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 11:
                /*Dualization of constraints  (l_j - u_j) x_ix_k + u_kx_ix_j + (u_i - l_i)x_jx_k - u_iu_k x_j  - l_ju_kx_i +(u_jl_i - u_il_j) x_k + u_il_ju_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->u[k]*psdp->delta_7[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - (psdp->l[j] - psdp->u[j]) *psdp->delta_7[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]=q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - ( psdp->u[i] - psdp->l[i])* psdp->delta_7[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]=q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->l[j]*psdp->u[k]*psdp->delta_7[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2*psdp->u[i]*psdp->u[k]*psdp->delta_7[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*(psdp->l[i]*psdp->u[j] -psdp->u[i]*psdp->l[j]) *psdp->delta_7[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]=l_beta[0] - 2*psdp->u[i]*psdp->l[j]*psdp->u[k]*psdp->delta_7[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 12:
                /*Dualization of constraints  (u_k - l_k)x_ix_j + u_jx_ix_k + (l_i - u_i)x_jx_k + (u_il_k - l_iu_k) x_j  - u_ju_kx_i - l_iu_jx_k + l_iu_ju_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - (psdp->u[k] - psdp->l[k])*psdp->delta_8[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] -  psdp->u[j]*psdp->delta_8[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - (psdp->l[i] - psdp->u[i])* psdp->delta_8[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->u[j]*psdp->u[k] *psdp->delta_8[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*(psdp->u[i]*psdp->l[k] - psdp->l[i]*psdp->u[k]) *psdp->delta_8[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] + 2*psdp->l[i]*psdp->u[j]*psdp->delta_8[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] - 2*psdp->l[i]*psdp->u[j]*psdp->u[k]*psdp->delta_8[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 13:
                /*Dualization of constraints  u_k x_ix_j + (u_j - l_j) x_ix_k + (l_i - u_i)x_jx_k - l_iu_k x_j  - u_ju_kx_i + (u_il_j- l_iu_j)x_k + l_iu_ju_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->u[k]*psdp->delta_9[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - ( psdp->u[j] - psdp->l[j])*psdp->delta_9[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - (psdp->l[i] - psdp->u[i])* psdp->delta_9[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] + 2*psdp->u[j]*psdp->u[k] *psdp->delta_9[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] + 2* psdp->l[i]*psdp->u[k] *psdp->delta_9[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*(psdp->u[i]*psdp->l[j] - psdp->l[i]*psdp->u[j])*psdp->delta_9[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] - 2*psdp->l[i]*psdp->u[j]*psdp->u[k]*psdp->delta_9[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 14:
                /*Dualization of constraints  (u_k - l_k) x_ix_j + (u_j - l_j) x_ix_k  - u_ix_jx_k + u_il_jx_k + u_il_k x_j+ (l_jl_k - u_iu_k)x_i  - u_il_jl_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - (psdp->u[k] - psdp->l[k])*psdp->delta_10[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - ( psdp->u[j] - psdp->l[j])*psdp->delta_10[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] +  psdp->u[i]* psdp->delta_10[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*(psdp->l[j]*psdp->l[k] - psdp->u[j]*psdp->u[k]) *psdp->delta_10[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2* psdp->u[i]*psdp->l[k] *psdp->delta_10[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*psdp->u[i]*psdp->l[j] *psdp->delta_10[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] + 2*psdp->u[i]*psdp->l[j]*psdp->l[k]*psdp->delta_10[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 15:
                /*Dualization of constraints  (u_k - l_k) x_ix_j - u_j x_ix_k  +(u_i - l_i)x_jx_k + l_iu_jx_k + (l_il_k - u_iu_k) x_j+ u_jl_kx_i - l_iu_jl_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - (psdp->u[k] - psdp->l[k])*psdp->delta_11[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] + psdp->u[j] *psdp->delta_11[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - ( psdp->u[i]- psdp->l[i])* psdp->delta_11[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*psdp->u[j]*psdp->l[k] *psdp->delta_11[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*( psdp->l[i]*psdp->l[k]- psdp->u[i]*psdp->u[k]) *psdp->delta_11[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*psdp->l[i]*psdp->u[j] *psdp->delta_11[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] + 2*psdp->l[i]*psdp->u[j]*psdp->l[k]*psdp->delta_11[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;

            case 16:
                /*Dualization of constraints  -u_k x_ix_j +(u_j -l_j) x_ix_k  +(u_i - l_i)x_jx_k + (l_il_j - u_iu_j) x_k + l_iu_k  x_j+ l_ju_kx_i - l_il_ju_k <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] + psdp->u[k] *psdp->delta_12[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(i,k,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)] - (psdp->u[j]- psdp->l[j]) *psdp->delta_12[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,i,psdp->n)]= q_beta[0][ij2k(i,k,psdp->n)];
                q_beta[0][ij2k(j,k,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)] - ( psdp->u[i]- psdp->l[i])* psdp->delta_12[ijk2l(i,j,k,psdp->n,psdp->n)];
                q_beta[0][ij2k(k,j,psdp->n)]= q_beta[0][ij2k(j,k,psdp->n)];
                c_beta[0][i] = c_beta[0][i] - 2*psdp->l[j]*psdp->u[k] *psdp->delta_12[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][j] = c_beta[0][j] - 2*psdp->l[i]*psdp->u[k] *psdp->delta_12[ijk2l(i,j,k,psdp->n,psdp->n)];
                c_beta[0][k] = c_beta[0][k] - 2*(psdp->l[i]*psdp->l[j] - psdp->u[i]*psdp->u[j])*psdp->delta_12[ijk2l(i,j,k,psdp->n,psdp->n)];
                l_beta[0]= l_beta[0] + 2*psdp->l[i]*psdp->l[j]*psdp->u[k]*psdp->delta_12[ijk2l(i,j,k,psdp->n,psdp->n)];
                break;
            default:
                printf("\n Error of constraint type (dualize objective function 0 1)\n");
                break;
            }
        }
        else
            break;
    }
    /* if (DEBUG ==1) */
    /*   { */

    /*     int cont_cor= nb_cont_Xij(psdp->n,psdp->n,0); */
    /*     int cont_triangle = nb_cont_Xij_triangular(psdp->n); */
    /*     printf("\n cont_cor : %d cont_triang : %d, \n (dualize obj function)\n",cont_cor, cont_triangle); */

    /*     printf("\n nb_cont : %d length : %d, \n psdp->constraints (dualize obj function)\n",psdp->nb_cont,psdp->length); */
    /*     print_vec(psdp->constraints,psdp->nb_cont); */


    /*     printf("\n psdp->tab (dualize obj function)\n"); */
    /*     for (i=0;i<psdp->nb_cont;i++) */
    /*     	{ */
    /*     	  printf("\n i : %d,psdp->constraints[i]-1 : %d\n", i,psdp->constraints[i]-1); */
    /*     	  if (psdp->constraints[i]-1 >= 0) */
    /*     	    { */
    /*     	      print_vec( psdp->tab[psdp->constraints[i]-1],4); */
    /*     	      printf("\n"); */
    /*     	    } */
    /*     	} */

    /*     printf("\n"); */
    /*     for  (i=0;i<psdp->nb_cont;i++) */
    /* 	{ */
    /* 	  if ( (psdp->constraints[i] > 0) && (psdp->constraints[i]<=cont_cor )) */
    /* 	    printf(" \ncont cor T1 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > cont_cor) && (psdp->constraints[i]<=2*cont_cor )) */
    /* 	    printf(" \ncont cor T2 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 2*cont_cor) && (psdp->constraints[i]<=3*cont_cor )) */
    /* 	    printf(" \ncont cor T3 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 3*cont_cor) && (psdp->constraints[i]<=4*cont_cor )) */
    /* 	    printf(" \ncont cor T4 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor) && (psdp->constraints[i]<=4*cont_cor+cont_triangle )) */
    /* 	    printf(" \ncont triang  T1 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+ 2*cont_triangle)) */
    /* 	    printf(" \ncont triang T2 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+2*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+3*cont_triangle)) */
    /* 	    printf(" \ncont triang T3 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] >4*cont_cor+ 3*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+4*cont_triangle)) */
    /* 	    printf(" \ncont triang T4 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+4*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+5*cont_triangle)) */
    /* 	    printf(" \ncont triang T5 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+5* cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+ 6*cont_triangle)) */
    /* 	    printf(" \ncont triang T6 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+6*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+7*cont_triangle)) */
    /* 	    printf(" \ncont triang T7 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+7*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+8*cont_triangle)) */
    /* 	    printf(" \ncont triang T8 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+8*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+9*cont_triangle)) */
    /* 	    printf(" \ncont triang T9 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+9*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+10*cont_triangle)) */
    /* 	    printf(" \ncont triang T10 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+10*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+11*cont_triangle)) */
    /* 	    printf(" \ncont triang T11 violée : %d \n", psdp->constraints[i]); */
    /* 	  if ( (psdp->constraints[i] > 4*cont_cor+11*cont_triangle) && (psdp->constraints[i]<= 4*cont_cor+12*cont_triangle)) */
    /* 	    printf(" \ncont triang T12 violée : %d \n", psdp->constraints[i]); */
    /*     	} */
    /*   } */
}

void compute_subgradient_triangle(double ** check_violated,SDP psdp)
{

    double * matrix;

    make_matrix_with_vector(psdp, &matrix);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,k,l;

    /* if (DEBUG ==1) */
    /*   { */

    /* printf("\npsdp->u (compute subgradient)\n"); */
    /* print_vec_d(psdp->u,psdp->n); */
    /* printf("\n"); */
    /* printf("\npsdp->l (compute subgradient)\n"); */
    /* print_vec_d(psdp->l,psdp->n); */
    /* printf("\n"); */
    /* printf("\nmatrix x (compute subgradient)\n"); */
    /* print_mat_d(matrix,psdp->n+1,psdp->n+1); */
    /* printf("\n"); */

    /*   } */
    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            k = psdp->tab[psdp->constraints[l]-1][2];
            switch (psdp->tab[psdp->constraints[l]-1][3])
            {
            case 1:
                /* subgradient =  - x_ix_j + u_ix_j + l_jx_i - u_il_j*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->l[j];
                break;

            case 2:
                /* subgradient = -x_ix_j + u_jx_i+ l_ix_j - u_jl_i*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)]+ psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)]- psdp->u[j]*psdp->l[i];
                break;

            case 3:
                /* subgradient =  x_ix_j - u_jx_i - u_ix_j + u_iu_j */
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)] - psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j];
                break;

            case 4:
                /* subgradient = x_ix_j- l_jx_i - l_ix_j + l_il_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)]- psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j];
                break;

            case 5:
                /* subgradient = -(l_k - u_k)x_ix_j - (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_kx_j - (u_ju_k - l_jl_k)x_i - u_i_u_jx_k + u_iu_ju_k  */
                check_violated[0][l] = - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]- (psdp->l[j] - psdp->u[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - (psdp->u[j]*psdp->u[k] -psdp->l[j]*psdp->l[k]) *matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
                break;

            case 6:
                /* subgradient =  -(l_k - u_k)x_ix_j + u_jx_ix_k - (l_i - u_i)x_jx_k - (u_iu_k - l_il_k)x_j - u_ju_k x_i - u_i_u_jx_k + u_iu_ju_k  */
                check_violated[0][l] = - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->l[i] -  psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->u[k] *matrix[ij2k(0,i+1,psdp->n+1)] - (psdp->u[i]*psdp->u[k] -psdp->l[i]*psdp->l[k])*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
                break;

            case 7:
                /* subgradient =   u_kx_ix_j - (l_j- u_j)x_ix_k -(l_i - u_i)x_jx_k - u_iu_kx_j - u_ju_k x_i - (u_i_u_j - l_il_j)x_k + u_iu_ju_k   */
                check_violated[0][l] = psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] -psdp->u[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->l[i] -  psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - (psdp->u[i]*psdp->u[j]-psdp->l[i]*psdp->l[j])*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->u[k] *matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->u[k] *matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
                break;

            case 8:
                /* subgradient =   -(l_k - u_k)x_ix_j - (u_j - l_j)x_ix_k - u_ix_jx_k + u_il_kx_j - (l_ju_k - u_jl_k) x_i + u_i_u_jx_k - u_iu_jl_k  */
                check_violated[0][l] =   - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->u[j] -psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - (psdp->l[j]*psdp->u[k] - psdp->u[j]*psdp->l[k]) *matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->l[k] *matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*psdp->l[k];
                break;

            case 9:
                /* subgradient =   -(l_k - u_k)x_ix_j - u_jx_ix_k - (u_i - l_i)x_jx_k - (l_iu_k- u_il_k) x_j  + u_jl_k x_i + u_i_u_jx_k - u_iu_jl_k */
                check_violated[0][l] =   - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - psdp->u[j] *matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->u[i]-psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->l[k]*matrix[ij2k(0,i+1,psdp->n+1)] - (psdp->l[i]*psdp->u[k] - psdp->u[i]*psdp->l[k])*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*psdp->l[k];
                break;

            case 10:
                /* subgradient =  -(u_k - l_k)x_ix_j - (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_k x_j  -( u_jl_k - l_ju_k)x_i + u_il_jx_k - u_il_ju_k */
                check_violated[0][l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] - psdp->u[j])  *matrix[ij2k(i+1,k+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->l[j]*matrix[ij2k(0,k+1,psdp->n+1)] -( psdp->u[j]*psdp->l[k] - psdp->l[j]*psdp->u[k])*matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*psdp->u[k];
                break;

            case 11:
                /* subgradient =   -(l_j - u_j) x_ix_k - u_kx_ix_j - (u_i - l_i)x_jx_k + u_iu_k x_j  + l_ju_kx_i -(u_jl_i - u_il_j) x_k - u_il_ju_k  */
                check_violated[0][l] =   - psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] - psdp->u[j])  *matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - (psdp->l[i]*psdp->u[j] - psdp->u[i]*psdp->l[j])*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->l[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*psdp->u[k];
                break;

            case 12:
                /* subgradient =     -(u_k - l_k)x_ix_j - u_jx_ix_k - (l_i - u_i)x_jx_k - (u_il_k - l_iu_k) x_j  + u_ju_kx_i + l_iu_jx_k - l_iu_ju_k  */
                check_violated[0][l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->l[i] - psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->l[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] -( psdp->u[i]*psdp->l[k] -  psdp->l[i]*psdp->u[k])*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*psdp->u[k];
                break;

            case 13:
                /* subgradient =    -u_k x_ix_j - (u_j - l_j) x_ix_k - (l_i - u_i)x_jx_k + l_iu_k x_j  + u_ju_kx_i - (u_il_j- l_iu_j)x_k - l_iu_ju_k */
                check_violated[0][l] =   - psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  -( psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->l[i] - psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] -( psdp->u[i]*psdp->l[j] - psdp->l[i]*psdp->u[j])*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] +  psdp->l[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*psdp->u[k];
                break;

            case 14:
                /* subgradient =    -(u_k - l_k) x_ix_j - (u_j - l_j) x_ix_k  + u_ix_jx_k - u_il_jx_k - u_il_k x_j- (l_jl_k - u_iu_k)x_i  + u_il_jl_k  */
                check_violated[0][l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  -( psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*matrix[ij2k(0,k+1,psdp->n+1)] -( psdp->l[j]*psdp->l[k] - psdp->u[j]*psdp->u[k])*matrix[ij2k(0,i+1,psdp->n+1)] -  psdp->u[i]*psdp->l[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->l[j]*psdp->l[k];
                break;


            case 15:
                /* subgradient =  -(u_k - l_k) x_ix_j + u_j x_ix_k  -(u_i - l_i)x_jx_k - l_iu_jx_k - (l_il_k - u_iu_k) x_j- u_jl_kx_i + l_iu_jl_k  */
                check_violated[0][l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->l[k]*matrix[ij2k(0,i+1,psdp->n+1)] - ( psdp->l[i]*psdp->l[k] -  psdp->u[i]*psdp->u[k])*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->u[j]*psdp->l[k];
                break;

            case 16:
                /* subgradient =  u_k x_ix_j -(u_j - l_j) x_ix_k  -(u_i - l_i)x_jx_k - (l_il_j - u_iu_j) x_k - l_iu_k  x_j- l_ju_kx_i + l_il_ju_k  */
                check_violated[0][l] =   psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]-(psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] -( psdp->l[i]*psdp->l[j] - psdp->u[i]*psdp->u[j])*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->l[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] -  psdp->l[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j]*psdp->u[k];
                break;

            default:
                printf("\n Erreur de type de contrainte (compute subgradient 0 1)\n");
                break;
            }
        }
        else
            break;

    }
    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\ncheck_violated_triangle (compute subgradient) \n"); */
    /*     print_vec_d(check_violated[0],psdp->nb_cont); */
    /*     printf("\n"); */
    /*   } */
    free_vector_d(matrix);
}


void compute_new_subgradient_triangle(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X)
{


    double * matrix;

    make_matrix_with_vector_new_sub(psdp, &matrix,X[0]);


    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,k,l;

    int * constraints_new=alloc_vector(new_length);
    int index=variable_indices[0][0];

    for(i=0; i<new_length; i++)
    {
        constraints_new[i]=psdp->constraints[index+i];
    }

    for(l=0; l<new_length; l++)
    {
        if (constraints_new[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            k = psdp->tab[psdp->constraints[l]-1][2];
            switch (psdp->tab[psdp->constraints[l]-1][3])
            {
            case 1:
                /* subgradient =  - x_ix_j + u_ix_j + l_jx_i - u_il_j*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->l[j];
                break;

            case 2:
                /* subgradient = -x_ix_j + u_jx_i+ l_ix_j - u_jl_i*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)]+ psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)]- psdp->u[j]*psdp->l[i];
                break;

            case 3:
                /* subgradient =  x_ix_j - u_jx_i - u_ix_j + u_iu_j */
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)] - psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j];
                break;

            case 4:
                /* subgradient = x_ix_j- l_jx_i - l_ix_j + l_il_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)]- psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j];
                break;

            case 5:
                /* subgradient = -(l_k - u_k)x_ix_j - (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_kx_j - (u_ju_k - l_jl_k)x_i - u_i_u_jx_k + u_iu_ju_k  */
                check_violated[0][l] = - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]- (psdp->l[j] - psdp->u[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - (psdp->u[j]*psdp->u[k] -psdp->l[j]*psdp->l[k]) *matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
                break;

            case 6:
                /* subgradient =  -(l_k - u_k)x_ix_j + u_jx_ix_k - (l_i - u_i)x_jx_k - (u_iu_k - l_il_k)x_j - u_ju_k x_i - u_i_u_jx_k + u_iu_ju_k  */
                check_violated[0][l] = - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->l[i] -  psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->u[k] *matrix[ij2k(0,i+1,psdp->n+1)] - (psdp->u[i]*psdp->u[k] -psdp->l[i]*psdp->l[k])*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
                break;

            case 7:
                /* subgradient =   u_kx_ix_j - (l_j- u_j)x_ix_k -(l_i - u_i)x_jx_k - u_iu_kx_j - u_ju_k x_i - (u_i_u_j - l_il_j)x_k + u_iu_ju_k   */
                check_violated[0][l] = psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] -psdp->u[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->l[i] -  psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - (psdp->u[i]*psdp->u[j]-psdp->l[i]*psdp->l[j])*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->u[k] *matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->u[k] *matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
                break;

            case 8:
                /* subgradient =   -(l_k - u_k)x_ix_j - (u_j - l_j)x_ix_k - u_ix_jx_k + u_il_kx_j - (l_ju_k - u_jl_k) x_i + u_i_u_jx_k - u_iu_jl_k  */
                check_violated[0][l] =   - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->u[j] -psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - (psdp->l[j]*psdp->u[k] - psdp->u[j]*psdp->l[k]) *matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->l[k] *matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*psdp->l[k];
                break;

            case 9:
                /* subgradient =   -(l_k - u_k)x_ix_j - u_jx_ix_k - (u_i - l_i)x_jx_k - (l_iu_k- u_il_k) x_j  + u_jl_k x_i + u_i_u_jx_k - u_iu_jl_k */
                check_violated[0][l] =   - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - psdp->u[j] *matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->u[i]-psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->l[k]*matrix[ij2k(0,i+1,psdp->n+1)] - (psdp->l[i]*psdp->u[k] - psdp->u[i]*psdp->l[k])*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*psdp->l[k];
                break;

            case 10:
                /* subgradient =  -(u_k - l_k)x_ix_j - (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_k x_j  -( u_jl_k - l_ju_k)x_i + u_il_jx_k - u_il_ju_k */
                check_violated[0][l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] - psdp->u[j])  *matrix[ij2k(i+1,k+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->l[j]*matrix[ij2k(0,k+1,psdp->n+1)] -( psdp->u[j]*psdp->l[k] - psdp->l[j]*psdp->u[k])*matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*psdp->u[k];
                break;

            case 11:
                /* subgradient =   -(l_j - u_j) x_ix_k - u_kx_ix_j - (u_i - l_i)x_jx_k + u_iu_k x_j  + l_ju_kx_i -(u_jl_i - u_il_j) x_k - u_il_ju_k  */
                check_violated[0][l] =   - psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] - psdp->u[j])  *matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - (psdp->l[i]*psdp->u[j] - psdp->u[i]*psdp->l[j])*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->l[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*psdp->u[k];
                break;

            case 12:
                /* subgradient =     -(u_k - l_k)x_ix_j - u_jx_ix_k - (l_i - u_i)x_jx_k - (u_il_k - l_iu_k) x_j  + u_ju_kx_i + l_iu_jx_k - l_iu_ju_k  */
                check_violated[0][l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->l[i] - psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->l[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] -( psdp->u[i]*psdp->l[k] -  psdp->l[i]*psdp->u[k])*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*psdp->u[k];
                break;

            case 13:
                /* subgradient =    -u_k x_ix_j - (u_j - l_j) x_ix_k - (l_i - u_i)x_jx_k + l_iu_k x_j  + u_ju_kx_i - (u_il_j- l_iu_j)x_k - l_iu_ju_k */
                check_violated[0][l] =   - psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  -( psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->l[i] - psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] -( psdp->u[i]*psdp->l[j] - psdp->l[i]*psdp->u[j])*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] +  psdp->l[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*psdp->u[k];
                break;

            case 14:
                /* subgradient =    -(u_k - l_k) x_ix_j - (u_j - l_j) x_ix_k  + u_ix_jx_k - u_il_jx_k - u_il_k x_j- (l_jl_k - u_iu_k)x_i  + u_il_jl_k  */
                check_violated[0][l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  -( psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*matrix[ij2k(0,k+1,psdp->n+1)] -( psdp->l[j]*psdp->l[k] - psdp->u[j]*psdp->u[k])*matrix[ij2k(0,i+1,psdp->n+1)] -  psdp->u[i]*psdp->l[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->l[j]*psdp->l[k];
                break;


            case 15:
                /* subgradient =  -(u_k - l_k) x_ix_j + u_j x_ix_k  -(u_i - l_i)x_jx_k - l_iu_jx_k - (l_il_k - u_iu_k) x_j- u_jl_kx_i + l_iu_jl_k  */
                check_violated[0][l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->l[k]*matrix[ij2k(0,i+1,psdp->n+1)] - ( psdp->l[i]*psdp->l[k] -  psdp->u[i]*psdp->u[k])*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->u[j]*psdp->l[k];
                break;

            case 16:
                /* subgradient =  u_k x_ix_j -(u_j - l_j) x_ix_k  -(u_i - l_i)x_jx_k - (l_il_j - u_iu_j) x_k - l_iu_k  x_j- l_ju_kx_i + l_il_ju_k  */
                check_violated[0][l] =   psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]-(psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] -( psdp->l[i]*psdp->l[j] - psdp->u[i]*psdp->u[j])*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->l[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] -  psdp->l[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j]*psdp->u[k];
                break;

            default:
                printf("\n Erreur de type de contrainte (compute subgradient 0 1)\n");
                break;
            }
        }
        else
            break;

    }


    /* if (DEBUG ==1) */
    /*   { */
    /*      printf("\ncheck_violated compute new subgradient\n"); */
    /*     print_vec_d(check_violated[0],psdp->nb_cont); */
    /*     printf("\n"); */
    /*   } */
    free_vector_d(matrix);
}

/* purge or add dualized constraints to the objective function */
int check_constraints_violated_triangle(SDP psdp, double ** check_violated, double ** x)
{

    int i,j,k,l;
    double * zcheck_violated=alloc_vector_d(psdp->length);
    double * matrix;
    int nb_cont_violated = 0;

    free_vector_d(check_violated[0]);
    make_matrix_with_vector_new_sub(psdp, &matrix,x[0]);

    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\npsdp->u (check_const_violated)\n"); */
    /*     print_vec_d(psdp->u,psdp->n); */
    /*     printf("\n"); */
    /*     printf("\npsdp->l (check_const_violated)\n"); */
    /*     print_vec_d(psdp->l,psdp->n); */
    /*     printf("\n"); */
    /*     printf("\nmatrix (check_const_violated_triangle\n"); */
    /*     print_mat_d(matrix,psdp->n+1,psdp->n+1); */
    /*     printf("\n"); */
    /*   } */

    for(l=0; l<psdp->length; l++)
    {
        i = psdp->tab[l][0];
        j = psdp->tab[l][1];
        k = psdp->tab[l][2];
        switch (psdp->tab[l][3])
        {
        case 1:
            /* subgradient =  - x_ix_j + u_ix_j + l_jx_i - u_il_j*/
            zcheck_violated[l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->l[j];
            break;

        case 2:
            /* subgradient = -x_ix_j + u_jx_i+ l_ix_j - u_jl_i*/
            zcheck_violated[l] = -matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)]+ psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)]- psdp->u[j]*psdp->l[i];
            break;

        case 3:
            /* subgradient =  x_ix_j - u_jx_i - u_ix_j + u_iu_j */
            zcheck_violated[l] = matrix[ij2k(i+1,j+1,psdp->n+1)] - psdp->u[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j];
            break;

        case 4:
            /* subgradient = x_ix_j- l_jx_i - l_ix_j + l_il_j*/
            zcheck_violated[l] = matrix[ij2k(i+1,j+1,psdp->n+1)]- psdp->l[j]*matrix[ij2k(0,i+1,psdp->n+1)] - psdp->l[i]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j];
            break;

        case 5:
            /* subgradient = -(l_k - u_k)x_ix_j - (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_kx_j - (u_ju_k - l_jl_k)x_i - u_i_u_jx_k + u_iu_ju_k  */
            zcheck_violated[l] = - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]- (psdp->l[j] - psdp->u[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - (psdp->u[j]*psdp->u[k] -psdp->l[j]*psdp->l[k]) *matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
            break;

        case 6:
            /* subgradient =  -(l_k - u_k)x_ix_j + u_jx_ix_k - (l_i - u_i)x_jx_k - (u_iu_k - l_il_k)x_j - u_ju_k x_i - u_i_u_jx_k + u_iu_ju_k  */
            zcheck_violated[l] = - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->l[i] -  psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->u[k] *matrix[ij2k(0,i+1,psdp->n+1)] - (psdp->u[i]*psdp->u[k] -psdp->l[i]*psdp->l[k])*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
            break;

        case 7:
            /* subgradient =   u_kx_ix_j - (l_j- u_j)x_ix_k -(l_i - u_i)x_jx_k - u_iu_kx_j - u_ju_k x_i - (u_i_u_j - l_il_j)x_k + u_iu_ju_k   */
            zcheck_violated[l] = psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] -psdp->u[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->l[i] -  psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - (psdp->u[i]*psdp->u[j]-psdp->l[i]*psdp->l[j])*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->u[k] *matrix[ij2k(0,i+1,psdp->n+1)] - psdp->u[i]*psdp->u[k] *matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*psdp->u[k];
            break;

        case 8:
            /* subgradient =   -(l_k - u_k)x_ix_j - (u_j - l_j)x_ix_k - u_ix_jx_k + u_il_kx_j - (l_ju_k - u_jl_k) x_i + u_i_u_jx_k - u_iu_jl_k  */
            zcheck_violated[l] =   - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->u[j] -psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - (psdp->l[j]*psdp->u[k] - psdp->u[j]*psdp->l[k]) *matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->l[k] *matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*psdp->l[k];
            break;

        case 9:
            /* subgradient =   -(l_k - u_k)x_ix_j - u_jx_ix_k - (u_i - l_i)x_jx_k - (l_iu_k- u_il_k) x_j  + u_jl_k x_i + u_i_u_jx_k - u_iu_jl_k */
            zcheck_violated[l] =   - (psdp->l[k] - psdp->u[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - psdp->u[j] *matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->u[i]-psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->l[k]*matrix[ij2k(0,i+1,psdp->n+1)] - (psdp->l[i]*psdp->u[k] - psdp->u[i]*psdp->l[k])*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->u[j]*psdp->l[k];
            break;

        case 10:
            /* subgradient =  -(u_k - l_k)x_ix_j - (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_k x_j  -( u_jl_k - l_ju_k)x_i + u_il_jx_k - u_il_ju_k */
            zcheck_violated[l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] - psdp->u[j])  *matrix[ij2k(i+1,k+1,psdp->n+1)] - psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->u[i]*psdp->l[j]*matrix[ij2k(0,k+1,psdp->n+1)] -( psdp->u[j]*psdp->l[k] - psdp->l[j]*psdp->u[k])*matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*psdp->u[k];
            break;

        case 11:
            /* subgradient =   -(l_j - u_j) x_ix_k - u_kx_ix_j - (u_i - l_i)x_jx_k + u_iu_k x_j  + l_ju_kx_i -(u_jl_i - u_il_j) x_k - u_il_ju_k  */
            zcheck_violated[l] =   - psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  - (psdp->l[j] - psdp->u[j])  *matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - (psdp->l[i]*psdp->u[j] - psdp->u[i]*psdp->l[j])*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->l[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] + psdp->u[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*psdp->u[k];
            break;

        case 12:
            /* subgradient =     -(u_k - l_k)x_ix_j - u_jx_ix_k - (l_i - u_i)x_jx_k - (u_il_k - l_iu_k) x_j  + u_ju_kx_i + l_iu_jx_k - l_iu_ju_k  */
            zcheck_violated[l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  - psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->l[i] - psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] + psdp->l[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] -( psdp->u[i]*psdp->l[k] -  psdp->l[i]*psdp->u[k])*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*psdp->u[k];
            break;

        case 13:
            /* subgradient =    -u_k x_ix_j - (u_j - l_j) x_ix_k - (l_i - u_i)x_jx_k + l_iu_k x_j  + u_ju_kx_i - (u_il_j- l_iu_j)x_k - l_iu_ju_k */
            zcheck_violated[l] =   - psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]  -( psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - (psdp->l[i] - psdp->u[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] -( psdp->u[i]*psdp->l[j] - psdp->l[i]*psdp->u[j])*matrix[ij2k(0,k+1,psdp->n+1)] + psdp->u[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] +  psdp->l[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*psdp->u[k];
            break;

        case 14:
            /* subgradient =    -(u_k - l_k) x_ix_j - (u_j - l_j) x_ix_k  + u_ix_jx_k - u_il_jx_k - u_il_k x_j- (l_jl_k - u_iu_k)x_i  + u_il_jl_k  */
            zcheck_violated[l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)]  -( psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] + psdp->u[i]*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->u[i]*psdp->l[j]*matrix[ij2k(0,k+1,psdp->n+1)] -( psdp->l[j]*psdp->l[k] - psdp->u[j]*psdp->u[k])*matrix[ij2k(0,i+1,psdp->n+1)] -  psdp->u[i]*psdp->l[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->u[i]*psdp->l[j]*psdp->l[k];
            break;


        case 15:
            /* subgradient =  -(u_k - l_k) x_ix_j + u_j x_ix_k  -(u_i - l_i)x_jx_k - l_iu_jx_k - (l_il_k - u_iu_k) x_j- u_jl_kx_i + l_iu_jl_k  */
            zcheck_violated[l] =   - (psdp->u[k] - psdp->l[k])*matrix[ij2k(i+1,j+1,psdp->n+1)] + psdp->u[j]*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] - psdp->l[i]*psdp->u[j]*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->u[j]*psdp->l[k]*matrix[ij2k(0,i+1,psdp->n+1)] - ( psdp->l[i]*psdp->l[k] -  psdp->u[i]*psdp->u[k])*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->u[j]*psdp->l[k];
            break;

        case 16:
            /* subgradient =  u_k x_ix_j -(u_j - l_j) x_ix_k  -(u_i - l_i)x_jx_k - (l_il_j - u_iu_j) x_k - l_iu_k  x_j- l_ju_kx_i + l_il_ju_k  */
            zcheck_violated[l] =   psdp->u[k]*matrix[ij2k(i+1,j+1,psdp->n+1)]-(psdp->u[j] - psdp->l[j])*matrix[ij2k(i+1,k+1,psdp->n+1)] - ( psdp->u[i] - psdp->l[i])*matrix[ij2k(j+1,k+1,psdp->n+1)] -( psdp->l[i]*psdp->l[j] - psdp->u[i]*psdp->u[j])*matrix[ij2k(0,k+1,psdp->n+1)] - psdp->l[j]*psdp->u[k]*matrix[ij2k(0,i+1,psdp->n+1)] -  psdp->l[i]*psdp->u[k]*matrix[ij2k(0,j+1,psdp->n+1)] + psdp->l[i]*psdp->l[j]*psdp->u[k];
            break;

        default:
            printf("\n Erreur de type de contrainte (check violated 0 1) \n");
            break;

        }
        if (Negative(zcheck_violated[l]))
            nb_cont_violated++;

    }

    check_violated[0]=zcheck_violated;

    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\ncheck_violated_triangle (check_const_violated_triangle\n)\n"); */
    /*     print_vec_d(check_violated[0],psdp->nb_cont); */
    /*     printf("\n"); */
    /*   } */



    for(i=0; i<psdp->length; i++)
        if (Negative(check_violated[0][i]))
            return 0;
    return 1;
}

void update_beta_value_triangle(SDP psdp, double * zbeta)
{
    int i,j,k,l;



    free_vector_d(psdp->beta_1);
    free_vector_d(psdp->beta_2);
    free_vector_d(psdp->beta_3);
    free_vector_d(psdp->beta_4);
    free_vector_d(psdp->delta_1);
    free_vector_d(psdp->delta_2);
    free_vector_d(psdp->delta_3);
    free_vector_d(psdp->delta_4);
    free_vector_d(psdp->delta_5);
    free_vector_d(psdp->delta_6);
    free_vector_d(psdp->delta_7);
    free_vector_d(psdp->delta_8);
    free_vector_d(psdp->delta_9);
    free_vector_d(psdp->delta_10);
    free_vector_d(psdp->delta_11);
    free_vector_d(psdp->delta_12);


    double * zbeta_1=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_2=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_3=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_4=alloc_matrix_d(psdp->n,psdp->n);
    double * zdelta_1=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_2=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_3=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_4=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_5=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_6=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_7=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_8=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_9=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_10=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_11=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);
    double * zdelta_12=alloc_matrix_3d(psdp->n,psdp->n,psdp->n);

    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
        {
            zbeta_1[ij2k(i,j,psdp->n)]=0;
            zbeta_2[ij2k(i,j,psdp->n)]=0;
            zbeta_3[ij2k(i,j,psdp->n)]=0;
            zbeta_4[ij2k(i,j,psdp->n)]=0;
        }

    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            for(k=0; k<psdp->n; k++)
            {
                zdelta_1[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_2[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_3[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_4[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_5[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_6[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_7[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_8[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_9[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_10[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_11[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
                zdelta_12[ijk2l(i,j,k,psdp->n,psdp->n)]=0;
            }


    /* if (DEBUG==1) */
    /*   { */
    /*     printf("\n beta entier\n\n"); */
    /*     print_vec_d(zbeta,psdp->nb_cont); */
    /*     printf("\n\n"); */

    /*   } */

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            k = psdp->tab[psdp->constraints[l]-1][2];
            switch (psdp->tab[psdp->constraints[l]-1][3])
            {
            case 1:
                /*Dual variables associated to constraints  x_ix_j - u_ix_j  - l_jx_i + u_il_j <= 0*/
                zbeta_1[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_1[ij2k(j,i,psdp->n)]=zbeta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dual variables associated to constraints  x_ix_j - u_jx_i  - l_ix_j + u_jl_i  <= 0*/
                zbeta_2[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_2[ij2k(j,i,psdp->n)]=zbeta_2[ij2k(i,j,psdp->n)];
                break;

            case 3:
                /*Dual variables associated to constraints - x_ix_j + u_jx_i + u_ix_j - u_iu_j <= 0*/
                zbeta_3[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_3[ij2k(j,i,psdp->n)]=zbeta_3[ij2k(i,j,psdp->n)];
                break;

            case 4:
                /*Dual variables associated to constraints - x_ix_j+ l_jx_i + l_ix_j - l_il_j <= 0*/
                zbeta_4[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_4[ij2k(j,i,psdp->n)]=zbeta_4[ij2k(i,j,psdp->n)];
                break;

            case 5:
                /*Dual variables associated to constraints  (l_k - u_k)x_ix_j + (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_kx_j + (u_ju_k - l_jl_k)x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
                zdelta_1[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_1[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 6:
                /*Dual variables associated to constraints  (l_k - u_k)x_ix_j - u_jx_ix_k +(l_i - u_i)x_jx_k + (u_iu_k - l_il_k)x_j + u_ju_k x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
                zdelta_2[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_2[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 7:
                /*Dual variables associated to constraints - u_kx_ix_j + (l_j- u_j)x_ix_k +(l_i - u_i)x_jx_k + u_iu_kx_j + u_ju_k x_i + (u_i_u_j - l_il_j)x_k - u_iu_ju_k <= 0*/
                zdelta_3[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_3[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 8:
                /*Dual variables associated to constraints   (l_k - u_k)x_ix_j + (u_j - l_j)x_ix_k + u_ix_jx_k - u_il_kx_j + (l_ju_k - u_jl_k) x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
                zdelta_4[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_4[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;
            case 9:
                /*Dual variables associated to constraints (l_k - u_k)x_ix_j + u_jx_ix_k + (u_i - l_i)x_jx_k + (l_iu_k- u_il_k) x_j  - u_jl_k x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
                zdelta_5[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_5[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 10:
                /*Dual variables associated to constraints  (u_k - l_k)x_ix_j + (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_k x_j  +( u_jl_k - l_ju_k)x_i - u_il_jx_k + u_il_ju_k <= 0*/
                zdelta_6[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_6[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 11:
                /*Dual variables associated to constraints  (l_j - u_j) x_ix_k + u_kx_ix_j + (u_i - l_i)x_jx_k - u_iu_k x_j  - l_ju_kx_i +(u_jl_i - u_il_j) x_k + u_il_ju_k <= 0*/
                zdelta_7[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_7[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 12:
                /*Dual variables associated to constraints (u_k - l_k)x_ix_j + u_jx_ix_k + (l_i - u_i)x_jx_k + (u_il_k - l_iu_k) x_j  - u_ju_kx_i - l_iu_jx_k + l_iu_ju_k <= 0*/
                zdelta_8[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_8[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 13:
                /*Dual variables associated to constraints u_k x_ix_j + (u_j - l_j) x_ix_k + (l_i - u_i)x_jx_k - l_iu_k x_j  - u_ju_kx_i + (u_il_j- l_iu_j)x_k + l_iu_ju_k <= 0*/
                zdelta_9[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_9[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 14:
                /*Dual variables associated to constraints (u_k - l_k) x_ix_j + (u_j - l_j) x_ix_k  - u_ix_jx_k + u_il_jx_k + u_il_k x_j+ (l_jl_k - u_iu_k)x_i  - u_il_jl_k <= 0*/
                zdelta_10[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_10[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 15:
                /*Dual variables associated to constraints (u_k - l_k) x_ix_j - u_j x_ix_k  +(u_i - l_i)x_jx_k + l_iu_jx_k + (l_il_k - u_iu_k) x_j+ u_jl_kx_i - l_iu_jl_k <= 0*/
                zdelta_11[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_11[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            case 16:
                /*Dual variables associated to constraints  -u_k x_ix_j +(u_j -l_j) x_ix_k  +(u_i - l_i)x_jx_k + (l_il_j - u_iu_j) x_k + l_iu_k  x_j+ l_ju_kx_i - l_il_ju_k <= 0*/
                zdelta_12[ijk2l(i,j,k,psdp->n,psdp->n)]=zdelta_12[ijk2l(i,j,k,psdp->n,psdp->n)]+zbeta[l]/2;
                break;

            default:
                printf("\n Erreur de type de contrainte (update delta) \n");
                break;
            }
        }
        else
            break;

    }

    psdp->beta_1 =zbeta_1;
    psdp->beta_2 =zbeta_2;
    psdp->beta_3 =zbeta_3;
    psdp->beta_4 =zbeta_4;
    psdp->delta_1 =zdelta_1;
    psdp->delta_2 =zdelta_2;
    psdp->delta_3 =zdelta_3;
    psdp->delta_4 =zdelta_4;
    psdp->delta_5 =zdelta_5;
    psdp->delta_6 =zdelta_6;
    psdp->delta_7 =zdelta_7;
    psdp->delta_8 =zdelta_8;
    psdp->delta_9 =zdelta_9;
    psdp->delta_10 =zdelta_10;
    psdp->delta_11 =zdelta_11;
    psdp->delta_12 =zdelta_12;


    /* if (DEBUG == 1) */
    /*   { */
    /*     printf("\n beta1 (update alpha beta triangle)\n"); */
    /*     print_mat_d(psdp->beta_1,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*       printf("\n beta2 (update alpha beta triangle)\n"); */
    /*     print_mat_d(psdp->beta_2,psdp->n,psdp->n); */
    /*     printf("\n"); */

    /*   printf("\n beta3 (update alpha beta triangle)\n"); */
    /*     print_mat_d(psdp->beta_3,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*       printf("\n beta4 (update alpha beta triangle)\n"); */
    /*     print_mat_d(psdp->beta_4,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*     printf("\n delta1 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_1,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta2 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_2,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */

    /*      printf("\n delta3 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_3,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta4 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_4,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta5 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_5,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */

    /*     printf("\n delta6 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_6,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta7 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_7,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta8 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_8,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta9 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_9,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta10 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_10,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta11 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_11,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*      printf("\n delta12 (update alpha beta triangle)\n"); */
    /*     print_mat_3d(psdp->delta_12,psdp->n,psdp->n,psdp->n); */
    /*     printf("\n"); */
    /*   } */


}


/*******************************************************************************************/
/*********************** utilities for the sdp solver QAP **********************************/
/*******************************************************************************************/
int nb_cont_Xij_qap(SDP psdp)
{
    return (psdp->nb_init_var)*(psdp->nb_init_var)*(psdp->nb_init_var-1)/2;
}

int nb_max_cont_qap(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3)
{

    int i,j;
    int cont_pos= nb_cont_Xij(psdp->n,psdp->n,0);
    int cont_cut = nb_cont_Xij_qap(psdp);

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */


    *ind_X1=1;
    *ind_X2=*ind_X1+cont_pos;
    *ind_X3=*ind_X2+cont_cut;

    return *ind_X3+ cont_cut-1;
}

void dualized_objective_function_qap(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{

    int i,j,k,l;

    *q_beta = copy_matrix_d(psdp->q,psdp->n,psdp->n);
    *c_beta = copy_vector_d(psdp->c,psdp->n);

    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = -q_beta[0][ij2k(i,j,psdp->n)];

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = -c_beta[0][i];

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /* Dualization of constraints - x_ix_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dualization of constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 3:
                /*Dualization of constraints  x_(ij)x_(kj) <= 0, i,j,k : i<k */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_3[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            default:
                printf("\n Error of constraint type (dualize objective function qap)\n");
                break;
            }
        }
        else
            break;
    }


}

void compute_subgradient_qap(double ** check_violated,SDP psdp)
{

    double * matrix;

    make_matrix_with_vector(psdp, &matrix);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,l;


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /* subgradient = x_ix_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 2:
                /* subgradient =   -x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 3:
                /* subgradient =  -x_(ij)x_(kj) , i,j,k : i<k*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;

            default:
                printf("\n Erreur de type de contrainte (compute subgradient qap)\n");
                break;
            }
        }
        else
            break;

    }

    free_vector_d(matrix);
}


void compute_new_subgradient_qap(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X)
{

    double * matrix;

    make_matrix_with_vector_new_sub(psdp, &matrix,X[0]);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,l;

    int * constraints_new=alloc_vector(new_length);
    int index=variable_indices[0][0];

    for(i=0; i<new_length; i++)
    {
        constraints_new[i]=psdp->constraints[index+i];
    }

    for(l=0; l<new_length; l++)
    {
        if (constraints_new[l] >0)
        {
            i = psdp->tab[constraints_new[l]-1][0];
            j = psdp->tab[constraints_new[l]-1][1];
            switch (psdp->tab[constraints_new[l]-1][2])
            {
            case 1:
                /* subgradient = x_ix_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 2:
                /* subgradient =   -x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 3:
                /* subgradient =  -x_(ij)x_(kj) , i,j,k : i<k*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            default:
                printf("\n Erreur de type de contrainte (compute new subgradient qap)\n");
                break;

            }
        }
        else
            break;

    }

    free_vector_d(matrix);
}


/* purge or add dualized constraints to the objective function */
int check_constraints_violated_qap(SDP psdp, double ** check_violated, double ** x)
{

    int i,j,k,l;
    double * zcheck_violated=alloc_vector_d(psdp->length);
    double * matrix;


    free_vector_d(check_violated[0]);
    make_matrix_with_vector_new_sub(psdp, &matrix,x[0]);


    for(l=0; l<psdp->length; l++)
    {
        i = psdp->tab[l][0];
        j = psdp->tab[l][1];

        switch (psdp->tab[l][2])
        {
        case 1:
            /* check_violated = x_ix_j*/
            zcheck_violated[l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        case 2:
            /* check_violated   =  -x_(ij)x_(il) , i,j,l : j<l */
            zcheck_violated[l] =  -matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        case 3:
            /*  check_violated  =  -x_(ij)x_(kj) , i,j,k : i<k*/
            zcheck_violated[l] =  -matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        default:
            printf("\n Erreur de type de contrainte (check violated qap) \n");
            break;

        }

    }



    check_violated[0]=zcheck_violated;

    free_vector_d(matrix);

    for(i=0; i<psdp->length; i++)
        if (Negative(check_violated[0][i]))
            return 0;
    return 1;
}


void update_beta_value_qap(SDP psdp, double * zbeta)
{
    int i,j,k,l;



    free_vector_d(psdp->beta_1);
    free_vector_d(psdp->beta_2);
    free_vector_d(psdp->beta_3);

    double * zbeta_1=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_2=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_3=alloc_matrix_d(psdp->n,psdp->n);


    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
        {
            zbeta_1[ij2k(i,j,psdp->n)]=0;
            zbeta_2[ij2k(i,j,psdp->n)]=0;
            zbeta_3[ij2k(i,j,psdp->n)]=0;
        }


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];

            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /*Dual variables associated to constraints - x_ix_j <= 0*/
                zbeta_1[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_1[ij2k(j,i,psdp->n)]=zbeta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dual variables associated to constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
                zbeta_2[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_2[ij2k(j,i,psdp->n)]=zbeta_2[ij2k(i,j,psdp->n)];
                break;

            case 3:
                /*Dual variables associated to constraints    x_(ij)x_(kj) = 0, i,j,k : i<k*/
                zbeta_3[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_3[ij2k(j,i,psdp->n)]=zbeta_3[ij2k(i,j,psdp->n)];
                break;

            default:
                printf("\n Erreur de type de contrainte (update delta) \n");
                break;
            }
        }
        else
            break;

    }

    psdp->beta_1 =zbeta_1;
    psdp->beta_2 =zbeta_2;
    psdp->beta_3 =zbeta_3;



}

/*******************************************************************************************/
/*********************** utilities for the sdp solver QAP V2********************************/
/*******************************************************************************************/
int nb_max_cont_qapv2(SDP psdp, int * ind_X1,int * ind_X2,int *ind_X3,int * ind_X4)
{

    int i,j;
    int cont_cut = nb_cont_Xij_qap(psdp);

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */


    *ind_X1=1;
    *ind_X2=*ind_X1+cont_cut;
    *ind_X3=*ind_X2+cont_cut;
    *ind_X4=*ind_X3+cont_cut;
    return *ind_X4+ cont_cut-1;
}

void dualized_objective_function_qapv2(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{

    int i,j,k,l;

    *q_beta = copy_matrix_d(psdp->q,psdp->n,psdp->n);
    *c_beta = copy_vector_d(psdp->c,psdp->n);

    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = -q_beta[0][ij2k(i,j,psdp->n)];

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = -c_beta[0][i];

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /*Dualization of constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dualization of constraints  -x_(ij)x_(il) <= 0, i,j,l : j<l */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 3:
                /*Dualization of constraints  x_(ij)x_(kj) <= 0, i,j,k : i<k */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_3[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 4:
                /*Dualization of constraints  -x_(ij)x_(kj) <= 0, i,j,k : i<k */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_4[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            default:
                printf("\n Error of constraint type (dualize objective function qapv2)\n");
                break;
            }
        }
        else
            break;
    }


}

void compute_subgradient_qapv2(double ** check_violated,SDP psdp)
{

    double * matrix;

    make_matrix_with_vector(psdp, &matrix);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,l;


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /* subgradient =   -x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 2:
                /* subgradient =   x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 3:
                /* subgradient =  -x_(ij)x_(kj) , i,j,k : i<k*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 4:
                /* subgradient =  x_(ij)x_(kj) , i,j,k : i<k*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;

            default:
                printf("\n Erreur de type de contrainte (compute subgradient qapv2)\n");
                break;
            }
        }
        else
            break;

    }

    free_vector_d(matrix);
}

void compute_new_subgradient_qapv2(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X)
{

    double * matrix;

    make_matrix_with_vector_new_sub(psdp, &matrix,X[0]);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,l;

    int * constraints_new=alloc_vector(new_length);
    int index=variable_indices[0][0];

    for(i=0; i<new_length; i++)
    {
        constraints_new[i]=psdp->constraints[index+i];
    }

    for(l=0; l<new_length; l++)
    {
        if (constraints_new[l] >0)
        {
            i = psdp->tab[constraints_new[l]-1][0];
            j = psdp->tab[constraints_new[l]-1][1];
            switch (psdp->tab[constraints_new[l]-1][2])
            {
            case 1:
                /* subgradient =   -x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 2:
                /* subgradient =   x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 3:
                /* subgradient =  -x_(ij)x_(kj) , i,j,k : i<k*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 4:
                /* subgradient =  x_(ij)x_(kj) , i,j,k : i<k*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            default:
                printf("\n Erreur de type de contrainte (compute new subgradient qapv2)\n");
                break;

            }
        }
        else
            break;

    }


    free_vector_d(matrix);
}

/* purge or add dualized constraints to the objective function */
int check_constraints_violated_qapv2(SDP psdp, double ** check_violated, double ** x)
{

    int i,j,k,l;
    double * zcheck_violated=alloc_vector_d(psdp->length);
    double * matrix;


    free_vector_d(check_violated[0]);
    make_matrix_with_vector_new_sub(psdp, &matrix,x[0]);


    for(l=0; l<psdp->length; l++)
    {
        i = psdp->tab[l][0];
        j = psdp->tab[l][1];

        switch (psdp->tab[l][2])
        {
        case 1:
            /* check_violated   =  -x_(ij)x_(il) , i,j,l : j<l */
            zcheck_violated[l] =  -matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        case 2:
            /* check_violated   =  x_(ij)x_(il) , i,j,l : j<l */
            zcheck_violated[l] =  matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        case 3:
            /*  check_violated  =  -x_(ij)x_(kj) , i,j,k : i<k*/
            zcheck_violated[l] =  -matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        case 4:
            /*  check_violated  =  x_(ij)x_(kj) , i,j,k : i<k*/
            zcheck_violated[l] =  matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        default:
            printf("\n Erreur de type de contrainte (check violated qapv2) \n");
            break;

        }

    }



    check_violated[0]=zcheck_violated;

    free_vector_d(matrix);


    for(i=0; i<psdp->length; i++)
        if (Negative(check_violated[0][i]))
            return 0;
    return 1;
}


void update_beta_value_qapv2(SDP psdp, double * zbeta)
{
    int i,j,k,l;



    free_vector_d(psdp->beta_1);
    free_vector_d(psdp->beta_2);
    free_vector_d(psdp->beta_3);
    free_vector_d(psdp->beta_4);


    double * zbeta_1=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_2=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_3=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_4=alloc_matrix_d(psdp->n,psdp->n);


    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
        {
            zbeta_1[ij2k(i,j,psdp->n)]=0;
            zbeta_2[ij2k(i,j,psdp->n)]=0;
            zbeta_3[ij2k(i,j,psdp->n)]=0;
            zbeta_4[ij2k(i,j,psdp->n)]=0;


        }


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];

            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /*Dual variables associated to constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
                zbeta_1[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_1[ij2k(j,i,psdp->n)]=zbeta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dual variables associated to constraints  -x_(ij)x_(il) <= 0, i,j,l : j<l */
                zbeta_2[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_2[ij2k(j,i,psdp->n)]=zbeta_2[ij2k(i,j,psdp->n)];
                break;

            case 3:
                /*Dual variables associated to constraints    x_(ij)x_(kj) = 0, i,j,k : i<k*/
                zbeta_3[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_3[ij2k(j,i,psdp->n)]=zbeta_3[ij2k(i,j,psdp->n)];
                break;

            case 4:
                /*Dual variables associated to constraints    x_(ij)x_(kj) = 0, i,j,k : i<k*/
                zbeta_4[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_4[ij2k(j,i,psdp->n)]=zbeta_4[ij2k(i,j,psdp->n)];
                break;

            default:
                printf("\n Erreur de type de contrainte (update delta) \n");
                break;
            }
        }
        else
            break;

    }

    psdp->beta_1 =zbeta_1;
    psdp->beta_2 =zbeta_2;
    psdp->beta_3 =zbeta_3;
    psdp->beta_4 =zbeta_4;




}

/*******************************************************************************************/
/*********************** utilities for the sdp solver QAP V3********************************/
/*******************************************************************************************/
int nb_cont_Xij_qapv3(SDP psdp)
{
    return (psdp->nb_init_var)*(psdp->nb_init_var-1)/2;
}

int nb_max_cont_qapv3(SDP psdp, int * ind_X1)
{

    int i,j;
    int cont_cut = nb_cont_Xij_qapv3(psdp);

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */


    *ind_X1=1;
    return *ind_X1+ cont_cut-1;
}


void dualized_objective_function_qapv3(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{

    int i,j,k,l;
    *q_beta = copy_matrix_d(psdp->q,psdp->n,psdp->n);
    *c_beta = copy_vector_d(psdp->c,psdp->n);

    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = -q_beta[0][ij2k(i,j,psdp->n)];

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = -c_beta[0][i];

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /*Dualization of constraints  -x_ix_j <= 0, i,j :i<j */
                for (k=0; k<psdp->nb_init_var; k++)
                {
                    q_beta[0][ij2k((i+k*psdp->nb_init_var),(j+k*psdp->nb_init_var),psdp->n)]= q_beta[0][ij2k((i+k*psdp->nb_init_var),(j+k*psdp->nb_init_var),psdp->n)] + psdp->beta_1[ij2k((i+k*psdp->nb_init_var),(j+k*psdp->nb_init_var),psdp->n)];
                    q_beta[0][ij2k((j+k*psdp->nb_init_var),(i+k*psdp->nb_init_var),psdp->n)]=q_beta[0][ij2k((i+k*psdp->nb_init_var),(j+k*psdp->nb_init_var),psdp->n)];
                }
                break;

            default:
                printf("\n Error of constraint type (dualize objective function qapv3)\n");
                break;
            }
        }
        else
            break;
    }


}


void compute_subgradient_qapv3(double ** check_violated,SDP psdp)
{

    double * matrix;

    make_matrix_with_vector(psdp, &matrix);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,k,l;


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /* subgradient =   x_ix_j <= 0, i,j :i<j */

                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;


            default:
                printf("\n Erreur de type de contrainte (compute subgradient qapv3)\n");
                break;
            }
        }
        else
            break;

    }

    free_vector_d(matrix);
}


void compute_new_subgradient_qapv3(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X)
{

    double * matrix;

    make_matrix_with_vector_new_sub(psdp, &matrix,X[0]);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,k,l;

    int * constraints_new=alloc_vector(new_length);
    int index=variable_indices[0][0];

    for(i=0; i<new_length; i++)
    {
        constraints_new[i]=psdp->constraints[index+i];
    }

    for(l=0; l<new_length; l++)
    {
        if (constraints_new[l] >0)
        {
            i = psdp->tab[constraints_new[l]-1][0];
            j = psdp->tab[constraints_new[l]-1][1];
            switch (psdp->tab[constraints_new[l]-1][2])
            {
            case 1:
                /* subgradient = x_ix_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;

            default:
                printf("\n Erreur de type de contrainte (compute new subgradient qapv3)\n");
                break;
            }
        }
        else
            break;
    }
    free_vector_d(matrix);
}


/* purge or add dualized constraints to the objective function */
int check_constraints_violated_qapv3(SDP psdp, double ** check_violated, double ** x)
{

    int i,j,k,l;
    double * zcheck_violated=alloc_vector_d(psdp->length);
    double * matrix;


    free_vector_d(check_violated[0]);
    make_matrix_with_vector_new_sub(psdp, &matrix,x[0]);


    for(l=0; l<psdp->length; l++)
    {
        i = psdp->tab[l][0];
        j = psdp->tab[l][1];

        switch (psdp->tab[l][2])
        {
        case 1:
            /* check_violated = x_ix_j*/
            zcheck_violated[l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        default:
            printf("\n Erreur de type de contrainte (check violated qapv3)\n");
            break;

        }

    }



    check_violated[0]=zcheck_violated;

    free_vector_d(matrix);


    for(i=0; i<psdp->length; i++)
        if (Negative(check_violated[0][i]))
            return 0;
    return 1;
}


void update_beta_value_qapv3(SDP psdp, double * zbeta)
{
    int i,j,k,l;



    free_vector_d(psdp->beta_1);



    double * zbeta_1=alloc_matrix_d(psdp->n,psdp->n);



    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
        {
            zbeta_1[ij2k(i,j,psdp->n)]=0;


        }


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];

            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /*Dual variables associated to constraints - x_ix_j <= 0*/
                zbeta_1[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_1[ij2k(j,i,psdp->n)]=zbeta_1[ij2k(i,j,psdp->n)];
                break;

            default:
                printf("\n Erreur de type de contrainte (update delta) \n");
                break;
            }
        }
        else
            break;

    }

    psdp->beta_1 =zbeta_1;




}

/*******************************************************************************************/
/*********************** utilities for the sdp solver MKC R4 **********************************/
/*******************************************************************************************/

int nb_cont_Xij_mkc(SDP psdp)
{
    int  k_mkc = (int) psdp->n/psdp->nb_init_var;
    printf("\n k_mkc : %d \n",k_mkc);
    return (psdp->nb_init_var)*(k_mkc)*(k_mkc-1)/2;
}


int nb_max_cont_mkc_r4(SDP psdp, int * ind_X1,int * ind_X2)
{

    int i,j;
    int cont_pos= nb_cont_Xij(psdp->n,psdp->n,0)-psdp->n;
    int cont_cut = nb_cont_Xij_mkc(psdp);

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */


    *ind_X1=1;
    *ind_X2=*ind_X1+cont_pos;

    return *ind_X2+ cont_cut-1;
}



void dualized_objective_function_mkc_r4(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{

    int i,j,k,l;

    *q_beta = copy_matrix_d(psdp->q,psdp->n,psdp->n);
    *c_beta = copy_vector_d(psdp->c,psdp->n);

    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = -q_beta[0][ij2k(i,j,psdp->n)];

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = -c_beta[0][i];

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /* Dualization of constraints - x_ix_j <= 0*/
                q_beta[0][ij2k(i,j,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dualization of constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            default:
                printf("\n Error of constraint type (dualize objective function mkc_r4)\n");
                break;
            }
        }
        else
            break;
    }


}



void compute_subgradient_mkc_r4(double ** check_violated,SDP psdp)
{

    double * matrix;

    make_matrix_with_vector(psdp, &matrix);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,l;


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /* subgradient = x_ix_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 2:
                /* subgradient =   -x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;

            default:
                printf("\n Erreur de type de contrainte (compute subgradient mkc_r4)\n");
                break;
            }
        }
        else
            break;

    }

    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\n psdp->length : %d\n\n psdp->nb_init_var : %d\n check_violated (compute subgradient)\n",psdp->length,psdp->nb_init_var); */

    /*     print_vec_d( check_violated[0],psdp->length); */
    /*     printf("\n\n"); */
    /*   } */
    free_vector_d(matrix);
}


void compute_new_subgradient_mkc_r4(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X)
{

    double * matrix;

    make_matrix_with_vector_new_sub(psdp, &matrix,X[0]);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,l;

    int * constraints_new=alloc_vector(new_length);
    int index=variable_indices[0][0];

    for(i=0; i<new_length; i++)
    {
        constraints_new[i]=psdp->constraints[index+i];
    }

    for(l=0; l<new_length; l++)
    {
        if (constraints_new[l] >0)
        {
            i = psdp->tab[constraints_new[l]-1][0];
            j = psdp->tab[constraints_new[l]-1][1];
            switch (psdp->tab[constraints_new[l]-1][2])
            {
            case 1:
                /* subgradient = x_ix_j*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 2:
                /* subgradient =   -x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            default:
                printf("\n Erreur de type de contrainte (compute new subgradient mkc_r4)\n");
                break;

            }
        }
        else
            break;

    }

    free_vector_d(matrix);
}


/* purge or add dualized constraints to the objective function */
int check_constraints_violated_mkc_r4(SDP psdp, double ** check_violated, double ** x)
{

    int i,j,k,l;
    double * zcheck_violated=alloc_vector_d(psdp->length);
    double * matrix;


    free_vector_d(check_violated[0]);
    make_matrix_with_vector_new_sub(psdp, &matrix,x[0]);


    for(l=0; l<psdp->length; l++)
    {
        i = psdp->tab[l][0];
        j = psdp->tab[l][1];

        switch (psdp->tab[l][2])
        {
        case 1:
            /* check_violated = x_ix_j*/
            zcheck_violated[l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        case 2:
            /* check_violated   =  -x_(ij)x_(il) , i,j,l : j<l */
            zcheck_violated[l] =  -matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;


        default:
            printf("\n Erreur de type de contrainte (check violated mkc_r4) \n");
            break;

        }

    }



    check_violated[0]=zcheck_violated;

    free_vector_d(matrix);

    for(i=0; i<psdp->length; i++)
        if (Negative(check_violated[0][i]))
            return 0;
    return 1;
}


void update_beta_value_mkc_r4(SDP psdp, double * zbeta)
{
    int i,j,k,l;



    free_vector_d(psdp->beta_1);
    free_vector_d(psdp->beta_2);

    double * zbeta_1=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_2=alloc_matrix_d(psdp->n,psdp->n);


    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
        {
            zbeta_1[ij2k(i,j,psdp->n)]=0;
            zbeta_2[ij2k(i,j,psdp->n)]=0;
        }


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];

            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /*Dual variables associated to constraints - x_ix_j <= 0*/
                zbeta_1[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_1[ij2k(j,i,psdp->n)]=zbeta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dual variables associated to constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
                zbeta_2[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_2[ij2k(j,i,psdp->n)]=zbeta_2[ij2k(i,j,psdp->n)];
                break;


            default:
                printf("\n Erreur de type de contrainte (update delta) \n");
                break;
            }
        }
        else
            break;

    }

    psdp->beta_1 =zbeta_1;
    psdp->beta_2 =zbeta_2;



}

/*******************************************************************************************/
/*********************** utilities for the sdp solver MKC R2********************************/
/*******************************************************************************************/
int nb_max_cont_mkc_r2(SDP psdp, int * ind_X1,int * ind_X2)
{

    int i,j;
    int cont_cut = nb_cont_Xij_mkc(psdp);

    /* Compute the number of constraints that ensure dual feasibility that are already in the sdp */


    *ind_X1=1;
    *ind_X2=*ind_X1+cont_cut;

    return *ind_X2+ cont_cut-1;
}


void dualized_objective_function_mkc_r2(SDP psdp, double ** q_beta, double ** c_beta, double * l_beta)
{

    int i,j,k,l;

    *q_beta = copy_matrix_d(psdp->q,psdp->n,psdp->n);
    *c_beta = copy_vector_d(psdp->c,psdp->n);

    *l_beta=psdp->cons;

    /* for all i,j q_ij = - q_ij and c_i = - c_i */
    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
            q_beta[0][ij2k(i,j,psdp->n)] = -q_beta[0][ij2k(i,j,psdp->n)];

    for(i=0; i<psdp->n; i++)
        c_beta[0][i] = -c_beta[0][i];

    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /*Dualization of constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] - psdp->beta_1[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dualization of constraints  -x_(ij)x_(il) <= 0, i,j,l : j<l */
                q_beta[0][ij2k(i,j,psdp->n)]= q_beta[0][ij2k(i,j,psdp->n)] + psdp->beta_2[ij2k(i,j,psdp->n)];
                q_beta[0][ij2k(j,i,psdp->n)]=q_beta[0][ij2k(i,j,psdp->n)];
                break;


            default:
                printf("\n Error of constraint type (dualize objective function mkc_r2)\n");
                break;
            }
        }
        else
            break;
    }


}


void compute_subgradient_mkc_r2(double ** check_violated,SDP psdp)
{

    double * matrix;

    make_matrix_with_vector(psdp, &matrix);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,l;


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];
            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /* subgradient =   -x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 2:
                /* subgradient =   x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            default:
                printf("\n Erreur de type de contrainte (compute subgradient mkc_r2)\n");
                break;
            }
        }
        else
            break;

    }



    free_vector_d(matrix);
}


void compute_new_subgradient_mkc_r2(double ** check_violated,SDP psdp,int new_length, int ** variable_indices, double ** X)
{

    double * matrix;

    make_matrix_with_vector_new_sub(psdp, &matrix,X[0]);

    /* Attention les indices de la matrice matrix sont décalés de 1*/

    int i,j,l;

    int * constraints_new=alloc_vector(new_length);
    int index=variable_indices[0][0];

    for(i=0; i<new_length; i++)
    {
        constraints_new[i]=psdp->constraints[index+i];
    }

    for(l=0; l<new_length; l++)
    {
        if (constraints_new[l] >0)
        {
            i = psdp->tab[constraints_new[l]-1][0];
            j = psdp->tab[constraints_new[l]-1][1];
            switch (psdp->tab[constraints_new[l]-1][2])
            {
            case 1:
                /* subgradient =   -x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = -matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            case 2:
                /* subgradient =   x_(ij)x_(il) , i,j,l : j<l*/
                check_violated[0][l] = matrix[ij2k(i+1,j+1,psdp->n+1)];
                break;
            default:
                printf("\n Erreur de type de contrainte (compute new subgradient mkc_r2)\n");
                break;

            }
        }
        else
            break;

    }


    free_vector_d(matrix);
}

/* purge or add dualized constraints to the objective function */
int check_constraints_violated_mkc_r2(SDP psdp, double ** check_violated, double ** x)
{

    int i,j,k,l;
    double * zcheck_violated=alloc_vector_d(psdp->length);
    double * matrix;


    free_vector_d(check_violated[0]);
    make_matrix_with_vector_new_sub(psdp, &matrix,x[0]);


    for(l=0; l<psdp->length; l++)
    {
        i = psdp->tab[l][0];
        j = psdp->tab[l][1];

        switch (psdp->tab[l][2])
        {
        case 1:
            /* check_violated   =  -x_(ij)x_(il) , i,j,l : j<l */
            zcheck_violated[l] =  -matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;

        case 2:
            /* check_violated   =  x_(ij)x_(il) , i,j,l : j<l */
            zcheck_violated[l] =  matrix[ij2k(i+1,j+1,psdp->n+1)];
            break;


        default:
            printf("\n Erreur de type de contrainte (check violated mkc_r2) \n");
            break;

        }

    }



    check_violated[0]=zcheck_violated;


    free_vector_d(matrix);


    for(i=0; i<psdp->length; i++)
        if (Negative(check_violated[0][i]))
            return 0;
    return 1;
}


void update_beta_value_mkc_r2(SDP psdp, double * zbeta)
{
    int i,j,k,l;



    free_vector_d(psdp->beta_1);
    free_vector_d(psdp->beta_2);


    double * zbeta_1=alloc_matrix_d(psdp->n,psdp->n);
    double * zbeta_2=alloc_matrix_d(psdp->n,psdp->n);


    for(i=0; i<psdp->n; i++)
        for(j=0; j<psdp->n; j++)
        {
            zbeta_1[ij2k(i,j,psdp->n)]=0;
            zbeta_2[ij2k(i,j,psdp->n)]=0;


        }


    for(l=0; l<psdp->nb_cont; l++)
    {
        if (psdp->constraints[l] >0)
        {
            i = psdp->tab[psdp->constraints[l]-1][0];
            j = psdp->tab[psdp->constraints[l]-1][1];

            switch (psdp->tab[psdp->constraints[l]-1][2])
            {
            case 1:
                /*Dual variables associated to constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
                zbeta_1[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_1[ij2k(j,i,psdp->n)]=zbeta_1[ij2k(i,j,psdp->n)];
                break;

            case 2:
                /*Dual variables associated to constraints  -x_(ij)x_(il) <= 0, i,j,l : j<l */
                zbeta_2[ij2k(i,j,psdp->n)]=zbeta[l]/2;
                zbeta_2[ij2k(j,i,psdp->n)]=zbeta_2[ij2k(i,j,psdp->n)];
                break;

            default:
                printf("\n Erreur de type de contrainte (update delta) \n");
                break;
            }
        }
        else
            break;

    }

    psdp->beta_1 =zbeta_1;
    psdp->beta_2 =zbeta_2;





}


/****************************************************************************************/
/****************** utilities for the branch and bound **********************************/
/****************************************************************************************/
double check_value(double a)
{
    double b;
    if (a - (int)a < EPS_INT)
    {
        b= (int )a;
    }
    else
    {
        if ((int)a - a == 0)
            b= (int )a;
        else
            b = (int )a +1;
    }

    return b;
}

int is_integer(double a)
{
    if (a - (int)a < EPS_INT)
        return 1;
    else if ((int)a+1 - a < EPS_INT)
        return 1;
    return 0;
}

double sum_r_b_r(double * b,int m)
{
    int r;
    double res=0;
    for(r=0; r<m; r++)
        res= res + b[r]*b[r];
    return res;
}

double sum_r_ari_b_r(double * a, double * b,int m, int n,int i)
{
    int r;
    double res=0;
    for(r=0; r<m; r++)
        res= res + a[ij2k(r,i,n)]*b[r];
    return res;
}

double sum_r_ari_arj(double * a, int m,int n, int i, int j)
{
    int r;
    double res=0;
    for(r=0; r<m; r++)
        res= res + a[ij2k(r,i,n)] * a[ij2k(r,j,n)];
    return res;
}


double sum_ij_qi_qj_x_ij(double * q, double * x, int n)
{
    int i,j;
    double res=0;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            res= res + q[ij2k(i,j,n)] * x[i] * x[j] ;
    return res;
}


double sum_i_ci_x_i(double * c, double * x, int n)
{
    int i;
    double res=0;
    for(i=0; i<n; i++)
        res= res + c[i] * x[i];
    return res;
}

double sum_r_alphaq_bq_r(double * b,double* alphaq,int m)
{
    int r;
    double res=0;
    for(r=0; r<m; r++)
        res= res + alphaq[r] * b[r];
    return res;
}

double sum_ij_dqi_dqj_x_ij(double * m, double * x, int n,int r)
{
    int i,j;
    double res=0;
    for(i=0; i<n; i++)
        res= res + 2*m[ijk2l(r,0,i+1,n+1,n+1)] * x[i] ;
    for(i=0; i<n; i++)
        for(j=0; j<n; j++)
            res= res + m[ijk2l(r,i+1,j+1,n+1,n+1)] * x[i] * x[j] ;
    return res;
}


/*Ne marche que pour les coef positifs*/
void refine_bounds(struct miqcp_bab bab, C_MIQCP cqp)
{
    int i,j,k;
    int i_0;

    double b,ci_0,delta;
    double grad_inf,grad_sup;



    /* attention que si contraintes quadratiques (inegalites pour le moment)*/
    for (i_0=0; i_0<cqp->n; i_0++)
    {
        for (k=0; k<cqp->pq; k++)
        {
            /*calcul du nouveau b*/
            b=cqp->eq[k];
            for (i=0; i<cqp->n; i++)
                if (i!=i_0)
                {
                    if (Positive_BB(cqp->dq[ijk2l(k,0,i+1,cqp->n+1,cqp->n+1)]))
                        b=b-2*cqp->dq[ijk2l(k,0,i+1,cqp->n+1,cqp->n+1)]*bab.l[i];
                    if  (Negative_BB(cqp->dq[ijk2l(k,0,i+1,cqp->n+1,cqp->n+1)]))
                        b=b-2*cqp->dq[ijk2l(k,0,i+1,cqp->n+1,cqp->n+1)]*bab.u[i];

                    for (j=0; j<cqp->n; j++)
                        if (j!=i_0)
                        {
                            if (Positive_BB(cqp->dq[ijk2l(k,i+1,j+1,cqp->n+1,cqp->n+1)]))
                                b=b-cqp->dq[ijk2l(k,i+1,j+1,cqp->n+1,cqp->n+1)]*bab.l[i]*bab.l[j];
                            if (Negative_BB(cqp->dq[ijk2l(k,i+1,j+1,cqp->n+1,cqp->n+1)]))
                                b=b-cqp->dq[ijk2l(k,i+1,j+1,cqp->n+1,cqp->n+1)]*bab.u[i]*bab.u[j];
                        }
                }

            ci_0=2*cqp->dq[ijk2l(k,0,i_0+1,cqp->n+1,cqp->n+1)];
            for (j=0; j<cqp->n; j++)
                if (j!= i_0)
                {
                    if (Positive_BB(cqp->dq[ijk2l(k,i_0+1,j+1,cqp->n+1,cqp->n+1)]))
                        ci_0=ci_0 + 2*cqp->dq[ijk2l(k,i_0+1,j+1,cqp->n+1,cqp->n+1)]*bab.l[j];
                    if (Negative_BB(cqp->dq[ijk2l(k,i_0+1,j+1,cqp->n+1,cqp->n+1)]))
                        ci_0=ci_0 + 2*cqp->dq[ijk2l(k,i_0+1,j+1,cqp->n+1,cqp->n+1)]*bab.u[j];
                }

            if (!Zero_BB(ci_0) && (Zero_BB(cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)])))
            {
                if  (Positive_BB(b/ci_0) && (b/ci_0 < bab.u[i_0]))
                {
                    if (i_0 <cqp->nb_int)
                        bab.u[i_0]=(int) (b/ci_0);
                    else
                        bab.u[i_0]=b/ci_0;
                }

                if (Negative_BB(b/ci_0) && (b/ci_0 > bab.l[i_0]))
                    bab.l[i_0]=0;
            }
            else if (!Zero_BB(cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]))
            {
                delta = ci_0*ci_0 + 4*b*cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)];
                if (Positive_BB(cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]) && (!Zero_BB(cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)])))
                {
                    if ((sqrt(delta) - ci_0)/(2*cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]) < bab.u[i_0])
                    {
                        if (i_0 <cqp->nb_int)
                            bab.u[i_0] = (int)( (sqrt(delta) - ci_0)/(2*cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]));
                        else
                            bab.u[i_0] = (sqrt(delta) - ci_0)/(2*cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]);
                    }
                }
                else if (Negative_BB(cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]) && (!Zero_BB(cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)])))
                {
                    if ((sqrt(delta) - ci_0)/(2*cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]) > bab.l[i_0])
                    {
                        if (i_0 <cqp->nb_int)
                            bab.l[i_0] = (int) ((sqrt(delta) - ci_0)/(2*cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]));
                        else
                            bab.l[i_0] = (sqrt(delta) - ci_0)/(2*cqp->dq[ijk2l(k,i_0+1,i_0+1,cqp->n+1,cqp->n+1)]);
                    }
                }
            }



        }
    }



    /* pb sans contraintes ou avec ineg quad pour grad_inf*/
    if ((Zero_BB(cqp->m)) && (Zero_BB(cqp->p)) &&(Zero_BB(cqp->mq)) && (Zero_BB(cqp->pq)))
    {
        for (i=0; i<cqp->n; i++)
        {
            grad_inf = cqp->c[i];
            for (j=0; j<cqp->n; j++)
                if (Positive_BB(cqp->q[ij2k(i,j,cqp->n)]) && (!Zero_BB(cqp->q[ij2k(i,j,cqp->n)])))
                    grad_inf=grad_inf + 2*cqp->q[ij2k(i,j,cqp->n)]*bab.l[j];
                else
                    grad_inf=grad_inf + 2*cqp->q[ij2k(i,j,cqp->n)]*bab.u[j];

            if(Positive_BB(grad_inf))
                bab.u[i]=bab.l[i];



            if ((Zero_BB(cqp->m)) && (Zero_BB(cqp->p)) &&(Zero_BB(cqp->mq)) && (Zero_BB(cqp->pq)))
            {
                grad_sup = cqp->c[i];
                {
                    for (j=0; j<cqp->n; j++)
                        if (Positive_BB(cqp->q[ij2k(i,j,cqp->n)]) && (!Zero_BB(cqp->q[ij2k(i,j,cqp->n)])))
                            grad_sup=grad_sup + 2*cqp->q[ij2k(i,j,cqp->n)]*bab.u[j];
                        else
                            grad_sup=grad_sup + 2*cqp->q[ij2k(i,j,cqp->n)]*bab.l[j];

                    if(Negative_BB(grad_sup))
                        bab.l[i]=bab.u[i];

                }
            }
        }
    }




}


/****************************************************************************************/
/****************** utilities for the branch and bound MIQCR ****************************/
/****************************************************************************************/


int compare_solution_miqcp(double sol_adm, double * best_sol_adm_miqcp,double * x_y, FILE * f, C_MIQCP cqp)
{
    int i,j,k;
    float bound;
    if (cqp->local_sol_adm > sol_adm)
        /* if(DEBUG==1)  */
        /*   { */
        /* 	printf("\n f(x) differente de f(x,y)"); */
        /* 	printf("\n f(x) : %lf \n",cqp->local_sol_adm); */
        /* 	printf("\n f(x,y) : %lf \n",sol_adm); */
        /*   } */

        if ( cqp->local_sol_adm -EPS_BB < *best_sol_adm_miqcp)
        {
            *best_sol_adm_miqcp = cqp->local_sol_adm;

            if (DEBUG == 1)
            {
                printf ("\nBest feasible solution: %lf \n", *best_sol_adm_miqcp);

                for(i=0; i<cqp->n; i++)
                    printf("x[%d] : %.2lf ",i,x_y[i]);
                printf("\n");
                if (Zero(cqp->nb_int))
                    printf ("Number of branch-and-bound nodes: %d \n\n", nb_int_node);
                else
                    printf ("Number of branch-and-bound nodes: %d \n\n", nb_int_node+nodecount);
            }
            else
            {
                fprintf (f,"\nBest feasible solution: %lf \n", *best_sol_adm_miqcp);

                for(i=0; i<cqp->n; i++)
                    fprintf(f, "x[%d] : %.2lf ",i,x_y[i]);
                fprintf(f,"\n");
                if (Zero(cqp->nb_int))
                    fprintf (f,"Number of branch-and-bound nodes: %d \n\n", nb_int_node);
                else
                    fprintf (f,"Number of branch-and-bound nodes: %d \n\n", nb_int_node+nodecount);

            }
            return 1;

        }
        else
            return 0;
}

int feasible_for_constraints(double * x_y, C_MIQCP cqp)
{
    int r,k;
    double fx=0;


    if (_5BRANCHES ==0)
        for(k=0; k<cqp->nb_int; k++)
            if (!is_integer(x_y[k]))
                return 0;

    for(r=0; r<cqp->mq; r++)
    {
        fx= sum_ij_dqi_dqj_x_ij(cqp->aq, x_y, cqp->n,r);
        if (fx != cqp->bq[r])
            return 0;
        fx=0;
    }

    for(r=0; r<cqp->pq; r++)
    {
        fx= sum_ij_dqi_dqj_x_ij(cqp->dq, x_y, cqp->n,r);
        if (fx > cqp->eq[r])
            return 0;
        fx=0;
    }



    return 1;
}
int update_boud_f_x_miqcp(double fx)
{
    int i,j,k;
    if (fx < upper_bound)
    {
        upper_bound = fx;
        return 1;
    }
    else
        return 0;
}



int is_feasible_miqcp( double * x_y, C_MIQCP cqp)
{
    int i,j;
    double tmp;
    for(i=0; i<cqp->n; i++)
        for(j=i; j<cqp->n; j++)
        {
            if (!Zero_BB(cqp->nb_var_y[ij2k(i,j,cqp->n)]))
                tmp = v_abs(x_y[i]*x_y[j] - x_y[cqp->var[cqp->n + ij2k(i,j,cqp->n)]]);
            if ( tmp > EPS_BRANCH)
                return 0;
        }

    return 1;
}


int  select_i_j_miqcrq(double * x_y, struct miqcp_bab bab, int *i, int *j, C_MIQCP cqp)
{
    int k,l;
    int feasible=1;
    double max=EPS_BRANCH;
    double slack_sol, slack_select, sum_select,sum_sol;

    /*  if (DEBUG == 1) */
    /* { */
    /*    printf("\n select_i_j \n "); */


    /*   printf("Number of branch-and-bound nodes: %d \n\n", nb_int_node); */
    /*   int m,p,ind=cqp->n; */
    /*   for(m=0;m<cqp->n;m++) */
    /* 	printf("x[%d] : %.2lf ",m+1,x_y[m]); */
    /*   printf("\n"); */
    /*   for(m=0;m<cqp->n;m++) */
    /* 	{ */
    /* 	  for(p=m;p<cqp->n;p++) */
    /* 	    if (!Zero_BB(cqp->nb_var_y[ij2k(m,p,cqp->n)])) */
    /* 	      { */
    /* 		printf("y[%d,%d] : %.2lf ",m+1,p+1,x_y[ind]); */
    /* 		ind++; */
    /* 	      } */
    /* 	  printf("\n"); */
    /* 	} */
    /*   printf("\n"); */

    /*   printf("\nl\n"); */


    /*   for(m=0;m<cqp->n;m++) */
    /* 	{ */
    /* 	  printf("%lf ",bab.l[m]); */
    /* 	} */
    /*   printf("\n"); */

    /*   printf("\nu\n"); */
    /*   for(m=0;m<cqp->n;m++) */
    /* 	{ */
    /* 	  printf("%lf ",bab.u[m]); */
    /* 	} */
    /*  printf("\n"); */

    /* } */


    /***********************************************************************/
    /* Methode generique ** max_ij |q_ij -s_ij | -y_ij - x_ix_j , on prend i ***/
    /***********************************************************************/
    /* for(k=0;k<bab.n;k++) */
    /*    if (!Zero_BB(x_y[k] - bab.u[k]) &&  !Zero_BB(x_y[k] - bab.l[k])) */
    /*     { */
    /* 	for(l=k;l<bab.n;l++) */
    /* 	  { */
    /* 	    if ((!Zero_BB(cqp->nb_var_y[ij2k(k,l,bab.n)]) || !Zero_BB(cqp->nb_var_y[ij2k(l,k,bab.n)])) && !Zero_BB(x_y[l] - bab.u[l]) &&  !Zero_BB(x_y[l] - bab.l[l])) */
    /* 	      { */
    /* 		slack_sol =  v_abs(x_y[k]*x_y[l] - x_y[cqp->var[(bab.n)+ ij2k(k,l,bab.n)]]); */
    /* 		slack_select=slack_sol*(cqp->new_q[ij2k(k,l,cqp->n)] - cqp->q[ij2k(k,l,cqp->n)]); */

    /* 		if ( !Zero_BB(slack_select) && ( v_abs(slack_select) > max)) */
    /* 		  { */
    /* 		    feasible =0; */
    /* 		    max=v_abs(slack_select); */

    /* 		    *i=k; */
    /* 		    *j=l; */
    /* 		  } */
    /* 		else */
    /* 		  if ((v_abs(slack_sol) > EPS_BRANCH) && ( v_abs(slack_sol) > max)) */
    /* 		  { */
    /* 		    feasible =0; */
    /* 		    max=v_abs(slack_sol); */

    /* 		    *i=k; */
    /* 		    *j=l; */
    /* 		  } */
    /* 	      } */
    /* 	  } */
    /*     } */

    /*********************************************************************************/
    /******************** max_i sum_j |q_ij -s_ij | -y_ij - x_ix_j|*******************/
    /*********************************************************************************/
    /* /\*Priorite sur les termes lineaires initiaux*\/ */
    /* for(k=0;k<bab.n;k++) */
    /*   if (!Zero_BB(x_y[k] - bab.u[k]) &&  !Zero_BB(x_y[k] - bab.l[k])&& k !=pere) */
    /*     { */
    /* 	if (!Zero_BB(cqp->nb_var_y[ij2k(k,k,bab.n)]) && !Zero_BB(cqp->c[k])) */
    /* 	  { */
    /* 	    slack_sol =  v_abs(x_y[k]*x_y[k] - x_y[cqp->var[(bab.n)+ ij2k(k,k,bab.n)]]); */
    /* 	    slack_select=slack_sol*(cqp->new_q[ij2k(k,k,cqp->n)] - cqp->q[ij2k(k,k,cqp->n)]); */
    /* 	    if ( !Zero_BB(slack_select) && (v_abs(slack_select) > max)) */
    /* 	      { */
    /* 		feasible =0; */
    /* 		max=v_abs(slack_select); */
    /* 		*i=k; */
    /* 		*j=k; */
    /* 	      } */
    /* 	    else */
    /* 	      if ((v_abs(slack_sol) > EPS_BRANCH) && ( v_abs(slack_sol) > max)) */
    /* 		{ */
    /* 		  feasible =0; */
    /* 		  max=v_abs(slack_sol); */
    /* 		  *i=k; */
    /* 		  *j=k; */
    /* 		} */
    /* 	  } */
    /*     } */

    /*Priorite sur la diagonale*/
    if (feasible==1)
    {
        for(k=0; k<bab.n; k++)
            if (!Zero_BB(x_y[k] - bab.u[k]) &&  !Zero_BB(x_y[k] - bab.l[k]) && k !=pere)
            {
                if (!Zero_BB(cqp->nb_var_y[ij2k(k,k,bab.n)]))
                {
                    slack_sol =  v_abs(x_y[k]*x_y[k] - x_y[cqp->var[(bab.n)+ ij2k(k,k,bab.n)]]);
                    slack_select=slack_sol*(cqp->new_q[ij2k(k,k,cqp->n)] - cqp->q[ij2k(k,k,cqp->n)]);
                    if ( !Zero_BB(slack_select) && (v_abs(slack_select) > max))
                    {
                        feasible =0;
                        max=v_abs(slack_select);
                        *i=k;
                        *j=k;
                    }
                    else if ((v_abs(slack_sol) > EPS_BRANCH) && ( v_abs(slack_sol) > max))
                    {
                        feasible =0;
                        max=v_abs(slack_sol);
                        *i=k;
                        *j=k;
                    }
                }
            }
    }


    if (feasible==1)
    {
        max=EPS_BRANCH;
        for(k=0; k<bab.n; k++)
            if (!Zero_BB(x_y[k] - bab.u[k]) &&  !Zero_BB(x_y[k] - bab.l[k]) && k != pere)
            {
                sum_sol=0;
                sum_select=0;
                for(l=k; l<bab.n; l++)
                {
                    if ((!Zero_BB(cqp->nb_var_y[ij2k(k,l,bab.n)]) || !Zero_BB(cqp->nb_var_y[ij2k(l,k,bab.n)])) && !Zero_BB(x_y[l] - bab.u[l]) &&  !Zero_BB(x_y[l] - bab.l[l]))
                    {
                        slack_sol =  v_abs(x_y[k]*x_y[l] - x_y[cqp->var[(bab.n)+ ij2k(k,l,bab.n)]]);
                        slack_select=slack_sol*(cqp->new_q[ij2k(k,l,cqp->n)] - cqp->q[ij2k(k,l,cqp->n)]);
                        sum_sol = sum_sol + slack_sol;
                        sum_select = sum_select + slack_select;
                    }
                    if ( !Zero_BB(sum_select) && ( v_abs(sum_select) > max))
                    {
                        feasible =0;
                        max=v_abs(sum_select);
                        *i=k;
                        *j=l;
                    }
                    else if ((v_abs(sum_sol) > EPS_BRANCH) && ( v_abs(sum_sol) > max))
                    {
                        feasible =0;
                        max=v_abs(sum_sol);
                        *i=k;
                        *j=l;
                    }
                }
            }
    }

    if (feasible ==0)
    {
        if (DEBUG ==1)
        {
            printf("\n i0 : %d (select i j )\n", *i +1);
            printf("\n j0 : %d (select i j )\n", *j + 1);
            printf("\n slack_sol : %lf\n",slack_sol);
            printf("\n max (ecart entre y_i0j0 et x_i0x_j0) : %lf (select i j )\n\n", max);
            printf("\n\nx_y \n\n");
            print_vec_d(x_y,cqp->nb_col);
            printf("\n\nx l\n\n");
            print_vec_d(bab.l,cqp->n);
            printf("\n\nx u\n\n");
            print_vec_d(bab.u,cqp->n);
        }
        pere = *i;
        return 1;
    }

    /* for(k=bab.nb_int;k<bab.n;k++) */
    /*   if (!Zero_BB(x_y[k] - bab.u[k]) &&  !Zero_BB(x_y[k] - bab.l[k])) */
    /*     for(l=k;l<bab.n;l++) */
    /* 	{ */
    /* 	  if (!Zero_BB(cqp->nb_var_y[ij2k(k,l,bab.n)]) && !Zero_BB(cqp->nb_var_y[ij2k(l,k,bab.n)]) && !Zero_BB(x_y[l] - bab.u[l]) &&  !Zero_BB(x_y[l] - bab.l[l])) */
    /* 	    { */
    /* 	      slack_sol =  v_abs(x_y[k]*x_y[l] - x_y[cqp->var[(bab.n)+ ij2k(k,l,bab.n)]]); */
    /* 	      slack_select=slack_sol*cqp->new_q[ij2k(k,l,cqp->n)]; */

    /* 	      if (( slack_sol > EPS_BRANCH) && (slack_select > max)) */
    /* 		{ */
    /* 		  feasible =0; */
    /* 		  max=slack_select; */

    /* 		  *i=k; */
    /* 		  *j=l; */
    /* 		} */
    /* 	    } */
    /* 	} */

    /* if (feasible ==0) */
    /*   return 1; */

    if (_5BRANCHES ==0)
        for(k=0; k<cqp->nb_int; k++)
            if (!is_integer(x_y[k]))
            {
                *i=k;
                *j=k;
                if (DEBUG == 1)
                {
                    printf("\n non integer : i0 : %d (select i j )\n", *i +1);

                }
                return 1;
            }



    if (feasible)
        return 0;



}



struct miqcp_bab realloc_mbab(struct miqcp_bab bab, int nb_col)
{
    struct miqcp_bab new_bab;
    new_bab.n=bab.n;
    new_bab.p=bab.p;
    new_bab.pq=bab.pq;
    new_bab.nb_int=bab.nb_int;

    new_bab.u=alloc_vector_d(nb_col);
    memcpy(new_bab.u,bab.u,nb_col*sizeof(double));

    new_bab.l=alloc_vector_d(nb_col);
    memcpy(new_bab.l,bab.l,nb_col*sizeof(double));




    return new_bab;
}




/*  update variables if i && j < nb_int   */

/*branch 1 (int * int) */
/* x_i = v_i && x_j = v_j */
struct miqcp_bab update_variables_int_branch_1(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);


    new_bab.i=*i;
    new_bab.j=*j;
    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  check_value(x_y[new_bab.i]);
    new_bab.l[new_bab.i]=  check_value(x_y[new_bab.i]);

    /* x_j = v_j*/
    new_bab.u[new_bab.j]=  check_value(x_y[new_bab.j]);
    new_bab.l[new_bab.j]=  check_value(x_y[new_bab.j]);
    if (DEBUG == 1)
        printf("\nupdate int branch 1\n");

    return new_bab;
}

/*branch 1 (int * int) i==j*/
/* x_i = v_i  */
struct miqcp_bab update_variables_int_branch_1_diag(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  check_value(x_y[new_bab.i]);
    new_bab.l[new_bab.i]=  check_value(x_y[new_bab.i]);
    if (DEBUG == 1)
        printf("\nupdate int branch 1 diag\n");

    return new_bab;
}

/*branch 2 (int * int)  */
/* x_i = v_i && x_j < = v_j -1 */
struct miqcp_bab  update_variables_int_branch_2(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  check_value(x_y[new_bab.i]);
    new_bab.l[new_bab.i]=  check_value(x_y[new_bab.i]);

    /* x_j <= v_j - 1*/
    new_bab.u[new_bab.j]=  check_value(x_y[new_bab.j]-1);

    if (DEBUG == 1)
        printf("\nupdate int branch 2\n");

    return new_bab;
}

/*branch 3 (int * int)  */
/* x_i = v_i && x_j >= v_j +1 */
struct miqcp_bab update_variables_int_branch_3(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  check_value(x_y[new_bab.i]);
    new_bab.l[new_bab.i]=  check_value(x_y[new_bab.i]);

    /* x_j >= v_j + 1*/
    new_bab.l[new_bab.j]=  check_value(x_y[new_bab.j]+1);

    if (DEBUG == 1)
        printf("\nupdate int branch 3\n");

    return new_bab;
}

/*branch 4 (int * int)  */
/* x_i <= v_i -1 */
struct miqcp_bab  update_variables_int_branch_4(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i <= v_i -1 */
    new_bab.u[new_bab.i]=  check_value(x_y[new_bab.i]-1);
    if (DEBUG == 1)
        printf("\nupdate int branch 4\n");

    return new_bab;
}

/*branch 4 (int * int) */
/* x_i >= v_i +1 */
struct miqcp_bab update_variables_int_branch_5(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i >= v_i + 1*/
    new_bab.l[new_bab.i]=  check_value(x_y[new_bab.i]+1);
    if (DEBUG == 1)
        printf("\nupdate int branch 5\n");

    return new_bab;
}

struct miqcp_bab  update_variables_int_branch_6(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i <= v_i */
    new_bab.u[new_bab.i]=  (int)(x_y[new_bab.i]);

    if (DEBUG == 1)
        printf("\nupdate cont branch 6\n");

    return new_bab;
}

/*branch 5 (cont * cont) */
/* x_i >= v_i */
struct miqcp_bab update_variables_int_branch_7(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i >= v_i */
    new_bab.l[new_bab.i]=  (int)(x_y[new_bab.i]) +1;

    if (DEBUG == 1)
        printf("\nupdate cont branch 7\n");

    return new_bab;
}


/*  update variables if i < nb_int  && j>= nb_int  */

/*branch 1 (int * cont) */
/* x_i = v_i */
struct miqcp_bab update_variables_mixed_branch_1(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;
    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  check_value(x_y[new_bab.i]);
    new_bab.l[new_bab.i]=  check_value(x_y[new_bab.i]);
    if (DEBUG == 1)
        printf("\nupdate mixed branch 1\n");

    return new_bab;
}

/*branch 2 (int * cont) */
/* x_i <= v_i -1 */
struct miqcp_bab  update_variables_mixed_branch_2(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);
    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i <= v_i - 1 */
    new_bab.u[new_bab.i]=  check_value(x_y[new_bab.i]-1);
    if (DEBUG == 1)
        printf("\nupdate mixed branch 2\n");

    return new_bab;
}

/*branch 3 (int * cont) */
/* x_i >= v_i +1 */
struct miqcp_bab update_variables_mixed_branch_3(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);
    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i >= v_i +1 */
    new_bab.l[new_bab.i]=  check_value(x_y[new_bab.i]+1);
    if (DEBUG == 1)
        printf("\nupdate mixed branch 3\n");

    return new_bab;
}


int test_bound_miqcp(struct miqcp_bab bab,int nb_col )
{
    int i;
    for(i=0; i<bab.n; i++)
        if (bab.u[i] - bab.l[i] <  -EPS_BB)
        {
            printf("\n non feasible test bound\n");
            return 0;
        }
    return 1;

}


/*  update variables if i && j > nb_int   */

/*branch 1 (cont * cont) */
/* x_i = v_i && x_j = v_j */
struct miqcp_bab update_variables_cont_branch_1(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);


    new_bab.i=*i;
    new_bab.j=*j;
    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  x_y[new_bab.i];
    new_bab.l[new_bab.i]=  x_y[new_bab.i];

    /* x_j = v_j*/
    new_bab.u[new_bab.j]=  x_y[new_bab.j];
    new_bab.l[new_bab.j]=  x_y[new_bab.j];

    if (DEBUG == 1)
        printf("\nupdate cont branch 1\n");

    return new_bab;
}

/*branch 1 (cont * cont) i==j*/
/* x_i = v_i  */
struct miqcp_bab update_variables_cont_branch_1_diag(struct miqcp_bab bab, double * x_y, int* i,int *j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  x_y[new_bab.i];
    new_bab.l[new_bab.i]=  x_y[new_bab.i];

    if (DEBUG == 1)
        printf("\nupdate cont branch 1 diag\n");

    return new_bab;
}

/*branch 2 (cont * cont)  */
/* x_i = v_i && x_j < = v_j */
struct miqcp_bab  update_variables_cont_branch_2(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  x_y[new_bab.i];
    new_bab.l[new_bab.i]= x_y[new_bab.i];

    /* x_j <= v_j */
    new_bab.u[new_bab.j]=  x_y[new_bab.j];

    if (DEBUG == 1)
        printf("\nupdate cont branch 2\n");

    return new_bab;
}

/*branch 3 (cont * cont)  */
/* x_i = v_i && x_j >= v_j  */
struct miqcp_bab update_variables_cont_branch_3(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i = v_i*/
    new_bab.u[new_bab.i]=  x_y[new_bab.i];
    new_bab.l[new_bab.i]=  x_y[new_bab.i];

    /* x_j >= v_j*/
    new_bab.l[new_bab.j]=  x_y[new_bab.j];

    if (DEBUG == 1)
        printf("\nupdate cont branch 3\n");

    return new_bab;
}

/*branch 4 (cont * cont)  */
/* x_i <= v_i */
struct miqcp_bab  update_variables_cont_branch_4(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i <= v_i */
    /*if (best_sol_adm_miqcp_s_bb <MAX_SOL_BB)
      new_bab.u[new_bab.i]=(1- GAMMA) * v_abs((bab.u[new_bab.i] + bab.l[new_bab.i])/2) + GAMMA*(x_y[new_bab.i] + best_x[new_bab.i])/2;
      else*/

    new_bab.u[new_bab.i]=(1- GAMMA) * v_abs((bab.u[new_bab.i] + bab.l[new_bab.i])/2) + GAMMA*x_y[new_bab.i];

    /*new_bab.u[new_bab.i]=(1- GAMMA) * v_abs(( best_x[new_bab.i] + x_y[new_bab.i])/2) + GAMMA*(x_y[new_bab.i]);*/

    if (DEBUG == 1)
    {
        printf("\n update cont branch 4 \n u \n");
        print_vec_d(new_bab.u,cqp->nb_col);
        printf("\n l \n");
        print_vec_d(new_bab.l,cqp->nb_col);
        printf("\n");
    }
    return new_bab;
}

/*branch 5 (cont * cont) */
/* x_i >= v_i */
struct miqcp_bab update_variables_cont_branch_5(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i >= v_i */
    /* if (best_sol_adm_miqcp_s_bb <MAX_SOL_BB)
       new_bab.l[new_bab.i]=(1- GAMMA) * v_abs((bab.u[new_bab.i] + bab.l[new_bab.i])/2) + GAMMA*(x_y[new_bab.i] + best_x[new_bab.i])/2;
    else*/
    new_bab.l[new_bab.i] =(1- GAMMA) * v_abs((bab.u[new_bab.i] + bab.l[new_bab.i])/2)+ GAMMA* x_y[new_bab.i];

    /*new_bab.l[new_bab.i]=(1- GAMMA) * v_abs(( best_x[new_bab.i] + x_y[new_bab.i])/2) + GAMMA*(x_y[new_bab.i]);*/
    if (DEBUG == 1)
    {
        printf("\n update cont branch 5\n u \n");
        print_vec_d(new_bab.u,cqp->nb_col);
        printf("\n l \n");
        print_vec_d(new_bab.l,cqp->nb_col);
        printf("\n");
    }
    return new_bab;
}

/*branch 2 (cont * cont)  */
/* x_i <= v_i && x_j < = v_j */
struct miqcp_bab  update_variables_cont_branch_1bis(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i <= v_i*/
    new_bab.u[new_bab.i]=  x_y[new_bab.i];


    /* x_j <= v_j */
    new_bab.u[new_bab.j]=  x_y[new_bab.j];

    if (DEBUG == 1)
        printf("\nupdate cont branch 2\n");

    return new_bab;
}

/*branch 3 (cont * cont)  */
/* x_i <= v_i && x_j >= v_j  */
struct miqcp_bab update_variables_cont_branch_2bis(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i <= v_i*/
    new_bab.u[new_bab.i]=  x_y[new_bab.i];


    /* x_j >= v_j*/
    new_bab.l[new_bab.j]=  x_y[new_bab.j];

    if (DEBUG == 1)
        printf("\nupdate cont branch 3\n");

    return new_bab;
}
/*branch 2 (cont * cont)  */
/* x_i >= v_i && x_j < = v_j */
struct miqcp_bab  update_variables_cont_branch_3bis(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i >= v_i*/
    new_bab.l[new_bab.i]=  x_y[new_bab.i];


    /* x_j <= v_j */
    new_bab.u[new_bab.j]=  x_y[new_bab.j];

    if (DEBUG == 1)
        printf("\nupdate cont branch 2\n");

    return new_bab;
}

/*branch 3 (cont * cont)  */
/* x_i >= v_i && x_j >= v_j  */
struct miqcp_bab update_variables_cont_branch_4bis(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i >= v_i*/
    new_bab.l[new_bab.i]=  x_y[new_bab.i];


    /* x_j >= v_j*/
    new_bab.l[new_bab.j]=  x_y[new_bab.j];

    if (DEBUG == 1)
        printf("\nupdate cont branch 3\n");

    return new_bab;
}



/*branch 1 spatial (cont * cont)  */
/* x_i <= (u_i + l_i)/2 */
struct miqcp_bab  update_variables_spatial_branch_1(struct miqcp_bab bab, double * x_y, int* i,int * j,  C_MIQCP cqp)
{
    int k;
    struct miqcp_bab new_bab = realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i <= (u_i + l_i)/2 */
    new_bab.u[new_bab.i]= v_abs((bab.u[new_bab.i] +bab.l[new_bab.i])/2);

    if (DEBUG == 1)
        printf("\nupdate spatial branch 1\n");

    return new_bab;
}

/*branch 2 spatial (cont * cont) */
/* x_i >=  (u_i + l_i)/2 */
struct miqcp_bab update_variables_spatial_branch_2(struct miqcp_bab bab, double * x_y, int *i,int *j,  C_MIQCP cqp)
{

    int k;
    struct miqcp_bab new_bab= realloc_mbab(bab,cqp->nb_col);

    new_bab.i=*i;
    new_bab.j=*j;

    /* x_i >= (u_i + l_i)/2 */
    new_bab.l[new_bab.i]=  v_abs((bab.u[new_bab.i] + bab.l[new_bab.i])/2);

    if (DEBUG == 1)
        printf("\nupdate spatial branch 2\n");

    return new_bab;
}
/*****************************************************************************/
/*************** Utilities for unconstrained programs ************************/
/*****************************************************************************/
double * add_bound_matrix(double * M, int m, int n, double * u)
{
    int i,j;
    for(i=0; i<m; i++)
        for(j=0; j<n; j++)
        {
            if (M[ij2k(i,j,n)] != 0)
                M[ij2k(i,j,n)]=M[ij2k(i,j,n)]*(int)u[i]*(int)u[j];
        }
    return M;
}

double * add_bound_vector(double * v,int n, double * u)
{
    int i;
    for(i=0; i<n; i++)
        if (v[i] != 0)
            v[i]=v[i]*(int)u[i];
    return v;
}

MIQCP reformulate_iqp(MIQCP qp)
{
    MIQCP  uqp =  new_miqcp();
    uqp->n = qp->n;
    uqp->u = copy_vector_d(qp->u,qp->n);
    uqp->solver= copy_string(qp->solver);
    uqp->solver_sdp= copy_string(qp->solver_sdp);
    uqp->q = add_bound_matrix(qp->q,qp->n,qp->n, qp->u);
    uqp->c = add_bound_vector(qp->c,qp->n,qp->u);
    return uqp;
}


void  binary_to_integer(MIQCP qp, FILE * file_save)
{
    int i;
    fprintf(file_save,"Solution after changing the variables\n");
    for(i=0; i<qp->n; i++)
        fprintf(file_save, "x[%d] : %.2lf ",i,qp->sol[i]*qp->u[i]);
    fprintf(file_save,"\n\n");
}

/***********************************************************************************/
/*********************** number of negative eigenvalues ****************************/
/***********************************************************************************/

void compute_number_neg_eigenvalues(MIQCP qp,double ** vspectre, int * nb_neg)
{
    int file_sol,i;
    pid_t pid_fils;

    write_file_scilab(qp->q, qp->n);
    pid_fils=fork();
    if (pid_fils==0)
        execlp("scilab","scilab","-nwni","-f",spectre_sce,NULL);
    else

        wait(NULL);
    /* We get back values of parameter lambda_min*/

    FILE * file_scilab;
    double * zspectre=alloc_vector_d(qp->n);

    file_scilab = fopen(spectre,"r");
    for(i=0; i<qp->n; i++)
        fscanf(file_scilab,"%lf",&zspectre[i]);
    fclose(file_scilab);

    *vspectre=zspectre;

    int nb_neg_eigenvalues=0;
    for(i=0; i<qp->n; i++)
        if (Negative_BB(zspectre[i]))
            nb_neg_eigenvalues++;

    *nb_neg = nb_neg_eigenvalues;


}


