/* -*-c-*-
 *
 *    Source     $RCSfile: in_out_csdp.h,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#ifndef IN_OUT_CSDP_H_INCLUDED
#define IN_OUT_CSDP_H_INCLUDED

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"quad_prog.h"
#include"in_out.h"

/*********************************************************************/
/****************************** Variable *****************************/
/*********************************************************************/


/*********************************************************************/
/****************************** Functions ****************************/
/*********************************************************************/
void write_file_csdp_qcr(MIQCP qp);
void write_file_csdp_qcr_minkcut(MIQCP qp);
void write_file_csdp_cqcr(MIQCP qp);
void write_file_csdp_miqcr(MIQCP qp);
void write_file_csdp_max_cut(MIQCP qp);
void write_file_csdp_miqcrq(MIQCP qp);
void write_file_csdp_miqcrq_mixed(MIQCP qp);
void write_file_csdp_miqcr_solver(SDP psdp);


#endif //IN_OUT_CSDP_H_INCLUDED
