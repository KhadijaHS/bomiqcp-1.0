/* -*-c-*-
 *
 *    Source     $RCSfile: quad_prog.c,v $
 *    Version    $Revision: 1.1.1.2 $
 *    Date       $Date: 2019/10/04 14:27:30 $
 *    Authors    Amelie LAMBERT, Khadija HADJ SALEM, Sourour ELLOUMI
 *
 --------------------------------------------------------------------------- */

/****************************************************************************
//
//  This file is part of the src of "SMIQCP",
//
//  Copyright (C) 2019  Amelie Lambert
//
//     CEDRIC - CNAM
//     292 rue saint martin
//     F-75141 Paris Cedex 03
//     France
//
//     amelie.lambert@cnam.fr    http://cedric.cnam.fr/~lamberta
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>

#include"quad_prog.h"
#include"utilities.h"
#include"in_out.h"


/******************************************************************************/
/******************* Structure for classical MIQCP ***************************/
/******************************************************************************/

MIQCP new_miqcp()
{
    return (MIQCP) malloc(sizeof(struct miqcp));
}

Q_MIQCP new_q_miqcp()
{
    return (Q_MIQCP) malloc(sizeof(struct q_miqcp));
}
MIQCP copy_miqcp(MIQCP qp)
{
    MIQCP cp=new_miqcp();
    cp->N=qp->N;
    cp->n=qp->n;
    cp->nb_int=qp->nb_int;
    cp->m=qp->m;
    cp->p=qp->p;
    cp->mq=qp->mq;
    cp->pq=qp->pq;
    cp->u = copy_vector_d(qp->u,qp->n);
    cp->l = copy_vector_d(qp->l,qp->n);
    cp->lg = copy_vector(qp->lg,qp->n);
    cp->lgc = copy_vector(qp->lgc,qp->n);



    cp->cons = qp->cons;
    cp->miqp=qp->miqp;
    cp->q = copy_matrix_d(qp->q, qp->n,qp->n);
    cp->c = copy_vector_d(qp->c,qp->n);

    if(!Zero(qp->m))
    {
        cp->a = copy_matrix_d(qp->a, qp->m,qp->n);
        cp->b = copy_vector_d(qp->b,qp->m);
    }
    else
    {
        cp->a=alloc_matrix_d(1,1);
        cp->b = copy_vector_d(qp->b,qp->m);
    }

    if(!Zero(qp->p))
    {
        cp->d= copy_matrix_d(qp->d, qp->p,qp->n);
        cp->e = copy_vector_d(qp->e,qp->p);
    }
    else
    {
        cp->d=alloc_matrix_d(1,1);
        cp->e = copy_vector_d(qp->e,qp->p);
    }

    if(!Zero(qp->mq))
    {
        cp->aq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }
    else
    {
        cp->aq=alloc_matrix_d(1,1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }

    if(!Zero(qp->pq))
    {
        cp->dq= copy_matrix_3d(qp->dq, qp->pq,qp->n+1,qp->n+1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }
    else
    {
        cp->dq=alloc_matrix_d(1,1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }
    
    return cp;
}

void update_qp(MIQCP qp, C_MIQCP cqp)
{
    if (cqp->p <qp->p)
    {
        qp->p = cqp->p;
        free(qp->d);
        free(qp->e);
        qp->d=copy_matrix_d(cqp->d,cqp->p,cqp->n);
        qp->e=copy_vector_d(cqp->e,cqp->p);
    }

    if (cqp->pq <qp->pq)
    {
        qp->pq = cqp->pq;
        free(qp->dq);
        free(qp->eq);
        qp->dq=copy_matrix_3d(cqp->dq,cqp->pq,cqp->n+1,cqp->n+1);
        qp->eq=copy_vector_d(cqp->eq,cqp->pq);

    }

    free(qp->u);
    free(qp->l);
    qp->u=copy_vector_d(cqp->u,cqp->n + cqp->n *(cqp->n+1)/2);
    qp->l=copy_vector_d(cqp->l,cqp->n + cqp->n *(cqp->n+1)/2);
}

Q_MIQCP create_q_miqcp(MIQCP qp, double * beta, double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag )
{
    int i,j,k,l,lim;
    int test =0;

    Q_MIQCP cp = new_q_miqcp();
    cp->N=qp->N;
    cp->n=qp->n;
    cp->nb_int=qp->nb_int;
    cp->m=qp->m;
    cp->p=qp->p;
    cp->mq=qp->mq;
    cp->pq=qp->pq;
    cp->u = copy_vector_d(qp->u,qp->n);
    cp->l = copy_vector_d(qp->l,qp->n);
    cp->lg = copy_vector(qp->lg,qp->n);
    cp->lgc = copy_vector(qp->lgc,qp->n);

    cp->flag_nb_var = flag;

    cp->cons = qp->cons;
    cp->miqp=qp->miqp;
    cp->q = copy_matrix_d(qp->q, qp->n,qp->n);
    cp->c = copy_vector_d(qp->c,qp->n);

    if(!Zero(qp->m))
    {
        cp->a = copy_matrix_d(qp->a, qp->m,qp->n);
        cp->b = copy_vector_d(qp->b,qp->m);
    }
    else
    {
        cp->a=alloc_matrix_d(1,1);
        cp->b = copy_vector_d(qp->b,qp->m);
    }

    if(!Zero(qp->p))
    {
        cp->d= copy_matrix_d(qp->d, qp->p,qp->n);
        cp->e = copy_vector_d(qp->e,qp->p);
    }
    else
    {
        cp->d=alloc_matrix_d(1,1);
        cp->e = copy_vector_d(qp->e,qp->p);
    }

    if(!Zero(qp->mq))
    {
        cp->aq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }
    else
    {
        cp->aq=alloc_matrix_d(1,1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }

    if(!Zero(qp->pq))
    {
        cp->dq= copy_matrix_3d(qp->dq, qp->pq,qp->n+1,qp->n+1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }
    else
    {
        cp->dq=alloc_matrix_d(1,1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }

    if(!Zero(qp->m))
        cp->alpha =alpha;
    else
        cp->alpha=0;

    if(!Zero(qp->p))
        cp->alphabis =alphabis;
    else
        cp->alphabis=0;

    if(!Zero(qp->mq))
    {
        cp->alphaq = copy_vector_d(alphaq,qp->mq);
        if (flag == 2)
        {
            cp->lambda_min_eg = copy_vector_d(lambda_min_eg,qp->mq);
            cp->lambda_max_eg = copy_vector_d(lambda_max_eg,qp->mq);
        }
        else
        {
            cp->lambda_min_eg= alloc_vector_d(cp->mq);
            cp->lambda_max_eg= alloc_vector_d(cp->mq);
            for(i=0; i<cp->mq; i++)
            {
                cp->lambda_min_eg[i] = 0;
                cp->lambda_max_eg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphaq = alloc_vector_d(cp->mq);
        cp->lambda_min_eg= alloc_vector_d(cp->mq);
        cp->lambda_max_eg= alloc_vector_d(cp->mq);
        for(i=0; i<cp->mq; i++)
        {
            cp->alphaq[i]=0;
            cp->lambda_min_eg[i] = 0;
            cp->lambda_max_eg[i] = 0;
        }
    }

    if(!Zero(qp->pq))
    {
        cp->alphabisq =copy_vector_d(alphabisq,qp->pq);
        if (flag == 2)
        {
            cp->lambda_min_ineg = copy_vector_d(lambda_min_ineg,qp->pq);
            cp->lambda_max_ineg = copy_vector_d(lambda_max_ineg,qp->pq);
        }
        else
        {
            cp->lambda_min_ineg= alloc_vector_d(cp->pq);
            cp->lambda_max_ineg= alloc_vector_d(cp->pq);
            for(i=0; i<cp->pq; i++)
            {
                cp->lambda_min_ineg[i] = 0;
                cp->lambda_max_ineg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphabisq = alloc_vector_d(cp->pq);
        cp->lambda_min_ineg= alloc_vector_d(cp->pq);
        cp->lambda_max_ineg= alloc_vector_d(cp->pq);
        for(i=0; i<cp->pq; i++)
        {
            cp->alphabisq[i]=0;
            cp->lambda_min_ineg[i] = 0;
            cp->lambda_max_ineg[i] = 0;
        }
    }

    cp->beta = copy_matrix_d(beta,qp->n,qp->n);

    int cont = 0;
    for(i=qp->n; i>qp->n - qp->nb_int; i--)
        cont=cont+i;

    int cont_bis = 0;
    for(i=qp->n-1; i>qp->n- 1 - qp->nb_int; i--)
        cont_bis=cont_bis+i;

    /*Computation of the initial number of constraints */
    /* Ax=b */
    cp->ind_eg = qp->m;
    /* Dx = e */
    cp->ind_ineg = cp->ind_eg + qp->p;
    /*x^TAqx = bq*/
    cp->ind_eg_q = cp->ind_ineg + qp->mq;
    /*x^TDqx = eq*/
    cp->ind_ineg_q = cp->ind_eg_q + qp->pq;
    /* x = sum_k 2^k t_ik*/
    if (flag == 1)
        cp->ind_dec_x=cp->ind_ineg_q +qp->nb_int;
    else
        cp->ind_dec_x=cp->ind_ineg+qp->nb_int;
    /* y_ij = sum_k 2^k z_ijk*/
    cp->ind_dec_y=cp->ind_dec_x + qp->nb_int*(qp->n);
    /*z_ijk <= u_jt_ik*/
    cp->ind_z_1=cp->ind_dec_y +(qp->n)*qp->N;
    /*z_ijk <= x_j*/
    cp->ind_z_2=cp->ind_z_1+(qp->n)*qp->N;
    /*z_ijk >= x_j - u_j(1 - t_ik)*/
    cp->ind_z_3=cp->ind_z_2+(qp->n)*qp->N;
    /*z_ijk >= 0*/
    cp->ind_z_4=cp->ind_z_3+(qp->n)*qp->N;
    /* y_ij >= u_ix_j + u_jx_i - u_iu_j */
    cp->ind_y_1=cp->ind_z_4 +cont;
    /* y_ii >= x_i */
    cp->ind_y_2= cp->ind_y_1 + qp->nb_int;
    /* y_ij = y_ji */
    cp->nrow =cp->ind_y_2 + cont_bis;

    /* initialisation of the new number of constraints*/
    /* Ax=b */
    cp->nb_eg =qp->m;
    /* Dx = e */
    cp->nb_ineg = qp->p;
    /*x^TAqx = bq*/
    cp->nb_eg_q = qp->mq;
    /*x^TDqx = eq*/
    cp->nb_ineg_q = qp->pq;
    /* x = sum_k 2^k t_ik*/
    cp->nb_dec_x=qp->nb_int;
    /* y_ij = sum_k 2^k z_ijk*/
    cp->nb_dec_y=0;
    /*z_ijk <= u_jt_ik*/
    cp->nb_z_1=0;
    /*z_ijk <= x_j*/
    cp->nb_z_2=0;
    /*z_ijk >= x_j - u_j(1 - t_ik)*/
    cp->nb_z_3=0;
    /*z_ijk >= 0*/
    cp->nb_z_4=0;
    /* y_ij >= u_ix_j + u_jx_i - u_iu_j */
    cp->nb_y_1=0;
    /* y_ii >= x_i */
    cp->nb_y_2=0;
    /* y_ij = y_ji */
    cp->nb_y_3 =0;

    /* real number of variables y and z*/
    cp->nb_var_y = alloc_matrix(qp->n,qp->n);
    if (flag ==1)
    {
        for(i=0; i<qp->n; i++)
            for(j=0; j<qp->n; j++)
            {
                /*initialisation*/
                cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                if (!Zero_BB(beta[ij2k(i,j,qp->n)]))
                    cp->nb_var_y[ij2k(i,j,qp->n)] =1;
            }

        for(l=0; l<qp->mq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->aq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)]) && ((i < cp->nb_int) || (j < cp->nb_int)))
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;

        for(l=0; l<qp->pq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->dq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)]) && ((i < cp->nb_int) || (j < cp->nb_int)))
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;
        cp->nb_y = nb_non_zero_matrix_i(cp->nb_var_y, qp->n, qp->n);

        cp->nb_z=0;
        for(i=0; i<qp->nb_int; i++)
        {
            lim=log_base_2(qp->u[i])+1;
            for(j=0; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    cp->nb_z=cp->nb_z+lim;
        }
    }
    else
    {
        for(i=0; i<qp->n; i++)
            for(j=0; j<qp->n; j++)
            {
                /*initialisation*/
                cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                if (!Zero_BB(beta[ij2k(i,j,qp->n)]) || ((i==j) && (i<qp->nb_int) && (j<qp->nb_int)))
                    cp->nb_var_y[ij2k(i,j,qp->n)] =1;
            }

        cp->nb_y = nb_non_zero_matrix_i(cp->nb_var_y, qp->n, qp->n);

        cp->nb_z=0;
        for(i=0; i<qp->nb_int; i++)
        {
            lim=log_base_2(qp->u[i])+1;
            for(j=0; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    cp->nb_z=cp->nb_z+lim;
        }
    }
    /*computation of the new number of constraints */
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for (j=i; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->nb_dec_y++;
                cp->nb_y_1++;
                cp->nb_z_1+=lim;
                cp->nb_z_2+=lim;
                cp->nb_z_3+=lim;
                cp->nb_z_4+=lim;
                if (i==j)
                    cp->nb_y_2++;
                else
                    cp->nb_y_3++;
            }
    }

    /* z variables from the triangle inf of the matrix*/
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for (j=0; j<i; j++)
            if ((!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)])) && (i!=j))
            {
                cp->nb_dec_y++;
                cp->nb_z_1+=lim;
                cp->nb_z_2+=lim;
                cp->nb_z_3+=lim;
                cp->nb_z_4+=lim;
            }
    }


    cp->nb_row =cp->ind_dec_x + cp->nb_dec_y + cp->nb_z_1+ cp->nb_z_2+ cp->nb_z_3+ cp->nb_z_4+  cp->nb_y_1+  cp->nb_y_2 + cp->nb_y_3;

    cp->cont = alloc_vector(cp->nrow);
    for(i=0; i<cp->nrow; i++)
        cp->cont[i]=-1;


    /* number of initial variables */

    cp->ind_max_int =qp->nb_int;
    cp->ind_max_x =qp->n;
    cp->ind_max_y = cp->ind_max_x + qp->nb_int*(qp->n ) + qp->nb_int*(qp->n -qp->nb_int);
    cp->ind_max_z =  cp->ind_max_y + (qp->n)*qp->N;
    cp->ncol = cp->ind_max_z + qp->N +1 ;

    int r;


    r=cp->ind_dec_x;

    for(i=0; i<r; i++)
        cp->cont[i]=i;

    /* y_ij = sum_k 2^k z_ijk*/
    for(i=0; i<qp->nb_int; i++)
    {
        for (j=0; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->cont[cp->ind_dec_x + i*(qp->n) +j] = r;
                r++;
            }
    }

    int cpt =0;
    int cpt_bis =0;

    /* z_ijk <= u_j_t_ik */
    r= cp->ind_dec_x + cp->nb_dec_y;

    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            for(k=0; k<lim; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_dec_y+ cpt_bis + j*lim + k+cpt] = r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }

    cpt =0;
    cpt_bis =0;
    /* z_ijk <= x_j */
    r= cp->ind_dec_x + cp->nb_dec_y+cp->nb_z_1 ;

    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            for(k=0; k<lim; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_z_1+ cpt_bis + j*lim + k+cpt] = r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }


    /* z_ijk >= x_j + u_j_t_ik - u_j */
    r= cp->ind_dec_x + cp->nb_dec_y +cp->nb_z_1 + cp->nb_z_2;
    cpt =0;
    cpt_bis =0;
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            for(k=0; k<lim; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_z_2+ cpt_bis + j*lim + k+cpt] = r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }


    /* z_ijk >= 0 */
    r= cp->ind_dec_x + cp->nb_dec_y + cp->nb_z_1 + cp->nb_z_2+ cp->nb_z_3;
    cpt =0;
    cpt_bis =0;
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            for(k=0; k<lim; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_z_3+ cpt_bis + j*lim + k+cpt] = r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }

    /* y_ij >= u_ix_j + u_jx_i - u_iu_j*/
    r=  cp->ind_dec_x+ cp->nb_dec_y+cp->nb_z_1+cp->nb_z_2+cp->nb_z_3+cp->nb_z_4;
    for(i=0; i<qp->nb_int; i++)
    {
        for (j=i; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->cont[cp->ind_z_4 + i* (2*(qp->n ) -i +1)/2 + j-i] = r;
                r++;
            }
    }

    /*y_ii >= x_i*/
    r= cp->ind_dec_x+ cp->nb_dec_y+cp->nb_z_1+cp->nb_z_2+cp->nb_z_3+cp->nb_z_4 + cp->nb_y_1;
    for(i=0; i<qp->nb_int; i++)
    {
        if(!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
        {
            cp->cont[cp->ind_y_1 + i] = r;
            r++;
        }
    }

    /*y_ij = y_ji*/
    r= cp->ind_dec_x+ cp->nb_dec_y+cp->nb_z_1+cp->nb_z_2+cp->nb_z_3+cp->nb_z_4+ cp->nb_y_1+ cp->nb_y_2;
    for(i=0; i<qp->nb_int; i++)
    {
        for (j=i+1; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->cont[cp->ind_y_2 +i* (2*(qp->n  ) -i-1)/2 + j-i-1] = r;
                r++;
            }
    }

    /* real number of variables +1 for constant term*/
    cp->nb_col =cp->ind_max_x  + cp->nb_y + cp->nb_z + qp->N+1;


    /*nb occurence of each x in constraints involving y or z variables*/
    cp->x_cont=alloc_vector(cp->ind_max_x);
    for(i=0; i<cp->ind_max_x; i++)
        /* initialization to 1 because of the binary expansion constraint for integer x_i*/
        if (i<cp->nb_int)
            cp->x_cont[i]=1;
        else
            cp->x_cont[i]=0;


    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        if (!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
            cp->x_cont[i] = cp->x_cont[i]+1;

        for(j=0; j<qp->n; j++)
        {
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->x_cont[i] = cp->x_cont[i]+1;

                for(k=0; k<lim; k++)
                    cp->x_cont[j] = cp->x_cont[j]+2;
            }
        }
    }

    for(i=qp->nb_int; i<cp->ind_max_x; i++)
    {
        for(j=0; j<qp->nb_int; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->x_cont[i] = cp->x_cont[i]+1;
            }
    }

    /*table with corresponding variables*/
    cp->var = alloc_vector(cp->ncol);

    /*variables x all created*/
    for(i=0; i<cp->ind_max_x; i++)
    {
        cp->var[i]=i;
    }

    /* variables y and z*/
    for(i; i<cp->ncol-qp->N; i++)
    {
        cp->var[i]=-1;
    }

    /*variables y*/
    r=cp->ind_max_x;

    for(i=0; i<qp->nb_int; i++)
    {
        for (j=0; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->var[(qp->n)*i + j + cp->ind_max_x] =r;
                r++;
            }
    }

    for(i=qp->nb_int; i<cp->ind_max_x; i++)
    {
        for (j=0; j<qp->nb_int; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->var[(qp->n)*qp->nb_int+ (i-qp->nb_int)*qp->nb_int + j +cp->ind_max_x] =r;
                r++;
            }
    }

    /*variables z*/
    cpt=0;
    cpt_bis=0;
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                for(k=0; k<lim; k++)
                {
                    cp->var[cp->ind_max_y +  cpt_bis+j*lim + k+cpt] =r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }

    /*variables t all created*/
    for(i=cp->ind_max_z; i<cp->ncol; i++)
    {
        cp->var[i]=r;
        r++;
    }

    return cp;
}

Q_MIQCP create_0_1_miqcp(MIQCP qp, double * beta, double * beta_1,double * beta_2,double * beta_3,double * beta_4,double * beta_c,double * delta, double * delta_c, double delta_l, double * delta_1, double * delta_2, double * delta_3, double * delta_4,double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag )
{
    int i,j,k,l,lim;
    int test =0;

    Q_MIQCP cp = new_q_miqcp();
    cp->N=qp->N;
    cp->n=qp->n;

    cp->nb_int=qp->nb_int;
    cp->m=qp->m;
    cp->p=qp->p;
    cp->mq=qp->mq;
    cp->pq=qp->pq;
    cp->u = copy_vector_d(qp->u,qp->n);
    cp->l = copy_vector_d(qp->l,qp->n);
    cp->lg = copy_vector(qp->lg,qp->n);
    cp->lgc = copy_vector(qp->lgc,qp->n);
    cp->cons = qp->cons;
    cp->flag_nb_var = flag;

   
    cp->miqp=qp->miqp;
    cp->q = copy_matrix_d(qp->q, qp->n,qp->n);
    cp->c = copy_vector_d(qp->c,qp->n);

    if(!Zero(qp->m))
    {
        cp->a = copy_matrix_d(qp->a, qp->m,qp->n);
        cp->b = copy_vector_d(qp->b,qp->m);
    }
    else
    {
        cp->a=alloc_matrix_d(1,1);
        cp->b = copy_vector_d(qp->b,qp->m);
    }

    if(!Zero(qp->p))
    {
        cp->d= copy_matrix_d(qp->d, qp->p,qp->n);
        cp->e = copy_vector_d(qp->e,qp->p);
    }
    else
    {
        cp->d=alloc_matrix_d(1,1);
        cp->e = copy_vector_d(qp->e,qp->p);
    }

    if(!Zero(qp->mq))
    {
        cp->aq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }
    else
    {
        cp->aq=alloc_matrix_d(1,1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }

    if(!Zero(qp->pq))
    {
        cp->dq= copy_matrix_3d(qp->dq, qp->pq,qp->n+1,qp->n+1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }
    else
    {
        cp->dq=alloc_matrix_d(1,1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }

    if(!Zero(qp->m))
        cp->alpha =alpha;
    else
        cp->alpha=0;

    if(!Zero(qp->p))
        cp->alphabis =alphabis;
    else
        cp->alphabis=0;

    if(!Zero(qp->mq))
    {
        cp->alphaq = copy_vector_d(alphaq,qp->mq);
        if (flag == 3)
        {
            cp->lambda_min_eg = copy_vector_d(lambda_min_eg,qp->mq);
            cp->lambda_max_eg = copy_vector_d(lambda_max_eg,qp->mq);
        }
        else
        {
            cp->lambda_min_eg= alloc_vector_d(cp->mq);
            cp->lambda_max_eg= alloc_vector_d(cp->mq);
            for(i=0; i<cp->mq; i++)
            {
                cp->lambda_min_eg[i] = 0;
                cp->lambda_max_eg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphaq = alloc_vector_d(cp->mq);
        cp->lambda_min_eg= alloc_vector_d(cp->mq);
        cp->lambda_max_eg= alloc_vector_d(cp->mq);
        for(i=0; i<cp->mq; i++)
        {
            cp->alphaq[i]=0;
            cp->lambda_min_eg[i] = 0;
            cp->lambda_max_eg[i] = 0;
        }
    }

    if(!Zero(qp->pq))
    {
        cp->alphabisq =copy_vector_d(alphabisq,qp->pq);
        if (flag ==3)
        {
            cp->lambda_min_ineg = copy_vector_d(lambda_min_ineg,qp->pq);
            cp->lambda_max_ineg = copy_vector_d(lambda_max_ineg,qp->pq);
        }
        else
        {
            cp->lambda_min_ineg= alloc_vector_d(cp->pq);
            cp->lambda_max_ineg= alloc_vector_d(cp->pq);
            for(i=0; i<cp->pq; i++)
            {
                cp->lambda_min_ineg[i] = 0;
                cp->lambda_max_ineg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphabisq = alloc_vector_d(cp->pq);
        cp->lambda_min_ineg= alloc_vector_d(cp->pq);
        cp->lambda_max_ineg= alloc_vector_d(cp->pq);
        for(i=0; i<cp->pq; i++)
        {
            cp->alphabisq[i]=0;
            cp->lambda_min_ineg[i] = 0;
            cp->lambda_max_ineg[i] = 0;
        }
    }

    cp->beta = copy_matrix_d(beta,qp->n,qp->n);

    cp->delta = copy_matrix_d(delta,qp->n,qp->n);
    if (delta_c!=NULL)
        cp->delta_c = copy_vector_d(delta_c,qp->n);

    cp->delta_l=delta_l;
    cp->delta_1 = copy_matrix_3d(delta_1,cp->n,cp->n,cp->n);
    cp->delta_2 = copy_matrix_3d(delta_2,cp->n,cp->n,cp->n);
    cp->delta_3 = copy_matrix_3d(delta_3,cp->n,cp->n,cp->n);
    cp->delta_4 = copy_matrix_3d(delta_4,cp->n,cp->n,cp->n);

    cp->beta_c = copy_vector_d(beta_c,cp->n);
    cp->beta_1 = copy_matrix_d(beta_1,cp->n,cp->n);
    cp->beta_2 = copy_matrix_d(beta_2,cp->n,cp->n);
    cp->beta_3 = copy_matrix_d(beta_3,cp->n,cp->n);
    cp->beta_4 = copy_matrix_d(beta_4,cp->n,cp->n);

    int nb_cont_cor = (qp->n-1)*qp->n/2;
    int nb_cont_triangle = 0;

    for (i=0; i<qp->n; i++)
        for (j=i+1; j<qp->n; j++)
            for (k=j+1; k<qp->n; k++)
                nb_cont_triangle++;

    /*Computation of the initial number of constraints */
    /* Ax=b */
    cp->ind_eg = qp->m;
    /* Dx = e */
    cp->ind_ineg = cp->ind_eg + qp->p;
    /*Aq,y + Aq_0 x = bq*/
    cp->ind_eg_q = cp->ind_ineg + qp->mq;
    /*Dq,y + Dq_0 x <= eq*/
    cp->ind_ineg_q = cp->ind_eg_q + qp->pq;
    /*y_ij = x_i */
    if (flag==1)
        cp->ind_y_1=cp->ind_ineg_q +cp->n;
    else
        /*initially no quadratic constraints, new created quad. constraints will not be in the ref. problem*/
        cp->ind_y_1=cp->ind_ineg +cp->n;
    /*y_ij <= x_j i<j */
    cp->ind_y_2=cp->ind_y_1 +nb_cont_cor;
    /*y_ij <= x_i i<j */
    cp->ind_y_3=cp->ind_y_2 +nb_cont_cor;
    /*y_ij >= x_j + x_i -1 i<j */
    cp->ind_y_4=cp->ind_y_3+nb_cont_cor;
    /*y_ij >= 0 i<j */
    cp->ind_y_5=cp->ind_y_4+nb_cont_cor;
    /* y_ik + y_jk -y_ij -x_k <= 0*/
    cp->ind_y_6=cp->ind_y_5+nb_cont_triangle;
    /* y_ij + y_ik -y_jk -x_i <= 0*/
    cp->ind_y_7=cp->ind_y_6+nb_cont_triangle;
    /* y_ij + y_jk -y_ik -x_j <= 0*/
    cp->ind_y_8=cp->ind_y_7+nb_cont_triangle;
    /* x_i+x_j+x_k -1 - y_ij - y_jk -y_ik <= 0*/
    cp->nrow=cp->ind_y_8+nb_cont_triangle;

    /* initialisation of the new number of constraints*/
    /* Ax=b */
    cp->nb_eg =qp->m;
    /* Dx = e */
    cp->nb_ineg = qp->p;
    /*Aq,y + Aq_0 x = bq*/
    cp->nb_eg_q =  qp->mq;
    /*Dq,y + Dq_0 x <= eq*/
    cp->nb_ineg_q =  qp->pq;

    /*y_ii = x_i */
    cp->nb_y_1=0;
    /*y_ij <= x_j i<j */
    cp->nb_y_2=0;
    /*y_ij <= x_i i<j */
    cp->nb_y_3=0;
    /*y_ij >= x_j+x_i-1 i<j */
    cp->nb_y_4=0;
    /*y_ij >= 0 i<j */
    cp->nb_y_5=0;
    /* y_ik + y_jk -y_ij -x_k <= 0*/
    cp->nb_y_6=0;
    /* y_ij + y_ik -y_jk -x_i <= 0*/
    cp->nb_y_7=0;
    /* y_ij + y_jk -y_ik -x_j <= 0*/
    cp->nb_y_8=0;
    /* x_i+x_j+x_k -1 - y_ij - y_jk -y_ik <= 0*/
    cp->nb_y_9=0;


    /* real number of variables y*/
    cp->nb_var_y = alloc_matrix(qp->n,qp->n);

    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            /*initialisation*/
            if (flag==1 || flag ==2)
            {
                cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                if (!Zero_BB(beta[ij2k(i,j,qp->n)]))
                    cp->nb_var_y[ij2k(i,j,qp->n)] =1;
            }

            else
            {
                cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                if (Positive_BB(beta[ij2k(i,j,qp->n)]) && (i<j))
                    cp->nb_var_y[ij2k(i,j,qp->n)] =1;
                if (Negative_BB(beta[ij2k(i,j,qp->n)]) && (i<j))
                    cp->nb_var_y[ij2k(i,j,qp->n)] =-1;
            }
        }
    if (flag ==1 || flag ==2)
    {
        for(l=0; l<qp->mq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->aq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)]) && ((i < cp->nb_int) || (j < cp->nb_int)))
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;

        for(l=0; l<qp->pq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->dq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)]) && ((i < cp->nb_int) || (j < cp->nb_int)))
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;


        for(l=0; l<qp->n; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if ( (!Zero_BB(cp->delta_1[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_2[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_3[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_4[ijk2l(l,i,j,qp->n,qp->n)])))
                    {
                        cp->nb_var_y[ij2k(l,i,qp->n)] =1;
                        cp->nb_var_y[ij2k(l,j,qp->n)] =1;
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;
                    }

    }

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            cp->nb_var_y[ij2k(j,i,qp->n)] = cp->nb_var_y[ij2k(i,j,qp->n)];


    /* printf("\ncp->nb_var_t create miqcr 01 triangle\n"); */
    /* print_mat(cp->nb_var_y,cp->n,cp->n); */
    /* printf("\n"); */

    cp->nb_y = nb_non_zero_matrix_sup_i(cp->nb_var_y,qp->n,qp->n);

    /*computation of the new number of constraints */
    for(i=0; i<qp->nb_int; i++)
        for (j=i; j<qp->n; j++)
        {
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && (i!=j))
            {
                if (flag ==1 || flag ==2)
                {
                    cp->nb_y_2++;
                    cp->nb_y_3++;
                    cp->nb_y_4++;
                    cp->nb_y_5++;
                    for (k=j+1; k<qp->n; k++)
                    {
                        if (!Zero_BB(cp->nb_var_y[ij2k(i,k,cp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,cp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,j,cp->n)]) && ((i!=j) && (i!=k) && (j!=k)))
                        {
                            if(!Zero_BB(cp->delta_1[ijk2l(i,j,k, cp->n,cp->n)]))
                                cp->nb_y_6++;

                            if(!Zero_BB(cp->delta_2[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_7++;

                            if(!Zero_BB(cp->delta_3[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_8++;

                            if (!Zero_BB(cp->delta_4[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_9++;

                        }
                    }
                }
                else
                {
                    if (Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    {
                        cp->nb_y_2++;
                        cp->nb_y_3++;
                    }
                    if  (Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    {
                        cp->nb_y_4++;
                        cp->nb_y_5++;
                    }
                }
            }
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && (i==j))
                cp->nb_y_1++;
        }

    if (flag==2)
    {
        cp->nb_cont =cp->m + cp->p +cp->mq + cp->pq +cp->nb_y_1+ cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4 + cp->nb_y_5;
        cp->nb_cut = cp->nb_y_6  +cp->nb_y_7+ cp->nb_y_8+ cp->nb_y_9;
    }

    cp->nb_row =cp->m + cp->p +cp->mq + cp->pq +cp->nb_y_1+ cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4 + cp->nb_y_5+ cp->nb_y_6  +cp->nb_y_7+ cp->nb_y_8+ cp->nb_y_9;



    cp->cont = alloc_vector(cp->nrow);
    for(i=0; i<cp->nrow; i++)
        cp->cont[i]=-1;

    /* number of initial variables */

    cp->ind_max_int =qp->nb_int;
    cp->ind_max_x =qp->n;
    cp->ncol = cp->ind_max_x + qp->n*qp->n;

    int r;

    if (flag ==1 || flag ==2)
        r=cp->ind_ineg_q;
    else
        r=cp->ind_ineg;

    /* initial constraints*/
    for(i=0; i<r; i++)
        cp->cont[i]=i;


    /* y_ii = x_i  */
    for(i=0; i<qp->n; i++)
        if(!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
        {
            cp->cont[cp->ind_ineg_q + i] = r;
            r++;
        }

    /* y_ij <= x_j  i<j */
    if (flag ==1 || flag ==2)
        r= cp->ind_ineg_q + cp->nb_y_1 ;
    else
        r= cp->ind_ineg + cp->nb_y_1 ;
    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if (flag ==1 || flag ==2)
                {
                    cp->cont[cp->ind_y_1 + i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }
                else if((Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)])) && (flag !=1))
                {
                    cp->cont[cp->ind_y_1 + i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }
            }

    /* y_ij <= x_i */
    if (flag ==1 || flag ==2)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2 ;
    else
        r= cp->ind_ineg + cp->nb_y_1+ cp->nb_y_2 ;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if(flag ==1 || flag ==2)
                {
                    cp->cont[cp->ind_y_2+ i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }
                else if((Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)])) && (flag!=1))
                {
                    cp->cont[cp->ind_y_2+ i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }
            }

    /* y_ij >= x_j + x_i -1 */
    if (flag ==1 || flag ==2)
        r= cp->ind_ineg_q + cp->nb_y_1+ cp->nb_y_2 + cp->nb_y_3 ;
    else
        r= cp->ind_ineg +cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if(flag ==1  || flag ==2)
                {
                    cp->cont[cp->ind_y_3+i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }
                else if((Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)])) && (flag!=1))
                {
                    cp->cont[cp->ind_y_3+i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }
            }

    /* y_ij >= 0 */
    if (flag==1 || flag ==2)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if(flag ==1 || flag ==2)
                {
                    cp->cont[cp->ind_y_4+ i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }
                else if((Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)])) && (flag!=1))
                {
                    cp->cont[cp->ind_y_4+ i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }
            }

    /* y_ik + y_jk -y_ij -x_k <= 0*/
    if (flag==1 || flag ==2)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5;

    int ind;
    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_1[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_5+ind] = r;
                    r++;
                }

    /* y_ij + y_ik -y_jk -x_i <= 0*/
    if (flag==1 || flag ==2)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_2[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_6+ ind] = r;
                    r++;
                }

    /* y_ij + y_jk -y_ik -x_j <= 0*/
    if (flag==1 || flag ==2)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_3[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_7+ind] = r;
                    r++;
                }

    /* x_i+x_j+x_k -1 - y_ij - y_jk -y_ik <= 0*/
    if (flag==1 || flag ==2)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_4[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_8+ind] = r;
                    r++;
                }



    /* real number of variables +1 for constant term*/
    cp->nb_col =cp->n  + cp->nb_y +1;

    /*nb occurence of each x in constraints involving y or z variables*/
    cp->x_cont=alloc_vector(cp->ind_max_x);
    for(i=0; i<cp->ind_max_x; i++)
        if (!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
            cp->x_cont[i]=1;
        else
            cp->x_cont[i]=0;


    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                if(flag==1 || flag ==2)
                {
                    cp->x_cont[i] = cp->x_cont[i]+2;
                    cp->x_cont[j] = cp->x_cont[j]+2;
                }
                else
                {
                    cp->x_cont[i] = cp->x_cont[i]+1;
                    cp->x_cont[j] = cp->x_cont[j]+1;
                }

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(k,j,qp->n)]) )
                    if(flag==1)
                    {
                        if(!Zero_BB(cp->delta_1[ijk2l(i,j,k, cp->n,cp->n)]))
                        {
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_2[ijk2l(i,j,k,cp->n,cp->n)]))
                        {

                            cp->x_cont[i] = cp->x_cont[i]+1;
                        }
                        if(!Zero_BB(cp->delta_3[ijk2l(i,j,k,cp->n,cp->n)]))
                        {

                            cp->x_cont[j] = cp->x_cont[j]+1;
                        }
                        if (!Zero_BB(cp->delta_4[ijk2l(i,j,k,cp->n,cp->n)]))
                        {


                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                    }


    /*table with corresponding variables*/
    cp->var = alloc_vector(cp->ncol);

    /*variables x all created*/
    for(i=0; i<cp->ind_max_x; i++)
    {
        cp->var[i]=i;
    }



    /*variables y*/
    r=cp->ind_max_x;

    for(i=0; i<qp->n; i++)
    {
        for (j=0; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if (i<=j)
                {
                    cp->var[(qp->n)*i + j + cp->ind_max_x] =r;
                    r++;
                }


            }
    }


    return cp;
}


Q_MIQCP create_qap_miqcp(MIQCP qp, double * beta, SDP psdp, int flag)
{
    int i,j,k,l,lim,s;
    int test =0;
    int nb_cont_cor ;
    Q_MIQCP cp = new_q_miqcp();

    cp->nb_init_var = psdp->nb_init_var;

    int nbcontquad3;

    cp->N=qp->N;
    cp->n=qp->n;

    cp->nb_int=qp->nb_int;
    cp->m=qp->m;
    cp->p=qp->p;
    cp->mq=qp->mq;
    cp->pq=qp->pq;
    cp->u = copy_vector_d(qp->u,qp->n);
    cp->l = copy_vector_d(qp->l,qp->n);

    cp->lg = copy_vector(qp->lg,qp->n);
    cp->lgc = copy_vector(qp->lgc,qp->n);



   
    cp->miqp=qp->miqp;
    cp->q = copy_matrix_d(qp->q, qp->n,qp->n);
    cp->c = copy_vector_d(qp->c,qp->n);

    cp->a = copy_matrix_d(qp->a, qp->m,qp->n);
    cp->b = copy_vector_d(qp->b,qp->m);

    if (flag!=3 && flag!=5)
    {
        cp->aq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
        cp->alphaq = copy_vector_d(psdp->alphaq,qp->mq);
    }

    if (flag==3)
    {
        nbcontquad3 =cp->nb_init_var*(cp->nb_init_var-1) + 2*(cp->nb_init_var*cp->nb_init_var - cp->nb_init_var);
        cp->aq= alloc_matrix_3d(nbcontquad3,cp->n+1,cp->n+1);


        s=0;
        /* contraintes sum_k(x_(ki)x_(kj)) = 0, i,j :i<j*/
        for(i=0; i<cp->nb_init_var; i++)
            for (j=i+1; j<cp->nb_init_var; j++)
            {
                for (k=0; k<cp->nb_init_var; k++)
                {
                    cp->aq[ijk2l(s,k*cp->nb_init_var+i+1,k*cp->nb_init_var+j+1,cp->n+1,cp->n+1)]=1;
                    cp->aq[ijk2l(s,k*cp->nb_init_var+j+1,k*cp->nb_init_var+i+1,cp->n+1,cp->n+1)]=1;
                }
                s++;
            }

        /* contraintes sum_k(x_(ik)x_(jk)) = 0, i,j :i<j */
        for(i=0; i<cp->nb_init_var; i++)
            for (j=i+1; j<cp->nb_init_var; j++)
            {
                for (k=0; k<cp->nb_init_var; k++)
                {
                    cp->aq[ijk2l(s,i*cp->nb_init_var+k+1,j*cp->nb_init_var+k+1,cp->n+1,cp->n+1)] =1;
                    cp->aq[ijk2l(s,j*cp->nb_init_var+k+1,i*cp->nb_init_var+k+1,cp->n+1,cp->n+1)] =1;
                }
                s++;
            }



        /* contraintes sum_(ij)(x_(ik)x_(jl)) = 1 : i<j k!=l */
        for(k=0; k<cp->nb_init_var; k++)
            for (l=0; l<cp->nb_init_var; l++)
                if (k!=l)
                {
                    for(i=0; i<cp->nb_init_var; i++)
                        for (j=i+1; j<cp->nb_init_var; j++)
                        {
                            cp->aq[ijk2l(s,i*cp->nb_init_var+k+1,j*cp->nb_init_var+l+1,cp->n+1,cp->n+1)] =1;
                            cp->aq[ijk2l(s,j*cp->nb_init_var+l+1,i*cp->nb_init_var+k+1,cp->n+1,cp->n+1)] =1;
                        }
                    s++;
                }


        /* contraintes sum_(kl)(x_(ik)x_(jl)) = 1 : k<l i!=j */
        for(i=0; i<cp->nb_init_var; i++)
            for (j=0; j<cp->nb_init_var; j++)
                if (i!=j)
                {
                    for(k=0; k<cp->nb_init_var; k++)
                        for (l=k+1; l<cp->nb_init_var; l++)
                        {
                            cp->aq[ijk2l(s,i*cp->nb_init_var+k+1,j*cp->nb_init_var+l+1,cp->n+1,cp->n+1)] =1;
                            cp->aq[ijk2l(s,j*cp->nb_init_var+l+1,i*cp->nb_init_var+k+1,cp->n+1,cp->n+1)] =1;
                        }
                    s++;
                }

        cp->bq = alloc_vector_d(nbcontquad3);
        for (i=0; i<cp->nb_init_var*(cp->nb_init_var-1); i++)
            cp->bq[i] = 0;
        for(i; i<nbcontquad3; i++)
            cp->bq[i] = 1;

        cp->alphaq =  copy_vector_d(psdp->alphaq, nbcontquad3);
    }
    if (flag != 5) /*flag == 5 : cplex direct*/
        cp->beta = copy_matrix_d(beta,qp->n,qp->n);

    if (flag==1)
    {
        cp->beta_1 = copy_matrix_d(psdp->beta_1,qp->n,qp->n);
        nb_cont_cor = (qp->n-1)*qp->n/2;


        /*Computation of the initial number of constraints */
        /* Ax=b */
        cp->ind_eg = qp->m;
        /*y_ij <= x_j i<j */
        cp->ind_y_1=cp->ind_eg +nb_cont_cor;
        /*y_ij <= x_i i<j */
        cp->ind_y_2=cp->ind_y_1 +nb_cont_cor;
        /*y_ij >= x_j + x_i -1 i<j */
        cp->ind_y_3=cp->ind_y_2 +nb_cont_cor;
        /*y_ij >= 0 i<j */
        cp->nrow=cp->ind_y_3+nb_cont_cor;



        /* initialisation of the new number of constraints*/
        /* Ax=b */
        cp->nb_eg =qp->m;
        /*y_ij <= x_j i<j */
        cp->nb_y_1=0;
        /*y_ij <= x_i i<j */
        cp->nb_y_2=0;
        /*y_ij >= x_j+x_i-1 i<j */
        cp->nb_y_3=0;
        /*y_ij >= 0 i<j */
        cp->nb_y_4=0;

        /* real number of variables y*/
        cp->nb_var_y = alloc_matrix(qp->n,qp->n);

        for(i=0; i<qp->n; i++)
            for(j=0; j<qp->n; j++)
            {
                cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                if (!Zero_BB(psdp->beta_1[ij2k(i,j,qp->n)]) && i!=j)
                    cp->nb_var_y[ij2k(i,j,qp->n)] =1;
            }

        cp->nb_y = nb_non_zero_matrix_sup_i(cp->nb_var_y,qp->n,qp->n);

        /*computation of the new number of constraints */
        for(i=0; i<qp->nb_int; i++)
            for (j=i; j<qp->n; j++)
            {
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && (i!=j))
                {
                    cp->nb_y_1++;
                    cp->nb_y_2++;
                    cp->nb_y_3++;
                    cp->nb_y_4++;
                }
            }

        cp->nb_row =cp->m + cp->nb_y_1+ cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4;

        cp->cont = alloc_vector(cp->nrow);
        for(i=0; i<cp->nrow; i++)
            cp->cont[i]=-1;

        /* number of initial variables */

        cp->ind_max_int =qp->nb_int;
        cp->ind_max_x =qp->n;
        cp->ncol = cp->ind_max_x + qp->n*qp->n +1 ;

        int r;

        r=cp->ind_eg;

        /* initial constraints*/
        for(i=0; i<r; i++)
            cp->cont[i]=i;


        /* y_ij <= x_j  i<j */
        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_eg + i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }


        /* y_ij <= x_i */

        r= cp->ind_eg+ cp->nb_y_1;

        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_1+ i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }

        /* y_ij >= x_j + x_i -1 */

        r= cp->ind_eg + cp->nb_y_1+ cp->nb_y_2;

        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_2+i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }


        /* y_ij >= 0 */
        r= cp->ind_eg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3;

        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_3+ i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }


        /* real number of variables +1 for constant term*/
        cp->nb_col =cp->n  + cp->nb_y +1;

        /*nb occurence of each x in constraints */
        cp->x_cont=alloc_vector(cp->ind_max_x);
        for(i=0; i<cp->ind_max_x; i++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
                cp->x_cont[i]=1;
            else
                cp->x_cont[i]=0;


        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->x_cont[i] = cp->x_cont[i]+2;
                    cp->x_cont[j] = cp->x_cont[j]+2;
                }

        /*table with corresponding variables*/
        cp->var = alloc_vector(cp->ncol);

        /*variables x all created*/
        for(i=0; i<cp->ind_max_x; i++)
        {
            cp->var[i]=i;
        }



        /*variables y*/
        r=cp->ind_max_x;

        for(i=0; i<qp->n; i++)
        {
            for (j=0; j<qp->n; j++)
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->var[(qp->n)*i + j + cp->ind_max_x] =r;
                    r++;
                }
        }

    }



    if (flag==4)
    {

        cp->beta_1 = copy_matrix_d(psdp->beta_1,qp->n,qp->n);
        nb_cont_cor = (qp->n-1)*qp->n/2;


        /*Computation of the initial number of constraints */
        /* Ax=b */
        cp->ind_eg = qp->m;
        /*x^TAqx = b */
        cp->ind_eg_q = cp->ind_eg +2*cp->nb_init_var*qp->n;
        /*y_ij >= 0 i<j */
        cp->nrow=cp->ind_eg_q +nb_cont_cor;


        /* initialisation of the new number of constraints*/
        /* Ax=b */
        cp->nb_eg =qp->m;
        /*x^TAqx = b */
        cp->nb_eg_q = cp->ind_eg +2*cp->nb_init_var*qp->n;
        /*y_ij >= 0 i<j */
        cp->nb_y_1=0;

        /* real number of variables y*/
        cp->nb_var_y = alloc_matrix(qp->n,qp->n);

        for(i=0; i<qp->n; i++)
            for(j=0; j<qp->n; j++)
            {
                cp->nb_var_y[ij2k(i,j,qp->n)] =1;
            }


        cp->nb_y = nb_non_zero_matrix_sup_i(cp->nb_var_y,qp->n,qp->n);

        /*computation of the new number of constraints */
        for(i=0; i<qp->nb_int; i++)
            cp->nb_y_1= cp->nb_y_1;


        cp->nb_row =cp->m +2*cp->nb_init_var*qp->n+ cp->nb_y - cp->n;

        cp->cont = alloc_vector(cp->nrow);
        for(i=0; i<cp->nrow; i++)
            cp->cont[i]=i;

        /* number of initial variables */

        cp->ind_max_int =qp->nb_int;
        cp->ind_max_x =qp->n;
        cp->ncol = cp->ind_max_x + qp->n*qp->n +1 ;



        /* real number of variables +1 for constant term*/
        cp->nb_col =cp->n  + cp->nb_y - cp->n  +1;

        /*nb occurence of each x in constraints */
        cp->x_cont=alloc_vector(cp->ind_max_x);
        for(i=0; i<cp->ind_max_x; i++)
            cp->x_cont[i]=0;


        /*table with corresponding variables*/
        cp->var = alloc_vector(cp->ncol);

        /*variables x all created*/
        for(i=0; i<cp->ind_max_x; i++)
        {
            cp->var[i]=i;
        }

        int r;

        /*variables y*/
        r=cp->ind_max_x;

        for(i=0; i<qp->n; i++)
        {
            for (j=0; j<qp->n; j++)
            {
                cp->var[(qp->n)*i + j + cp->ind_max_x] =r;
                r++;
            }
        }

    }


    if (flag==6)
    {

        cp->beta_1 = copy_matrix_d(psdp->beta_1,qp->n,qp->n);
        nb_cont_cor = (qp->n-1)*qp->n/2;


        /*Computation of the initial number of constraints */
        /* Ax=b */
        cp->ind_eg = qp->m;
        /*y_ij >= x_j + x_i -1 i<j */
        cp->nrow=cp->ind_eg +nb_cont_cor;


        /* initialisation of the new number of constraints*/
        /* Ax=b */
        cp->nb_eg =qp->m;
        /*y_ij >= x_j+x_i-1 i<j */
        cp->nb_y_1=0;

        /* real number of variables y*/
        cp->nb_var_y = alloc_matrix(qp->n,qp->n);

        for(i=0; i<cp->nb_init_var; i++)
            for(j=0; j<cp->nb_init_var; j++)
                for(k=0; k<cp->nb_init_var; k++)
                    for(l=0; l<cp->nb_init_var; l++)
                    {
                        cp->nb_var_y[ij2k(ij2k(i,j,cp->nb_init_var),ij2k(k,l,cp->nb_init_var),qp->n)] =0;
                        if (!Zero_BB(psdp->beta_1[ij2k(ij2k(i,j,cp->nb_init_var),ij2k(k,l,cp->nb_init_var),qp->n)]) && i!=k && j!=l)
                            cp->nb_var_y[ij2k(ij2k(i,j,cp->nb_init_var),ij2k(k,l,cp->nb_init_var),qp->n)] =1;

                    }


        /* On ne considere pas les y_ijij*/
        cp->nb_y = nb_non_zero_matrix_sup_i(cp->nb_var_y,qp->n,qp->n);

        /*computation of the new number of constraints */
        for(i=0; i<qp->nb_int; i++)
            for (j=i+1; j<qp->n; j++)
            {
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && (i!=j))
                {
                    cp->nb_y_1++;

                }
            }

        cp->nb_row =cp->m + cp->nb_y_1;

        cp->cont = alloc_vector(cp->nrow);
        for(i=0; i<cp->nrow; i++)
            cp->cont[i]=-1;

        /* number of initial variables */

        cp->ind_max_int =qp->nb_int;
        cp->ind_max_x =qp->n;
        cp->ncol = cp->ind_max_x + qp->n*qp->n -1 ;

        int r;

        r=cp->ind_eg;

        /* initial constraints*/
        for(i=0; i<r; i++)
            cp->cont[i]=i;


        /* y_ij >= x_j + x_i -1 */


        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_eg+i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }

        /* real number of variables +1 for constant term*/
        cp->nb_col =cp->n  + cp->nb_y +1;

        /*nb occurence of each x in constraints */
        cp->x_cont=alloc_vector(cp->ind_max_x);
        for(i=0; i<cp->ind_max_x; i++)
            cp->x_cont[i]=2;


        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->x_cont[i] = cp->x_cont[i]+1;
                    cp->x_cont[j] = cp->x_cont[j]+1;
                }

        /*table with corresponding variables*/
        cp->var = alloc_vector(cp->ncol);

        /*variables x all created*/
        for(i=0; i<cp->ind_max_x; i++)
        {
            cp->var[i]=i;
        }



        /*variables y*/
        r=cp->ind_max_x;

        for(i=0; i<qp->n; i++)
        {
            for (j=0; j<qp->n; j++)
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->var[(qp->n)*i + j + cp->ind_max_x] =r;
                    r++;
                }
        }




    }



    return cp;
}

Q_MIQCP create_mkc_miqcp(MIQCP qp, double * beta, SDP psdp, int flag)
{
    int i,j,k,l,lim,s;
    int test =0;
    int nb_cont_cor ;
    Q_MIQCP cp = new_q_miqcp();

    if (flag!=5)
        cp->nb_init_var = psdp->nb_init_var;


    int nbcontquad3;

    cp->N=qp->N;
    cp->n=qp->n;

    cp->nb_int=qp->nb_int;
    cp->m=qp->m;
    cp->p=qp->p;
    cp->mq=qp->mq;
    cp->pq=qp->pq;
    cp->u = copy_vector_d(qp->u,qp->n);
    cp->l = copy_vector_d(qp->l,qp->n);

    cp->lg = copy_vector(qp->lg,qp->n);
    cp->lgc = copy_vector(qp->lgc,qp->n);



   
    cp->miqp=qp->miqp;
    cp->q = copy_matrix_d(qp->q, qp->n,qp->n);
    cp->c = copy_vector_d(qp->c,qp->n);

    cp->a = copy_matrix_d(qp->a, qp->m,qp->n);
    cp->b = copy_vector_d(qp->b,qp->m);

    if (flag!=5)
        cp->beta = copy_matrix_d(beta,qp->n,qp->n);

    cp->aq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
    cp->bq = copy_vector_d(qp->bq,qp->mq);
    if (flag!=5)
        cp->alphaq = copy_vector_d(psdp->alphaq,qp->mq);

    if (flag ==4)
    {

        nb_cont_cor = (qp->n-1)*qp->n/2;


        /*Computation of the initial number of constraints */
        /* Ax=b */
        cp->ind_eg = qp->m;
        /*y_ij >= x_j + x_i -1 i<j */
        cp->nrow=cp->ind_eg +nb_cont_cor;


        /* initialisation of the new number of constraints*/
        /* Ax=b */
        cp->nb_eg =qp->m;
        /*y_ij >= x_j+x_i-1 i<j */
        cp->nb_y_1=0;

        /* real number of variables y*/
        cp->nb_var_y = alloc_matrix(qp->n,qp->n);
        int k_mkc = (int) cp->n/ cp->nb_init_var;
        for(i=0; i<cp->nb_init_var; i++)
            for(j=0; j<k_mkc; j++)
                for(k=0; k<cp->nb_init_var; k++)
                    for(l=0; l<k_mkc; l++)
                    {

                        cp->nb_var_y[ij2k(ij2k(i,j,k_mkc),ij2k(k,l,k_mkc),qp->n)] =0;
                        if ((i!=k) && (!Zero_BB(cp->beta[ij2k(ij2k(i,j,k_mkc),ij2k(k,l,k_mkc),qp->n)])))
                            cp->nb_var_y[ij2k(ij2k(i,j,k_mkc),ij2k(k,l,k_mkc),qp->n)] =1;

                    }


        /* On ne considere pas les y_ijij*/
        cp->nb_y = nb_non_zero_matrix_without_diag_i(cp->nb_var_y,qp->n,qp->n)/2;

        /*computation of the new number of constraints */
        for(i=0; i<qp->n; i++)
            for (j=i+1; j<qp->n; j++)
            {
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->nb_y_1++;

                }
            }

        cp->nb_row =cp->m + cp->nb_y_1 ;

        cp->cont = alloc_vector(cp->nrow);
        for(i=0; i<cp->nrow; i++)
            cp->cont[i]=-1;

        /* number of initial variables */

        cp->ind_max_int =qp->nb_int;
        cp->ind_max_x =qp->n;
        cp->ncol = cp->ind_max_x + qp->n*(qp->n-1);

        int r;

        r=cp->ind_eg;

        /* initial constraints*/
        for(i=0; i<r; i++)
            cp->cont[i]=i;


        /* y_ij >= x_j + x_i -1 */
        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_eg+i*(2*(qp->n) -i-1)/2 + j-i-1] = r;
                    r++;
                }



        /* real number of variables +1 for constant term*/
        cp->nb_col =cp->n  + cp->nb_y;

        /*nb occurence of each x in constraints */
        cp->x_cont=alloc_vector(cp->ind_max_x);
        cp->x_cont[0]=2;
        for(i=1; i<cp->n; i++)
            cp->x_cont[i]=1;


        for(i=0; i<qp->n; i++)
            for(j=i+1; j<qp->n; j++)
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->x_cont[i] = cp->x_cont[i]+1;
                    cp->x_cont[j] = cp->x_cont[j]+1;

                }

        /*table with corresponding variables*/
        cp->var = alloc_vector(cp->ncol);

        /*variables x all created*/
        for(i=0; i<cp->ind_max_x; i++)
        {
            cp->var[i]=i;
        }



        /*variables y*/
        r=cp->ind_max_x;

        for(i=0; i<qp->n; i++)
        {
            for (j=0; j<qp->n; j++)
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->var[(qp->n)*i + j + cp->ind_max_x] =r;
                    r++;
                }
        }
    }

    return cp;
}


/*******************************************************************************/
/******************* Structure for general MIQCP ***************************/
/******************************************************************************/
Q_MIQCP create_g_miqcp(MIQCP qp, double * beta, double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag )
{
    int i,j,k,l,lim;
    int test =0;

    Q_MIQCP cp = new_q_miqcp();
    cp->N=qp->N;
    cp->n=qp->n;
    cp->nb_int=qp->nb_int;
    cp->m=qp->m;
    cp->p=qp->p;
    cp->mq=qp->mq;
    cp->pq=qp->pq;
    cp->u = copy_vector_d(qp->u,qp->n);
    cp->l = copy_vector_d(qp->l,qp->n);
    cp->lg = copy_vector(qp->lg,qp->n);
    cp->lgc = copy_vector(qp->lgc,qp->n);

    cp->flag_nb_var = flag;

   
    cp->miqp=qp->miqp;
    cp->q = copy_matrix_d(qp->q, qp->n,qp->n);
    cp->c = copy_vector_d(qp->c,qp->n);

    if(!Zero(qp->m))
    {
        cp->a = copy_matrix_d(qp->a, qp->m,qp->n);
        cp->b = copy_vector_d(qp->b,qp->m);
    }
    else
    {
        cp->a=alloc_matrix_d(1,1);
        cp->b = copy_vector_d(qp->b,qp->m);
    }

    if(!Zero(qp->p))
    {
        cp->d= copy_matrix_d(qp->d, qp->p,qp->n);
        cp->e = copy_vector_d(qp->e,qp->p);
    }
    else
    {
        cp->d=alloc_matrix_d(1,1);
        cp->e = copy_vector_d(qp->e,qp->p);
    }

    if(!Zero(qp->mq))
    {
        cp->aq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }
    else
    {
        cp->aq=alloc_matrix_d(1,1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }

    if(!Zero(qp->pq))
    {
        cp->dq= copy_matrix_3d(qp->dq, qp->pq,qp->n+1,qp->n+1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }
    else
    {
        cp->dq=alloc_matrix_d(1,1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }

    if(!Zero(qp->m))
        cp->alpha =alpha;
    else
        cp->alpha=0;

    if(!Zero(qp->p))
        cp->alphabis =alphabis;
    else
        cp->alphabis=0;

    if(!Zero(qp->mq))
    {
        cp->alphaq = copy_vector_d(alphaq,qp->mq);
        if (flag == 2)
        {
            cp->lambda_min_eg = copy_vector_d(lambda_min_eg,qp->mq);
            cp->lambda_max_eg = copy_vector_d(lambda_max_eg,qp->mq);
        }
        else
        {
            cp->lambda_min_eg= alloc_vector_d(cp->mq);
            cp->lambda_max_eg= alloc_vector_d(cp->mq);
            for(i=0; i<cp->mq; i++)
            {
                cp->lambda_min_eg[i] = 0;
                cp->lambda_max_eg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphaq = alloc_vector_d(cp->mq);
        cp->lambda_min_eg= alloc_vector_d(cp->mq);
        cp->lambda_max_eg= alloc_vector_d(cp->mq);
        for(i=0; i<cp->mq; i++)
        {
            cp->alphaq[i]=0;
            cp->lambda_min_eg[i] = 0;
            cp->lambda_max_eg[i] = 0;
        }
    }

    if(!Zero(qp->pq))
    {
        cp->alphabisq =copy_vector_d(alphabisq,qp->pq);
        if (flag == 2)
        {
            cp->lambda_min_ineg = copy_vector_d(lambda_min_ineg,qp->pq);
            cp->lambda_max_ineg = copy_vector_d(lambda_max_ineg,qp->pq);
        }
        else
        {
            cp->lambda_min_ineg= alloc_vector_d(cp->pq);
            cp->lambda_max_ineg= alloc_vector_d(cp->pq);
            for(i=0; i<cp->pq; i++)
            {
                cp->lambda_min_ineg[i] = 0;
                cp->lambda_max_ineg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphabisq = alloc_vector_d(cp->pq);
        cp->lambda_min_ineg= alloc_vector_d(cp->pq);
        cp->lambda_max_ineg= alloc_vector_d(cp->pq);
        for(i=0; i<cp->pq; i++)
        {
            cp->alphabisq[i]=0;
            cp->lambda_min_ineg[i] = 0;
            cp->lambda_max_ineg[i] = 0;
        }
    }

    cp->beta = copy_matrix_d(beta,qp->n,qp->n);

    int cont = 0;
    for(i=qp->n; i>qp->n - qp->nb_int; i--)
        cont=cont+i;

    int cont_bis = 0;
    for(i=qp->n-1; i>qp->n- 1 - qp->nb_int; i--)
        cont_bis=cont_bis+i;

    /*Computation of the initial number of constraints */
    /* Ax=b */
    cp->ind_eg = qp->m;
    /* Dx = e */
    cp->ind_ineg = cp->ind_eg + qp->p;
    /*x^TAqx = bq*/
    cp->ind_eg_q = cp->ind_ineg + qp->mq;
    /*x^TDqx = eq*/
    cp->ind_ineg_q = cp->ind_eg_q + qp->pq;
    /* x = sum_k 2^k t_ik*/
    if (flag ==3)
        cp->ind_dec_x=cp->ind_ineg_q+qp->nb_int;
    else
        cp->ind_dec_x=cp->ind_ineg+qp->nb_int;
    /* y_ij = sum_k 2^k z_ijk*/
    cp->ind_dec_y=cp->ind_dec_x + qp->nb_int*(qp->n);
    /*z_ijk <= u_jt_ik*/
    cp->ind_z_1=cp->ind_dec_y +(qp->n)*qp->N;
    /*z_ijk <= x_j*/
    cp->ind_z_2=cp->ind_z_1+(qp->n)*qp->N;
    /*z_ijk >= x_j - u_j(1 - t_ik)*/
    cp->ind_z_3=cp->ind_z_2+(qp->n)*qp->N;
    /*z_ijk >= 0*/
    cp->ind_z_4=cp->ind_z_3+(qp->n)*qp->N;
    /* y_ij >= u_ix_j + u_jx_i - u_iu_j */
    cp->ind_y_1=cp->ind_z_4 +cont;
    /* y_ii >= x_i */
    cp->ind_y_2= cp->ind_y_1 + qp->nb_int;
    /* y_ij = y_ji */
    cp->ind_y_3 =cp->ind_y_2 + cont_bis;
    /* y_ij <= u_ix_j  for x_i integer and x_j real*/
    cp->nrow =cp->ind_y_3 + qp->nb_int * (qp->n - qp->nb_int);

    /* initialisation of the new number of constraints*/
    /* Ax=b */
    cp->nb_eg =qp->m;
    /* Dx = e */
    cp->nb_ineg = qp->p;
    /*x^TAqx = bq*/
    cp->nb_eg_q = qp->mq;
    /*x^TDqx = eq*/
    cp->nb_ineg_q = qp->pq;
    /* x = sum_k 2^k t_ik*/
    cp->nb_dec_x=qp->nb_int;
    /* y_ij = sum_k 2^k z_ijk*/
    cp->nb_dec_y=0;
    /*z_ijk <= u_jt_ik*/
    cp->nb_z_1=0;
    /*z_ijk <= x_j*/
    cp->nb_z_2=0;
    /*z_ijk >= x_j - u_j(1 - t_ik)*/
    cp->nb_z_3=0;
    /*z_ijk >= 0*/
    cp->nb_z_4=0;
    /* y_ij >= u_ix_j + u_jx_i - u_iu_j */
    cp->nb_y_1=0;
    /* y_ii >= x_i */
    cp->nb_y_2=0;
    /* y_ij = y_ji */
    cp->nb_y_3 =0;
    /* y_ij <= u_ix_j  for x_i integer and x_j real*/
    cp->nb_y_4=0;

    /* real number of variables y and z*/
    cp->nb_var_y = alloc_matrix(qp->n,qp->n);
    if ((flag ==1) || (flag ==3))
    {
        for(i=0; i<qp->n; i++)
            for(j=0; j<qp->n; j++)
            {
                /*initialisation*/
                cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                if (!Zero_BB(beta[ij2k(i,j,qp->n)]))
                    cp->nb_var_y[ij2k(i,j,qp->n)] =1;
            }

        for(l=0; l<qp->mq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->aq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)]) && ((i < cp->nb_int) || (j < cp->nb_int)))
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;

        for(l=0; l<qp->pq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->dq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)]) && ((i < cp->nb_int) || (j < cp->nb_int)))
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;

        cp->nb_y = nb_non_zero_matrix_i(cp->nb_var_y, qp->n, qp->n);

        cp->nb_z=0;
        for(i=0; i<qp->nb_int; i++)
        {
            lim=log_base_2(qp->u[i])+1;
            for(j=0; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    cp->nb_z=cp->nb_z+lim;
        }
    }
    else
    {
        for(i=0; i<qp->n; i++)
            for(j=0; j<qp->n; j++)
            {
                /*initialisation*/
                cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                if (!Zero_BB(beta[ij2k(i,j,qp->n)]) || ((i==j) && (i<qp->nb_int) && (j<qp->nb_int)))
                    cp->nb_var_y[ij2k(i,j,qp->n)] =1;
            }

        cp->nb_y = nb_non_zero_matrix_i(cp->nb_var_y, qp->n, qp->n);

        cp->nb_z=0;
        for(i=0; i<qp->nb_int; i++)
        {
            lim=log_base_2(qp->u[i])+1;
            for(j=0; j<qp->n; j++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    cp->nb_z=cp->nb_z+lim;
        }
    }
    /*computation of the new number of constraints */
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for (j=i; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->nb_dec_y++;
                cp->nb_y_1++;
                cp->nb_z_1+=lim;
                cp->nb_z_2+=lim;
                cp->nb_z_3+=lim;
                cp->nb_z_4+=lim;
                if (j>=qp->nb_int)
                    cp->nb_y_4++;
                if (i==j)
                    cp->nb_y_2++;
                else
                    cp->nb_y_3++;
            }
    }

    /* z variables from the triangle inf of the matrix*/
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for (j=0; j<i; j++)
            if ((!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)])) && (i!=j))
            {
                cp->nb_dec_y++;
                cp->nb_z_1+=lim;
                cp->nb_z_2+=lim;
                cp->nb_z_3+=lim;
                cp->nb_z_4+=lim;
            }
    }

    if (flag==3)
        cp->nb_row =cp->nb_eg + cp->nb_ineg + cp->nb_eg_q + cp->nb_ineg_q + cp->nb_dec_x + cp->nb_dec_y + cp->nb_z_1+ cp->nb_z_2+ cp->nb_z_3+ cp->nb_z_4+  cp->nb_y_1+  cp->nb_y_2 + cp->nb_y_3+ cp->nb_y_4;
    else
        cp->nb_row =cp->nb_eg + cp->nb_ineg + cp->nb_dec_x + cp->nb_dec_y + cp->nb_z_1+ cp->nb_z_2+ cp->nb_z_3+ cp->nb_z_4+  cp->nb_y_1+  cp->nb_y_2 + cp->nb_y_3+ cp->nb_y_4;

    cp->cont = alloc_vector(cp->nrow);
    for(i=0; i<cp->nrow; i++)
        cp->cont[i]=-1;


    /* number of initial variables */

    cp->ind_max_int =qp->nb_int;
    cp->ind_max_x =qp->n;
    cp->ind_max_y = cp->ind_max_x + qp->nb_int*(qp->n) + qp->nb_int*(qp->n -qp->nb_int);
    cp->ind_max_z =  cp->ind_max_y + (qp->n)*qp->N;
    cp->ncol = cp->ind_max_z + qp->N +1 ;

    int r;


    r=cp->ind_dec_x;

    for(i=0; i<r; i++)
        cp->cont[i]=i;

    /* y_ij = sum_k 2^k z_ijk*/
    for(i=0; i<qp->nb_int; i++)
    {
        for (j=0; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->cont[cp->ind_dec_x + i*(qp->n) +j] = r;
                r++;
            }
    }

    int cpt =0;
    int cpt_bis =0;

    /* z_ijk <= u_j_t_ik */
    r= cp->ind_dec_x + cp->nb_dec_y;

    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            for(k=0; k<lim; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_dec_y+ cpt_bis + j*lim + k+cpt] = r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }

    cpt =0;
    cpt_bis =0;
    /* z_ijk <= x_j */
    r= cp->ind_dec_x + cp->nb_dec_y+cp->nb_z_1 ;

    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            for(k=0; k<lim; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_z_1+ cpt_bis + j*lim + k+cpt] = r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }


    /* z_ijk >= x_j + u_j_t_ik - u_j */
    r= cp->ind_dec_x + cp->nb_dec_y +cp->nb_z_1 + cp->nb_z_2;
    cpt =0;
    cpt_bis =0;
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            for(k=0; k<lim; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_z_2+ cpt_bis + j*lim + k+cpt] = r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }


    /* z_ijk >= 0 */
    r= cp->ind_dec_x + cp->nb_dec_y + cp->nb_z_1 + cp->nb_z_2+ cp->nb_z_3;
    cpt =0;
    cpt_bis =0;
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            for(k=0; k<lim; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_z_3+ cpt_bis + j*lim + k+cpt] = r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }

    /* y_ij >= u_ix_j + u_jx_i - u_iu_j*/
    r=  cp->ind_dec_x+ cp->nb_dec_y+cp->nb_z_1+cp->nb_z_2+cp->nb_z_3+cp->nb_z_4;
    for(i=0; i<qp->nb_int; i++)
    {
        for (j=i; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->cont[cp->ind_z_4 + i* (2*(qp->n) -i +1)/2 + j-i] = r;
                r++;
            }
    }

    /*y_ii >= x_i*/
    r= cp->ind_dec_x+ cp->nb_dec_y+cp->nb_z_1+cp->nb_z_2+cp->nb_z_3+cp->nb_z_4 + cp->nb_y_1;
    for(i=0; i<qp->nb_int; i++)
    {
        if(!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
        {
            cp->cont[cp->ind_y_1 + i] = r;
            r++;
        }
    }

    /*y_ij = y_ji*/
    r= cp->ind_dec_x+ cp->nb_dec_y+cp->nb_z_1+cp->nb_z_2+cp->nb_z_3+cp->nb_z_4+ cp->nb_y_1+ cp->nb_y_2;
    for(i=0; i<qp->nb_int; i++)
    {
        for (j=i+1; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->cont[cp->ind_y_2 +i* (2*(qp->n ) -i-1)/2 + j-i-1] = r;
                r++;
            }
    }

    /* y_ij <= u_ix_j  for x_i integer and x_j real*/
    r= cp->ind_dec_x+ cp->nb_dec_y+cp->nb_z_1+cp->nb_z_2+cp->nb_z_3+cp->nb_z_4+ cp->nb_y_1+ cp->nb_y_2+ cp->nb_y_3;
    for(i=0; i<qp->nb_int; i++)
    {
        for (j=qp->nb_int; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->cont[cp->ind_y_3 + i*(qp->n  - qp->nb_int)+ j-qp->nb_int] = r;
                r++;
            }
    }

    /* real number of variables +1 for constant term*/
    cp->nb_col =cp->ind_max_x  + cp->nb_y + cp->nb_z + qp->N;


    /*nb occurence of each x in constraints involving y or z variables*/
    cp->x_cont=alloc_vector(cp->ind_max_x);
    for(i=0; i<cp->ind_max_x; i++)
        /* initialization to 1 because of the binary expansion constraint for integer x_i*/
        if (i<cp->nb_int)
            cp->x_cont[i]=1;
        else
            cp->x_cont[i]=0;


    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        if (!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
            cp->x_cont[i] = cp->x_cont[i]+1;

        for(j=0; j<qp->n; j++)
        {
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->x_cont[i] = cp->x_cont[i]+1;

                for(k=0; k<lim; k++)
                    cp->x_cont[j] = cp->x_cont[j]+2;

                if (j >= qp->nb_int)
                    cp->x_cont[j] = cp->x_cont[j]+1;
            }
        }
    }

    for(i=qp->nb_int; i<cp->ind_max_x; i++)
    {
        for(j=0; j<qp->nb_int; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->x_cont[i] = cp->x_cont[i]+1;
            }
    }

    /*table with corresponding variables*/
    cp->var = alloc_vector(cp->ncol);

    /*variables x all created*/
    for(i=0; i<cp->ind_max_x; i++)
    {
        cp->var[i]=i;
    }

    /* variables y and z*/
    for(i; i<cp->ncol-qp->N; i++)
    {
        cp->var[i]=-1;
    }

    /*variables y*/
    r=cp->ind_max_x;

    for(i=0; i<qp->nb_int; i++)
    {
        for (j=0; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->var[(qp->n)*i + j + cp->ind_max_x] =r;
                r++;
            }
    }

    for(i=qp->nb_int; i<cp->ind_max_x; i++)
    {
        for (j=0; j<qp->nb_int; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->var[(qp->n)*qp->nb_int+ (i-qp->nb_int)*qp->nb_int + j +cp->ind_max_x] =r;
                r++;
            }
    }

    /*variables z*/
    cpt=0;
    cpt_bis=0;
    for(i=0; i<qp->nb_int; i++)
    {
        lim=log_base_2(qp->u[i])+1;
        for(j=0; j<qp->n; j++)
        {
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                for(k=0; k<lim; k++)
                {
                    cp->var[cp->ind_max_y +  cpt_bis+j*lim + k+cpt] =r;
                    r++;
                }
        }
        cpt=cpt+(qp->n  - qp->nb_int)*lim;
        cpt_bis=cpt_bis+ (qp->nb_int)*lim;
    }

    /*variables t all created*/
    for(i=cp->ind_max_z; i<cp->ncol; i++)
    {
        cp->var[i]=r;
        r++;
    }

    return cp;
}

/******************************************************************************/
/******************* Structures for branch and bound **************************/
/******************************************************************************/

/******************************************************************************/
/******************************* Create C_MIQCP ********************************/
/******************************************************************************/
C_MIQCP new_c_miqcp()
{
    return (C_MIQCP) malloc(sizeof(struct c_miqcp));
}

void reduce_c_miqcp_obbt(C_MIQCP qp, int num_ineg)
{
    int new_p,new_pq;;
    double * zd;
    double * ze;
    double * zdq;
    double * zeq;
    int i,j,k;

    if (num_ineg -qp->m < qp->p)
    {
        new_p=qp->p-1;
        if(!Zero(new_p))
        {
            zd= alloc_matrix_d(new_p,qp->n);
            ze =alloc_vector_d(new_p);

            for (k=0; k<new_p; k++)
                if (k + qp->m < num_ineg)
                    for (i=0; i<qp->n; i++)
                        zd[ij2k(k,i,qp->n)] = qp->d[ij2k(k,i,qp->n)];
                else if (k + qp->m >= num_ineg)
                    for (i=0; i<qp->n; i++)
                        zd[ij2k(k,i,qp->n)] = qp->d[ij2k(k+1,i,qp->n)];

            for (k=0; k<new_p; k++)
                if (k + qp->m < num_ineg)
                    ze[k] = qp->e[k];
                else if (k + qp->m >= num_ineg)
                    ze[k] = qp->e[k+1];

            qp->p = new_p;
            free(qp->d);
            free(qp->e);
            qp->d=copy_matrix_d(zd,new_p,qp->n);
            qp->e=copy_vector_d(ze,new_p);
            free(zd);
            free(ze);
        }
    }
    else if (num_ineg -qp->m-qp->p-qp->mq < qp->pq)
    {
        new_pq=qp->pq-1;
        if(!Zero(new_pq))
        {
            zdq= alloc_matrix_3d(new_pq,qp->n+1,qp->n+1);
            zeq =alloc_vector_d(new_pq);

            for (k=0; k<new_pq; k++)
                if (k + qp->m +qp->p +qp->mq < num_ineg)
                    for (i=0; i<qp->n+1; i++)
                        for (j=0; j<qp->n+1; j++)
                            zdq[ijk2l(k,i,j,qp->n+1,qp->n+1)] = qp->dq[ijk2l(k,i,j,qp->n+1,qp->n+1)];
                else if (k + qp->m +qp->p +qp->mq >= num_ineg)
                    for (i=0; i<qp->n; i++)
                        for (j=0; j<qp->n+1; j++)
                            zdq[ijk2l(k,i,j,qp->n+1,qp->n+1)] = qp->dq[ijk2l(k+1,i,j,qp->n+1,qp->n+1)];

            for (k=0; k<new_pq; k++)
                if (k + qp->m +qp->p +qp->mq  < num_ineg)
                    zeq[k] = qp->eq[k];
                else if (k + qp->m +qp->p +qp->mq  >= num_ineg)
                    zeq[k] = qp->eq[k+1];

            qp->pq = new_pq;
            free(qp->dq);
            free(qp->eq);
            qp->dq=copy_matrix_3d(zdq,new_pq,qp->n+1,qp->n+1);
            qp->eq=copy_vector_d(zeq,new_pq);
            free(zdq);
            free(zeq);
        }

    }

}


C_MIQCP create_c_miqcp(MIQCP qp, double * beta, double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag)
{
    int i,j,l;

    C_MIQCP cp = new_c_miqcp();

    double * Q_S = alloc_matrix_d(qp->n,qp->n);
    double * sum_r_Q_r = alloc_matrix_d(qp->n,qp->n);

    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            sum_r_Q_r[ij2k(i,j,qp->n)]=0;

    if ((flag !=4) && (flag!=5))
    {
        for(l=0; l<qp->mq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->aq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)]))
                        sum_r_Q_r[ij2k(i,j,qp->n)]= sum_r_Q_r[ij2k(i,j,qp->n)] + alphaq[l]*qp->aq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)];
    }

    for(l=0; l<qp->pq; l++)
        for(i=0; i<qp->n; i++)
            for(j=0; j<qp->n; j++)
                if (!Zero_BB(qp->dq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)]))
                    sum_r_Q_r[ij2k(i,j,qp->n)]= sum_r_Q_r[ij2k(i,j,qp->n)] + alphabisq[l]*qp->dq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)];


    /*Calcul de Q - S */
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            Q_S[ij2k(i,j,qp->n)]=0;
            Q_S[ij2k(i,j,qp->n)] = - beta[ij2k(i,j,qp->n)] -  sum_r_Q_r[ij2k(i,j,qp->n)];
        }

    cp->N=qp->N;
    cp->n=qp->n;
    cp->nb_int=qp->nb_int;
    cp->m=qp->m;
    cp->p=qp->p;
    cp->mq=qp->mq;
    cp->pq=qp->pq;
    cp->cons = qp->cons;
    cp->u = copy_vector_d(qp->u,qp->n + (qp->n+1)*qp->n/2);
    cp->l = copy_vector_d(qp->l,qp->n + (qp->n+1)*qp->n/2);
    cp->lg = copy_vector(qp->lg,qp->n);
    cp->lgc = copy_vector(qp->lgc,qp->n);

    cp->flag_nb_var = flag;

   
    cp->miqp=qp->miqp;
    cp->q = copy_matrix_d(qp->q, qp->n,qp->n);
    cp->c = copy_vector_d(qp->c,qp->n);
    if ( cp->local_sol == NULL)
    {
        cp->local_sol=alloc_vector_d(cp->n);
        cp->local_sol_adm=MAX_SOL_BB;
    }
    else
    {
        cp->local_sol=copy_vector_d(qp->local_sol,qp->n);
        cp->local_sol_adm= qp->sol_adm;
    }

    if(!Zero(qp->m))
    {
        cp->a = copy_matrix_d(qp->a, qp->m,qp->n);
        cp->b = copy_vector_d(qp->b,qp->m);
    }
    else
    {
        cp->a=alloc_matrix_d(1,1);
        cp->b = copy_vector_d(qp->b,qp->m);
    }

    if(!Zero(qp->p))
    {
        cp->d= copy_matrix_d(qp->d, qp->p,qp->n);
        cp->e = copy_vector_d(qp->e,qp->p);
    }
    else
    {
        cp->d=alloc_matrix_d(1,1);
        cp->e = copy_vector_d(qp->e,qp->p);
    }

    if(!Zero(qp->mq))
    {
        cp->aq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }
    else
    {
        cp->aq=alloc_matrix_d(1,1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }

    if(!Zero(qp->pq))
    {
        cp->dq= copy_matrix_3d(qp->dq, qp->pq,qp->n+1,qp->n+1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }
    else
    {
        cp->dq=alloc_matrix_d(1,1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }


    if(!Zero(qp->mq))
    {
        cp->alphaq = copy_vector_d(alphaq,qp->mq);
        if (flag == 3)
        {
            cp->lambda_min_eg = copy_vector_d(lambda_min_eg,qp->mq);
            cp->lambda_max_eg = copy_vector_d(lambda_max_eg,qp->mq);
        }
        else
        {
            cp->lambda_min_eg= alloc_vector_d(cp->mq);
            cp->lambda_max_eg= alloc_vector_d(cp->mq);
            for(i=0; i<cp->mq; i++)
            {
                cp->lambda_min_eg[i] = 0;
                cp->lambda_max_eg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphaq = alloc_vector_d(cp->mq);
        cp->lambda_min_eg= alloc_vector_d(cp->mq);
        cp->lambda_max_eg= alloc_vector_d(cp->mq);
        for(i=0; i<cp->mq; i++)
        {
            cp->alphaq[i]=0;
            cp->lambda_min_eg[i] = 0;
            cp->lambda_max_eg[i] = 0;
        }
    }

    if(!Zero(qp->pq))
    {
        cp->alphabisq =copy_vector_d(alphabisq,qp->pq);
        if (flag == 3)
        {
            cp->lambda_min_ineg = copy_vector_d(lambda_min_ineg,qp->pq);
            cp->lambda_max_ineg = copy_vector_d(lambda_max_ineg,qp->pq);
        }
        else
        {
            cp->lambda_min_ineg= alloc_vector_d(cp->pq);
            cp->lambda_max_ineg= alloc_vector_d(cp->pq);
            for(i=0; i<cp->pq; i++)
            {
                cp->lambda_min_ineg[i] = 0;
                cp->lambda_max_ineg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphabisq = alloc_vector_d(cp->pq);
        cp->lambda_min_ineg= alloc_vector_d(cp->pq);
        cp->lambda_max_ineg= alloc_vector_d(cp->pq);
        for(i=0; i<cp->pq; i++)
        {
            cp->alphabisq[i]=0;
            cp->lambda_min_ineg[i] = 0;
            cp->lambda_max_ineg[i] = 0;
        }
    }

    cp->beta = copy_matrix_d(beta,qp->n,qp->n);

    int cont = (cp->n+1)*(cp->n)/2;

    int cont_bis = (cp->n-1)*(cp->n)/2;

    /*Computation of the initial number of constraints */
    /* Ax=b */
    cp->ind_eg = qp->m;
    /* Dx = e */
    cp->ind_ineg = cp->ind_eg + qp->p;
    /*x^TAqx = bq*/
    if ((flag ==4) || (flag ==5))
        cp->ind_eg_q = cp->ind_ineg;
    else
        cp->ind_eg_q = cp->ind_ineg + qp->mq;

    /*x^TDqx = eq*/
    cp->ind_ineg_q = cp->ind_eg_q + qp->pq;
    /* y_ij <= u_jx_i + l_ix_j - u_jl_j*/
    if (flag ==4)
        cp->ind_y_1=cp->ind_ineg_q + cont_bis;
    else
        cp->ind_y_1=cp->ind_ineg_q + cont;
    /* y_ij <= u_ix_j + l_jx_i - u_il_j*/
    cp->ind_y_2=cp->ind_y_1 + cont_bis;
    /* y_ij >= u_ix_j + u_jx_i - u_iu_j */
    if (flag ==4)
        cp->ind_y_3=cp->ind_y_2 +cont_bis;
    else
        cp->ind_y_3=cp->ind_y_2 +cont;
    /* y_ij >= l_jx_i + l_ix_j - l_il_j */
    if (flag ==4)
        cp->ind_y_4= cp->ind_y_3 + cont_bis;
    else
        cp->ind_y_4= cp->ind_y_3 + cont;
    /* y_ii >= x_i */
    cp->nrow= cp->ind_y_4 + qp->nb_int;


    int nb_init_cont;
    if (flag == 3)
        nb_init_cont = cp->ind_ineg;
    else
        nb_init_cont = cp->ind_ineg_q ;

    /* initialisation of the new number of constraints*/
    /* Ax=b */
    cp->nb_eg =qp->m;
    /* Dx = e */
    cp->nb_ineg = qp->p;
    /*x^TAqx = bq*/
    cp->nb_eg_q = qp->mq;
    /*x^TDqx = eq*/
    cp->nb_ineg_q = qp->pq;
    /* y_ij <= u_jx_i + l_ix_j - u_jl_j*/
    cp->nb_y_1=0;
    /* y_ij <= u_ix_j + l_jx_i - u_il_j*/
    cp->nb_y_2=0;
    /* y_ij >= u_ix_j + u_jx_i - u_iu_j */
    cp->nb_y_3=0;
    /* y_ij >= l_jx_i + l_ix_j - l_il_j */
    cp->nb_y_4= 0;
    /* y_ii >= x_i */
    cp->nb_y_5= 0;

    cp->nb_var_y = alloc_matrix(qp->n,qp->n);

    for(i=0; i<qp->n; i++)
        for(j=i; j<qp->n; j++)
        {
            /*initialisation*/
            cp->nb_var_y[ij2k(i,j,qp->n)] =0;
            if (Positive_BB(Q_S[ij2k(i,j,qp->n)]))
            {
                cp->nb_var_y[ij2k(i,j,qp->n)] =1;
                cp->nb_var_y[ij2k(j,i,qp->n)] =1;
            }
            if (Negative_BB(Q_S[ij2k(i,j,qp->n)]))
            {
                cp->nb_var_y[ij2k(i,j,qp->n)] =-1;
                cp->nb_var_y[ij2k(j,i,qp->n)] =-1;
            }
        }

    cp->nb_y = nb_non_zero_matrix_sup_i(cp->nb_var_y, qp->n, qp->n);

    /*computation of the new number of constraints */
    for(i=0; i<qp->n; i++)
    {
        for (j=i; j<qp->n; j++)
        {
            if ((flag == 2) || (flag ==3) || (flag ==5))
            {
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) )
                {
                    if (i==j)
                    {
                        cp->nb_y_1++;
                        cp->nb_y_3++;
                        cp->nb_y_4++;
                    }
                    else
                    {
                        cp->nb_y_1++;
                        cp->nb_y_2++;

                        cp->nb_y_3++;
                        cp->nb_y_4++;
                    }
                    if ((i==j) && (i<cp->nb_int) )
                        cp->nb_y_5++;
                }
            }

            if ((flag == 1) || (flag==4))
            {
                if (Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) )
                {
                    if ((i==j) && (flag==1))
                        cp->nb_y_1++;
                    else
                    {
                        if (i!=j)
                        {
                            cp->nb_y_1++;
                            cp->nb_y_2++;
                        }
                    }
                }

                if (Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) )
                {
                    if (((i!=j) && (flag ==4)) || (flag==1))
                    {
                        cp->nb_y_3++;
                        cp->nb_y_4++;
                    }
                }
                if ((flag==1) && (i==j) && (i<cp->nb_int) &&  Positive_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
                    cp->nb_y_5++;
                if ((flag==4) && (i==j)  &&  !Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
                    cp->nb_y_5++;
            }
        }
    }

    if ((flag ==4) || (flag==5))
        cp->nb_row =  cp->nb_eg +cp->nb_ineg +cp->nb_ineg_q + cp->nb_y_1+  cp->nb_y_2 + cp->nb_y_3+ cp->nb_y_4+ cp->nb_y_5;

    else
        cp->nb_row = cp->nb_eg +cp->nb_ineg + cp->nb_eg_q  +cp->nb_ineg_q +   cp->nb_y_1+  cp->nb_y_2 + cp->nb_y_3+ cp->nb_y_4+ cp->nb_y_5;

    cp->cont = alloc_vector(cp->nrow);
    for(i=0; i<cp->nrow; i++)
        cp->cont[i]=-1;

    int r;
    r=nb_init_cont;

    for(i=0; i<r; i++)
        cp->cont[i]=i;

    /* y_ij <= l_ix_j + u_jx_i - l_iu_j*/
    for(i=0; i<qp->n; i++)
    {
        for (j=i; j<qp->n; j++)
        {
            if ((flag==2) || (flag == 3)|| (flag == 5) )
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_ineg_q + i* (2*(qp->n ) -i +1)/2 + j-i] =  r;
                    r++;
                }
            if (flag==1)
            {
                if (Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_ineg_q + i* (2*(qp->n ) -i +1)/2 + j-i] =  r;
                    r++;
                }
            }
            if (flag==4)
            {
                if (Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && (j>i))
                {
                    cp->cont[cp->ind_ineg_q + i* (2*(qp->n ) -i -1)/2 + j-i-1] =  r;
                    r++;
                }
            }
        }
    }

    /* y_ij <= u_ix_j + l_jx_i - u_il_j*/
    r= nb_init_cont+ cp->nb_y_1;

    for(i=0; i<qp->n; i++)
    {
        for (j=i+1; j<qp->n; j++)
        {
            if ((flag==2) || (flag == 3)|| (flag == 5))
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_1 + i* (2*(qp->n ) - i -1)/2 + j-i-1]=  r;
                    r++;
                }
            if ((flag==1) || (flag ==4))
            {
                if (Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_1 + i* (2*(qp->n ) - i -1)/2 + j-i-1]=  r;
                    r++;
                }
            }
        }
    }

    /* y_ij >= u_ix_j + u_jx_i - u_iu_j*/
    r= nb_init_cont+ cp->nb_y_1+cp->nb_y_2;

    for(i=0; i<qp->n; i++)
    {
        for (j=i; j<qp->n; j++)
        {
            if ((flag==2) || (flag == 3)|| (flag == 5))
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_2 + i* (2*(qp->n ) -i +1)/2 + j-i] = r;
                    r++;
                }
            if (flag==1)
            {
                if (Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_2 + i* (2*(qp->n ) -i +1)/2 + j-i] = r;
                    r++;
                }
            }
            if (flag==4)
            {
                if (Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && (j>i))
                {
                    cp->cont[cp->ind_y_2 + i* (2*(qp->n ) -i -1)/2 + j-i-1] = r;
                    r++;
                }
            }

        }
    }

    /* y_ij >= l_ix_j + l_jx_i - l_il_j*/
    r= nb_init_cont+ cp->nb_y_1+cp->nb_y_2+cp->nb_y_3;

    for(i=0; i<qp->n; i++)
    {
        for (j=i; j<qp->n; j++)
        {
            if ((flag==2) || (flag == 3)|| (flag == 5))
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_3 + i* (2*(qp->n ) - i +1)/2 + j-i]= r;
                    r++;
                }
            if (flag==1)
            {
                if (Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    cp->cont[cp->ind_y_3 + i* (2*(qp->n ) - i +1)/2 + j-i]= r;
                    r++;
                }
            }
            if (flag==4)
            {
                if (Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && (j>i))
                {
                    cp->cont[cp->ind_y_3 + i* (2*(qp->n ) - i -1)/2 + j-i-1]= r;
                    r++;
                }
            }
        }
    }


    /* y_ii >= x_i or y_ii = x_i (case binary)*/
    r= nb_init_cont+ cp->nb_y_1+cp->nb_y_2+cp->nb_y_3+cp->nb_y_4;

    for(i=0; i<qp->nb_int; i++)
    {
        {
            if ((flag==2) || (flag == 3)|| (flag == 5))
                if (!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
                {
                    cp->cont[cp->ind_y_4 + i]= r;
                    r++;
                }
            if (flag==1)
            {
                if (Positive_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
                {
                    cp->cont[cp->ind_y_4 + i]= r;
                    r++;
                }
            }
            if (flag ==4)
            {
                if (!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]) && (j>i))
                {
                    cp->cont[cp->ind_y_4 + i]= r;
                    r++;
                }
            }
        }
    }

    /* number of initial variables */

    cp->ind_max_int =qp->nb_int;
    cp->ind_max_x =qp->n ;
    cp->ncol =cp->ind_max_x + qp->n*qp->n ;


    /* real number of variables*/
    if ((flag ==4)|| (flag == 5) || (flag==2))
        cp->nb_col =cp->ind_max_x + cp->nb_y+1;
    else
        cp->nb_col =cp->ind_max_x + cp->nb_y;

    /*nb occurence of each x in constraints */
    cp->x_cont=alloc_vector(cp->ind_max_x);
    for(i=0; i<cp->ind_max_x; i++)
        cp->x_cont[i]=0;

    for(i=0; i<cp->ind_max_x; i++)
    {
        for (j=i; j<cp->ind_max_x; j++)
        {
            if ((flag==2) || (flag == 3)|| (flag == 5))
            {
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    if ((i==j) && (i<cp->nb_int))
                    {
                        cp->diag=1;
                        cp->x_cont[i] = cp->x_cont[i]+4;
                    }
                    else if (i==j)
                    {
                        cp->diag=1;
                        cp->x_cont[i] = cp->x_cont[i]+3;
                    }
                    else
                    {
                        cp->diag=0;
                        cp->x_cont[i] = cp->x_cont[i]+4;
                        cp->x_cont[j] = cp->x_cont[j]+4;
                    }
                }
            }
            if (flag ==1)
            {
                if (Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    if (i==j)
                    {
                        cp->diag=1;
                        cp->x_cont[i] = cp->x_cont[i]+1;
                    }
                    else
                    {
                        cp->diag=0;
                        cp->x_cont[i] = cp->x_cont[i]+2;
                        cp->x_cont[j] = cp->x_cont[j]+2;
                    }
                }

                if (Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    if ((i==j) && (i<cp->nb_int))
                    {
                        cp->diag=1;
                        cp->x_cont[i] = cp->x_cont[i]+3;
                    }
                    else if (i==j)
                    {
                        cp->diag=1;
                        cp->x_cont[i] = cp->x_cont[i]+2;
                    }
                    else
                    {
                        cp->diag=0;
                        cp->x_cont[i] = cp->x_cont[i]+2;
                        cp->x_cont[j] = cp->x_cont[j]+2;
                    }
                }
            }
            if (flag ==4)
            {
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                {
                    if (i==j)
                    {
                        cp->diag=1;
                        cp->x_cont[i] = cp->x_cont[i]+1;
                    }
                    else
                    {
                        if (Negative(cp->nb_var_y[ij2k(i,j,qp->n)]))
                        {
                            cp->diag=0;
                            cp->x_cont[i] = cp->x_cont[i]+2;
                            cp->x_cont[j] = cp->x_cont[j]+2;
                        }

                        if (Positive(cp->nb_var_y[ij2k(i,j,qp->n)]))
                        {
                            cp->diag=0;
                            cp->x_cont[i] = cp->x_cont[i]+2;
                            cp->x_cont[j] = cp->x_cont[j]+2;
                        }
                    }
                }
            }
        }

    }

    /*table with corresponding variables*/
    cp->var = alloc_vector(cp->ncol);

    for(i=0; i<cp->ind_max_x; i++)
    {
        cp->var[i]=i;
    }

    for(i; i<cp->ncol; i++)
    {
        cp->var[i]=-1;
    }

    r=cp->ind_max_x;

    for(i=0; i<qp->n; i++)
    {
        for (j=i; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                cp->var[(qp->n)*i + j +cp->ind_max_x] =r;
                r++;
            }
    }

    return cp;
}

C_MIQCP create_triangular_miqcp(MIQCP qp, double * beta, double * beta_1,double * beta_2,double * beta_3,double * beta_4,double * beta_c,double * delta, double * delta_c, double delta_l, double * delta_1, double * delta_2, double * delta_3, double * delta_4,double * delta_5, double * delta_6, double * delta_7, double * delta_8,double * delta_9, double * delta_10, double * delta_11, double * delta_12,double alpha, double alphabis, double *alphaq, double * alphabisq, double* lambda_min_ineg, double* lambda_max_ineg, double* lambda_min_eg, double* lambda_max_eg, int flag )
{
    int i,j,k,l,lim;
    int test =0;

    C_MIQCP cp = new_c_miqcp();
    cp->N=qp->N;
    cp->n=qp->n;

    cp->nb_int=qp->nb_int;
    cp->m=qp->m;
    cp->p=qp->p;
    cp->mq=qp->mq;
    cp->pq=qp->pq;
    cp->cons=qp->cons;
    cp->u = copy_vector_d(qp->u,qp->n);
    cp->l = copy_vector_d(qp->l,qp->n);
    cp->lg = copy_vector(qp->lg,qp->n);
    cp->lgc = copy_vector(qp->lgc,qp->n);

    cp->flag_nb_var = flag;

    cp->miqp=qp->miqp;
    cp->q = copy_matrix_d(qp->q, qp->n,qp->n);
    cp->c = copy_vector_d(qp->c,qp->n);
    if ( cp->local_sol == NULL)
    {
        cp->local_sol=alloc_vector_d(cp->n);
        cp->local_sol_adm=MAX_SOL_BB;
    }
    else
    {
        cp->local_sol=copy_vector_d(qp->local_sol,qp->n);
        cp->local_sol_adm= qp->sol_adm;
    }
    if(!Zero(qp->m))
    {
        cp->a = copy_matrix_d(qp->a, qp->m,qp->n);
        cp->b = copy_vector_d(qp->b,qp->m);
    }
    else
    {
        cp->a=alloc_matrix_d(1,1);
        cp->b = copy_vector_d(qp->b,qp->m);
    }

    if(!Zero(qp->p))
    {
        cp->d= copy_matrix_d(qp->d, qp->p,qp->n);
        cp->e = copy_vector_d(qp->e,qp->p);
    }
    else
    {
        cp->d=alloc_matrix_d(1,1);
        cp->e = copy_vector_d(qp->e,qp->p);
    }

    if(!Zero(qp->mq))
    {
        cp->aq = copy_matrix_3d(qp->aq, qp->mq,qp->n+1,qp->n+1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }
    else
    {
        cp->aq=alloc_matrix_d(1,1);
        cp->bq = copy_vector_d(qp->bq,qp->mq);
    }

    if(!Zero(qp->pq))
    {
        cp->dq= copy_matrix_3d(qp->dq, qp->pq,qp->n+1,qp->n+1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }
    else
    {
        cp->dq=alloc_matrix_d(1,1);
        cp->eq = copy_vector_d(qp->eq,qp->pq);
    }

    if(!Zero(qp->m))
        cp->alpha =alpha;
    else
        cp->alpha=0;

    if(!Zero(qp->p))
        cp->alphabis =alphabis;
    else
        cp->alphabis=0;

    if(!Zero(qp->mq))
    {
        cp->alphaq = copy_vector_d(alphaq,qp->mq);
        if (flag == 3)
        {
            cp->lambda_min_eg = copy_vector_d(lambda_min_eg,qp->mq);
            cp->lambda_max_eg = copy_vector_d(lambda_max_eg,qp->mq);
        }
        else
        {
            cp->lambda_min_eg= alloc_vector_d(cp->mq);
            cp->lambda_max_eg= alloc_vector_d(cp->mq);
            for(i=0; i<cp->mq; i++)
            {
                cp->lambda_min_eg[i] = 0;
                cp->lambda_max_eg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphaq = alloc_vector_d(cp->mq);
        cp->lambda_min_eg= alloc_vector_d(cp->mq);
        cp->lambda_max_eg= alloc_vector_d(cp->mq);
        for(i=0; i<cp->mq; i++)
        {
            cp->alphaq[i]=0;
            cp->lambda_min_eg[i] = 0;
            cp->lambda_max_eg[i] = 0;
        }
    }

    if(!Zero(qp->pq))
    {
        cp->alphabisq =copy_vector_d(alphabisq,qp->pq);
        if (flag == 3)
        {
            cp->lambda_min_ineg = copy_vector_d(lambda_min_ineg,qp->pq);
            cp->lambda_max_ineg = copy_vector_d(lambda_max_ineg,qp->pq);
        }
        else
        {
            cp->lambda_min_ineg= alloc_vector_d(cp->pq);
            cp->lambda_max_ineg= alloc_vector_d(cp->pq);
            for(i=0; i<cp->pq; i++)
            {
                cp->lambda_min_ineg[i] = 0;
                cp->lambda_max_ineg[i] = 0;
            }
        }
    }
    else
    {
        cp->alphabisq = alloc_vector_d(cp->pq);
        cp->lambda_min_ineg= alloc_vector_d(cp->pq);
        cp->lambda_max_ineg= alloc_vector_d(cp->pq);
        for(i=0; i<cp->pq; i++)
        {
            cp->alphabisq[i]=0;
            cp->lambda_min_ineg[i] = 0;
            cp->lambda_max_ineg[i] = 0;
        }
    }

    cp->beta = copy_matrix_d(beta,qp->n,qp->n);

    cp->delta = copy_matrix_d(delta,qp->n,qp->n);
    if (delta_c!=NULL)
        cp->delta_c = copy_vector_d(delta_c,qp->n);

    cp->delta_l=delta_l;
    cp->delta_1 = copy_matrix_3d(delta_1,cp->n,cp->n,cp->n);
    cp->delta_2 = copy_matrix_3d(delta_2,cp->n,cp->n,cp->n);
    cp->delta_3 = copy_matrix_3d(delta_3,cp->n,cp->n,cp->n);
    cp->delta_4 = copy_matrix_3d(delta_4,cp->n,cp->n,cp->n);
    cp->delta_5 = copy_matrix_3d(delta_5,cp->n,cp->n,cp->n);
    cp->delta_6 = copy_matrix_3d(delta_6,cp->n,cp->n,cp->n);
    cp->delta_7 = copy_matrix_3d(delta_7,cp->n,cp->n,cp->n);
    cp->delta_8 = copy_matrix_3d(delta_8,cp->n,cp->n,cp->n);
    cp->delta_9 = copy_matrix_3d(delta_9,cp->n,cp->n,cp->n);
    cp->delta_10 = copy_matrix_3d(delta_10,cp->n,cp->n,cp->n);
    cp->delta_11 = copy_matrix_3d(delta_11,cp->n,cp->n,cp->n);
    cp->delta_12 = copy_matrix_3d(delta_12,cp->n,cp->n,cp->n);

    cp->beta_c = copy_vector_d(beta_c,cp->n);
    cp->beta_1 = copy_matrix_d(beta_1,cp->n,cp->n);
    cp->beta_2 = copy_matrix_d(beta_2,cp->n,cp->n);
    cp->beta_3 = copy_matrix_d(beta_3,cp->n,cp->n);
    cp->beta_4 = copy_matrix_d(beta_4,cp->n,cp->n);

    int nb_cont_cor = (qp->n+1)*qp->n/2;
    int nb_cont_triangle =(qp->n)*(qp->n-1)*(qp->n-2)/6;

    /*Computation of the initial number of constraints */
    /* Ax=b */
    cp->ind_eg = qp->m;
    /* Dx = e */
    cp->ind_ineg = cp->ind_eg + qp->p;
    /*Aq,y + Aq_0 x = bq*/
    cp->ind_eg_q = cp->ind_ineg + qp->mq;
    /*Dq,y + Dq_0 x <= eq*/
    cp->ind_ineg_q = cp->ind_eg_q + qp->pq;
    /*y_ij - u_ix_j - l_jx_i + u_il_j<= 0 */
    if (flag==1)
        cp->ind_y_1=cp->ind_ineg_q +nb_cont_cor;
    else
        /*initially no quadratic constraints, new created quad. constraints will not be in the ref. problem*/
        cp->ind_y_1=cp->ind_ineg +nb_cont_cor;


    /*y_ij  - u_jx_i -l_ix_j + u_jl_i<= 0*/
    cp->ind_y_2=cp->ind_y_1 +nb_cont_cor;
    /*-y_ij + u_jx_i + u_ix_j - u_iu_j <= 0*/
    cp->ind_y_3=cp->ind_y_2 +nb_cont_cor;
    /* - y_ij+ l_jx_i + l_ix_j - l_il_j <= 0 */
    cp->ind_y_4=cp->ind_y_3+nb_cont_cor;
    /* (l_k - u_k)x_ix_j + (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_kx_j + (u_ju_k - l_jl_k)x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
    cp->ind_y_5=cp->ind_y_4+nb_cont_triangle;
    /*  (l_k - u_k)x_ix_j - u_jx_ix_k +(l_i - u_i)x_jx_k + (u_iu_k - l_il_k)x_j + u_ju_k x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
    cp->ind_y_6=cp->ind_y_5+nb_cont_triangle;
    /*- u_kx_ix_j + (l_j- u_j)x_ix_k +(l_i - u_i)x_jx_k + u_iu_kx_j + u_ju_k x_i + (u_i_u_j - l_il_j)x_k - u_iu_ju_k <= 0*/
    cp->ind_y_7=cp->ind_y_6+nb_cont_triangle;
    /*(l_k - u_k)x_ix_j + (u_j - l_j)x_ix_k + u_ix_jx_k - u_il_kx_j + (l_ju_k - u_jl_k) x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
    cp->ind_y_8=cp->ind_y_7+nb_cont_triangle;
    /*(l_k - u_k)x_ix_j + u_jx_ix_k + (u_i - l_i)x_jx_k + (l_iu_k- u_il_k) x_j  - u_jl_k x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
    cp->ind_y_9=cp->ind_y_8+nb_cont_triangle;
    /*(u_k - l_k)x_ix_j + (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_k x_j  +( u_jl_k - l_ju_k)x_i - u_il_jx_k + u_il_ju_k <= 0*/
    cp->ind_y_10=cp->ind_y_9+nb_cont_triangle;
    /*(l_j - u_j) x_ix_k + u_kx_ix_j + (u_i - l_i)x_jx_k - u_iu_k x_j  - l_ju_kx_i +(u_jl_i - u_il_j) x_k + u_il_ju_k <= 0*/
    cp->ind_y_11=cp->ind_y_10+nb_cont_triangle;
    /* (u_k - l_k)x_ix_j + u_jx_ix_k + (l_i - u_i)x_jx_k + (u_il_k - l_iu_k) x_j  - u_ju_kx_i - l_iu_jx_k + l_iu_ju_k <= 0*/
    cp->ind_y_12 = cp->ind_y_11+nb_cont_triangle;
    /* u_k x_ix_j + (u_j - l_j) x_ix_k + (l_i - u_i)x_jx_k - l_iu_k x_j  - u_ju_kx_i + (u_il_j- l_iu_j)x_k + l_iu_ju_k <= 0*/
    cp->ind_y_13 = cp->ind_y_12+nb_cont_triangle;
    /*(u_k - l_k) x_ix_j + (u_j - l_j) x_ix_k  - u_ix_jx_k + u_il_jx_k + u_il_k x_j+ (l_jl_k - u_iu_k)x_i  - u_il_jl_k <= 0*/
    cp->ind_y_14 = cp->ind_y_13+nb_cont_triangle;
    /*(u_k - l_k) x_ix_j - u_j x_ix_k  +(u_i - l_i)x_jx_k + l_iu_jx_k + (l_il_k - u_iu_k) x_j+ u_jl_kx_i - l_iu_jl_k <= 0*/
    cp->ind_y_15 = cp->ind_y_14+nb_cont_triangle;
    /* -u_k x_ix_j +(u_j -l_j) x_ix_k  +(u_i - l_i)x_jx_k + (l_il_j - u_iu_j) x_k + l_iu_k  x_j+ l_ju_kx_i - l_il_ju_k <= 0*/
    cp->ind_y_16 = cp->ind_y_15+nb_cont_triangle;
    /*y_ii >= x_i */
    if (flag ==1)
        cp->nrow=cp->ind_y_16 + cp->nb_int;
    else //if flag==2 unconstrained
        cp->nrow=cp->ind_y_16;

    /* ICI*********************************/
    /* initialisation of the new number of constraints*/
    /* Ax=b */
    cp->nb_eg =qp->m;
    /* Dx = e */
    cp->nb_ineg = qp->p;
    /*Aq,y + Aq_0 x = bq*/
    cp->nb_eg_q =  qp->mq;
    /*Dq,y + Dq_0 x <= eq*/
    cp->nb_ineg_q =  qp->pq;
    /*y_ij - u_ix_j - l_jx_i + u_il_j<= 0 */
    cp->nb_y_1=0;
    /*y_ij  - u_jx_i -l_ix_j + u_jl_i<= 0*/
    cp->nb_y_2=0;
    /*-y_ij + u_jx_i + u_ix_j - u_iu_j <= 0*/
    cp->nb_y_3=0;
    /* - y_ij+ l_jx_i + l_ix_j - l_il_j <= 0 */
    cp->nb_y_4=0;
    /* (l_k - u_k)x_ix_j + (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_kx_j + (u_ju_k - l_jl_k)x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
    cp->nb_y_5=0;
    /*  (l_k - u_k)x_ix_j - u_jx_ix_k +(l_i - u_i)x_jx_k + (u_iu_k - l_il_k)x_j + u_ju_k x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
    cp->nb_y_6=0;
    /*- u_kx_ix_j + (l_j- u_j)x_ix_k +(l_i - u_i)x_jx_k + u_iu_kx_j + u_ju_k x_i + (u_i_u_j - l_il_j)x_k - u_iu_ju_k <= 0*/
    cp->nb_y_7=0;
    /*(l_k - u_k)x_ix_j + (u_j - l_j)x_ix_k + u_ix_jx_k - u_il_kx_j + (l_ju_k - u_jl_k) x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
    cp->nb_y_8=0;
    /*(l_k - u_k)x_ix_j + u_jx_ix_k + (u_i - l_i)x_jx_k + (l_iu_k- u_il_k) x_j  - u_jl_k x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
    cp->nb_y_9=0;
    /*(u_k - l_k)x_ix_j + (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_k x_j  +( u_jl_k - l_ju_k)x_i - u_il_jx_k + u_il_ju_k <= 0*/
    cp->nb_y_10=0;
    /*(l_j - u_j) x_ix_k + u_kx_ix_j + (u_i - l_i)x_jx_k - u_iu_k x_j  - l_ju_kx_i +(u_jl_i - u_il_j) x_k + u_il_ju_k <= 0*/
    cp->nb_y_11=0;
    /* (u_k - l_k)x_ix_j + u_jx_ix_k + (l_i - u_i)x_jx_k + (u_il_k - l_iu_k) x_j  - u_ju_kx_i - l_iu_jx_k + l_iu_ju_k <= 0*/
    cp->nb_y_12 = 0;
    /* u_k x_ix_j + (u_j - l_j) x_ix_k + (l_i - u_i)x_jx_k - l_iu_k x_j  - u_ju_kx_i + (u_il_j- l_iu_j)x_k + l_iu_ju_k <= 0*/
    cp->nb_y_13 = 0;
    /*(u_k - l_k) x_ix_j + (u_j - l_j) x_ix_k  - u_ix_jx_k + u_il_jx_k + u_il_k x_j+ (l_jl_k - u_iu_k)x_i  - u_il_jl_k <= 0*/
    cp->nb_y_14 = 0;
    /*(u_k - l_k) x_ix_j - u_j x_ix_k  +(u_i - l_i)x_jx_k + l_iu_jx_k + (l_il_k - u_iu_k) x_j+ u_jl_kx_i - l_iu_jl_k <= 0*/
    cp->nb_y_15 = 0;
    /* -u_k x_ix_j +(u_j -l_j) x_ix_k  +(u_i - l_i)x_jx_k + (l_il_j - u_iu_j) x_k + l_iu_k  x_j+ l_ju_kx_i - l_il_ju_k <= 0*/
    cp->nb_y_16 = 0;
    /*y_ii >= x_i */
    cp->nb_y_17=0;

    /* real number of variables y*/
    cp->nb_var_y = alloc_matrix(qp->n,qp->n);

    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            /*initialisation*/
            if (flag==1 )
            {
                cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                if (!Zero_BB(beta[ij2k(i,j,qp->n)]))
                    cp->nb_var_y[ij2k(i,j,qp->n)] =1;
            }

            else
            {
                if (flag==2 )
                {
                    /*if the problem is unconstrained*/
                    cp->nb_var_y[ij2k(i,j,qp->n)] =0;
                    if (Positive_BB((beta[ij2k(i,j,qp->n)]+ delta[ij2k(i,j,qp->n) ])))
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;
                    if (Negative_BB((beta[ij2k(i,j,qp->n)]+ delta[ij2k(i,j,qp->n) ])))
                        cp->nb_var_y[ij2k(i,j,qp->n)] =-1;
                }
            }
        }

    if (flag ==1)
    {
        /*initial or created quadratic constraints + triangular*/
        for(l=0; l<qp->mq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->aq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)])) /*&& ((i < cp->nb_int) || (j < cp->nb_int))*/
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;

        for(l=0; l<qp->pq; l++)
            for(i=0; i<qp->n; i++)
                for(j=0; j<qp->n; j++)
                    if (!Zero_BB(qp->dq[ijk2l(l,i+1,j+1,qp->n+1,qp->n+1)])) /*&& ((i < cp->nb_int) || (j < cp->nb_int))*/
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;


        for(l=0; l<qp->n; l++)
            for(i=l+1; i<qp->n; i++)
                for(j=i+1; j<qp->n; j++)
                    if ( (!Zero_BB(cp->delta_1[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_2[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_3[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_4[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_5[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_6[ijk2l(l,i,j,qp->n,qp->n)])) ||  (!Zero_BB(cp->delta_7[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_8[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_9[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_10[ijk2l(l,i,j,qp->n,qp->n)])) ||  (!Zero_BB(cp->delta_11[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_12[ijk2l(l,i,j,qp->n,qp->n)])))
                    {

                        cp->nb_var_y[ij2k(l,i,qp->n)] =1;
                        cp->nb_var_y[ij2k(l,j,qp->n)] =1;
                        cp->nb_var_y[ij2k(i,j,qp->n)] =1;
                    }

    }
    else
    {
        //unconstrained
        for(l=0; l<qp->n; l++)
            for(i=l+1; i<qp->n; i++)
                for(j=i+1; j<qp->n; j++)
                    if ( (!Zero_BB(cp->delta_1[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_2[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_3[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_4[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_5[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_6[ijk2l(l,i,j,qp->n,qp->n)])) ||  (!Zero_BB(cp->delta_7[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_8[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_9[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_10[ijk2l(l,i,j,qp->n,qp->n)])) ||  (!Zero_BB(cp->delta_11[ijk2l(l,i,j,qp->n,qp->n)])) || (!Zero_BB(cp->delta_12[ijk2l(l,i,j,qp->n,qp->n)])))
                    {
                        if (cp->nb_var_y[ij2k(l,i,qp->n)] == 0)
                            cp->nb_var_y[ij2k(l,i,qp->n)] =1;
                        if (cp->nb_var_y[ij2k(l,j,qp->n)] == 0)
                            cp->nb_var_y[ij2k(l,j,qp->n)] =1;
                        if (cp->nb_var_y[ij2k(i,j,qp->n)] == 0)
                            cp->nb_var_y[ij2k(i,j,qp->n)] =1;

                    }
    }

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            cp->nb_var_y[ij2k(j,i,qp->n)] = cp->nb_var_y[ij2k(i,j,qp->n)];




    cp->nb_y = nb_non_zero_matrix_sup_i(cp->nb_var_y,qp->n,qp->n);

    /*computation of the new number of constraints */
    for(i=0; i<qp->n; i++)
        for (j=i; j<qp->n; j++)
        {
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if (flag ==1 || flag ==2)
                {
                    if (flag==1)
                    {
                        cp->nb_y_1++;
                        cp->nb_y_2++;
                        cp->nb_y_3++;
                        cp->nb_y_4++;
                    }
                    if (flag == 2) // unconstrained
                    {
                        if (Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                        {
                            cp->nb_y_1++;
                            cp->nb_y_2++;
                        }
                        if  (Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                        {
                            cp->nb_y_3++;
                            cp->nb_y_4++;
                        }
                    }
                    for (k=j+1; k<qp->n; k++)
                    {
                        if (!Zero_BB(cp->nb_var_y[ij2k(i,k,cp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,cp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,j,cp->n)]) && ((i!=j) && (i!=k) && (j!=k)))
                        {
                            if(!Zero_BB(cp->delta_1[ijk2l(i,j,k, cp->n,cp->n)]))
                                cp->nb_y_5++;

                            if(!Zero_BB(cp->delta_2[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_6++;

                            if(!Zero_BB(cp->delta_3[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_7++;

                            if (!Zero_BB(cp->delta_4[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_8++;

                            if(!Zero_BB(cp->delta_5[ijk2l(i,j,k, cp->n,cp->n)]))
                                cp->nb_y_9++;

                            if(!Zero_BB(cp->delta_6[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_10++;

                            if(!Zero_BB(cp->delta_7[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_11++;

                            if (!Zero_BB(cp->delta_8[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_12++;

                            if(!Zero_BB(cp->delta_9[ijk2l(i,j,k, cp->n,cp->n)]))
                                cp->nb_y_13++;

                            if(!Zero_BB(cp->delta_10[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_14++;

                            if(!Zero_BB(cp->delta_11[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_15++;

                            if (!Zero_BB(cp->delta_12[ijk2l(i,j,k,cp->n,cp->n)]))
                                cp->nb_y_16++;

                        }
                    }
                    if (flag==1)
                        if (i==j && i< cp->nb_int)
                            cp->nb_y_17++;
                }

            }
        }

    /* if (flag==3) //for branch and cut*/
    /*   { */
    /*     cp->nb_cont =cp->m + cp->p +cp->mq + cp->pq +cp->nb_y_1+ cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4 ; */
    /*     cp->nb_cut = cp->nb_y_5 + cp->nb_y_6  +cp->nb_y_7+ cp->nb_y_8+ cp->nb_y_9+ cp->nb_y_10  +cp->nb_y_11+ cp->nb_y_12+ cp->nb_y_13+ cp->nb_y_14  +cp->nb_y_15+ cp->nb_y_16+ cp->nb_y_17; */
    /*   } */

    cp->nb_row =cp->m + cp->p +cp->mq + cp->pq +cp->nb_y_1+ cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4 + cp->nb_y_5+ cp->nb_y_6  +cp->nb_y_7+ cp->nb_y_8+ cp->nb_y_9+ cp->nb_y_10  +cp->nb_y_11+ cp->nb_y_12+ cp->nb_y_13+ cp->nb_y_14  +cp->nb_y_15+ cp->nb_y_16+ cp->nb_y_17;



    cp->cont = alloc_vector(cp->nrow);
    for(i=0; i<cp->nrow; i++)
        cp->cont[i]=-1;

    /* number of initial variables */

    cp->ind_max_int =qp->nb_int;
    cp->ind_max_x =qp->n;
    cp->ncol = cp->ind_max_x + qp->n*qp->n;

    int r;

    if (flag ==1)
        r=cp->ind_ineg_q;
    else // if flag == 2 unconstrained
        r=cp->ind_ineg;

    /* initial constraints*/
    for(i=0; i<r; i++)
        cp->cont[i]=i;


    /*y_ij - u_ix_j - l_jx_i + u_il_j<= 0 */
    for(i=0; i<qp->n; i++)
        for(j=i; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if (flag ==1)
                {
                    cp->cont[cp->ind_ineg_q + i*(2*(qp->n) -i +1)/2 + j-i] = r;
                    r++;
                }
                else// if flag == 2 unconstrained
                    if(Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    {
                        cp->cont[cp->ind_ineg_q + i*(2*(qp->n) -i+1)/2 + j-i] = r;
                        r++;
                    }
            }

    /*y_ij  - u_jx_i -l_ix_j + u_jl_i<= 0*/
    if (flag ==1)
        r= cp->ind_ineg_q + cp->nb_y_1 ;
    else
        r= cp->ind_ineg + cp->nb_y_1 ;
    for(i=0; i<qp->n; i++)
        for(j=i; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if (flag ==1)
                {
                    cp->cont[cp->ind_y_1 + i*(2*(qp->n) -i+1)/2 + j-i] = r;
                    r++;
                }

                else// if flag == 2 unconstrained
                    if(Positive_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    {
                        cp->cont[cp->ind_y_1+ i*(2*(qp->n) -i+1)/2 + j-i] = r;
                        r++;
                    }
            }


    /*-y_ij + u_jx_i + u_ix_j - u_iu_j <= 0*/
    if (flag ==1)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2 ;
    else
        r= cp->ind_ineg + cp->nb_y_1+ cp->nb_y_2 ;

    for(i=0; i<qp->n; i++)
        for(j=i; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if(flag ==1)
                {
                    cp->cont[cp->ind_y_2+ i*(2*(qp->n) -i+1)/2 + j-i] = r;
                    r++;
                }
                else// if flag == 2 unconstrained
                    if(Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    {
                        cp->cont[cp->ind_y_2+ i*(2*(qp->n) -i+1)/2 + j-i] = r;
                        r++;
                    }
            }

    /* - y_ij+ l_jx_i + l_ix_j - l_il_j <= 0 */
    if (flag ==1)
        r= cp->ind_ineg_q + cp->nb_y_1+ cp->nb_y_2 + cp->nb_y_3 ;
    else
        r= cp->ind_ineg +cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3;

    for(i=0; i<qp->n; i++)
        for(j=i; j<qp->n; j++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if(flag ==1 )
                {
                    cp->cont[cp->ind_y_3+i*(2*(qp->n) -i+1)/2 + j-i] = r;
                    r++;
                }
                else// if flag == 2 unconstrained
                    if(Negative_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                    {
                        cp->cont[cp->ind_y_3+i*(2*(qp->n) -i+1)/2 + j-i] = r;
                        r++;
                    }
            }

    int ind;

    /*   (l_k - u_k)x_ix_j + (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_kx_j + (u_ju_k - l_jl_k)x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
    if (flag==1)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_1[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_4+ind] = r;
                    r++;
                }

    /*   (l_k - u_k)x_ix_j - u_jx_ix_k +(l_i - u_i)x_jx_k + (u_iu_k - l_il_k)x_j + u_ju_k x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
    if (flag==1 )
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5;


    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_2[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_5+ind] = r;
                    r++;
                }

    /*- u_kx_ix_j + (l_j- u_j)x_ix_k +(l_i - u_i)x_jx_k + u_iu_kx_j + u_ju_k x_i + (u_i_u_j - l_il_j)x_k - u_iu_ju_k <= 0*/
    if (flag==1)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_3[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_6+ ind] = r;
                    r++;
                }

    /*  (l_k - u_k)x_ix_j + (u_j - l_j)x_ix_k + u_ix_jx_k - u_il_kx_j + (l_ju_k - u_jl_k) x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
    if (flag==1)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_4[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_7+ind] = r;
                    r++;
                }


    /*(l_k - u_k)x_ix_j + u_jx_ix_k + (u_i - l_i)x_jx_k + (l_iu_k- u_il_k) x_j  - u_jl_k x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
    if (flag==1 )
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_5[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_8+ind] = r;
                    r++;
                }

    /* (u_k - l_k)x_ix_j + (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_k x_j  +( u_jl_k - l_ju_k)x_i - u_il_jx_k + u_il_ju_k <= 0*/
    if (flag==1)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_6[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_9+ind] = r;
                    r++;
                }

    /* (l_j - u_j) x_ix_k + u_kx_ix_j + (u_i - l_i)x_jx_k - u_iu_k x_j  - l_ju_kx_i +(u_jl_i - u_il_j) x_k + u_il_ju_k <= 0*/
    if (flag==1)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9+cp->nb_y_10;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9+cp->nb_y_10;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_7[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_10+ind] = r;
                    r++;
                }

    /* (u_k - l_k)x_ix_j + u_jx_ix_k + (l_i - u_i)x_jx_k + (u_il_k - l_iu_k) x_j  - u_ju_kx_i - l_iu_jx_k + l_iu_ju_k <= 0*/
    if (flag==1 )
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9 +cp->nb_y_10 +cp->nb_y_11;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9+cp->nb_y_10 +cp->nb_y_11;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_8[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_11+ind] = r;
                    r++;
                }

    /* u_k x_ix_j + (u_j - l_j) x_ix_k + (l_i - u_i)x_jx_k - l_iu_k x_j  - u_ju_kx_i + (u_il_j- l_iu_j)x_k + l_iu_ju_k <= 0*/
    if (flag==1)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9 +cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9+cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_9[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_12+ind] = r;
                    r++;
                }


    /* (u_k - l_k) x_ix_j + (u_j - l_j) x_ix_k  - u_ix_jx_k + u_il_jx_k + u_il_k x_j+ (l_jl_k - u_iu_k)x_i  - u_il_jl_k <= 0*/
    if (flag==1 )
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9 +cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12+cp->nb_y_13;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9+cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12+cp->nb_y_13;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_10[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_13+ind] = r;
                    r++;
                }

    /*  (u_k - l_k) x_ix_j - u_j x_ix_k  +(u_i - l_i)x_jx_k + l_iu_jx_k + (l_il_k - u_iu_k) x_j+ u_jl_kx_i - l_iu_jl_k <= 0*/
    if (flag==1 )
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9 +cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12+cp->nb_y_13+cp->nb_y_14;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9+cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12+cp->nb_y_13+cp->nb_y_14;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_11[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_14+ind] = r;
                    r++;
                }

    /*  -u_k x_ix_j +(u_j -l_j) x_ix_k  +(u_i - l_i)x_jx_k + (l_il_j - u_iu_j) x_k + l_iu_k  x_j+ l_ju_kx_i - l_il_ju_k <= 0 */
    if (flag==1)
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9 +cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12+cp->nb_y_13+cp->nb_y_14+cp->nb_y_15;
    else
        r= cp->ind_ineg+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6+ cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9+cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12+cp->nb_y_13+cp->nb_y_14+cp->nb_y_15;

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if(!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(j,k,qp->n)]) && !Zero_BB(cp->delta_12[ijk2l(i,j,k, cp->n,cp->n)]))
                {
                    ind = ijk2l_triangular(i,j,k,qp->n);
                    cp->cont[cp->ind_y_15+ind] = r;
                    r++;
                }

    /* y_ii - x_i <= 0*/
    if (flag==1 )
    {
        r= cp->ind_ineg_q+ cp->nb_y_1 + cp->nb_y_2+ cp->nb_y_3+ cp->nb_y_4+cp->nb_y_5+cp->nb_y_6 + cp->nb_y_7+ cp->nb_y_8 +cp->nb_y_9 +cp->nb_y_10 +cp->nb_y_11+cp->nb_y_12+cp->nb_y_13+cp->nb_y_14+cp->nb_y_15+cp->nb_y_16;
        for(i=0; i<qp->nb_int; i++)
            if(!Zero_BB(cp->nb_var_y[ij2k(i,i,qp->n)]))
            {
                cp->cont[cp->ind_y_16+i] = r;
                r++;
            }
    }

    /* real number of variables +1 for constant term*/
    cp->nb_col =cp->n  + cp->nb_y +1;

    /*nb occurence of each x in constraints involving y variables*/
    cp->x_cont=alloc_vector(cp->ind_max_x);
    for(i=0; i<cp->ind_max_x; i++)
        cp->x_cont[i]=0;


    for(i=0; i<qp->n; i++)
        for(j=i; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
                if(flag==1)
                {
                    if (i==j)
                    {
                        cp->x_cont[i] = cp->x_cont[i]+4;
                        if (i<cp->nb_int)
                            cp->x_cont[i]++;
                    }
                    else
                    {
                        cp->x_cont[i] = cp->x_cont[i]+4;
                        cp->x_cont[j] = cp->x_cont[j]+4;
                    }
                }
                else
                {
                    // if flag == 2 uncontrained
                    cp->x_cont[i] = cp->x_cont[i]+2;
                    if (j!=i)
                        cp->x_cont[j] = cp->x_cont[j]+2;
                }

    for(i=0; i<qp->n; i++)
        for(j=i+1; j<qp->n; j++)
            for(k=j+1; k<qp->n; k++)
                if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(i,k,qp->n)]) && !Zero_BB(cp->nb_var_y[ij2k(k,j,qp->n)]) )
                    if(flag==1 || flag ==2)
                    {
                        if(!Zero_BB(cp->delta_1[ijk2l(i,j,k, cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_2[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_3[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if (!Zero_BB(cp->delta_4[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_5[ijk2l(i,j,k, cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_6[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_7[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if (!Zero_BB(cp->delta_8[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_9[ijk2l(i,j,k, cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_10[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if(!Zero_BB(cp->delta_11[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                        if (!Zero_BB(cp->delta_12[ijk2l(i,j,k,cp->n,cp->n)]))
                        {
                            cp->x_cont[i] = cp->x_cont[i]+1;
                            cp->x_cont[j] = cp->x_cont[j]+1;
                            cp->x_cont[k] = cp->x_cont[k]+1;
                        }
                    }


    /*table with corresponding variables*/
    cp->var = alloc_vector(cp->ncol);

    /*variables x all created*/
    for(i=0; i<cp->ind_max_x; i++)
    {
        cp->var[i]=i;
    }

    /*variables y*/
    r=cp->ind_max_x;

    for(i=0; i<qp->n; i++)
    {
        for (j=0; j<qp->n; j++)
            if (!Zero_BB(cp->nb_var_y[ij2k(i,j,qp->n)]))
            {
                if (i<=j)
                {
                    cp->var[(qp->n)*i + j + cp->ind_max_x] =r;
                    r++;
                }


            }
    }

    return cp;
}


/******************************************************************************/
/******************************* Create BAB ***********************************/
/******************************************************************************/
struct miqcp_bab create_bab(MIQCP qp)
{
    int i,j,k;
    struct miqcp_bab bab;

    bab.n=qp->n;

    bab.u=alloc_vector_d(bab.n + bab.n*bab.n);
    bab.l=alloc_vector_d(bab.n + bab.n*bab.n);

    for(i=0; i<bab.n; i++)
    {
        bab.u[i] = qp->u[i];
        bab.l[i] = qp->l[i];
    }
    for(j=0; j<bab.n; j++)
        for(k=j; k<bab.n; k++)
        {
            bab.u[i]=qp->u[j]*qp->u[k];
            i++;
        }

    i = bab.n;

    for(j=0; j<bab.n; j++)
        for(k=j; k<bab.n; k++)
        {
            bab.l[i]=qp->l[j]*qp->l[k];
            i++;
        }

    bab.i=0;
    bab.j=0;
    return bab;
}


/******************************************************************************/
/******************************* Create MBAB **********************************/
/******************************************************************************/
MIQCP_BAB create_mbab_liste()
{
    MIQCP_BAB bab = (MIQCP_BAB)malloc(sizeof(struct miqcp_bab));
    return bab;
}

struct miqcp_bab create_mbab(MIQCP qp,C_MIQCP cqp, int flag)
{
    int i,j,k;
    struct miqcp_bab bab;

    bab.n=qp->n;
    bab.p=qp->p;
    bab.pq=qp->pq;
    bab.mq=qp->mq;
    bab.nb_int=qp->nb_int;

    bab.u=alloc_vector_d(cqp->nb_col);
    bab.l=alloc_vector_d(cqp->nb_col);

    for(i=0; i<bab.n; i++)
    {
        bab.u[i] = qp->u[i];
        bab.l[i] = qp->l[i];
    }

    /* Attention cas general le faire pour les 4 produits possible*/
    for(j=0; j<bab.n; j++)
        for(k=j; k<bab.n; k++)
        {
            if (!Zero_BB(cqp->nb_var_y[ij2k(j,k,bab.n)]))
            {
                /* /\*for p_miqcr (creation of y_ij if beta_ij <> 0 ) *\/ */
                /*   bab.u[i]=qp->u[qp->n + j* (2*(cqp->n) - j +1)/2 + k-j]; */
                /*   bab.l[i]=qp->l[qp->n + j* (2*(cqp->n) - j +1)/2 + k-j]; */
                /*   i++; */
                /* }  */
                if (k==j)
                {
                    if ((Positive_BB(qp->u[j])||Zero_BB(qp->u[j])) && (Positive_BB(qp->l[j]) || Zero_BB(qp->l[j])))
                    {
                        bab.u[i]=qp->u[j]*qp->u[j];
                        bab.l[i]=0;
                        i++;
                    }
                    else
                    {
                        if ((Positive_BB(qp->u[j])|| Zero_BB(qp->u[j])) && Negative_BB(qp->l[j]))
                        {
                            if (qp->u[j]*qp->u[j] >qp->l[j]*qp->l[j])
                                bab.u[i]=qp->u[j]*qp->u[j];
                            else
                                bab.u[i]=qp->l[j]*qp->l[j];
                            bab.l[i]=0;
                            i++;
                        }
                        else if (Negative_BB(qp->u[j]) && Negative_BB(qp->l[j]))
                        {
                            bab.u[i]=qp->l[j]*qp->l[j];
                            bab.l[i]=0;
                            i++;
                        }
                    }
                }
                else
                {
                    if ((Positive_BB(qp->u[j])||Zero_BB(qp->u[j])) && (Positive_BB(qp->u[k]) || Zero_BB(qp->u[k])))
                    {
                        if (Negative_BB(qp->l[j]) && Negative_BB(qp->l[k]))
                        {
                            if (qp->u[j]*qp->u[k] > qp->l[j]*qp->l[k])
                                bab.u[i]=qp->u[j]*qp->u[k];
                            else
                                bab.u[i]=qp->l[j]*qp->l[k];
                            if  (qp->l[j]*qp->u[k] > qp->l[k]*qp->u[j])
                                bab.l[i]=qp->l[j]*qp->u[k];
                            else
                                bab.l[i]=qp->l[k]*qp->u[j];
                            i++;
                        }

                        else
                        {
                            if (Negative_BB(qp->l[j]))
                            {
                                bab.u[i]=qp->u[j]*qp->u[k];
                                bab.l[i]=qp->l[j]*qp->u[k];
                                i++;
                            }
                            else
                            {
                                if (Negative_BB(qp->l[k]))
                                {
                                    bab.u[i]=qp->u[j]*qp->u[k];
                                    bab.l[i]=qp->u[j]*qp->l[k];
                                    i++;
                                }
                                else
                                {
                                    bab.u[i]=qp->u[j]*qp->u[k];
                                    bab.l[i]=qp->l[j]*qp->l[k];
                                    i++;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Negative_BB(qp->u[j]) && Negative_BB(qp->u[k]) )
                        {
                            bab.u[i]=qp->l[k]*qp->l[j];
                            bab.l[i]=qp->u[k]*qp->u[j];
                            i++;
                        }
                        else
                        {
                            if (Negative_BB(qp->u[j]))
                            {
                                bab.u[i]=qp->l[k]*qp->l[j];
                                bab.l[i]=qp->u[k]*qp->l[j];
                                i++;
                            }
                            else if (Negative_BB(qp->u[k]))
                            {
                                bab.u[i]=qp->l[k]*qp->l[j];
                                bab.l[i]=qp->u[j]*qp->l[k];
                                i++;
                            }

                        }
                    }

                }

            }
        }
 
    bab.i=0;
    bab.j=0;
    return bab;
}


MIQCP_BAB realloc_mbab_liste(struct miqcp_bab bab, int nb_col)
{
    MIQCP_BAB new_bab =  create_mbab_liste();
    new_bab->n=bab.n;
    new_bab->p=bab.p;
    new_bab->pq=bab.pq;
    new_bab->nb_int=bab.nb_int;

    new_bab->u=alloc_vector_d(nb_col);
    memcpy(new_bab->u,bab.u,nb_col*sizeof(double));

    new_bab->l=alloc_vector_d(nb_col);
    memcpy(new_bab->l,bab.l,nb_col*sizeof(double));

    return new_bab;
}

/******************************************************************************/
/******************* Structures for the PSD solver ****************************/
/******************************************************************************/
SDP new_sdp()
{
    return (SDP) malloc(sizeof(struct sdp));
}

/******************************************************************************/
/******************************* Create tab cont*******************************/
/******************************************************************************/

TAB_CONT create_tab_cont(SDP psdp)
{
    int i,j;

    TAB_CONT tab;

    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(3);

    int cont_X_1=psdp->i1;
    /* Constraints  x_ix_j - u_ix_j <= 0*/
    for(i=0; i<psdp->nb_int; i++)
    {
        for(j=i+1; j<psdp->n ; j++)
        {
            tab[cont_X_1-1][0]=i;
            tab[cont_X_1-1][1]=j;
            tab[cont_X_1-1][2]=1;
            cont_X_1++;
        }
    }

    /* Constraints x_ix_j - u_jx_i <= 0*/
    int cont_X_2=psdp->i2;
    for(i=0; i<psdp->nb_int; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_2-1][0]=i;
            tab[cont_X_2-1][1]=j;
            tab[cont_X_2-1][2]=2;
            cont_X_2++;
        }
    }

    /* Constraints - x_ix_j + u_jx_i + u_iu_j - u_iu_j <= 0*/
    int cont_X_3=psdp->i3;
    for(i=0; i<psdp->nb_int; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_3-1][0]=i;
            tab[cont_X_3-1][1]=j;
            tab[cont_X_3-1][2]=3;
            cont_X_3++;
        }
    }

    /* Constraints - x_ix_j <= 0*/
    int cont_X_4=psdp->i4;
    for(i=0; i<psdp->nb_int; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_4-1][0]=i;
            tab[cont_X_4-1][1]=j;
            tab[cont_X_4-1][2]=4;
            cont_X_4++;
        }
    }

    return tab;

}

TAB_CONT create_tab_cont_mixed(SDP psdp)
{
    int i,j;

    TAB_CONT tab;

    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(3);

    int cont_X_1=psdp->i1;
    /* Constraints  x_ix_j - u_ix_j <= 0*/
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n ; j++)
        {
            tab[cont_X_1-1][0]=i;
            tab[cont_X_1-1][1]=j;
            tab[cont_X_1-1][2]=1;
            cont_X_1++;
        }
    }

    /* Constraints x_ix_j - u_jx_i <= 0*/
    int cont_X_2=psdp->i2;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_2-1][0]=i;
            tab[cont_X_2-1][1]=j;
            tab[cont_X_2-1][2]=2;
            cont_X_2++;
        }
    }

    /* Constraints - x_ix_j + u_jx_i + u_iu_j - u_iu_j <= 0*/
    int cont_X_3=psdp->i3;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_3-1][0]=i;
            tab[cont_X_3-1][1]=j;
            tab[cont_X_3-1][2]=3;
            cont_X_3++;
        }
    }

    /* Constraints - x_ix_j <= 0*/
    int cont_X_4=psdp->i4;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_4-1][0]=i;
            tab[cont_X_4-1][1]=j;
            tab[cont_X_4-1][2]=4;
            cont_X_4++;
        }
    }

    return tab;

}


TAB_CONT create_tab_cont_0_1(SDP psdp)
{
    int i,j,k;

    TAB_CONT tab;

    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(4);

    int cont_X_1=psdp->i1;
    /* Constraints  x_ix_j - x_j <= 0*/
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_1-1][0]=i;
            tab[cont_X_1-1][1]=j;
            tab[cont_X_1-1][2]=0;
            tab[cont_X_1-1][3]=1;
            cont_X_1++;
        }
    }

    /* Constraints x_ix_j - x_i <= 0*/
    int cont_X_2=psdp->i2;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_2-1][0]=i;
            tab[cont_X_2-1][1]=j;
            tab[cont_X_2-1][2]=0;
            tab[cont_X_2-1][3]=2;
            cont_X_2++;
        }
    }

    /* Constraints -x_ix_j + x_i + x_j - 1 <= 0*/
    int cont_X_3=psdp->i3;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_3-1][0]=i;
            tab[cont_X_3-1][1]=j;
            tab[cont_X_3-1][2]=0;
            tab[cont_X_3-1][3]=3;
            cont_X_3++;
        }
    }

    /* Constraints - x_ix_j <= 0*/
    int cont_X_4=psdp->i4;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_4-1][0]=i;
            tab[cont_X_4-1][1]=j;
            tab[cont_X_4-1][2]=0;
            tab[cont_X_4-1][3]=4;
            cont_X_4++;
        }
    }

    /* Constraints  x_ix_k +x_jx_k -x_ix_j -x_k <= 0*/
    int cont_X_5=psdp->i5;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_5-1][0]=i;
                tab[cont_X_5-1][1]=j;
                tab[cont_X_5-1][2]=k;
                tab[cont_X_5-1][3]=5;
                cont_X_5++;
            }

    /* Constraints  x_ix_j +x_ix_k -x_jx_k -x_i <= 0*/
    int cont_X_6=psdp->i6;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_6-1][0]=i;
                tab[cont_X_6-1][1]=j;
                tab[cont_X_6-1][2]=k;
                tab[cont_X_6-1][3]=6;
                cont_X_6++;
            }

    /* Constraints  x_ix_j +x_jx_k -x_ix_k -x_j <= 0*/
    int cont_X_7=psdp->i7;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_7-1][0]=i;
                tab[cont_X_7-1][1]=j;
                tab[cont_X_7-1][2]=k;
                tab[cont_X_7-1][3]=7;
                cont_X_7++;
            }

    /* Constraints  x_i +x_j+x_k - x_ix_j - x_jx_k -x_ix_k -1 <= 0*/
    int cont_X_8=psdp->i8;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_8-1][0]=i;
                tab[cont_X_8-1][1]=j;
                tab[cont_X_8-1][2]=k;
                tab[cont_X_8-1][3]=8;
                cont_X_8++;
            }

    return tab;
}

TAB_CONT create_tab_cont_triangle(SDP psdp)
{
    int i,j,k;

    TAB_CONT tab;

    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(4);

    int cont_X_1=psdp->i1;
    /* Constraints  x_ix_j - u_ix_j  - l_jx_i + u_il_j <= 0*/
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_1-1][0]=i;
            tab[cont_X_1-1][1]=j;
            tab[cont_X_1-1][2]=0;
            tab[cont_X_1-1][3]=1;
            cont_X_1++;
        }
    }

    /* Constraints x_ix_j - u_jx_i  - l_ix_j + u_jl_i  <= 0*/
    int cont_X_2=psdp->i2;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_2-1][0]=i;
            tab[cont_X_2-1][1]=j;
            tab[cont_X_2-1][2]=0;
            tab[cont_X_2-1][3]=2;
            cont_X_2++;
        }
    }

    /* Constraints - x_ix_j + u_jx_i + u_ix_j - u_iu_j  <= 0*/
    int cont_X_3=psdp->i3;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_3-1][0]=i;
            tab[cont_X_3-1][1]=j;
            tab[cont_X_3-1][2]=0;
            tab[cont_X_3-1][3]=3;
            cont_X_3++;
        }
    }

    /* Constraints  - x_ix_j+ l_jx_i + l_ix_j - l_il_j<= 0*/
    int cont_X_4=psdp->i4;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_4-1][0]=i;
            tab[cont_X_4-1][1]=j;
            tab[cont_X_4-1][2]=0;
            tab[cont_X_4-1][3]=4;
            cont_X_4++;
        }
    }

    /* Constraints   (l_k - u_k)x_ix_j + (l_j - u_j)x_ix_k - u_ix_jx_k + u_iu_kx_j + (u_ju_k - l_jl_k)x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
    int cont_X_5=psdp->i5;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_5-1][0]=i;
                tab[cont_X_5-1][1]=j;
                tab[cont_X_5-1][2]=k;
                tab[cont_X_5-1][3]=5;
                cont_X_5++;
            }



    /* Constraints   (l_k - u_k)x_ix_j - u_jx_ix_k +(l_i - u_i)x_jx_k + (u_iu_k - l_il_k)x_j + u_ju_k x_i + u_i_u_jx_k - u_iu_ju_k <= 0*/
    int cont_X_6=psdp->i6;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_6-1][0]=i;
                tab[cont_X_6-1][1]=j;
                tab[cont_X_6-1][2]=k;
                tab[cont_X_6-1][3]=6;
                cont_X_6++;
            }


    /* Constraints    - u_kx_ix_j + (l_j- u_j)x_ix_k +(l_i - u_i)x_jx_k + u_iu_kx_j + u_ju_k x_i + (u_i_u_j - l_il_j)x_k - u_iu_ju_k <= 0*/
    int cont_X_7=psdp->i7;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_7-1][0]=i;
                tab[cont_X_7-1][1]=j;
                tab[cont_X_7-1][2]=k;
                tab[cont_X_7-1][3]=7;
                cont_X_7++;
            }

    /* Constraints   (l_k - u_k)x_ix_j + (u_j - l_j)x_ix_k + u_ix_jx_k - u_il_kx_j + (l_ju_k - u_jl_k) x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
    int cont_X_8=psdp->i8;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_8-1][0]=i;
                tab[cont_X_8-1][1]=j;
                tab[cont_X_8-1][2]=k;
                tab[cont_X_8-1][3]=8;
                cont_X_8++;
            }

    /* Constraints    (l_k - u_k)x_ix_j + u_jx_ix_k + (u_i - l_i)x_jx_k + (l_iu_k- u_il_k) x_j  - u_jl_k x_i - u_i_u_jx_k + u_iu_jl_k <= 0*/
    int cont_X_9=psdp->i9;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_9-1][0]=i;
                tab[cont_X_9-1][1]=j;
                tab[cont_X_9-1][2]=k;
                tab[cont_X_9-1][3]=9;
                cont_X_9++;
            }
    /* Constraints   (u_k - l_k)x_ix_j + (l_j - u_j)x_ix_k + u_ix_jx_k - u_iu_k x_j  +( u_jl_k - l_ju_k)x_i - u_il_jx_k + u_il_ju_k <= 0*/
    int cont_X_10=psdp->i10;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_10-1][0]=i;
                tab[cont_X_10-1][1]=j;
                tab[cont_X_10-1][2]=k;
                tab[cont_X_10-1][3]=10;
                cont_X_10++;
            }

    /* Constraints    (l_j - u_j) x_ix_k + u_kx_ix_j + (u_i - l_i)x_jx_k - u_iu_k x_j  - l_ju_kx_i +(u_jl_i - u_il_j) x_k + u_il_ju_k <= 0*/
    int cont_X_11=psdp->i11;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_11-1][0]=i;
                tab[cont_X_11-1][1]=j;
                tab[cont_X_11-1][2]=k;
                tab[cont_X_11-1][3]=11;
                cont_X_11++;
            }

    /* Constraints   (u_k - l_k)x_ix_j + u_jx_ix_k + (l_i - u_i)x_jx_k + (u_il_k - l_iu_k) x_j  - u_ju_kx_i - l_iu_jx_k + l_iu_ju_k <= 0*/
    int cont_X_12=psdp->i12;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_12-1][0]=i;
                tab[cont_X_12-1][1]=j;
                tab[cont_X_12-1][2]=k;
                tab[cont_X_12-1][3]=12;
                cont_X_12++;
            }

    /* Constraints   u_k x_ix_j + (u_j - l_j) x_ix_k + (l_i - u_i)x_jx_k - l_iu_k x_j  - u_ju_kx_i + (u_il_j- l_iu_j)x_k + l_iu_ju_k <= 0*/
    int cont_X_13=psdp->i13;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_13-1][0]=i;
                tab[cont_X_13-1][1]=j;
                tab[cont_X_13-1][2]=k;
                tab[cont_X_13-1][3]=13;
                cont_X_13++;
            }

    /* Constraints  (u_k - l_k) x_ix_j + (u_j - l_j) x_ix_k  - u_ix_jx_k + u_il_jx_k + u_il_k x_j+ (l_jl_k - u_iu_k)x_i  - u_il_jl_k <= 0*/
    int cont_X_14=psdp->i14;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_14-1][0]=i;
                tab[cont_X_14-1][1]=j;
                tab[cont_X_14-1][2]=k;
                tab[cont_X_14-1][3]=14;
                cont_X_14++;
            }


    /* Constraints   (u_k - l_k) x_ix_j - u_j x_ix_k  +(u_i - l_i)x_jx_k + l_iu_jx_k + (l_il_k - u_iu_k) x_j+ u_jl_kx_i - l_iu_jl_k <= 0*/
    int cont_X_15=psdp->i15;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_15-1][0]=i;
                tab[cont_X_15-1][1]=j;
                tab[cont_X_15-1][2]=k;
                tab[cont_X_15-1][3]=15;
                cont_X_15++;
            }


    /* Constraints -u_k x_ix_j +(u_j -l_j) x_ix_k  +(u_i - l_i)x_jx_k + (l_il_j - u_iu_j) x_k + l_iu_k  x_j+ l_ju_kx_i - l_il_ju_k <= 0*/
    int cont_X_16=psdp->i16;
    for(i=0; i<psdp->n; i++)
        for(j=i+1; j<psdp->n; j++)
            for(k=j+1; k<psdp->n; k++)
            {
                tab[cont_X_16-1][0]=i;
                tab[cont_X_16-1][1]=j;
                tab[cont_X_16-1][2]=k;
                tab[cont_X_16-1][3]=16;
                cont_X_16++;
            }

    /* if (DEBUG ==1) */
    /*   { */
    /*     printf("\n i1 : %d\n",psdp->i1); */
    /*     printf("\n i2 : %d\n",psdp->i2); */
    /*     printf("\n i3 : %d\n",psdp->i3); */
    /*     printf("\n i4 : %d\n",psdp->i4); */
    /*     printf("\n i5 : %d\n",psdp->i5); */
    /*     printf("\n i6 : %d\n",psdp->i6); */
    /*     printf("\n i7 : %d\n",psdp->i7); */
    /*     printf("\n i8 : %d\n",psdp->i8); */
    /*     printf("\n i9 : %d\n",psdp->i9); */
    /*     printf("\n i10 : %d\n",psdp->i10); */
    /*     printf("\n i11 : %d\n",psdp->i11); */
    /*     printf("\n i12 : %d\n",psdp->i12); */
    /*     printf("\n i13 : %d\n",psdp->i13); */
    /*     printf("\n i14 : %d\n",psdp->i14); */
    /*     printf("\n i15 : %d\n",psdp->i15); */
    /*     printf("\n i16 : %d\n",psdp->i16); */
    /*     printf("\n psdp->length: %d\n",psdp->length); */
    /*     printf("\n tab cont\n"); */
    /*     for (i=0;i<psdp->length;i++) */
    /* 	{ */
    /* 	  print_vec(tab[i],4); */
    /* 	  printf("\n"); */
    /* 	} */
    /*   } */

    return tab;




}

TAB_CONT create_tab_cont_qap(SDP psdp)
{
    int i,j,k,l;

    TAB_CONT tab;


    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(3);

    /* Constraints - x_ix_j <= 0*/
    int cont_X_1=psdp->i1;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_1-1][0]=i;
            tab[cont_X_1-1][1]=j;
            tab[cont_X_1-1][2]=1;
            cont_X_1++;
        }
    }

    /* Constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
    int cont_X_2=psdp->i2;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<psdp->nb_init_var; j++)
            for(l=j+1; l<psdp->nb_init_var; l++)
            {
                tab[cont_X_2-1][0]=i*psdp->nb_init_var + j;
                tab[cont_X_2-1][1]=i*psdp->nb_init_var + l;
                tab[cont_X_2-1][2]=2;
                cont_X_2++;
            }

    /* Constraints  x_(ij)x_(kj) <= 0, i,j,k : i<k */
    int cont_X_3=psdp->i3;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<psdp->nb_init_var; j++)
            for(k=i+1; k<psdp->nb_init_var; k++)
            {
                tab[cont_X_3-1][0]=i*psdp->nb_init_var + j;
                tab[cont_X_3-1][1]=k*psdp->nb_init_var + j;
                tab[cont_X_3-1][2]=3;
                cont_X_3++;
            }

    return tab;

}

TAB_CONT create_tab_cont_qapv2(SDP psdp)
{
    int i,j,k,l;

    TAB_CONT tab;
    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(3);

    /* Constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
    int cont_X_1=psdp->i1;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<psdp->nb_init_var; j++)
            for(l=j+1; l<psdp->nb_init_var; l++)
            {
                tab[cont_X_1-1][0]=i*psdp->nb_init_var + j;
                tab[cont_X_1-1][1]=i*psdp->nb_init_var + l;
                tab[cont_X_1-1][2]=1;
                cont_X_1++;
            }

    /* Constraints  -x_(ij)x_(il) <= 0, i,j,l : j<l */
    int cont_X_2=psdp->i2;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<psdp->nb_init_var; j++)
            for(l=j+1; l<psdp->nb_init_var; l++)
            {
                tab[cont_X_2-1][0]=i*psdp->nb_init_var + j;
                tab[cont_X_2-1][1]=i*psdp->nb_init_var + l;
                tab[cont_X_2-1][2]=2;
                cont_X_2++;
            }

    /* Constraints  x_(ij)x_(kj) <= 0, i,j,k : i<k */
    int cont_X_3=psdp->i3;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<psdp->nb_init_var; j++)
            for(k=i+1; k<psdp->nb_init_var; k++)
            {
                tab[cont_X_3-1][0]=i*psdp->nb_init_var + j;
                tab[cont_X_3-1][1]=k*psdp->nb_init_var + j;
                tab[cont_X_3-1][2]=3;
                cont_X_3++;
            }

    /* Constraints  -x_(ij)x_(kj) <= 0, i,j,k : i<k */
    int cont_X_4=psdp->i4;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<psdp->nb_init_var; j++)
            for(k=i+1; k<psdp->nb_init_var; k++)
            {
                tab[cont_X_4-1][0]=i*psdp->nb_init_var + j;
                tab[cont_X_4-1][1]=k*psdp->nb_init_var + j;
                tab[cont_X_4-1][2]=4;
                cont_X_4++;
            }

    return tab;
}

TAB_CONT create_tab_cont_qapv3(SDP psdp)
{
    int i,j,k,l;

    TAB_CONT tab;
    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(3);

    /* Constraints  -x_ix_j <= 0, i,j :i<j */
    int cont_X_1=psdp->i1;
    for(i=0; i<psdp->nb_init_var; i++)
    {
        for(j=i+1; j<psdp->nb_init_var; j++)
        {
            tab[cont_X_1-1][0]=i;
            tab[cont_X_1-1][1]=j;
            tab[cont_X_1-1][2]=1;
            cont_X_1++;
        }
    }

    return tab;

}

TAB_CONT create_tab_cont_mkc_r2(SDP psdp)
{
    int i,j,k,l;

    TAB_CONT tab;
    int k_mkc = (int) psdp->n/psdp->nb_init_var;

    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(3);
    /*Attention trouver un moyen de calculer le nb de var init*/

    int cont_X_1=psdp->i1;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<k_mkc; j++)
            for(l=j+1; l<k_mkc; l++)
            {
                tab[cont_X_1-1][0]=i*k_mkc + j;
                tab[cont_X_1-1][1]=i*k_mkc + l;
                tab[cont_X_1-1][2]=1;
                cont_X_1++;
            }

    /* Constraints  -x_(ij)x_(il) <= 0, i,j,l : j<l */
    int cont_X_2=psdp->i2;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<k_mkc; j++)
            for(l=j+1; l<k_mkc; l++)
            {
                tab[cont_X_2-1][0]=i*k_mkc + j;
                tab[cont_X_2-1][1]=i*k_mkc + l;
                tab[cont_X_2-1][2]=2;
                cont_X_2++;
            }


    if (DEBUG ==1)
    {
        printf("\n i1 : %d\n",psdp->i1);
        printf("\n i2 : %d\n",psdp->i2);

        for (i=0; i<psdp->length; i++)
        {
            print_vec(tab[i],3);
            printf("\n");
        }
    }
    return tab;

}

TAB_CONT create_tab_cont_mkc_r4(SDP psdp)
{
    int i,j,k,l;

    TAB_CONT tab;
    int k_mkc = (int) psdp->n/psdp->nb_init_var;

    tab = (int **)calloc(psdp->length,sizeof(int*));
    for(i=0; i<psdp->length; i++)
        tab[i]=alloc_vector(3);

    /* Constraints - x_ix_j <= 0*/
    int cont_X_1=psdp->i1;
    for(i=0; i<psdp->n; i++)
    {
        for(j=i+1; j<psdp->n; j++)
        {
            tab[cont_X_1-1][0]=i;
            tab[cont_X_1-1][1]=j;
            tab[cont_X_1-1][2]=1;
            cont_X_1++;
        }
    }

    /* Constraints  x_(ij)x_(il) <= 0, i,j,l : j<l */
    int cont_X_2=psdp->i2;
    for(i=0; i<psdp->nb_init_var; i++)
        for(j=0; j<k_mkc; j++)
            for(l=j+1; l<k_mkc; l++)
            {
                tab[cont_X_2-1][0]=i*k_mkc + j;
                tab[cont_X_2-1][1]=i*k_mkc + l;
                tab[cont_X_2-1][2]=2;
                cont_X_2++;
            }

    if (DEBUG ==1)
    {
        printf("\n i1 : %d\n",psdp->i1);
        printf("\n i2 : %d\n",psdp->i2);

        for (i=0; i<psdp->length; i++)
        {
            print_vec(tab[i],3);
            printf("\n");
        }
    }

    return tab;
}


/******************************************************************************/
/******************************* Create SDP ***********************************/
/******************************************************************************/
SDP create_sdp(MIQCP qp)
{
    int i,j,k;
    SDP psdp = new_sdp();
    psdp->n = qp->n;
    psdp->nb_int = qp->nb_int;
    psdp->m = qp->m;
    psdp->p = qp->p;
    psdp->mq = qp->mq;
    psdp->pq = qp->pq;
    psdp->miqp = qp->miqp;
    psdp->cons = -qp->cons;

    psdp->q = copy_matrix_d(qp->q,qp->n,qp->n);

    psdp->a = copy_matrix_d(qp->a,qp->m,qp->n);
    psdp->d = copy_matrix_d(qp->d,qp->p,qp->n);

    psdp->aq = copy_matrix_3d(qp->aq,qp->mq,qp->n+1,qp->n+1);
    psdp->dq = copy_matrix_3d(qp->dq,qp->pq,qp->n+1,qp->n+1);

    psdp->c = copy_vector_d(qp->c,qp->n);
    psdp->u = copy_vector_d(qp->u,qp->n);
    psdp->l = copy_vector_d(qp->l,qp->n);
    psdp->b = copy_vector_d(qp->b,qp->m);
    psdp->e = copy_vector_d(qp->e,qp->p);

    psdp->bq = copy_vector_d(qp->bq,qp->mq);
    psdp->eq = copy_vector_d(qp->eq,qp->pq);


    psdp->beta_1=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_2=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_3=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_4=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_diag=alloc_vector_d(qp->nb_int);

    /*initialize beta matrix*/
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            psdp->beta_1[ij2k(i,j,qp->n)]=0;
            psdp->beta_2[ij2k(i,j,qp->n)]=0;
            psdp->beta_3[ij2k(i,j,qp->n)]=0;
            psdp->beta_4[ij2k(i,j,qp->n)]=0;
        }

    for(i=0; i<qp->nb_int; i++)
        psdp->beta_diag[i]=0;

    psdp->alpha=0;
    psdp->alphabis=0;

    psdp->length=nb_max_cont(psdp,&psdp->i1,&psdp->i2,&psdp->i3,&psdp->i4);
    psdp->nb_cont = FACTOR * psdp->length;
    psdp->nb_max_cont = psdp->nb_cont;

    psdp->constraints=alloc_vector(psdp->nb_cont );
    psdp->constraints_old=alloc_vector( psdp->nb_cont );
    for(i=0; i<psdp->nb_cont; i++)
    {
        psdp->constraints[i]=0;
        psdp->constraints_old[i]=0;
    }
   
    psdp->tab=create_tab_cont(psdp);

    return psdp;
}

SDP create_sdp_mixed(MIQCP qp)
{
    int i,j,k;
    SDP psdp = new_sdp();
    psdp->n = qp->n;
    psdp->nb_int = qp->nb_int;
    psdp->m = qp->m;
    psdp->p = qp->p;
    psdp->mq = qp->mq;
    psdp->pq = qp->pq;
    psdp->miqp = qp->miqp;
    psdp->cons = -qp->cons;

    psdp->q = copy_matrix_d(qp->q,qp->n,qp->n);

    psdp->a = copy_matrix_d(qp->a,qp->m,qp->n);
    psdp->d = copy_matrix_d(qp->d,qp->p,qp->n);

    psdp->aq = copy_matrix_3d(qp->aq,qp->mq,qp->n+1,qp->n+1);
    psdp->dq = copy_matrix_3d(qp->dq,qp->pq,qp->n+1,qp->n+1);

    psdp->c = copy_vector_d(qp->c,qp->n);
    psdp->u = copy_vector_d(qp->u,qp->n);
    psdp->l = copy_vector_d(qp->l,qp->n);
    psdp->b = copy_vector_d(qp->b,qp->m);
    psdp->e = copy_vector_d(qp->e,qp->p);

    psdp->bq = copy_vector_d(qp->bq,qp->mq);
    psdp->eq = copy_vector_d(qp->eq,qp->pq);

    psdp->beta_1=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_2=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_3=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_4=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_diag=alloc_vector_d(qp->n);

    /*initialize beta matrix*/
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            psdp->beta_1[ij2k(i,j,qp->n)]=0;
            psdp->beta_2[ij2k(i,j,qp->n)]=0;
            psdp->beta_3[ij2k(i,j,qp->n)]=0;
            psdp->beta_4[ij2k(i,j,qp->n)]=0;
        }

    for(i=0; i<qp->n; i++)
        psdp->beta_diag[i]=0;

    psdp->alpha=0;
    psdp->alphabis=0;

    psdp->length=nb_max_cont_mixed(psdp,&psdp->i1,&psdp->i2,&psdp->i3,&psdp->i4);
    psdp->nb_cont = FACTOR * psdp->length;
    psdp->nb_max_cont = psdp->nb_cont;

    psdp->constraints=alloc_vector(psdp->nb_cont );
    psdp->constraints_old=alloc_vector( psdp->nb_cont );
    for(i=0; i<psdp->nb_cont; i++)
    {
        psdp->constraints[i]=0;
        psdp->constraints_old[i]=0;
    }
   
    psdp->tab=create_tab_cont_mixed(psdp);

    return psdp;
}

SDP create_sdp_0_1(MIQCP qp)
{
    int i,j,k;
    SDP psdp = new_sdp();
    psdp->n = qp->n;
    psdp->nb_int = qp->nb_int;
    psdp->m = qp->m;
    psdp->p = qp->p;
    psdp->mq = qp->mq;
    psdp->pq = qp->pq;
    psdp->miqp = qp->miqp;
    psdp->cons = -qp->cons;

    psdp->q = copy_matrix_d(qp->q,qp->n,qp->n);

    psdp->a = copy_matrix_d(qp->a,qp->m,qp->n);
    psdp->d = copy_matrix_d(qp->d,qp->p,qp->n);

    psdp->aq = copy_matrix_3d(qp->aq,qp->mq,qp->n+1,qp->n+1);
    psdp->dq = copy_matrix_3d(qp->dq,qp->pq,qp->n+1,qp->n+1);

    psdp->c = copy_vector_d(qp->c,qp->n);
    psdp->u = copy_vector_d(qp->u,qp->n);
    psdp->l = copy_vector_d(qp->l,qp->n);
    psdp->b = copy_vector_d(qp->b,qp->m);
    psdp->e = copy_vector_d(qp->e,qp->p);

    psdp->bq = copy_vector_d(qp->bq,qp->mq);
    psdp->eq = copy_vector_d(qp->eq,qp->pq);


    psdp->beta_1=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_2=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_3=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_4=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_diag=alloc_vector_d(qp->n);

    psdp->delta_1=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_2=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_3=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_4=alloc_matrix_3d(qp->n,qp->n,qp->n);

    /*initialize beta matrix*/
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            psdp->beta_1[ij2k(i,j,qp->n)]=0;
            psdp->beta_2[ij2k(i,j,qp->n)]=0;
            psdp->beta_3[ij2k(i,j,qp->n)]=0;
            psdp->beta_4[ij2k(i,j,qp->n)]=0;
        }

    for(i=0; i<qp->n; i++)
        psdp->beta_diag[i]=0;

    /*initialize delta matrix*/
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            for(k=0; k<qp->n; k++)
            {
                psdp->delta_1[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_2[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_3[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_4[ijk2l(i,j,k,qp->n,qp->n)]=0;
            }

    psdp->alpha=0;
    psdp->alphabis=0;

    psdp->length=nb_max_cont_0_1(psdp,&psdp->i1,&psdp->i2,&psdp->i3,&psdp->i4,&psdp->i5,&psdp->i6,&psdp->i7,&psdp->i8);
    psdp->nb_cont = FACTOR * psdp->length;
    psdp->nb_max_cont = psdp->nb_cont;

    psdp->constraints=alloc_vector(psdp->nb_cont);
    psdp->constraints_old=alloc_vector( psdp->nb_cont );
    for(i=0; i<psdp->nb_cont; i++)
    {
        psdp->constraints[i]=0;
        psdp->constraints_old[i]=0;
    }
    
    psdp->tab=create_tab_cont_0_1(psdp);

    return psdp;
}

SDP create_sdp_triangle(MIQCP qp)
{
    int i,j,k;
    SDP psdp = new_sdp();
    psdp->n = qp->n;
    psdp->nb_int = qp->nb_int;
    psdp->m = qp->m;
    psdp->p = qp->p;
    psdp->mq = qp->mq;
    psdp->pq = qp->pq;
    psdp->miqp = qp->miqp;
    psdp->cons = -qp->cons;

    psdp->q = copy_matrix_d(qp->q,qp->n,qp->n);

    psdp->a = copy_matrix_d(qp->a,qp->m,qp->n);
    psdp->d = copy_matrix_d(qp->d,qp->p,qp->n);

    psdp->aq = copy_matrix_3d(qp->aq,qp->mq,qp->n+1,qp->n+1);
    psdp->dq = copy_matrix_3d(qp->dq,qp->pq,qp->n+1,qp->n+1);

    psdp->c = copy_vector_d(qp->c,qp->n);
    psdp->u = copy_vector_d(qp->u,qp->n);
    psdp->l = copy_vector_d(qp->l,qp->n);
    psdp->b = copy_vector_d(qp->b,qp->m);
    psdp->e = copy_vector_d(qp->e,qp->p);

    psdp->bq = copy_vector_d(qp->bq,qp->mq);
    psdp->eq = copy_vector_d(qp->eq,qp->pq);


    psdp->beta_1=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_2=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_3=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_4=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_diag=alloc_vector_d(qp->n);

    psdp->delta_1=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_2=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_3=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_4=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_5=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_6=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_7=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_8=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_9=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_10=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_11=alloc_matrix_3d(qp->n,qp->n,qp->n);
    psdp->delta_12=alloc_matrix_3d(qp->n,qp->n,qp->n);
    /*initialize beta matrix*/
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            psdp->beta_1[ij2k(i,j,qp->n)]=0;
            psdp->beta_2[ij2k(i,j,qp->n)]=0;
            psdp->beta_3[ij2k(i,j,qp->n)]=0;
            psdp->beta_4[ij2k(i,j,qp->n)]=0;
        }

    for(i=0; i<qp->n; i++)
        psdp->beta_diag[i]=0;

    /*initialize delta matrix*/
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
            for(k=0; k<qp->n; k++)
            {
                psdp->delta_1[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_2[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_3[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_4[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_5[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_6[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_7[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_8[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_9[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_10[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_11[ijk2l(i,j,k,qp->n,qp->n)]=0;
                psdp->delta_12[ijk2l(i,j,k,qp->n,qp->n)]=0;
            }

    psdp->alpha=0;
    psdp->alphabis=0;

    psdp->length=nb_max_cont_triangle(psdp,&psdp->i1,&psdp->i2,&psdp->i3,&psdp->i4,&psdp->i5,&psdp->i6,&psdp->i7,&psdp->i8,&psdp->i9,&psdp->i10,&psdp->i11,&psdp->i12,&psdp->i13,&psdp->i14,&psdp->i15,&psdp->i16);

    if (DEBUG == 1)
    {
        printf("\n psdp->length : %d \n", psdp->length );
    }

    psdp->nb_cont = FACTOR * psdp->length;
    psdp->nb_max_cont = psdp->nb_cont;

    psdp->constraints=alloc_vector(psdp->nb_cont);
    psdp->constraints_old=alloc_vector( psdp->nb_cont );
    for(i=0; i<psdp->nb_cont; i++)
    {
        psdp->constraints[i]=0;
        psdp->constraints_old[i]=0;
    }

    psdp->tab=create_tab_cont_triangle(psdp);

    return psdp;
}

SDP create_sdp_qap(MIQCP qp, int flag)
{
    int i,j,k;

    SDP psdp = new_sdp();
    psdp->nb_init_var = (int)sqrt(qp->n);
    psdp->n = qp->n;
    psdp->nb_int = qp->nb_int;
    psdp->m = qp->m;
    psdp->p = qp->p;
    psdp->mq = qp->mq;
    psdp->pq = qp->pq;
    psdp->miqp = qp->miqp;
    psdp->cons = -qp->cons;

    psdp->q = copy_matrix_d(qp->q,qp->n,qp->n);

    psdp->a = copy_matrix_d(qp->a,qp->m,qp->n);
    psdp->d = copy_matrix_d(qp->d,qp->p,qp->n);

    psdp->aq = copy_matrix_3d(qp->aq,qp->mq,qp->n+1,qp->n+1);
    psdp->dq = copy_matrix_3d(qp->dq,qp->pq,qp->n+1,qp->n+1);

    psdp->c = copy_vector_d(qp->c,qp->n);
    psdp->u = copy_vector_d(qp->u,qp->n);
    psdp->l = copy_vector_d(qp->l,qp->n);
    psdp->b = copy_vector_d(qp->b,qp->m);
    psdp->e = copy_vector_d(qp->e,qp->p);

    psdp->bq = copy_vector_d(qp->bq,qp->mq);
    psdp->eq = copy_vector_d(qp->eq,qp->pq);



    psdp->beta_1=alloc_matrix_d(qp->n,qp->n);

    if ((flag == 1) || (flag==2))
    {
        psdp->beta_2=alloc_matrix_d(qp->n,qp->n);
        psdp->beta_3=alloc_matrix_d(qp->n,qp->n);
        if (flag == 2)
            psdp->beta_4=alloc_matrix_d(qp->n,qp->n);
    }

    psdp->beta_diag=alloc_vector_d(qp->n);



    /*initialize beta matrix*/
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            // if flag == 3 only one type of constraints
            psdp->beta_1[ij2k(i,j,qp->n)]=0;
            if  ((flag == 1) || (flag ==2))
            {
                psdp->beta_2[ij2k(i,j,qp->n)]=0;
                psdp->beta_3[ij2k(i,j,qp->n)]=0;
                if  (flag == 2)
                    psdp->beta_4[ij2k(i,j,qp->n)]=0;
            }
        }

    for(i=0; i<qp->n; i++)
        psdp->beta_diag[i]=0;

    psdp->alpha=0;
    psdp->alphabis=0;
    if (flag ==1)
        psdp->length=nb_max_cont_qap(psdp,&psdp->i1,&psdp->i2,&psdp->i3);
    if (flag ==2)
        psdp->length=nb_max_cont_qapv2(psdp,&psdp->i1,&psdp->i2,&psdp->i3,&psdp->i4);
    if (flag ==3)
        psdp->length=nb_max_cont_qapv3(psdp,&psdp->i1);

    psdp->nb_cont = FACTOR * psdp->length;
    psdp->nb_max_cont = psdp->nb_cont;

    psdp->constraints=alloc_vector(psdp->nb_cont);
    psdp->constraints_old=alloc_vector( psdp->nb_cont );
    for(i=0; i<psdp->nb_cont; i++)
    {
        psdp->constraints[i]=0;
        psdp->constraints_old[i]=0;
    }

    if (flag == 1)
        psdp->tab=create_tab_cont_qap(psdp);
    if (flag == 2)
        psdp->tab=create_tab_cont_qapv2(psdp);
    if (flag == 3)
        psdp->tab=create_tab_cont_qapv3(psdp);
    return psdp;
}


SDP create_sdp_mkc(MIQCP qp, int flag, int k)
{
    int i,j;

    SDP psdp = new_sdp();

    psdp->nb_init_var = qp->n/k;
    psdp->n = qp->n;
    psdp->nb_int = qp->nb_int;
    psdp->m = qp->m;
    psdp->p = qp->p;
    psdp->mq = qp->mq;
    psdp->pq = qp->pq;
    psdp->miqp = qp->miqp;
    psdp->cons = -qp->cons;

    psdp->q = copy_matrix_d(qp->q,qp->n,qp->n);

    psdp->a = copy_matrix_d(qp->a,qp->m,qp->n);
    psdp->d = copy_matrix_d(qp->d,qp->p,qp->n);

    psdp->aq = copy_matrix_3d(qp->aq,qp->mq,qp->n+1,qp->n+1);
    psdp->dq = copy_matrix_3d(qp->dq,qp->pq,qp->n+1,qp->n+1);

    psdp->c = copy_vector_d(qp->c,qp->n);
    psdp->u = copy_vector_d(qp->u,qp->n);
    psdp->l = copy_vector_d(qp->l,qp->n);
    psdp->b = copy_vector_d(qp->b,qp->m);
    psdp->e = copy_vector_d(qp->e,qp->p);

    psdp->bq = copy_vector_d(qp->bq,qp->mq);
    psdp->eq = copy_vector_d(qp->eq,qp->pq);



    psdp->beta_1=alloc_matrix_d(qp->n,qp->n);
    psdp->beta_2=alloc_matrix_d(qp->n,qp->n);


    psdp->beta_diag=alloc_vector_d(qp->n);



    /*initialize beta matrix*/
    for(i=0; i<qp->n; i++)
        for(j=0; j<qp->n; j++)
        {
            psdp->beta_1[ij2k(i,j,qp->n)]=0;
            psdp->beta_2[ij2k(i,j,qp->n)]=0;

        }

    for(i=0; i<qp->n; i++)
        psdp->beta_diag[i]=0;

    psdp->alpha=0;
    psdp->alphabis=0;
    if (flag ==4)
        psdp->length=nb_max_cont_mkc_r4(psdp,&psdp->i1,&psdp->i2);
    if (flag ==2)
        psdp->length=nb_max_cont_mkc_r2(psdp,&psdp->i1,&psdp->i2);

    psdp->nb_cont = FACTOR * psdp->length;
    psdp->nb_max_cont = psdp->nb_cont;

    psdp->constraints=alloc_vector(psdp->nb_cont);
    psdp->constraints_old=alloc_vector( psdp->nb_cont );
    for(i=0; i<psdp->nb_cont; i++)
    {
        psdp->constraints[i]=0;
        psdp->constraints_old[i]=0;
    }

    if (flag == 4)
        psdp->tab=create_tab_cont_mkc_r4(psdp);
    if (flag == 2)
        psdp->tab=create_tab_cont_mkc_r2(psdp);
    return psdp;
}



/******************************************************************************/
/******************************* Free Memory **********************************/
/******************************************************************************/

void free_miqcp(MIQCP qp)
{
    if(Positive(qp->n))
    {
        free_vector_d(qp->q);
        free_vector_d(qp->c);
        free_vector_d(qp->u);
        free_vector_d(qp->l);
        free_vector(qp->lg);
        free_vector(qp->lgc);
    }

    if(Positive(qp->m))
    {
        free_vector_d(qp->a);
        free_vector_d(qp->b);
    }

    if(Positive(qp->p))
    {
        free_vector_d(qp->d);
        free_vector_d(qp->e);
    }

    if(Positive(qp->mq))
    {
        free_vector_d(qp->aq);
        free_vector_d(qp->bq);
    }

    if(Positive(qp->pq))
    {
        free_vector_d(qp->dq);
        free_vector_d(qp->eq);
    }

    free(qp);
}

void free_sdp( SDP psdp)
{
    if(Positive(psdp->n))
    {
        free_vector_d(psdp->q);
        free_vector_d(psdp->c);
        free_vector_d(psdp->u);
        free_vector_d(psdp->beta_1);
        free_vector_d(psdp->beta_2);
        free_vector_d(psdp->beta_3);
        free_vector_d(psdp->beta_4);
        free_vector(psdp->constraints);
    }

    if(Positive(psdp->m))
    {
        free_vector_d(psdp->a);
        free_vector_d(psdp->b);
    }

    if(Positive(psdp->p))
    {
        free_vector_d(psdp->d);
        free_vector_d(psdp->e);
    }

    if(Positive(psdp->mq))
    {
        free_vector_d(psdp->aq);
        free_vector_d(psdp->bq);
    }

    if(Positive(psdp->pq))
    {
        free_vector_d(psdp->dq);
        free_vector_d(psdp->eq);
    }

    free(psdp);
}


void free_c_miqcp(C_MIQCP qp)
{
    if(qp->n>0)
    {
        free_vector_d(qp->q);
        free_vector_d(qp->new_q);
        free_vector_d(qp->c);
        free_vector_d(qp->u);
        free_vector_d(qp->l);
        free_vector(qp->lg);
        free_vector(qp->lgc);
    }

    if(Positive(qp->m))
    {
        free_vector_d(qp->a);
        free_vector_d(qp->a_t_a);
        free_vector_d(qp->b);
    }

    if(Positive(qp->p))
    {
        free_vector_d(qp->d);
        free_vector_d(qp->d_t_d);
        free_vector_d(qp->e);
    }

    if(Positive(qp->mq))
    {
        free_vector_d(qp->aq);
        free_vector_d(qp->bq);
    }

    if(Positive(qp->pq))
    {
        free_vector_d(qp->dq);
        free_vector_d(qp->eq);
    }

   
    free_vector_d(qp->beta);
    free(qp);
}

void free_tab_cont(TAB_CONT tab, int n)
{
    int i;
    for (i=0; i<n; i++)
        free(tab[i]);
}
